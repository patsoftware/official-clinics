/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with diabetes */
Clinic GPMPDiabetes {
	Id 96de868e-daf1-4338-94c2-9eb8150f3ae9
	Filter Diabetes
	Questions {
		Patient {
			LipidLoweringAgentUseNeeded
			StatinConcern
		}

		Checklist {
		}

		Staff {
			DiabetesCycleCareNeeded
			FootStructureCheckNeeded
			NeuropathyCheckNeeded
		}

	}
}

Clinic GPMPDiabetesCore {
	Id 96de868e-daf1-4338-94c2-9eb8150f3ae0
	Filter Diabetes
	Questions {
		Patient {
			LipidLoweringAgentUseNeeded
			StatinConcern
		}

		Checklist {
		}

		Staff {
			DiabetesCycleCareNeeded
		}

	}
}

Filter Diabetes {
	Match {
		keyword "Diabetes"
		keyword "mellitus"
		keyword "Impaired glucose tolerance"
		keyword "Poor glycaemic control"
		keyword "T1DM"
		keyword "T2DM"
		keyword "IDDM"
		keyword "NIDDM"
	}
	Tests matching only this {
		"IDDM"
		"NIDDM"
		"T2DM"
		"T1DM"
		"Diabetes"
		"mellitus"
		"Diabetes mellitus"
		"poor glycaemic control"
		"Impaired glucose tolerance"
		"Impaired glucose tolerance associated with pancreatic disease"
		"latent diabetes"
	}
}


Question DiabetesCycleCareNeeded {
	"If the patient has #diabetes# check their HbA1c today. 
	
	They should also be enrolled in an annual cycle of diabetic care. 
	
	Do you want to add a diabetic cycle of care reminder?"
	
	YesNoNA
	NAText "N/A - no Diabetes "
	Category "blood sugar"
	
	Triggers {
		Yes: problem DiabeticCycleOfCareNeeded
	}
	Id c34abc1b-4b30-4ef4-be69-24d859b3639d
	Changelog {
		2017-01-14: ""
	}
}


