/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Education WeightEducation {
    Questions {
        WeightEducationIntro
        WeightUnhelpfulBeliefs1
        WeightUnhelpfulBeliefs2

        WeightAllOrNothingInfo
        DietBlackAndWhiteInfo
        WeightLossFastAndFuriousInfo
        GoodAndBadFoodsInfo
        EatWhenDownOrBoredInfo

        EmotionalEating
        MindfulEatingInfo

        WeightLossGoodIdeasPage1
        WeightLossGoodIdeasPage2
        WeightLossAction

    }
    AppliesTo "PATv3"
    Id e3a7f9a7-7ebe-4a04-b24c-40c1cc414b7d
}

Question WeightEducationIntro {
    "*Weight For Health*
    
    Being overweight is a complex problem so a ‘one size fits all’ solution cannot be given.
    However we can show you some ideas that may be be personally useful to you.
    
    #Before# pressing yes to proceed please think #carefully#:

    * What do you think has caused you to be overweight?
    * What do you think you might be able to do about it?
    "
    
    Display
    Category "lifestyle"
    Id ef44dc96-1dbd-4499-8c82-0e385ab12e41
}

Question WeightUnhelpfulBeliefs1 {
    "#Here are some common beliefs about weight loss.#

    Press _any_  or _all_  boxes that you are interested in or think may apply to you."
    
    SubQuestions {
        WeightAllOrNothing
        DietBlackAndWhite
        WeightLossFastAndFurious
        NoneOfTheAbove
    }
    Category "lifestyle"
    DisplaySettings "allow-next-when-answered: 1"
    Id 44aa4be9-6dd3-489b-9e83-b0693c4d5688
}
Question WeightUnhelpfulBeliefs2 {
    "#Here are some more common beliefs about weight loss.#

    Press _any_  or _all_  boxes that you are interested in or think may apply to you."
    
    SubQuestions {
        GoodAndBadFoods
        EatWhenDownOrBored
        FoodAddiction
        NoneOfTheAbove
    }
    Category "lifestyle"
    DisplaySettings "allow-next-when-answered: 1"
    Id 44aa4be9-6dd3-489b-9e83-b0693c4d5689
}




Question WeightAllOrNothing {
    "There is a healthy weight for me and unless I get to it I will not be happy with myself"
    
    YesNo
    Triggers {
		Blank No: AutoAnswer {
			WeightAllOrNothingInfo NA
		}
    }
    Category "lifestyle"
    Id 6725aadf-3270-4df9-b841-bcaa044f3a29
}


Question DietBlackAndWhite {
    "I should stick to my diet all the time so if I ‘cheat’ it means total failure and  I may as well give up"
    
    YesNo
    Triggers {
		Blank No: AutoAnswer {
			DietBlackAndWhiteInfo NA
		}
    }
    Category "lifestyle"
    Id 8ab2331e-5f9a-4909-ab42-0c8a8864e2dc
}

Question WeightLossFastAndFurious {
    "If I decide to lose weight the fastest way is the best way"
    
    YesNo
    Triggers {
		Blank No: AutoAnswer {
			WeightLossFastAndFuriousInfo NA
		}
    }
    Category "lifestyle"
    Id 18d6f7be-e45b-4b13-915e-537abc2de2c3
}

Question GoodAndBadFoods {
    "I should not eat any bad foods but I can eat as much as I like of the good foods"
    
    YesNo
    Triggers {
		Blank No: AutoAnswer {
			GoodAndBadFoodsInfo NA
		}
    }
    Category "lifestyle"
    Id 4ccd5ced-9157-441e-a2a2-4c724ade328f
}

Question EatWhenDownOrBored {
    "I tend to over-eat when I am stressed or down. I have a habit of over-eating when bored."
    
    YesNo
    Triggers {
		Blank No: AutoAnswer {
			EatWhenDownOrBoredInfo NA
		}
    }
    Category "lifestyle"
    Id cba0878b-f23b-4ae7-9af6-b45be2d57433
}

Question FoodAddiction {
    "I get cravings for high-fat or high-sugar food or drinks. They give me an immediate boost."
    
    YesNo
    Triggers {
		Yes: show MindfulEatingInfo
    }
    Category "lifestyle"
    Id ada110aa-e2a5-4076-b205-160567491942
}

Question NoneOfTheAbove {
    "None of the above"
    
    YesNo
    Category "lifestyle"
    Id 76b04055-3149-4df4-9fda-ef1708276bb0
}

Question EmotionalEating {
    "
    === Emotional Eating

    [.right]
    image:weight-education/EmotionalEating.png[]

    [.big]
    Sometimes eating has an ‘emotional component’ called [purple]*‘emotional hunger’*. Here are some of its characteristics:

    * It comes on suddenly.
    * You crave specific comfort foods.
    * You eat mindlessly, for example a whole block of chocolate.
    * Your hunger is not satisfied once your stomach is full.
    * The hunger is not located in your stomach but in your head with a specific focus on texture and taste.
    * It often leads to regret or shame soon after you are finished eating.

    [.big]
    Do you have one or more of the above habits?
    "
    
    YesNo
    Triggers {
		Yes Unsure: show MindfulEatingInfo
    }
    Category "lifestyle"
    Id 6725aadf-3270-4df9-b841-bcaa05555555
}



Question WeightAllOrNothingInfo {
    "
    === All or Nothing
    
    Just 5 kg of weight loss can reduce your risk of developing diabetes by 60% and
    drop your blood pressure by 7 to 8 points.  Very few people achieve ideal BMI weight.
    
    image:weight-education/WeightAllOrNothingInfo.png[]"
    
    Display
    Category "lifestyle"
    Id 9a39f9a7-4693-470c-9dfa-e541b30c3412
}
Question DietBlackAndWhiteInfo {
    "
    === Black and White
    
    Weight loss depends on what we do MOST of the time.
    No one is perfect every day and everyone lapses.
    Forgive yourself and get back on track tomorrow.
    Weight loss works best if it is a by-product of discovering both long-term
    enjoyable exercise and a pleasurable life-long way of eating.
    
    image:weight-education/DietBlackAndWhiteInfo.png[]"
    
    Display
    Category "lifestyle"
    Id 108d6449-b0ef-48bc-bec7-c3943e3d5d1c
}
Question WeightLossFastAndFuriousInfo {
    "
    === Fast and Furious
    
    Rapid reduction in calorie intake can fire off your starvation response, causing a long-term reduction in metabolic rate (the energy used by your body even when not doing physical activities). This means that when you start eating normally again you will put on weight more rapidly.
    
    Metabolic rate slows by over 10% so if using a low-energy diet always combine it with intensive exercise to maintain metabolic rate.
    
    image:weight-education/WeightLossFastAndFuriousInfo.png[]"
    
    Display
    Category "lifestyle"
    Id 13323b98-ee1d-48ac-a8a6-9db1e063513b
}
Question GoodAndBadFoodsInfo {
    "
    === Good and Bad
    
    There are no good or bad foods. All food contains calories and any extra calories from whatever source will become fat. Some food will provide a lot of calories in a small amount (‘eat occasionally’), and other foods will provide the same calories in a larger amount (‘eat frequently’).

    Reduction in plate size to a 25cm diameter plate instead of a ‘normal’ 30cm plate reduces intake by 22%!
    
    Combine it with dividing the plate into quarters (¼ protein, ¼ carbohydrate, ½ salad or vegetables) for even lower intake.

    image:weight-education/GoodAndBadFoodsInfo2.png[]"
    
    Display
    Category "lifestyle"
    Id 8f22173c-684a-41c6-ba53-92cf031803f8
}
Question EatWhenDownOrBoredInfo {
    "
    === Down and Bored

    Reward eating is an important part of life and is OK as a special treat.
    Ask your doctor to help you look for other ways to handle stress or sad feelings.
    Boredom eating requires a deliberate plan to have low calorie snacks available and recognising your triggers for eating.
    Limiting the amount of high calorie snacks kept in the house is also very important.

    image:weight-education/EatWhenDownOrBoredInfo.png[]"
    
    Display
    Category "lifestyle"
    Id 9f4b9ef9-33de-42ff-a8e8-d6deab555bd5
}

Question MindfulEatingInfo {
    "
    [.right]
    image:weight-education/MindfulEating.png[]
    
    #Mindful eating# is the solution to ‘emotional’ eating and includes the following suggestions:

    * Being aware of emotions that trigger eating (e.g. stressful day at work) or physical things (e.g. watching TV) that trigger your eating.
    * Learning to tell between physical (empty stomach) hunger and emotional hunger (craving).
    * Learning to meet your emotional hunger in other ways than by eating.
    * Concentrating on eating to a ‘stomach feels full’ sensation, not ‘my craving is satisfied’.
    * Choosing foods that will give you #both# enjoyment and nourishment.
    "
    
    Display
    Category "lifestyle"
    Id 1781af0e-6211-4dba-8543-63a324c0499e
}


Question WeightLossGoodIdeasPage1 {
    "Despite the large range of weight loss programs available, there are only two ways to achieve sustainable weight loss.

    1. Control your calorie (energy) intake. Reducing plate size to a 25cm diameter plate instead of a ‘normal’ 30cm plate reduces calories by 22%!

    [.text-center]
    image:weight-education/SizePlate.png[width=\"300\"]
    "
    
    Display
    DisplaySettings "large-image: yes"
    Category "lifestyle"
    Id 67344448-4b07-4df7-a045-c0e1f5ea7a7d
}

Question WeightLossGoodIdeasPage2 {
    "
    [start=2]
    1. Increase your metabolic rate. Our metabolic rate is like the idle speed of an engine.  It is the rate at which our bodies function to keep us alive (heart pumping, breathing, digesting food, etc). Physical activity is the most important contributor to metabolic rate.

    [.text-center]
    image:weight-education/WeightLossGoodIdeasPage.png[idth=\"300\"]
    "
    
    Display
    DisplaySettings "large-image: yes"
    Category "lifestyle"
    Id 2ac05da4-c66a-43a8-af11-5f5e5c992339
}



Question WeightLossAction {
    "Achieving a more healthy weight works best if it is a #by-product# of focussing on both
    long-term enjoyable exercise to become more active and a pleasurable life-long way of eating.

    Considering all the information on the previous pages, choose one of the four
    options by pressing it."
    
    Select one WeightLossAction
    DisplaySettings "selection-background-colour: '#FFFF80'"
    Category "lifestyle"
    Id 06b9d79b-4836-47ed-bbd2-e928aed6d4f2
}

Selection WeightLossAction {
    Option byMyself { "I wish to try weight reduction by myself" }
    Option doctorsInfo { "I want my doctor to give me specific information so I can try by myself" }
    Option weightProgramReferral { "I want a referral to a weight reduction program" }
    Option dieticianReferral { "I want a referral to a dietician to get what I normally eat analysed so I will know what to change" }
    Id e402117a-f990-4107-8831-c512b59c7ecc
}

