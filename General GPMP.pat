/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Clinic GeneralGPMPExpanded {
	Id 13885b18-d54c-4ce1-ba4c-4e2509c9fabf
	Questions {
		Patient {
			ConsentForGPMP
			ResearchConsent
			AlcoholIntakeMoreThanRecommended
			Smoker
			PhysicalActivityNeeded
			WeightLossTipsDesired
            FHOfPrematureCVDLessThan60Years

			ForgetToTakeMedication
			StopTakingWhenWell
			CarelessSometimesWithTaking
			SideEffectOfMedicationSuspected

			DepressionPresent
			AnxietyPresent
			AdvanceHealthDirectiveNeeded

			FallInLastYear
			FractureMinimalTrauma
			CarerNeeded
			ExtraHelpNeeded
			NationalSupportOrganisationReferralNeeded

			NationalBowelScreenNeeded
			BreastScreenNeeded
			BreastScreenNeeded40to49
			CervicalScreeningNeeded
			InfluenzaMedicalConditionImmunisationNeeded
            InfluenzaImmunisationHealthy 
			ShinglesImmunisationNeeded
			PertussisImmunisationNeeded
			SexualHealthConcerns
			ErectileProblem
		}

		Checklist {
			ConsentGPMP
			ReminderGPMP
			ReminderGPMPReview
			ReminderDiabetesCyclePlanEvery12Months
			HomeSystolicBPAverage
			HypotensionCheck 
            RecordHeightWeightWaist
			RecordSmokingAlcohol
			ECG
			DopplerABI
			PrintHealthSummary
			SmokerBasicSpirometry
			MMSE
			RecordCarerDetails
		}

		Staff {
			HealthSummaryAndMedicationListReviewNeeded
			MedicationReviewNeeded
			PathologyCheck
			BPMedicationChangeNeeded
			MedicationsOrOTCThatMightElevateBP
			ResistantBPScreenNeeded
			SleepApnoeaScreenNeeded
			LipidLoweringAgentNeeded
			StatinInitiationBaselineTestsNeeded
			BGLAbnormal-TestNeeded
			CKDPresent
			CKDCauseInvestigationNeeded
			CKD-MedicationDoseReductionNeeded
			CKD-MedicationsAdverselyAffectingKidneyFunctionPresent
			CKD-ACEIOrARBNeeded
			CKD4Or5Present
            MetabolicSyndromePresent
			CardiovascularRiskAutomaticallyHigh
			AbsoluteCardiovascularRiskCalculatorMediumOrHigher
			NSAIDInUse
			NSAIDRiskBenefitAnalysisNeeded
			FHAbdominalAorticAneurysm
			PneumococcalImmunisationNeeded
			ECGAbnormal
			AnkleBrachialIndexAbnormal
			OsteoporosisChronicDiseaseRiskFactorPresent
			OsteoporosisRiskFactorsPresent
			DementiaPresent
			PhysicalExaminationAbnormal
			RepeatScriptNeeded
			PracticeNurseReferral
			EPCNeeded
			PersonalisingThePlan


			MainGoals
			PatientSaysAlreadyDoing
			DoctorSaysGoingWell
			PatientImportance0to10
			PatientConfidence0to10
			ChecklistCopyToPatient
			ChecklistCopiesToTeamWithConsent
			ChecklistReferralFormsCompleted
			ReviewDate
		}

	}

	SubClinics {
		GPMPAF
		GPMPAnxiety
		GPMPAorticAneurysm
		GPMPArthritisInflammatory
		GPMPArthritisOsteoarthritis
		GPMPGout
		GPMPAsthma
		GPMPBackPain
        GPMPBariatricSurgery
		GPMPBipolar
		GPMPCancer
		GPMPCarotidStenosis
		GPMPCHD
		GPMPCHF
		GPMPChronicPain
		GPMPCoeliac
		GPMPCOPD
		GPMPCVD
		GPMPDementia
		GPMPDepression
		GPMPDiabetes
		GPMPEpilepsy
		GPMPGORD
		GPMPHeadaches
        GPMPHormoneReplacementTherapy
		GPMPHTN
		GPMPIBD
		GPMPOsteoporosis
		GPMPMultipleSclerosis
		GPMPParkinsonsDisease
		GPMPPCOS
		GPMPPVD
        GPMPTestosteroneReplacement 
		GPMPThyroid
	}
}

Question MetabolicSyndromePresent {
	" Metabolic syndrome is a disease state in it's #own right#. 
    
    Do you wish to #treat Metabolic Syndrome#?"
    YesNoNA
	NAText "Not present"
    Category "lifestyle"	
    Triggers {
		Yes: education optional MetabolicSyndromeCriteria
		Yes: problem MetabolicSyndromePresent
	}
    Id c72f9e2d-1485-44ff-baed-e875dad80c29
    Changelog {
        2020-04-19: ""
    }
}

Education MetabolicSyndromeCriteria {
	Message "Metabolic Syndrome Criteria"
    Link "https://www.labtestsonline.org.au/learning/index-of-conditions/metabolic"
    Id b69d43db-7fee-440c-b47a-6913b1bae42f
    Changelog {
        2020-04-19: ""
    }
}


Problem MetabolicSyndromePresent {
	Goal "Reduce risk of progression"
    Plan "Encourage focus on waist reduction and exercise."
    Id 1a8bdf3c-8360-4ee8-9c2f-4118a5d8fc9e
    Changelog {
        2020-04-19: ""
    }
}





Clinic GeneralGPMPCore {
	Id 13885b18-d54c-4ce1-ba4c-4e2509c9fab0
	Questions {
		Patient {
			ConsentForGPMP
			ResearchConsent
			AlcoholIntakeMoreThanRecommended
			Smoker
			PhysicalActivityNeeded
			WeightLossTipsDesired
            FHOfPrematureCVDLessThan60Years
			ForgetToTakeMedication
			StopTakingWhenWell
			CarelessSometimesWithTaking
			SideEffectOfMedicationSuspected
			DepressionPresent
			AnxietyPresent
			AdvanceHealthDirectiveNeeded
			FallInLastYear
			FractureMinimalTrauma
			CarerNeeded
			ExtraHelpNeeded
			NationalSupportOrganisationReferralNeeded
			NationalBowelScreenNeeded
			BreastScreenNeeded
			BreastScreenNeeded40to49
			CervicalScreeningNeeded
			InfluenzaMedicalConditionImmunisationNeeded
            InfluenzaImmunisationHealthy 
			ShinglesImmunisationNeeded
			PertussisImmunisationNeeded
			SexualHealthConcerns
			ErectileProblem
		}

		Checklist {
			ConsentGPMP
			ReminderGPMP
			ReminderGPMPReview
			ReminderDiabetesCyclePlanEvery12Months
			HomeSystolicBPAverage
			HypotensionCheck 
            RecordHeightWeightWaist
			RecordSmokingAlcohol
			ECG
			DopplerABI
			PrintHealthSummary
			SmokerBasicSpirometry
			MMSE
			RecordCarerDetails
		}

		Staff {
			HealthSummaryAndMedicationListReviewNeeded
			BPMedicationChangeNeeded
			SleepApnoeaScreenNeeded
			LipidLoweringAgentNeeded
			CKDPresent
			CardiovascularRiskAutomaticallyHigh
			AbsoluteCardiovascularRiskCalculatorMediumOrHigher
			PneumococcalImmunisationNeeded
			ECGAbnormal
			AnkleBrachialIndexAbnormal
			DementiaPresent
			PhysicalExaminationAbnormal
			EPCNeeded
			PersonalisingThePlan


			MainGoals
			PatientSaysAlreadyDoing
			DoctorSaysGoingWell
			PatientImportance0to10
			PatientConfidence0to10
			ChecklistCopyToPatient
			ChecklistCopiesToTeamWithConsent
			ChecklistReferralFormsCompleted
			ReviewDate
		}

	}

	SubClinics {
		GPMPAFCore
		GPMPAnxiety
		GPMPAorticAneurysmCore
		GPMPArthritisInflammatoryCore
		GPMPArthritisOsteoarthritisCore
		GPMPGout
		GPMPAsthma
		GPMPBackPain
        GPMPBariatricSurgery
		GPMPBipolar
		GPMPCancer
		GPMPCarotidStenosisCore
		GPMPCHDCore
		GPMPCHFCore
		GPMPChronicPain
		GPMPCoeliac
		GPMPCOPD
		GPMPCVDCore
		GPMPDementiaCore
		GPMPDepression
		GPMPDiabetesCore
		GPMPEpilepsyCore
		GPMPGORD
		GPMPHeadaches
        GPMPHormoneReplacementTherapy
		GPMPHTN
		GPMPIBD
		GPMPOsteoporosisCore
		GPMPMultipleSclerosis
		GPMPParkinsonsDisease
		GPMPPCOS
		GPMPPVDCore
        GPMPTestosteroneReplacement
		GPMPThyroid
	}
}


Question PracticeNurseReferral {
	"
	Chronic disease patients can often benefit from extra care such as education, wound care, and immunisations.

    Item 10997 is available for the practice nurse to provide services during the year consistent with the GPMP.

	Do you want to incorporate the Practice Nurse into the GPMP? 
    "
    
    YesNo
    Category "general"
    Triggers {
		Yes: problem PracticeNurseReferral
	}
    Id 84ef52cd-b2de-427b-b990-46b7d45f9219
    Changelog {
        2018-10-09: ""
    }
}

Problem PracticeNurseReferral {
	Goal "Improve care"
    Plan "Practice Nurse to provide chronic disease education / wound care / immunisations."
    Id c1e1d105-6c1e-4f30-8d1e-d1a93f9bc6e7
    Changelog {
        2018-10-09: ""
    }
}


Question NationalSupportOrganisationReferralNeeded {
	"
    All major medical conditions have a national not-for-profit organisation that provides support and information for people with that condition.

	Would you like the website contact details?
    "
   
    YesNo
	Category "social"
    Triggers {
		Yes: problem NationalSupportWebsiteReferralNeeded
	}
    Id a919ab63-66b2-47bb-af7e-dea511e8c589
    Changelog {
        2018-08-16: ""
    }
}

Problem NationalSupportWebsiteReferralNeeded {
	Goal "Increase support"
    Plan "Do a Google search for your condition and add 'Australia' to the search"
    Id ef69f85b-ea32-40a1-9a78-fd4c47650ebc
    Changelog {
        2018-08-16: ""
    }
}



Question InfluenzaImmunisationHealthy {
	"Some #healthy# people need an annual influenza (flu) immunisation.
	
    After #65 years age# your immune system is weaker (just like your skin is thinner) so boosting it with an annual immunization is a great idea. 
    
    Do you want to discuss having an #annual flu immunisation# with your doctor?"
    
    YesNo
	Category "prevention"
    Triggers {
		Yes: problem InfluenzaAtRiskGroup
	}
    Id 6115432a-624b-4789-942a-e94c6283dfe9
    Changelog {
        2018-04-14: ""
    }
}


Problem CardiovascularRiskAutomaticallyHigh {
	Goal "Identify pre-existing high CV risk"
    Plan "Treat aggressively with lipid lowering and BP lowering medication unless contraindicated or clinically inappropriate. No need to perform a CV risk calculation."
    Id 9c44c087-3ded-4674-9c00-d11856824867
    Changelog {
        2018-01-20: ""
    }
}


Question HypotensionCheck {
	"Record BP within 1 minute of standing if on tablets - notify Dr if greater than 20 drop
    "
    
    Check optional
    Category "Heart / Brain"
    
    Id 923b3341-6f5c-4dcf-adac-4a0c99f8d4bc
    Changelog {
        2017-10-23: ""
    }
}



Question ConsentForGPMP {
	"
We would like to give you a #full check-up# as part of a General Practice Management Plan (Medicare item 721) with the help of this computer application, the Patient Assistance Tool (PAT), which is designed to help patients and their healthcare team create thorough care plans, securely share them and keep them up to date. To enable this PAT Pty Ltd needs your consent to securely handle and store your data in accordance with its https://www.patsoftware.com.au/privacy[Privacy Policy] - a summary of it is below.

== Collection of personal information

PAT collects personal information about you through the PAT application from information provided by you and your healthcare practitioner. If you do not provide all the information requested by the PAT application, it may compromise the value of the information and services provided to you.

Our purposes for collecting your personal information

The PAT application collects personal information to:

* customise your care plan to your medical conditions and to give your healthcare practitioners access to relevant information as they work on your care plan
* allow users to, view, comment on and share their care plan
* send users reminders regarding certain goals in their care plan
* enable data to be used by your medical practice for internal audits to ensure good care
* if you provide additional consent on the next screen, to enable data to be used for medical research that is approved by an external ethics committee
* if you provide additional consent, to enable care plans to be shared with health practitioners that you are referred to
* provide the PAT application, including troubleshooting and handling complaints
* maintain, protect and improve the PAT application, and to develop new products or services;
* provide users with information about new products and services that may be of interest to them
* for any purpose required or authorised by Australian law

== Disclosure of your personal information

Data loaded into the PAT application could be seen by authorised personnel at the medical practice using the PAT system, healthcare practitioners that your doctor may refer you to (with your additional consent), trusted medical research organisations (if you consent on the next screen), yourself & the people you authorise, third parties by court order and a limited number of authorised personnel at PAT. We may disclose the information we collect about you to our service providers, our consultants, advisors and auditors, regulatory bodies and government authorities as required or authorised by law in a way that ensures these parties will safeguard your privacy and for purposes consistent with this Privacy Policy and the Privacy Act.

We will never sell your personal information. We will not share your information with the MyHealthRecord system without your explicit consent.

== Access, correction and complaints
:linkattrs:
	
Information on how to access or correct the personal information we hold about you, and about our privacy complaint management procedures can be found in our Privacy Policy at  https://www.patsoftware.com.au/privacy[https://www.patsoftware.com.au/privacy,title=\"PAT Privacy Policy\"]

Do you give consent to PAT handling your personal information as set out above?
	"
	
	Consent required
	Category "admin"
	
	Id ecdcde6d-8550-4ab7-9efd-578060008c07
	Changelog {
		2017-01-14: ""
	}
}

Question ResearchConsent {
	"
    Medical research helps to improve patient care. Do you consent to your information being used for research that is approved by an ethics committee?
	"
	
	YesNoUnsure
	Category "admin"
	
	Id d2058f09-a602-449c-9c21-f8c89ad50405
	Changelog {
		2017-01-14: ""
	}
}


Question AlcoholIntakeMoreThanRecommended {
	"A small amount of alcohol is safe, however if your #*daily*# intake is #more than 2 standard drinks# your health can be affected. 
	
	A standard drink is 10oz of heavy beer, or 100mL of wine or 1 nip of spirits.
	
	image:AlcoholIntakeMoreThanRecommended.jpg[width=400]
	
	Is your alcohol intake more than 2 standard drinks per day? 
    "
	
	YesNoUnsure
	Category "lifestyle"
	
	Triggers {
		Yes Unsure: problem AlcoholIntakeMoreThanRecommended
	}
	Id ae225c4e-ae67-4a18-91f7-251f3e2c9eb2
	Changelog {
		2017-01-14: ""
	}
}


Question Smoker {
	"Are you a smoker? 
	
	image:Smoker.jpg[width=350]"
	
	YesNo
	Category "smoking"
	
	Triggers {
		Yes: problem Smoking
		Yes: education required SmokingEducation
	}
	Id 6bfb62c8-f196-402f-bad3-3bf28faa09fb
	Changelog {
		2017-01-14: ""
	}
}


Question PhysicalActivityNeeded {
	"Physical activity for at least 150 minutes per week (2½ hours), which #can be accumulated in multiples of 10 minutes# over the week, is as good for your heart as quitting smoking. 
	
	Do you do #150 minutes# (2.5 hours) of physical activity per week?"
	
	YesNoUnsure
	Category "lifestyle"
	
	Triggers {
		No Unsure: problem PhysicalActivityNeeded
		No Unsure: education required ExerciseEducation
	}
	Id 454e6785-c379-4adc-ac23-a7a3af1cfb00
	Changelog {
		2017-01-14: ""
	}
}


Question WeightLossTipsDesired {
    "Would you like to view some tips to help you lose weight?"
	
	YesNoUnsure
	Category "lifestyle"
	
	Triggers {
		Yes: problem WeightLossDesired
		Yes: education required WeightEducation
	}
	Id 08b0c6c8-3c65-477a-b9f9-691d8830e150
	Changelog {
		2017-01-14: ""
	}
}




Question ForgetToTakeMedication {
	"One the best ways of managing your condition is to take your medications regularly. We are very interested in helping you to do this. 
	
	Do you ever #forget# to take your medication?"
	
	YesNoUnsure
	Category "medications"
	
	Triggers {
		Yes Unsure: problem AdherenceProblem
	}
	Id 3e4ba3d4-fd66-46e0-9c30-2b09bb67c402
	References {
		"4 item Morisky Medication Adherence Questionnaire"
	}
	Changelog {
		2017-01-14: ""
	}
}


Question StopTakingWhenWell {
	"When you #feel better#, do you sometimes #stop taking# your medication?"
	
	YesNoUnsure
	Category "medications"
	
	Triggers {
		Yes Unsure: problem AdherenceProblem
	}
	Id d1bc0fe7-7832-41e5-b925-f99e4c85168c
	Changelog {
		2017-01-14: ""
	}
}


Question CarelessSometimesWithTaking {
	"Are you #careless at times# about taking your medication?"
	
	YesNoUnsure
	Category "medications"
	
	Triggers {
		Yes Unsure: problem AdherenceProblem
	}
	Id 133ccd8c-a515-4b8c-a207-708f48ba1042
	Changelog {
		2017-01-14: ""
	}
}


Question SideEffectOfMedicationSuspected {
	"Sometimes, if you #feel worse# when you take your medication, do you #stop taking# it?"
	
	YesNoUnsure
	Category "medications"
	
	Triggers {
		Yes Unsure: problem AdherenceProblem
	}
	Id e0b5e8a5-9dc1-4f68-9e40-65a0e79e72eb
	Changelog {
		2017-01-14: ""
	}
}

Question DepressionPresent {
	"Often feeling down (depressed) can be a problem, but there are many ways that we can help you. So: 
	
	- During the past month have you *often* been bothered by #feeling down#, depressed, or hopeless? 
	
	OR 
	
	- During the past month have you *often* been bothered by #having little interest or pleasure# in doing things?"
	
	YesNo
	Category "psychology"
	
	Triggers {
		Yes: problem DepressionOrAnxietyPresent
	}
	Id 1e4f59d8-8fe4-46c4-9544-5b509b551bd2
	References {
		"PHQ-2"
	}
	Changelog {
		2017-01-14: ""
	}
}


Question AnxietyPresent {
	"#Feeling anxious is common.# Counselling can help. 
	
	- Have you been worried often in the last month? 
	- Do you think you have always been a worrier? 
	- Do you find that you are using alcohol or other substances to help you cope with your worry? 
	- In the past month have you felt so fidgety or restless that you could not sit still? 
	
	Did you answer *yes* to #any one# of these?"
	
	YesNo
	Category "psychology"
	
	Triggers {
		Yes: problem DepressionOrAnxietyPresent
	}
	Id bf09b391-c7a3-4aa3-a6f2-e247aeaa5eea
	Changelog {
		2017-01-14: ""
	}
    References {
        "The FEAR: a rapid screening instrument for generalized anxiety in elderly primary care attenders (Krasucki et al, 1999)"
    }
}


Question AdvanceHealthDirectiveNeeded {
	"An #Advance Health Directive# enables you to #make your decisions now# about future health possibilities when you could be too unwell to decide and it might be difficult for your family to make decisions for you.
	
	If you do not have an Advance Health Directive already, would you like to discuss this with your doctor?"
	
	YesNoNA
	NAText "N/A - already have an Advance Health Directive"
	Category "general"
	
	Triggers {
		Yes: problem AdvanceHealthDirectiveNeeded
	}
	Id e8c2cfdf-9a21-4389-a93f-9f99a1a19008
	Changelog {
		2017-01-14: ""
	}
}


Question FallInLastYear {
	"Have you had #a fall# in the last 12 months?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem FallsAssessmentNeeded
	}
	Id 609de37a-adbb-4aa6-919f-d19b6c82b675
	Changelog {
		2017-01-14: ""
	}
}


Question CarerNeeded {
	"A #carer#, either a family member or someone else, to assist you may be needed because of age or illness.

    You may already have a carer.

    However, if you do not, would you like one? 
    "
	
	YesNoNA
	NAText "N/A - I do not need a carer"
	Category "social"
	
	Triggers {
		Yes: problem CarerNeeded
	}
	Id d3e2e694-f074-42b1-9b61-52ec0e32a196
	Changelog {
		2017-01-14: ""
	}
}


Question ExtraHelpNeeded {
	"When you have a long term medical condition sometimes you may need #extra help# at home with 
	
	* House cleaning 
	* Cooking 
	* Showering 
	* Transport 
	
	Do need assistance with one or more of the above?"
	
	YesNo
	Category "social"
	
	Triggers {
		Yes: problem ExtraHelpRequired
	}
	Id 3362bc9d-4530-4ae9-8260-11f6f11afe47
	Changelog {
		2017-01-14: ""
	}
}


Question NationalBowelScreenNeeded {
	"The #National Bowel Cancer screening program# starts at age 50 and continues every 2 years to age 74. 
	
	The programme mails out a free simple home test kit to you. 
	
	It allows early detection and we *strongly recommend* you start participating at age 50. 
	
	Do you wish to discuss this program with your doctor?"
	
	Age 50 to 75
	YesNoNA
	NAText "N/A - already having bowel screening"
	Category "prevention"
	
	Triggers {
		Yes: problem NationalBowelCancerScreeningProgramNeeded
	}
	Id 204a86a5-5169-4314-968d-5c42b7753407
	Changelog {
		2017-01-14: ""
	}
}


Question BreastScreenNeeded {
	"
    Women between 50 to 74 years of age are recommended to have #BreastScreen mammograms#.

    These should be done every 2 years for most people, but more often in some cases where there is breast cancer in the family.
	
	Do you wish to #discuss BreastScreen# more with your doctor?"
	
	YesNo
	Age 50 to 75
	Gender female
	Category "prevention"

	Triggers {
		Yes: problem BreastScreenDiscussion
		No: problem BreastScreenNeeded
	}
    Id bcdeba97-32e2-4c38-ad41-e51b9b298700
	Changelog {
		2019-05-16: "separate question for ages 40-49"
	}
}
Question BreastScreenNeeded40to49 {
	"
    While women between 50 to 74 years of age are recommended to have #BreastScreen mammograms#, between 40 and 49 years BreastScreen is optional.

    This is because in this age group there is a higher rate of false positives (incorrect diagnosis of cancer) and false negatives (incorrect diagnosis that there is no cancer).
	
	Do you wish to #discuss BreastScreen# more with your doctor?"
	
	YesNoNA
	NAText "Already having BreastScreen"
	Age 40 to 49
	Gender female
	Category "prevention"

	Triggers {
		Yes: problem BreastScreenDiscussion40to49
	}
	Id 4ff515de-52e8-4901-8061-93b05dc6f2cb
	Changelog {
		2019-05-16: "separate question for ages 40-49"
	}
}


Question CervicalScreeningNeeded {
	"The #National Cervical Screening Program# starts at age 25 and continues regularly to age 75. It allows early detection of cervical cancer and we *strongly recommend* you start participating from age 25.

	From 2018 a new Cervical Screening Test detects the virus that causes cervical cancer. If there are no abnormalities it should be done 2 years after a Pap Smear test, and then every 5 years.
	
	Do you need to #arrange an appointment# for a Cervical Smear?"
	
	YesNoNA
	NAText "N/A - Hysterectomy so not needed"
	Age 25 to 75
	Gender female
	Category "prevention"

	Triggers {
		Yes: problem CervicalScreeningNeeded
	}
	Id 29ad8d7d-4908-4be9-9abc-b407e9e83e27
	Changelog {
		2017-01-14: ""
	}
}


Question InfluenzaMedicalConditionImmunisationNeeded {
	"A lot of patients with a #chronic disease# benefit from an annual influenza (flu) immunisation. 
	
	If you have pre-existing heart disease or if you have risk factors (such as high blood pressure or cholesterol) the flu causes #inflammation# inside your arteries and so increases your risk of blood clots and death during the flu.
		
	If you have #any lung problems# such as asthma requiring preventers or COPD, flu severity is also increased.
	
	Do you need an #annual flu immunization#?"
	
	YesNoNA
	NAText "N/A - I am healthy"
	Category "prevention"
	
	Triggers {
		Yes: problem InfluenzaAtRiskGroup
        No: AutoAnswer {
			InfluenzaImmunisationHealthy NA
		}
		
		
	}
	Id 71c67951-e393-454b-82a2-7c93cff8e214
	Changelog {
		2017-01-14: ""
	}
}


Question ShinglesImmunisationNeeded {
	"#Shingles immunisation# is a single injection and is now provided free for patients aged 70 to 79 years. 

    Without the benefit of shingles immunisation the lifetime risk of shingles is 30 to 35% of patients. The most common complication is persistent neuralgic pain which can go for years. 
	
	It is designed to prevent shingles and we *strongly recommend* you start participating at age 70. 
	
	Do you wish to discuss this program with your doctor?"
	
	YesNoNA
	Age 70 to 79
	NAText "N/A - Already had it / or not in the age group"
	Category "prevention"
	
	Triggers {
		Yes: problem ShinglesImmunisationNeeded
	}
	Id 2ae7252a-1c77-4e87-846a-fb77ac0bcf50
	Changelog {
		2017-01-14: ""
	}
}


Question PertussisImmunisationNeeded {
	"#Whooping cough (pertussis)# causes severe illness in people over 65 years age. The violent cough attacks, interfering with sleep, can last for 3 months.
	
	This single injection also covers for tetanus, and is recommended at age 65 years and every 10 years after that. It is #not provided free# and costs about $50 at the chemist. 
	
	Do you wish to discuss this program with your doctor?"
	
	YesNoNA
	Age > 65
	NAText "N/A - already had one / or not in the age group"
	Category "prevention"
	
	Triggers {
		Yes: problem PertussisImmunisationNeeded
	}
	Id 8e0bd250-3aae-44e3-9cb9-6d96af921330
	Changelog {
		2017-01-14: ""
	}
}


Question SexualHealthConcerns {
	"One of the purposes of this check up is to see if your sexual health is ok. We can often help if there are problems. 

	* Heavy periods. 
	* Painful periods. 
	* Pain (or vaginal dryness) with intercourse. 
	* Contraception concerns. 
	* Concerns re change of life. 
	* Reduced desire (libido). 
	* Urine incontinence (stress or urge)
	
	Do you wish to discuss #any# of the above problems with your doctor?"
	
	YesNoUnsure
	Gender female
	Category "Women's health"

	Triggers {
		Yes Unsure: problem SexualHealthConcerns
	}
	Id db7d9b6d-7b20-417d-a2ef-c6a3f1d66fe8
	Changelog {
		2017-01-14: ""
	}
}


Question ErectileProblem {
	"High blood pressure (hypertension) and other risk factors can #decrease the blood flow to the penis# and interfere with erection strength. 
	
	We can help you if you are having any difficulties. 
	
	Are you having #any# problems with #erections#?"
	
	YesNoUnsure
	Gender male
	Category "Men's health"

	Triggers {
		Yes Unsure: problem ErectileProblem
	}
	Id 54d0211b-4732-4c63-9574-c0c0af582705
	Changelog {
		2017-01-14: ""
	}
}


Question ConsentGPMP {
	"Record consent to proceed with 721 (GP management plan) and 723 (TCA)"
	
	Check required
	Category "admin"
	Id 67e41a0d-ce0d-e352-889a-1d3d4dfe76c9
	Changelog {
		2017-01-14: ""
	}
}


Question ReminderGPMP {
	"All patients: enter reminder for General GPMP for 12 months (+ Cardio if ECG/ABI needed)"
	
	Check required
	Category "admin"
	Id c12d38f6-d337-ba5a-95b5-569be716ec31
	Changelog {
		2017-01-14: ""
	}
}


Question ReminderGPMPReview {
	"All patients: enter reminder  General GPMP Review for 6 months (+ Cardio if CV or diabetes)"
	
	Check required
	Category "admin"
	Id b17ab40d-8a50-6e55-8857-72e24af25796
	Changelog {
		2017-01-14: ""
	}
}


Question ReminderDiabetesCyclePlanEvery12Months {
	"If  diabetic, ensure reminder in place for annual Diabetes Cycle Plan"
	
	Check optional
	Category "admin"
	Id 0c8faf8b-5645-aa5f-9f68-4da4745f18d6
	Changelog {
		2017-01-14: ""
	}
}


Question HomeSystolicBPAverage {
	"Record home BP average systolic & diastolic as sitting BP in clinical notes. Add systolic as score"
	
	Number required
	Category "Heart / Brain"
	Id eef602f0-038c-7155-ab36-e9e37cfdf918
	Changelog {
		2017-01-14: ""
	}
}


Question RecordHeightWeightWaist {
	"Record waist hip ratio, height & weight in the clinical notes"
	
	Check required
	Category "Heart / Brain"
	Id 3a7d4bc0-36a7-635c-9f5d-efe01a9baaae
	Changelog {
		2017-01-14: ""
	}
}


Question ECG {
	"ECG is optional. Do if has diabetes or established CV disease. Add 'General GPMP Cardio'"
	
	Check optional
	Category "Heart / Brain"
	Id ae3dcae6-0680-6059-92c4-f4dba7568596
	Changelog {
		2017-01-14: ""
	}
}


Question DopplerABI {
	"Doppler ABI is optional. Do if diabetes or established CV disease. Add 'General GPMP Cardio'."
	
	Check optional
	Category "Heart / Brain"
	Id dac2297a-2810-655c-8c3f-a8fa61c0e7bc
	Changelog {
		2017-01-14: ""
	}
}


Question SmokerBasicSpirometry {
	"If a smoker or ex smoker perform spirometry (no need to administer Ventolin)"
	
	Check optional
	Category "lungs"
	Id 509bfdf1-b459-8f5f-b663-100a86668889
	Changelog {
		2017-01-14: ""
	}
}


Question MMSE {
	"If possible dementia perform MMSE score in clinical notes and enter score here"
	
	Number optional
	Category "neurology"
	Id bc64fe95-1ef9-695c-9829-a7741e68fa6a
	Changelog {
		2017-01-14: ""
	}
}


Question RecordCarerDetails {
	"Record next of kin and/or carer details in appropriate section of clinical notes"
	
	Check required
	Category "admin"
	Id d5df9b06-2074-f35d-8ac0-9851c2817f01
	Changelog {
		2017-01-14: ""
	}
}


Question HealthSummaryAndMedicationListReviewNeeded {
	"[big]#To ensure a comprehensive review, it is first necessary to review and update the health summary.#
		
	Do you want to review the health summary?"
	
	YesNo
	Category "general"
	
	Education "Essential elements of Health Summary update" {"

    * #Clean# the data by ensuring all diagnoses are from the \"drop down\" list and not free texted diagnoses.
	* Ensure #allergies# are correct.
	* Ensure the #active and inactive problem lists# are correct and relevant. 
	* Review each #medication# - reason for use and is it still valid. 
	* Ensure #immunisations# are up to date and delete old influenza immunisations. 
	* Ensure #family history# is recorded. 	
	* Add #new problems# discovered today. 
	* Add any #extra details# relevant to that diagnosis via free text in the additional information box(e.g. echo result if CHF). 
	"}
	Triggers {
		Yes: problem HealthSummaryReviewNeeded
	}
	Id f54b8e91-3aae-48a7-a96e-24f801bd9de1
	Changelog {
		2017-01-14: ""
	}
}


Question MedicationReviewNeeded {
	"To ensure a #medication review#, it is necessary to review the bag of medications the patient has brought in and update medications in the health summary. 
	
	NPS defines polypharmacy as taking 5 or more medications. 
	
	Do you want to do a 'polypharmacy' review?"
	
	YesNoNA
	NAText "N/A - Reviewed with health summary "
	Category "general"
	
	Triggers {
		Yes: problem MedicationReviewNeeded
		Yes: education optional Polypharmacy
	}
	Id 9114f21f-c587-422c-b95a-f54fefac87b3
	Changelog {
		2017-01-14: ""
	}
}


Question PathologyCheck {
	"It is very useful to check pathology before proceeding to the next screens."
	
	Display
	Category "general"
	
	Links {
		ACR
		BSL
		EGFR
		HbA1C
		HDLCholesterol
		LDLCholesterol
		TotalCholesterol
		Triglycerides
	}
	Id e5c03c18-2bf9-434e-9702-fbf4499598c0
	Changelog {
		2017-01-14: ""
	}
}


Question BPMedicationChangeNeeded {
	"After checking the recommended target BP do you want to alter BP treatment?"
	
	YesNoNA
    NAText "Already on maximal therapy"
	Category "Heart / Brain"

	Education "BP targets" {"
		The #home# BP limits recommended by the National Heart Foundation (NHF) 2016 Guidelines are:
			
			* < #135/85# for #uncomplicated# (1) Hypertension or (2) CKD with no microalbuminuria (3) Diabetes with no microalbuminuria)at #any# age (equivalent to 140/90 office BP) 
			* < #125/75# for most #complicated high risk# patients (= with any associated clinical conditions or target organ damage e.g. microalbuminuria ) (equivalent to 130/80 office BP) 
			* < #120/75# for selected #sub group of complicated high risk# patients (see NHF 2016 HT guidelines) 
			
			A large Australian hypertension study, VIPER-BP, showed that, in patients with BP not controlled, #reviewing more frequently# with #escalation of treatment# significantly improved % controlled. 

		== References

		* https://www.heartfoundation.org.au/for-professionals/clinical-information/hypertension[NHF Guide to management of Hypertension in Adults, 2016 ]
	"}
	
	Triggers {
		Yes NA: show MedicationsOrOTCThatMightElevateBP
		Yes NA: show ResistantBPScreenNeeded
		Yes: problem BPOutsideRange
		Yes: education optional HomeBPMonitoringChart
		Yes: education optional HeartFoundationHypertensionGuidelines2016
	}
	Links {
		HomeSystolicBPAverage
	}
	Id 95c83e62-2b4a-48dc-8263-e19c7f9a0e60
	Changelog {
		2017-01-14: ""
	}
}

Education HeartFoundationHypertensionGuidelines2016 {
	Message "Hypertension Guidelines 2016 - National Heart Foundation"
	Link "https://www.heartfoundation.org.au/for-professionals/clinical-information/hypertension"
	Id f2c1fd96-e32f-4d43-9bf6-82824ca89ee1
}


Question SleepApnoeaScreenNeeded {
	"If the patient has #more than one# of the following four symptoms they may have sleep apnoea: 
	
	1. Loud snoring 
	2. Often feeling sleepy during the day 
	3. Had someone observe them stop breathing during sleep 
	4. On treatment for high blood pressure 
		
	Do you want to refer for a sleep study?"
	
	YesNoNA
	NAText "N/A - Already diagnosed/referral to sleep physician"
	Category "lungs"
	Education "Medicare requirements for a direct referral sleep study" {"

	#Medicare requires# for direct referral for diagnostic sleep study either one of:
	
	- OSA-50 Score > or = 5 
	
	- or Stop Bang > or = 4
	
	- or Berlin Questionnaire - high risk 
	
	#PLUS# 
	
	an EES Score > OR = 8 
		"}

	Education " OSA-50 Questionnaire" {"

		* https://www.sahealth.sa.gov.au/wps/wcm/connect/c5dd56804835ab82b096bb8158025913/Outpatient+Service+Description+and+Triage+Guidelines+Respiratory+and+Sleep+Services+OSA+50+Screening+Questionnaire+SALHN.pdf?MOD=AJPERES[OSA-50 questionnaire]
	"}

	Education " Stop Bang Questionnaire" {"

		* https://www.sleepservices.com.au/stop-bang-questionnaire/[Stop Bang questionnaire]		
	"}

	Education "Epworth Sleepiness Scale (EES)" {"

		* https://www.thecalculator.co/health/Epworth-Sleepiness-Scale-Calculator-905.html[Epworth Sleepiness Scale]
		"}
	Triggers {
		Yes: problem SleepApnoeaScreenNeeded
		Yes: education optional SleepApnoeaScreen
	}
	Id e48ebde0-a033-4420-aa90-4e15c7f56ea3
	Changelog {
		2017-01-14: ""
	}
}


Question LipidLoweringAgentNeeded {
	"The PBS Statement on Lipid-Lowering Drugs has been removed so prescribing can now be based on Heart Foundation guidelines.
	
	Do you want to start a lipid lowering agent?
    "
	
	YesNoNA
	NAText "N/A - on medication"
	Category "Heart / Brain"

    Education "Lipid Management guidelines" {"
        Primary prevention:
        
        * https://www.heartfoundation.org.au/images/uploads/publications/Absolute-CVD-Risk-Full-Guidelines.pdf[Guidelines for the management of absolute cardiovascular disease risk (2012)]
        * https://www.heartfoundation.org.au/images/uploads/publications/Absolute-CVD-Risk-Quick-Reference-Guide.pdf[Quick Reference Guide]
        * https://www.heartfoundation.org.au/for-professionals/clinical-information/absolute-risk[Absolute risk resources for health professionals]

        Secondary prevention: 
        
        * https://www.heartfoundation.org.au/images/uploads/publications/Reducing-risk-in-heart-disease.pdf[Reducing risk in heart disease (2012)]

    "}

	Triggers {
		Yes: problem LipidAgentNeeded
		No NA: AutoAnswer {
			StatinInitiationBaselineTestsNeeded NA
		}
	}
	Links {
		HDLCholesterol
		LDLCholesterol
		TotalCholesterol
		Triglycerides
	}
	Id 87c3c406-d234-4704-b2be-d6ca8ce7736d
	Changelog {
		2017-01-14: ""
	}
}

Question BGLAbnormal-TestNeeded {
	"If the fasting BGL is #outside range (> 6mmol)# diabetes or impaired glucose tolerance needs to be excluded. Options are: 
	
	* glucose tolerance test (OGTT) or
	* HbA1c (>6.5 is positive) 
	
	Do you want to order #a test?#"
	
	YesNoNA
	NAText "N/A - known diabetic"
	Category "blood sugar"

	Triggers {
		Yes: problem BGLAbnormal-TestNeeded
	}
	Links {
		BSL
        HbA1C
	}
	Id e7785672-866d-452c-acbc-acee36b48f5d
	Changelog {
		2017-01-14: ""
	}
}


Question CKDPresent {
	"Review the path results for microalbuminuria (ideally a First Void urinary ACR), the eGFR, and any PH kidney problems to determine if CKD (Chronic Kidney disease) is present.


	Is CKD present?"
	
	YesNo
	Category "kidneys"

	Education "Classification of grades of Chronic Kidney Disease (Kidney Health Australia)" {"

	- Stage 1. #Normal GFR (> 90 ml)# with other evidence of chronic kidney damage.* 
	- Stage 2. #Mild impairment GFR 60 - 89# with other evidence of chronic kidney damage.* 
	- Stage 3. #Moderate impairment GFR 30 - 59 (3a is 45-59 and 3b is 30-44)# 
	- Stage 4. #Severe impairment GFR 15 - 29# 
	- Stage 5. #Established renal failure (ERF) GFR < 15 or on dialysis#.
	
	This other evidence of kidney damage may be: 
	
	* #Persistent microalbuminuria# 
	* #Persistent proteinuria# 
	* #Persistent glomerular haematuria# 
	* #Histological abnormalities (renal biopsy)# 
	* #Anatomical abnormalities (abnormal ultrasounds, X-rays or CT)# 
	
	[red]#Note if patients found to have a GFR of greater than 60# [underline blue]#without# [red]#other evidence as above CKD is# #not# [red]#present.#	

	== References

	* https://kidney.org.au/cms_uploads/docs/02_algorithm-for-inital-detection-of-ckd.pdf[Algorithm for initial detection of CKD. CKD management in General Practice 3rd edition (2015), Kidney Health Australia]
	"}
	
	
	Triggers {
		No: AutoAnswer {
			CKDCauseInvestigationNeeded NA
			CKD-MedicationDoseReductionNeeded NA
			CKD-MedicationsAdverselyAffectingKidneyFunctionPresent NA
			CKD-ACEIOrARBNeeded NA
			CKD4Or5Present NA
		}
		Yes: problem ChronicKidneyDiseasePresent
	}
	Links {
		ACR
		EGFR
	}
	Id 216888d8-c2e1-43c9-bd06-3da51a42cd58
	Changelog {
		2017-01-14: ""
	}
}


Question CKDCauseInvestigationNeeded {
	"If they have CKD, has an #underlying diagnosis# already been made? 

	(see link below for urgent causes to exclude if this is the #initial diagnosis of CKD#)"
	
	YesNoNA
	NAText "N/A - no CKD"
	Category "kidneys"
	
	Education "Common causes of CKD" {"
	* diabetic nephropathy 
	* glomerulonephritis 
	* hypertensive vascular disease 
	* polycystic kidney disease 

	== References

	* https://kidney.org.au/health-professionals/prevent/chronic-kidney-disease-management-handbook[CKD Management in General Practice 3rd edition (2015), Kidney Health Australia]
	
	"}
	Triggers {
		No: problem ChronicKidneyDiseasePresent
		Yes: education optional CKDInitialDiagnosis
	}
	Id f95fd7a7-4171-4fe2-9d23-0c8ce0e36023
	Changelog {
		2017-01-14: ""
	}
}


Question CKD-MedicationDoseReductionNeeded {
	"Some commonly prescribed drugs may need to be #reduced in dose or ceased# in CKD #once eGFR falls below 60:# (Kidney Health Australia Guidelines) 

	Do you need to reduce the dose (or cease) #any# of the current medications because of CKD stage 3-5?"
	
	YesNoNA
	NAText "N/A - no CKD stage 3-5"
	Category "kidneys", "medications"
	
	Education "Medications that may need to be altered in CKD" {"
	,===
	Antivirals, Benzodiazepines
	Colchicine, Digoxin
	Fenofibrate, Gabapentin
	Glibenclamide, Glipizide
	Gliclazide, Glimeprimide
	Gliptins (Saxa, Sita, Vilda)
	Lithium, Opioid analgesics
	NOACs- Apixaban, Dabigatran,
	Rivaroxaban, Sotalol
	Spironolactone
	Metformin - this increases the risk of lactic acidosis when eGFR < 50 so reduce dose to 1000mg and if <30 cease,
	,===

	== References

	* https://kidney.org.au/health-professionals/prevent/chronic-kidney-disease-management-handbook[CKD Management in General Practice 3rd edition (2015), Kidney Health Australia]
	"}
	
	Triggers {
		Yes: problem CKDDoseReductionNeeded
	}
	Id e4010744-9e66-437a-bbaf-345a83d99e88
	Changelog {
		2017-01-14: ""
	}
}


Question CKD-MedicationsAdverselyAffectingKidneyFunctionPresent {
	"With CKD some medications can adversely affect kidney function.

	Do you need to alter any medications?"
	
	YesNoNA
	NAText "N/A - no CKD"
	Category "kidneys", "medications"

	Education "Medications affecting kidney function" {"
		* NSAIDs #or# COX-2 inhibitors. The \"#Triple whammy#\" (NSAID or COX-2 INHIBITOR + ACEI + Diuretic) is a potentially fatal interaction in CKD.
		* Remember low dose aspirin is ok with ACEI and diuretic. 
		* Consider the effect of radiographic dye on CKD when ordering contrast scans. 
		* Consider the effect of aminoglycosides on CKD when prescribing for infections. 
		* Monitor lithium levels and renal function regularly. 


		== References
		* https://kidney.org.au/health-professionals/prevent/chronic-kidney-disease-management-handbook[CKD Management in General Practice 3rd edition (2015), Kidney Health Australia]
	"}
	
	Triggers {
		Yes: problem CKDDoseReductionNeeded
	}
	Id e0a32367-680c-4a95-affa-6a779d722000
	Changelog {
		2017-01-14: ""
	}
}


Question CKD-ACEIOrARBNeeded {
	"In the presence of CKD, treating #any hypertension# to the target level is extremely important.
	
	#ACEI or ARB# is recommended.
    
    Do you want to start or increase ACEI or ARB?"
	
	YesNoNA
	NAText "N/A - no CKD"
	Category "kidneys"

	Education "BP targets for CKD" {"
		* CKD with #any microalbuminuria# target is <125/75 home average (130/80 office). 
		* CKD with #diabetes# target is <125/75 home average (130/80 office). 
		* CKD #alone# target is <135/85 home average (140/90 office). 
		
		ACE inhibitor (or ARB if ACEI not tolerated) is recommended as first line therapy for both CKD and hypertension and dose should be titrated to ensure BP target control.

		== References

		* https://kidney.org.au/health-professionals/prevent/chronic-kidney-disease-management-handbook[CKD Management in General Practice 3rd edition (2015), Kidney Health Australia]
	"}
	
	Triggers {
		Yes: problem CKD-SoACEIOrARBNeeded
	}
	Id cbc5f003-fda1-43d4-b0e0-11d3cb3ae156
	Changelog {
		2017-01-14: ""
	}
}


Question CKD4Or5Present {
	"Referral is usually not necessary if: 

	- Stable eGFR >30 
	- Urine ACR <30 mg/mmol 
	- Controlled BP 

	Otherwise:

	* CKD #stage 4# (eGFR 15 to 29) requires third monthly eGFR monitoring and usually #referral to nephrologist.# 
	* CKD #stage 5# (eGFR <15) definitely requires #referral to nephrologist.#

	Do they have CKD stage 4 or 5?"
	
	YesNoNA
	NAText "N/A - no CKD"
	Category "kidneys"

	Education "References" {"
	* https://kidney.org.au/health-professionals/prevent/chronic-kidney-disease-management-handbook[CKD Management in General Practice 3rd edition (2015), Kidney Health Australia]
	"}
	
	Triggers {
		Yes: problem CKD4Or5Present
		Yes: education optional NephrologistReferralIndications
	}
	Id ed3e1cba-5163-4d9a-92a7-b2312862d75e
	Changelog {
		2017-01-14: ""
	}
}


Question AbsoluteCardiovascularRiskCalculatorMediumOrHigher {
	"Performing a full CVD risk assessment has 2 steps: 
	
	1. Ideally use #initial (pre-treatment) values# values of BP and cholesterol in your medical software Absolute Cardiovascular #Risk Calculator#. 
	If not available, be aware treated BP and Cholesterol values only give current risk.  

	2. Check for any #non-calculator factors*# that may cause the risk score to be under-estimated. (e.g. FH of CVD before 55 years).

	#*# Or just click below for a full list of non-calculator factors. 
	
	Are they at medium (10 to 15%) or high risk (>15%)?"
	
	YesNoNA
	NAText "N/A - Established CV disease"
	Category "Heart / Brain"

    Links {
        FHOfPrematureCVDLessThan60Years
    }
	
	Triggers {
		Yes: problem AbsoluteCardiovascularRiskIsMediumOrHigh
		Yes: education optional NonCalculatorHigherRiskFactors
	}
	Id 2193ff19-778c-4343-b9f4-dbdd3651e3e3
	Changelog {
		2017-01-14: ""
	}
}

Question CardiovascularRiskAutomaticallyHigh {
	// TODO: remove first paragraph?
	"Some patients do not need an Absolute cardiovascular risk calculation to be performed.

	Does the patient have a condition resulting in #automatic assumption# of high CVD risk?"
	
    YesNo
	Category "Heart / Brain"

    Triggers {
		Yes: AutoAnswer {
			AbsoluteCardiovascularRiskCalculatorMediumOrHigher NA
		}
		Yes: education optional NonCalculatorHigherRiskFactors
		Yes: problem CardiovascularRiskAutomaticallyHigh
    }
    Id c3013320-56fa-4d1e-a44c-56087bc7718b
    Changelog {
        2018-01-20: ""
    }
}



Question NSAIDUseRequiredDespiteRisk {
	"NSAIDs and COXIBs are associated with increased cardiovascular and GI risk so it is recommended to #calculate the risk for both.# 
	
			
	Is NSAID or COXIB use required despite risk? 
	
	[small]#GI risk calculator - click below# +
	[small]#CV risk calculator - should be built-in to your practice software#
    "
	
	YesNoNA
	NAText "N/A - Not indicated"
	Category "medications"
	
	Education "Risk calculation and NSAID use" {"
		*Some patients will still require use of a NSAID or COXIB because of a rheumatic condition so: 
	
	If at [red]#medium risk# on #CV risk calculator#, and #require a NSAID# consider using naproxen (which is in the very low risk category and has the least cardiac risk of the NSAIDs) with the aspirin, and add a PPI to protect the stomach. 
	
	If at [red]#high risk# on #CV risk calculator# *AND* [red]#high risk# on #GI# #risk calculator# *AND* #requires regular NSAID# use a COXIB with the aspirin and add a PPI to protect the stomach. Celecoxib is in the very low risk category.  
	"}
	
	Triggers {
		Yes: problem AspirinOrNSAIDRiskFactors
		Yes: education optional GIRiskCalculator
	}
	Id 0cac131d-63b9-4c3e-85bf-2ce895c6e7f9
	Changelog {
		2017-01-14: ""
	}
}


Question FHAbdominalAorticAneurysm {
	"Having a first degree relative who has had an #abdominal aortic aneurysm# (AAA) increases risk 2 fold and 4 fold if ruptured AAA so screening starts at age 50 years. 
	
	Do you want to order an USS screen? 
	
	image:FHAbdominalAorticAneurysm.jpg[width=888]"
	
	YesNoUnsure
	Category "prevention"
	
	Triggers {
		Yes Unsure: problem FamilyHistoryPositiveForAorticAneurysmAAA
	}
	Id 5c0a5bff-907c-449b-b55a-297bee9c4d1a
	Changelog {
		2017-01-14: ""
	}
}


Question PneumococcalImmunisationNeeded {
	"If the patient is
	
	* #> 65# years 
	* *or* has any #chronic medical condition#

	Check if they have had the correct course of pneumococcal immunisations.
    
    Do they need a #pneumococcal immunisation#?
	"
	
	YesNoNA
	Category "prevention"
	
	Triggers {
		Yes: problem PneumococcalImmunisationNeeded
		Yes: education optional PneumococcalVaccinationNIP
	}
	Id af98569d-2bd0-444c-862f-f220ec7da751
	Changelog {
		2017-01-14: ""
	}
}


Question ECGAbnormal {
	"An abnormal ECG may require #investigation, and# it may alter #absolute cardiovascular risk calculation.# (e.g. any degree of left ventricular hypertrophy on ECG will alter risk).
	
	Opportunistic screening for atrial fibrillation is recommended for patients over 65 years of age.

	Is the ECG #abnormal#?"
	
	YesNoNA
	NAText "N/A - ECG not done"
	Category "Heart / Brain"

	Education "References" {"
		* https://www.mja.com.au/journal/2018/209/10/national-heart-foundation-australia-and-cardiac-society-australia-and-new[National Heart Foundation of Australia and Cardiac Society of Australia and New Zealand: Australian clinical guidelines for the diagnosis and management of atrial fibrillation 2018]
	"}
	
	Triggers {
		Yes: problem ECGAbnormal
	}
	Id a8dbe99a-b398-46b3-9657-35670caf2496
	Changelog {
		2017-01-14: ""
	}
}


Question AnkleBrachialIndexAbnormal {
	"A #low ABI# #<0.90# #doubles# the 10 year cardiovascular mortality rate for each level of risk in the Framingham risk calculator. 
	
	So this will alter your interpretation of the absolute cardiovascular risk calculation. 
	
	ABI has 95% sensitivity and 100% specificity for PAD. 
	
	Is the ankle brachial index #abnormal#?"
	
	YesNoNA
	NAText "N/A - ABI not done "
	Category "Heart / Brain"
	
	Triggers {
		Yes: problem ABISuggestiveOfPVDSoAssessmentNeeded
	}
	Id 76235e6b-d80e-4e75-8ecf-c714d644c604
	Changelog {
		2017-01-14: ""
	}
}


Question PhysicalExaminationAbnormal {
	"Is the relevant physical examination #abnormal#?"
	
	YesNo
	Category "general"

	Education "Physical examination" {"
		*Check for the following on physical examination if any CV problems:*
		
		* Abnormal rhythm? 
		* Cardiac enlargement? (any suspicion requires echo) 
		* Is echo indicated by clinical or ECG criteria? 
		* Murmur? 
		* Abnormal carotid/femoral pulses or bruit? 
		* Abnormal renal palpation or bruit? 
		* Aortic aneurysm? 
		* Crackles/wheeze? 
		* Peripheral oedema? 
	"}
	
	Triggers {
		Yes: problem PhysicalExaminationAbnormal
	}
	Id 36ded842-c59b-4c3d-bb6c-88a339cd914b
	Changelog {
		2017-01-14: ""
	}
}

Question FractureMinimalTrauma {
	"
    #Osteoporosis# results in bones that break easily.

    Have you ever had:
    
    * crushing or wedging of the bones in your back (vertebrae)?

    or

    * broken bones with a minor accident or fall from standing height or less?
    "
	
	YesNo
	Category "osteoporosis"
    Id c065f618-8359-4659-ac7c-4936e56a369a
	Changelog {
		2017-01-14: ""
	}
}


Question OsteoporosisChronicDiseaseRiskFactorPresent {
	"Osteoporosis #screening# is indicated in adults with a number of chronic medical conditions or medications.
	
	Click link below for a full list.
	
	Do they need a #BMD#?"
	
	YesNoNA
	Education "Medical conditions causing osteoporosis" {"
		Osteoporosis #screening# is indicated in adults with #any one# of these #chronic medical conditions or medications:# 
		
		* endocrine disorders (e.g. hypogonadism, premature menopause, Cushing's, hyperparathyroidism, hyperthyroidism) 
		* inflammatory conditions e.g. Rheumatoid Arthritis 
		* malabsorption e.g. coeliac disease 
		* organ or bone marrow transplant 
		* CKD or chronic liver disease 
		* drugs such as corticosteroids(including ICS = or > 800mcg budesonide daily) , excess thyroxine, antiepileptic, anti-oestrogen, anti-androgenic, SSRIs 
		* multiple myeloma 
			
		== References
		* https://www.racgp.org.au/download/Documents/Guidelines/Musculoskeletal/osteoporosis-algorithm.pdf[Osteoporosis risk assessment diagnosis and management (2017), RACGP]

	"}
	NAText "N/A - already on treatment"
	Category "osteoporosis"
	
	Triggers {
		Yes: AutoAnswer {
			OsteoporosisRiskFactorsPresent NA
		}
		Yes: problem OsteoporosisRiskFactorPresent
	}
	Id 9dc985b8-bdf7-4dd3-953d-d2b0e3f1aef1
	Changelog {
		2017-01-14: ""
	}
}


Question OsteoporosisRiskFactorsPresent {
	"There are #age-based# osteoporosis screening indications. Click link below for a full list.

	Do they need a #BMD#?"
	YesNoNA

    Links {
        FractureMinimalTrauma
    }
	
	Education "Age-based indications for osteoporosis screening" {"
		#Age-based# Osteoporosis #screening# is indicated if:
		
		* Age >70 years as a #single risk factor,# or
		* Age >50 in females or >60 in males #plus one other factor#:
		- FH of minimal trauma fracture 
		- Smoking 
		- High alcohol intake (>4 std drinks per day in men, >2 in women) 
		- Diet lacking in calcium 
		- Low body weight 
		- Recurrent falls 
		- Sedentary lifestyle over many years
	
		== References
		* https://www.racgp.org.au/download/Documents/Guidelines/Musculoskeletal/osteoporosis-algorithm.pdf[Osteoporosis risk assessment diagnosis and management (2017), RACGP]

	"}
	NAText "N/A - already on treatment"
	Category "osteoporosis"
	
	Triggers {
		Yes: education optional OsteoporosisTreatmentAlgorithm
		Yes: problem OsteoporosisRiskFactorPresent
	}
	Id c5e38e51-8a53-4204-83ce-e31f9333a05e
	Changelog {
		2017-01-14: ""
	}
}

Education OsteoporosisTreatmentAlgorithm {
    Link "http://www.racgp.org.au/download/Documents/Guidelines/Musculoskeletal/osteoporosis-algorithm.pdf"
    Id bc91edeb-faf8-417a-a1be-12c92a80d72c
    Changelog {
        2017-07-02: ""
    }
}



Question DementiaPresent {
	"Is #dementia# present or suspected?"
	
	YesNoNA
	NAText "N/A - no dementia "
	Category "neurology"
	
	Triggers {
		Yes: problem DementiaPresentOrSuspected
	}
	Links {
		MMSERequired
		MMSE
	}
	Id bb66a048-bc3f-482b-94d7-577c56c6ef2d
	Changelog {
		2017-01-14: ""
	}
}


Question RepeatScriptNeeded {
	"Have you given the patient a full #6 month's supply# of medications to last until next review?"
	
	YesNo
	Category "medications"
	
	Triggers {
		No: problem RepeatsScriptsNeeded
	}
	Id dce61542-e5ef-4bdd-8c30-61a6c6327992
	Changelog {
		2017-01-14: ""
	}
}


Question EPCNeeded {
	"Do they #need# the assistance of at least 2 other collaborating #health# care providers(one of whom may be another medical practitioner).? 
	
	(PAT generates both the GPMP (721) and the TCA (723) for an EPC to be arranged)"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem CareFrom2OrMoreOtherHealthCareProvidersOtherThanYourself
	}
	Id e5239060-ae94-4b8c-8245-41d86953eb68
	Changelog {
		2017-01-14: ""
	}
}


Question PersonalisingThePlan {
	"The #art# of medicine is to #tailor# the treatment to the patient before you. 
	
	* image:PersonalisingThePlan_0.jpg[width=24] In the GPMP use the 'add row' icon to add other significant problems e.g. OA knee. 
	* image:PersonalisingThePlan_2.png[width=24] Use the 'delete row' icon to remove less relevant rows.
	"
	
	Finish
	Category "general"
	
	Education "Tips for better patient recall and adherence" {"
     
	* #Alter the generic advice# in the GPMP to a personalised form (e.g. 'start simvastatin 40mg tomorow' instead of 'start Statin') 

    * Think of the GPMP as a dynamic active problem list 

    * So, the most used selection will be #commenced/continuing# to enable the GPMP review to monitor active conditions 

	* Deleting a problem or marking it as completed reduces the number of rows in the printed out plan.

	* Completed problems can always be seen again by clicking on Show Completed box. 

	* #\"Deface\" the GPMP# once it is printed by drawing on it with #highlighters.#

	* Suggest patient photograph the Health Summary and save it on their mobile phone.
	  "}
	Id ba2a73b9-63e0-4bdd-b659-c09ff4c2c4c4
	Changelog {
		2017-01-14: ""
	}
}


Problem HealthSummaryReviewNeeded {
	Goal "Accurate HS with copy to patient"
	Plan "Print out the updated health summary. Suggest patient photograph and save it on their mobile phone. Offer to upload to My Health Record"
	Id d1696a6a-29be-4193-9370-869bd5be997d
	Changelog {
		2017-01-14: ""
	}
}


Problem BPOutsideRange {
	Goal "
    Uncomplicated hypertension: 135/85 at home
    Complicated hypertension: 125/75 at home
    "
	Plan "
    Within 5mmHg: use lifestyle measures if possible.
    Otherwise: review every 4 weeks to adjust treatment until controlled.
    "
	Id a612247a-6351-4ab0-a793-1ae94352a766
	Changelog {
		2017-01-14: ""
	}
}


Problem FamilyHistoryPositiveForAorticAneurysmAAA {
	Goal "Detect AAA early in this high risk group"
	Plan "Needs baseline US scan at age 50 years. UpToDate says 2 fold increase risk if just FH of an AAA, and 4 fold increase risk if FH of ruptured AAA."
	Id a92e861d-f43d-4372-9b6e-7dd74445f91e
	Changelog {
		2017-01-14: ""
	}
}


Problem BGLAbnormal-TestNeeded {
	Goal "Exclude diabetes"
	Plan "Oral glucose tolerance test or HbA1c and review."
	Id e6ba7c50-b9b1-4e3e-839f-30d766d58358
	Changelog {
		2017-01-14: ""
	}
}


Problem Smoking {
	Goal "Achieve non-smoking status"
	Plan "Ask nicotine dependence questions.
    Review the smoking motivational interview page. Then move onto barriers, solutions and time frames.
    Enroll in Quitline.
    Suggest 'My QuitBuddy' phone app.
    Arrange follow-up."
	Id d393e232-55c2-411e-8ffa-181382acf662
	Changelog {
		2017-01-14: ""
	}
}


Problem CervicalScreeningNeeded {
	Goal "Early detection cervical cancer."
	Plan "Arrange cervical smear and enroll in recall system."
	Id 36ebaba9-7fda-4fef-8ff0-4426358795e8
	Changelog {
		2017-01-14: ""
	}
}


Problem AlcoholIntakeMoreThanRecommended {
	Goal "
    Drinking no more than two standard drinks on any day.
    Determine if there's a drinking problem."
	Plan "
    Motivational interviewing approach to move to recommended levels.
    If problem drinking arrange another consultation.
    "
	Id 067d54e0-e9ff-4318-8dde-a65302258e28
    Education "CAGE and FLAGS mnemonics" {"

        CAGE:

        - Have you ever felt you needed to #Cut# down on your drinking?
        - Have people #Annoyed# you by criticizing your drinking?
        - Have you ever felt #Guilty# about drinking?
        - Have you ever felt you needed a drink first thing in the morning (#Eye-opener#) to steady your nerves or to get rid of a hangover?

        FLAGS:

        - brief #Feedback#
        - #Listening# to readiness to change
        - clear #Advice# on benefits of change
        - help set #Goals#
        - practical #Strategies#
    "}
	Changelog {
		2017-01-14: ""
	}
}


Problem PhysicalActivityNeeded {
	Goal "At least 150 minutes (2.5 hours) of physical activity per week"
	Plan "
    Look for \"factor P\" (Purpose or Pleasure) in exercise.
    Understanding that exercise halves deaths from strokes and heart attacks.
    Motivational interviewing.
    "
	Id 75c3c507-c84d-4eed-a561-6777bfffd2ab
	Changelog {
		2017-01-14: ""
	}
}


Problem BreastScreenNeeded {
	Goal "Detect breast cancer early"
	Plan "BreastScreen every 2 years"
	Id 0b676857-a85f-4a45-a04a-581fa0470a19
	Changelog {
		2017-01-14: ""
	}
}
Problem BreastScreenDiscussion {
	Goal "Clarify concerns"
	Plan "Discuss pros and cons of screening."
	Id 0b676857-a85f-4a45-a04a-581fa0470a17
	Changelog {
		2017-01-14: ""
	}
}
Problem BreastScreenDiscussion40to49 {
	Goal "Decide whether to have BreastScreen"
	Plan "Discuss pros and cons of screening.
    Explain that denser breast tissue makes mammograms more difficult to read before the age of 50."
	Id 0b676857-a85f-4a45-a04a-581fa0470a18
	Changelog {
		2017-01-14: ""
	}
}


Problem NationalBowelCancerScreeningProgramNeeded {
	Goal "Promote participation from age 50 to age 74."
	Plan "We strongly recommend you start participating at age 50."
	Id 3132ffb4-72d9-4928-8bd1-8b700b137e6c
	Changelog {
		2017-01-14: ""
	}
}


Problem DepressionOrAnxietyPresent {
	Goal "Clarify diagnosis and concern"
	Plan "Acknowledge the problem and arrange a follow-up consultation to do a Mental Health Assessment."
	Id a73817a4-e282-4425-82e8-228d321e3459
	Changelog {
		2017-01-14: ""
	}
}


Problem ErectileProblem {
	Goal "Determine cause."
	Plan "Arrange a specific consult to discuss this in depth using the protocol on www.andrologyaustralia.org"
	Id c39c4816-543b-41b7-9738-759ba3430214
	Changelog {
		2017-01-14: ""
	}
}


Problem SexualHealthConcerns {
	Goal "Clarify all concerns, determine the causes"
	Plan ""
	Id 6c9aad5b-ef33-44ce-8ea3-a09f74ff66b2
	Changelog {
		2017-01-14: ""
	}
}


Problem AdherenceProblem {
	Goal "Improve medication adherence"
	Plan "
    Explore any adverse effects.
    'Anchor' to a current habit done without fail.
    Consider smartphone automatic alert.
    "
	Id a012a1d0-93b8-4bd7-a84e-90ea0a74027e
	Changelog {
		2017-01-14: ""
	}
}


Problem FallsAssessmentNeeded {
	Goal "Prevent falls"
	Plan "
    Perform a 'sit to stand test'.
    If positive review footwear, eyesight, sedative medications, and pain / loss of flexibility in joints. Then consider physio assessment."
    Education "Sit to stand test" {"
        Ask patient to stand from a sitting position with arms folded as many times as possible in 12 seconds (normal is > 5)
    "}
    Education "A guide to preventing falls for older people (Dept of Health)" {"
        https://www.health.gov.au/internet/main/publishing.nsf/Content/E23F5F7BF8F07264CA257BF0002043F5/$File/Don%27t%20fall%20for%20it.pdf
    "}
	Id fb16e13a-bada-43f8-9562-46c9faf746e2
	Changelog {
		2017-01-14: ""
	}
}


Problem PhysicalExaminationAbnormal {
	Goal "Check for progression if pre-existing, or exclude problems if new"
	Plan "Organise tests as appropriate."
	Id e182b4d0-26b8-461d-87d6-3f6930ab9ee1
	Changelog {
		2017-01-14: ""
	}
}


Problem CarerNeeded {
	Goal "Improve support and include carers in management"
	Plan "
    Assess and arrange continuing or increased assistance.
    Inform about personal support available via Carers Australia 1800 242 636.
    "
	Id 7debbb02-9b6b-4cb2-93a1-a149f4cb4469
	Changelog {
		2017-01-14: ""
	}
}


Problem AdvanceHealthDirectiveNeeded {
	Goal "All patients with a chronic medical condition to have an Advance Health Directive. This lets your wishes be respected if you are too unwell to decide and eases the burden on your family."
	Plan "
    Pick up Advance Health Directive Form from newsagent or print online.
    GP appointment to discuss & complete.
    "
	Id 8e0de7fc-29d4-49d1-bf01-1627b64a076b
	Changelog {
		2017-01-14: ""
	}
}


Problem ExtraHelpRequired {
	Goal "Appropriate support"
	Plan "Arrange review for TCA(723) and referrals."
	Id 07381bd1-fc89-4f9c-8d14-ec769e77a110
	Changelog {
		2017-01-14: ""
	}
}


Problem OsteoporosisRiskFactorPresent {
	Goal "Clarify if osteoporosis present"
	Plan "Check bone mineral density (Medicare rebate except for some of the drug causes). If positive check lateral thoracic spine x-ray. Perform work up for secondary causes."
	Id 3c6dd2ee-d357-4838-9757-b9fbfe6900cd
	Changelog {
		2017-01-14: ""
	}
}


Problem PlanNotPersonalised {
	Goal "Enhance patient recall"
	Plan "For each row alter the plan to specific treatment. Highlight once printed out."
	Id 354a44bf-c70c-4545-a461-6aabfe943d30
	Changelog {
		2017-01-14: ""
	}
}


Problem ChronicKidneyDiseasePresent {
	Goal "Determine grade. Determine cause."
	Plan "Control BP with ACEI/ARB. Check for baseline USS KUB and UMCS. (microalbuminuria if not done, and red cell morphology if any microscopic haematuria).FBC, CRP, ESR. Add CKD diagnosis and its cause to history as active problem."
	Id 22ac07e1-2273-438f-ae97-68cf28d28c23
	Changelog {
		2017-01-14: ""
	}
}


Problem CKDDoseReductionNeeded {
	Goal "Reduce renally excreted medications if eGFR <60, or stop if necessary."
	Plan "Review each relevant medication and find a solution. Reduce metformin if eGFR <50 and cease if eGFR is <30."
	Id bef80a55-026e-4a59-8773-79334a89c68f
	Changelog {
		2017-01-14: ""
	}
}


Problem CKD-SoACEIOrARBNeeded {
	Goal "Control BP to <125/75 home average if microalbuminuria."
	Plan "Commence ACEI or ARB and titrate up every 2 to 4 weeks to maximally tolerated dose while monitoring creatinine."
	Id dc74bd81-7a01-4a79-b918-7d2ed6765775
	Changelog {
		2017-01-14: ""
	}
}


Problem CKD4Or5Present {
	Goal "Clarify"
	Plan "Refer to nephrologist."
	Id 3204130c-293d-43c4-898d-dcf4003fc2c9
	Changelog {
		2017-01-14: ""
	}
}


Problem PneumococcalImmunisationNeeded {
	Goal "Protect against pneumococcal disease."
	Plan "Explain the difference between pneumococcal disease which is bacterial and influenza which is viral. Organize immunisation and recall."
	Id feb6372b-97db-4db5-a202-ce1b09c583f3
	Changelog {
		2017-01-14: ""
	}
}


Problem LipidAgentNeeded {
	Goal "Ensure all patients are appropriately treated for dyslipidaemia."
	Plan "
    Start statin.
    Consider adding fibrate if mixed picture or especially in diabetic retinopathy.
    Arrange baseline LFTs and CK. Repeat cholesterol level in 6 weeks.  LFTs need only be checked if clinically indicated.
    Escalate dose if not in range in 6 weeks."
	Id b39bb585-3b10-4e14-8830-1def16e3c3a0
	Changelog {
		2017-01-14: ""
	}
}


Problem ECGAbnormal {
	Goal "If known ischaemic heart disease check for change from previous ECG. If no PH of IHD investigate."
	Plan "Organise stress test if appropriate and review. Organise ECHO if appropriate."
	Id fa2e18a9-f502-41a8-826b-e03109e6a079
	Changelog {
		2017-01-14: ""
	}
}


Problem ABISuggestiveOfPVDSoAssessmentNeeded {
	Goal "Exclude or confirm Peripheral Arterial Disease"
	Plan "
    Check for symptoms of PAD.
    Consider duplex arterial scan.
    "
	Id 1edf561a-208c-4bb1-b265-fcd2e1fee184
	Changelog {
		2017-01-14: ""
	}
}



// TODO
Problem InfluenzaAtRiskGroup {
	Goal "Annual influenza immunization"
	Plan "Determine which of the following 4 factors is operating:
	 - lack of knowledge
	 - lack of personal relevance
	 - lack of net benefit
	 - lack of opportunity"
	Id 1bfa73f4-1b8f-4578-aadb-7a32bab9acb0
	Changelog {
		2017-01-14: ""
	}
}


Problem RepeatsScriptsNeeded {
	Goal "Adequate medications to avoid unnecessary consultations."
	Plan "Check each drug and repeat it as required. Try to coordinate supply so sufficient until next planned consultation."
	Id d4525ecb-69f1-4c9c-bdf3-02236e2cf6e0
	Changelog {
		2017-01-14: ""
	}
}


Problem CareFrom2OrMoreOtherHealthCareProvidersOtherThanYourself {
	Goal "Identify health care providers needed and confirm their co-operation for referral by email, fax or phone."
	Plan "Arrange a follow-up consultation for an item 723 (team care arrangement) to be drawn up. Explain eligibility for 5 allied health visits in 12 months under EPC."
	Id 065a6375-3fc7-4e8e-a719-f806d8ec17fa
	Changelog {
		2017-01-14: ""
	}
}


Problem MedicationReviewNeeded {
	Goal "Minimise side effects"
	Plan "Use NPS resource. Refer to pharmacist if required for formal Medication Review."
	Id 65720057-350c-466e-afda-d9667df9472c
	Changelog {
		2017-01-14: ""
	}
}


Problem ShinglesImmunisationNeeded {
	Goal "Prevent shingles"
	Plan "Arrange appointment to come back for shingles immunisation."
	Id 1d80cf0d-6ea0-4457-b3a7-67bbef8364f1
	Changelog {
		2017-01-14: ""
	}
}


Problem PertussisImmunisationNeeded {
	Goal "Prevent whooping cough (pertussis)"
	Plan "Write a script and arrange appointment to come back for immunisation."
	Id 90d1ae17-9b98-475d-be15-c57d520cbc02
	Changelog {
		2017-01-14: ""
	}
}


Problem SleepApnoeaScreenNeeded {
	Goal "Detect and treat OSAS"
	Plan "Perform STOP-BANG questionnaire. If positive, refer for home or hospital sleep study."
	Id 5c523ece-7794-4155-915c-7a0ff29094f1
	Changelog {
		2017-01-14: ""
	}
}


Education HomeBPMonitoringChart {
	File "rtf"
	Id 101ddd12-645e-49c9-9594-42f0942f2f0d
	Changelog {
		2017-01-14: ""
	}
}


Education NonCalculatorHigherRiskFactors {
	File "rtf"
	Id 837b76e1-2c61-41e3-b640-8d52d5c6526a
	Changelog {
		2017-01-14: ""
	}
}


Education NephrologistReferralIndications {
	File "rtf"
	Id e1a40669-6092-4e13-98aa-184e7a52362a
	Changelog {
		2017-01-14: ""
	}
}

Education CKDInitialDiagnosis {
	Message "CKD causes to exclude on initial diagnosis"
	File "rtf"
	Id c70d0687-e801-4884-b50a-4b568ca3d2db
	Changelog {
		2017-01-14: ""
	}
}


Education Polypharmacy {
	Message "NPS polypharmacy advice"
	File "rtf"
	Id 6fa73580-a55d-4723-a573-b8094c488201
	Changelog {
		2017-01-14: ""
	}
}


Education PneumococcalVaccinationNIP {
	Message "Recommendations for pneumococcal vaccination"
	Link "http://www.communityimmunity.com.au/docs/Recommendations_for_Pneumococcal_Vaccination.pdf"
	Id 9305a2ff-964e-4955-888f-54df5c7b84b1
	Changelog {
		2017-01-14: ""
	}
}


Education SleepApnoeaScreen {
	Message "Sleep apnoea screen"
	Link "http://www.stopbang.ca/osa/screening.php"
	Id 25d25a4e-bb6e-40a0-aa4e-71695bb5f9a4
	Changelog {
		2017-01-14: ""
	}
}
