/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with coeliac disease */
Clinic GPMPCoeliac {
	Id 14f6094b-b74b-4f84-bd82-d65d56374ab4
	Filter Coeliac
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			CoeliacDiseasePresent
		}

	}
	Changelog {
		2017-01-14: ""
	}
}


Filter Coeliac {
	Match {
		keyword "Coeliac"
		regex "(gluten|wheat).*(sprue|enteropathy)"
		regex "non-?tropical sprue"
	}
	Tests matching only this {
		"Coeliac"
		"Coeliac sprue"
		"Coeliac disease"
		"adult coeliac disease"
		"Gluten-sensitive enteropathy"
		"Gluten enteropathy"
		"Gluten-induced enteropathy"
		"Gluten-responsive sprue"
		"Wheat-sensitive enteropathy"
		"Nontropical sprue"
		"Non-tropical sprue"
	}
}


Question CoeliacDiseasePresent {
	"#Coeliac disease# requires long term follow up: 
	
	
	Do you wish to #take any action# for the coeliac disease?"
	
	YesNoNA
	NAText "N/A - No coeliac disease"
	Category "coeliac"

	Education "Coeliac Australia recommendations" {"

	* Join Coeliac Australia. 

	* Bone density as a baseline for all adults with coeliac and should be repeated every 2 years if required. 

	* Ensure screening of all family members has occurred. 

	* Check understanding of the initial Dietitian education and refer if needed. 

	* Coeliac serology should be done annually .(This should have gradually normalised and re-elevation is indicator of gluten re-exposure) 

	* Because coeliac is a genetically linked illness they are susceptible to other immune conditions.

	* Annual pathology screening for associated conditions: 
	- fasting E& LFTs (Diabetes, Liver disease, Addison's, calcium)
	- TSH (thyroid disease)
	- FBC platelets, ferritin and B12, and zinc and magnesium (nutrient deficiencies). 

	*
	https://www.coeliac.org.au/resources/[ Coeliac Australia resources]

	"}
	Triggers {
		Yes: problem CoeliacDiseaseNeeded
	}
	Id 058a2d3d-7446-4aa5-bf78-fdccc2afe3e9
	Changelog {
		2017-01-14: ""
	}
}


Problem CoeliacDiseaseNeeded {
	Goal "Monitor for complications"
	Plan "Add appropriate reminder for check up. Promote joining www.coeliac.org.au. Pre check up order TTG, DGP annually. FBC, fasting E&LTFs, ferritin, Zn, Mg, TSH annually. Consider dietitian and bone density."
	Id dfbe2e1e-6621-4511-b221-982fa4d4ea75
	Changelog {
		2017-01-14: ""
	}
}


