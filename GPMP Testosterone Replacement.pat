/* 
 * Copyright (c) 2019 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with testosterone replacement */
Clinic GPMPTestosteroneReplacement {
	Id ba709fd1-0e02-4ec2-add8-e8110a00ecbe
	Filter TestosteroneReplacement
	Questions {
		Patient {
			TestosteroneTherapy
		}

		Checklist {
			
		}

		Staff {
			TestosteroneMonitoringReviewNeeded
			
			
		}

	}
}

Question TestosteroneMonitoringReviewNeeded {
	"
    The Endocrine Society recommends evaluating the patient #after testosterone treatment initiation# to assess whether the patient has responded to treatment, is suffering any adverse effects, and is complying with the treatment regimen.

    Do you want to bring them back for #review#?"
    
    YesNoNA
	NAText "Not on testosterone"
    Category "medications"

    Education
     "Testosterone replacement monitoring summary" {"

	  * Maintain serum testosterone concentrations during treatment in the mid-normal range for healthy young men.

      * Evaluate symptoms and signs of testosterone deficiency and formulation-specific adverse events at each visit.

      * Monitoring includes measuring testosterone and hematocrit at 3 to 6 months (depending upon the formulation) and measuring testosterone and hematocrit at 12 months and annually after initiating testosterone therapy.

      * For those who choose prostate monitoring, monitoring should include PSA and DRE 3 to 12 months after treatment initiation. After 1 year, prostate monitoring should conform to guidelines for prostate cancer screening, depending on the race and age of the patient.

      * Urological consultation for hypogonadal men receiving testosterone treatment if during the first 12 months of testosterone treatment there is a confirmed increase in PSA concentration > 1.4 ng/mL above baseline, a confirmed PSA > 4.0 ng/mL, or a prostatic abnormality detected on digital rectal examination. 
      
      * After 1 year, prostate monitoring should conform to standard guidelines for prostate cancer screening based on the race and age of the patient.   
	"}
Education 
"Testosterone Therapy in Men With Hypogonadism An Endocrine Society Clinical Practice Guideline " {"

		* https://academic.oup.com/jcem/article/103/5/1715/4939465[Endocrine Society 2018 Guideline]
		"}

    Triggers {
		Yes: problem TestosteroneTherapyReviewNeeded
	}
    Id 0765dd5c-4097-4692-b9f5-3d66b7dafdba
    Changelog {
        2019-01-17: ""
    }
}


Question TestosteroneTherapy {
	"
    If you are on #testosterone# replacement are you:
    
    - #Either# having any side effects?
    
    - #Or# have your symptoms failed to settle?
    "
    
    YesNoNA
	NAText "Not on testosterone"
	Category "Men's health"
    
    Triggers {
		Yes: problem TestosteroneTherapyReviewNeeded
	}
    Id 5efc02a9-1bfa-41fa-bce4-42a9147b3923
    Changelog {
        2019-01-17: ""
    }
}

Problem TestosteroneTherapyReviewNeeded {
	Goal "Assess and optimise therapy"
    Plan "Modify testosterone therapy as needed"
    Id f8246485-ed91-487b-b033-dc0a22ba7534
    Changelog {
        2019-01-17: ""
    }
}



Filter TestosteroneReplacement {
	Match {
		keyword "Testosterone deficiency"
		keyword "Testosterone replacement"
		keyword "Testosterone implant"
		keyword "Testosterone injection"
        keyword "Testosterone therapy"
		keyword "Hypogonadism "
        keyword "Androgen deficiency"
		keyword "Androgen implant"
		keyword "Androgen injection"
	}
	Tests matching only this {
		"Testosterone Replacement therapy"
		"Male hypogonadism"
	}
}











