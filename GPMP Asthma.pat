/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with asthma */
Clinic GPMPAsthma {
	Id c9e41ada-7b8e-4f39-98c9-57a63130990b
	Filter Asthma
	Questions {
		Patient {
			Asthma-ControlLessThan20
		}

		Checklist {
			AsthmaScoreFromHome
		}

		Staff {
			AsthmaHistory
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter Asthma {
	Match {
		keyword "Asthma"
		keyword "Asthmatic"
		keyword "Hyperreactive airway"
	}
	Tests matching only this {
		"Asthma"
		"mild asthma"
		"asthmatic"
		"asthma on ventolin"
		"exercise-induced asthma"
		"Hyperreactive airway disease"
		"Hyperreactive airways disease"
		"Hyperreactive airways"
		"steroid dependent asthma"
		"mixed asthma"
	}
}

Question AsthmaHistory {
	"If they have a #history of asthma# they should be in a reminder system for an annual asthma cycle of care if they:
	
	* are on a *preventer* 

	*OR* 
	
	* are not on a preventer, but are *uncontrolled* according to the asthma score. 
	
	Do you want to arrange #an asthma cycle# of care reminder?"
	
	YesNoNA
	NAText "N/A - no asthma "
	Category "lungs"
	
	Triggers {
		Yes: problem AsthmaHistory
	}
	Links {
		AsthmaScoreFromHome
		AsthmaScoreAdministered
	}
	Id 83513bf5-38ae-46f7-8ebc-62ae51d5faa2
	Changelog {
		2017-01-14: ""
	}
}


Problem AsthmaHistory {
	Goal "Assess control"
	Plan "Add appropriate reminder for an annual asthma cycle of care if on a preventer, or if not on a preventer they are not controlled by asthma score."
	Id ff6ee919-6e97-4aa4-8db0-0bb76b7ca77a
	Changelog {
		2017-01-14: ""
	}
}


