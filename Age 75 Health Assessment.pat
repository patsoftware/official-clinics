/* 
 * Copyright (c) 2017 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Clinic Age75HealthAssessment {
	Id 77337c03-eae5-4fef-9dfb-1b5bb3925200
	Questions {
		Patient {
			MedicationSelfManagementProblem
			MedicationCurrentlyLmanagedByChemist
			MedicationPolypharmacyProblem
			ForgetToTakeMedication
			CarelessSometimesWithTaking
			StopTakingWhenWell
			SideEffectOfMedicationSuspected
			Smoker
			AlcoholIntakeMoreThanRecommended
			PhysicalActivityNeeded
			LessThan3MealsDaily
			NutritionQualityProblem
			HomeMealPreparationProblem
			DentalHealthProblem
			DentalCheckUpRoutinelyDone
			IncontinenceUrinePresent
			IncontinenceBowelPresent
			PodiatryIndicationsDespiteNormalFeet
			DepressionPresent
			AnxietyPresent
			LivingAlone
			SocialSupportNeeded
			LivingAsACouple
			CarerNeeded
			PerformingRoleAsACarer
			CareProvisionInPlace
			ExtraCareNeeded
			AdvanceHealthDirectiveNeeded
			MedicalPowerOfAttorneyNeeded
			FallInLastYear
			MobilityProblemPresent
			MobilityAidNeeded
			DriversLicenceSoNoTransportProblems
			DrivingConfidenceProblem
			PublicTransportProblem
			TaxiTransportNeeded
			OutsideHomeAssistanceNeeded
			InsideHomeAssistanceNeeded
			HomeSafetyProblem
			HomeInsideSafetyProblem
			HomeOutsideSafetyProblem
			BathroomSafetyProblem
			ToiletSafetyProblem
			VisionProblem
			HearingProblem
		}

		Checklist {
			HealthAssessmentConsentAge75
			ReminderHealthAsessment12months
			RecordBPPulse
			RecordHeightWeightWaist
			RecordSmokingAlcohol
			SmokerBasicSpirometry
			RecordCarerDetails
			MMSERequired
			GeriatricDepressionScore
			RecordVisualAcuity
			RecordUrineTest
			ADLScore
		}

		Staff {
			HealthSummaryAndMedicationListReviewNeeded
			MedicationHomeReviewNeeded
			ImmunisationNeeded
			EarWaxRemovalNeeded
			DementiaPresent
			FallsAssessmentNeeded
			PhysicalExaminationProblemsPresent
			HealthAssessmentEditing

			MainGoals
			PatientSaysAlreadyDoing
			DoctorSaysGoingWell
			PatientImportance0to10
			PatientConfidence0to10
			ChecklistCopyToPatient
			ChecklistCopiesToTeamWithConsent
			ReviewDate
		}

	}
	Changelog {
		2017-01-14: ""
	}
}


Question MedicationSelfManagementProblem {
	"Do you think you manage your own medicines accurately?"
	
	YesNo
	Category "medications"
	
	Triggers {
		Yes: AutoAnswer {
			MedicationCurrentlyLmanagedByChemist NA
		}
		No: problem MedicationManagementRequired
	}
	Id dafcc9b2-cf26-4bde-b430-71efee6d1193
	Changelog {
		2017-01-14: ""
	}
}


Question MedicationCurrentlyLmanagedByChemist {
	"If you do not manage your own medication do you have a dossette packed by your chemist?"
	
	YesNoNA
	NAText "N/A - self manage medication"
	Category "medications"
	
	Triggers {
		Yes: problem MedicationManagementRequired
	}
	Id 083a5d4c-7328-493d-9090-8b636f007cc9
	Changelog {
		2017-01-14: ""
	}
}


Question MedicationPolypharmacyProblem {
	"Are you on more than 5 prescribed medications?"
	
	YesNoNA
	Category "medications"
	
	Triggers {
		Yes: problem MedicationManagementRequired
	}
	Id 37e22e5c-cb4d-46b4-9617-3d8784d15632
	Changelog {
		2017-01-14: ""
	}
}

Question LessThan3MealsDaily {
	"Do you eat three meals every day?"
	
	YesNo
	Category "lifestyle"
	
	Triggers {
		No: problem NutritionAdviceNeeded
	}
	Id 4013eb34-9eda-43c1-96b9-31fadbb13715
	Changelog {
		2017-01-14: ""
	}
}


Question NutritionQualityProblem {
	"Do you eat fruit, vegetables and dairy #most# days?"
	
	YesNo
	Category "lifestyle"
	
	Triggers {
		No: problem NutritionAdviceNeeded
	}
	Id 25e78fca-f423-4ef0-b0d8-fed24d3d27c0
	Changelog {
		2017-01-14: ""
	}
}


Question HomeMealPreparationProblem {
	"Are you able to cook and shop for yourself?"
	
	YesNo
	Category "lifestyle"
	
	Triggers {
		No: problem HomeMealPreparationProblem
	}
	Id 274d9d31-2a07-4e9d-9b9b-e7823dca85fb
	Changelog {
		2017-01-14: ""
	}
}


Question DentalHealthProblem {
	"Are you having #any# problems with the following? 
	
	* teeth, or 
	* mouth, or 
	* gums, or 
	* dentures ( if you have them)."
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem DentalHealthProblemPresent
	}
	Id 2ff465be-7953-44b0-95dc-d5c22c100791
	Changelog {
		2017-01-14: ""
	}
}


Question DentalCheckUpRoutinelyDone {
	"Do you have regular dental check-ups?"
	
	YesNo
	Category "general"
	
	Triggers {
		No: problem DentalHealthProblemPresent
	}
	Id 753e8623-49ec-4833-af3e-efc68714f61f
	Changelog {
		2017-01-14: ""
	}
}


Question IncontinenceBowelPresent {
	"Do you have problems with bowel incontinence?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem IncontinenceBowelPresent
	}
	Id 17a5915e-bbd6-425a-be2d-566c8476ae30
	Changelog {
		2017-01-14: ""
	}
}

Question LivingAlone {
	"Are you living alone?"
	
	YesNo
	Category "social"
	
	Triggers {
		Yes: AutoAnswer {
			LivingAsACouple NA
		}
		Yes: problem SocialIsolationProblem
	}
	Id 6f898535-200a-447d-b382-ebe92c9175fd
	Changelog {
		2017-01-14: ""
	}
}


Question SocialSupportNeeded {
	"During the last 4 weeks would someone have been available to help you if you had needed or wanted help?"
	
	YesNo
	Category "social"
	
	Triggers {
		No: problem SocialIsolationProblem
	}
	Id f5d011f8-6244-4591-aa51-1ccaf41e35f1
	Changelog {
		2017-01-14: ""
	}
}


Question LivingAsACouple {
	"Are you living as a couple?"
	
	YesNoNA
	Category "social"
	
	Triggers {
		No: problem SocialIsolationProblem
	}
	Id 161c732b-1ce4-4884-a677-8cc4a2e387b6
	Changelog {
		2017-01-14: ""
	}
}

Question PerformingRoleAsACarer {
	"Are you a carer?"
	
	YesNoNA
	Category "social"
	
	Triggers {
		Yes: problem CarerNeeded
	}
	Id 4503b6d1-df66-409c-93d8-e8be760b9a2a
	Changelog {
		2017-01-14: ""
	}
}


Question CareProvisionInPlace {
	"Do you receive regular care from #any other# source: 
	
	* Community Nursing? or 
	* Day care? or 
	* Respite Care? or 
	* Home help? or 
	* Meals on Wheels?"
	
	YesNo
	Category "social"
	
	Triggers {
		Yes: AutoAnswer {
			ExtraCareNeeded NA
		}
		Yes: problem CarerNeeded
	}
	Id e23e23a5-8ed1-4e30-bf4e-118f72cb8a9a
	Changelog {
		2017-01-14: ""
	}
}


Question ExtraCareNeeded {
	"Do you think you need care from #any other# source such as: 
	
	* CommunityNursing? or 
	* Day-care? or 
	* Respite Care? or 
	* Home help? or 
	* Meals on Wheels?"
	
	YesNoNA
	NAText "N/A - Already has care provision"
	Category "social"
	
	Triggers {
		Yes: problem CarerNeeded
	}
	Id 25482ea9-2a8e-4e87-88df-5f97d842eb61
	Changelog {
		2017-01-14: ""
	}
}


Question MedicalPowerOfAttorneyNeeded {
	"A Medical Enduring Power of Attorney allows someone (usually a relative) to make decisions for you if you were to become seriously ill and were unable to make health decisions for yourself. 
	
	Do you have a #Medical Enduring Power of Attorney#?"
	
	YesNo
	Category "social"
	
	Triggers {
		No: problem MedicalPowerOfAttorneyRequired
	}
	Id a972f6b8-027c-4335-9acc-0c8db3e9d43c
	Changelog {
		2017-01-14: ""
	}
}

Question MobilityProblemPresent {
	"Do you think you #a lot of problem# with any #one# of the following: 
	
	* bending, kneeling or stooping? 
	* climbing one flight of stairs? 
	* walking 100 metres at your own pace without stopping?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem MobilityProblem
	}
	Id d6a628db-baee-4411-a3f7-d384bb9b825e
	Changelog {
		2017-01-14: ""
	}
}

Question DriversLicenceSoNoTransportProblems {
	"Do you hold a #current drivers licence#?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: AutoAnswer {
			PublicTransportProblem NA
			TaxiTransportNeeded NA
		}
		No: problem TransportAssistanceNeeded
	}
	Id 6201e1a1-dcb1-4a73-a434-9668f532b5d3
	Changelog {
		2017-01-14: ""
	}
}


Question DrivingConfidenceProblem {
	"Do you #feel confident# driving?"
	
	YesNoNA
	Category "general"
	
	Triggers {
		No: problem TransportAssistanceNeeded
	}
	Id 965754a4-d2c3-4bc6-8ba6-adb346118974
	Changelog {
		2017-01-14: ""
	}
}


Question PublicTransportProblem {
	"If unable to drive, are you able to #get about on public transport#?"
	
	YesNoNA
	Category "social"
	
	Triggers {
		No: problem TransportAssistanceNeeded
	}
	Id 6ecd7090-5b98-48e9-a9d4-cf579f499021
	Changelog {
		2017-01-14: ""
	}
}


Question TaxiTransportNeeded {
	"Do you have #a taxi concession card#?"
	
	YesNoNA
	Category "general"
	
	Triggers {
		Yes: problem TaxiTransportNeeded
	}
	Id f52bbd9e-5d7a-4f99-831b-a0bb47ef54a4
	Changelog {
		2017-01-14: ""
	}
}


Question OutsideHomeAssistanceNeeded {
	"Are you able to maintain the #outside of your home# - gardens and lawn?"
	
	YesNo
	Category "general"
	
	Triggers {
		No: problem OutsideHomeMaintenanceNeeded
	}
	Id 397a8644-ba03-4ee8-acc5-6a8d969ab1bc
	Changelog {
		2017-01-14: ""
	}
}


Question InsideHomeAssistanceNeeded {
	"Can you do your #own house cleaning#?"
	
	YesNo
	Category "general"
	
	Triggers {
		No: problem InsideHomeAssistanceNeeded
	}
	Id 64ea7601-5bba-4e78-b8f6-51bc3322fb56
	Changelog {
		2017-01-14: ""
	}
}


Question HomeSafetyProblem {
	"After thinking about #each of the following# tasks: 
	
	* getting in and out of bed easily and safely 
	* dressing and showering yourself 
	* carrying meals easily from the kitchen to your dining area 
	* gripping utensils and handrails 
	
	Do you think #you can move safely and easily# around the inside of your home?"
	
	YesNo
	Category "general"
	
	Triggers {
		No: problem InsideHomeAssistanceNeeded
	}
	Id da40fede-b067-4e8e-8c76-61c6000218ef
	Changelog {
		2017-01-14: ""
	}
}


Question HomeInsideSafetyProblem {
	"After thinking about #each# of the following:
	
	* Are your floor mats non-slip? 
	* Do you use non-slip mats in the bath/shower recess? 
	* Is all household lighting adequate inside and out? 
	* Are smoke alarms fitted and working? 
	
	Do you think you #need to modify the inside# of your house?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem InsideHomeSafetyProblem
	}
	Id f4b9c9cc-5692-46da-8d6a-2716837edd7e
	Changelog {
		2017-01-14: ""
	}
}


Question HomeOutsideSafetyProblem {
	"After thinking about #each# of the following: 
	
	* Are your pathways clear and are the surfaces even? 
	* Are the edges of the steps/stairs easily identifiable? 
	* Are there rails for the front and back steps? 
	
	Do you think you can #move easily and safely# around the #outside# for your house?"
	
	YesNo
	Category "general"
	
	Triggers {
		No: problem HomeOutsideSafetyProblem
	}
	Id 7d45ca50-413d-4ec2-a0ce-1dd266412785
	Changelog {
		2017-01-14: ""
	}
}


Question BathroomSafetyProblem {
	"Can you get in and out of the bath or #shower safely#?"
	
	YesNo
	Category "general"
	
	Triggers {
		No: problem BathroomSafetyProblem
	}
	Id fa09bc29-bef6-4174-80d0-911149df59c7
	Changelog {
		2017-01-14: ""
	}
}


Question ToiletSafetyProblem {
	"Can you get off and on the #toilet easily#?"
	
	YesNo
	Category "general"
	
	Triggers {
		No: problem ToiletSafetyProblem
	}
	Id 55b05dd9-28e1-41a8-8600-82bad5bf54a2
	Changelog {
		2017-01-14: ""
	}
}


Question VisionProblem {
	"Are you #able to read# newspapers and books, and #watch TV#?"
	
	YesNo
	Category "social"
	
	Triggers {
		No: problem VisionProblem
	}
	Id 39327265-b2ae-4eef-9661-517f0b00ed3a
	Changelog {
		2017-01-14: ""
	}
}


Question HearingProblem {
	"Are you able to #hear adequately# and use the telephone and TV?"
	
	YesNo
	Category "social"
	
	Triggers {
		No: problem HearingProblem
	}
	Id f2546f66-db35-4d04-b3c4-6645909891bb
	Changelog {
		2017-01-14: ""
	}
}


Question HealthAssessmentConsentAge75 {
	"Record consent to proceed with age 75 Health Assessment"
	
	Check required
	Category "admin"
	Id cf9fb8f4-0f71-745c-98dd-b2f5fd60bf75
	Changelog {
		2017-01-14: ""
	}
}


Question ReminderHealthAsessment12months {
	"All patients: enter reminder for \"Age 75 Health Assessment\" for 12 months"
	
	Check required
	Category "admin"
	Id 9801083c-9962-d557-b9eb-c0bf77d75261
	Changelog {
		2017-01-14: ""
	}
}


Question RecordBPPulse {
	"Record the BP, pulse rate and rhythm in the clinical notes"
	
	Check required
	Category "Heart / Brain"
	Id db8e814e-71f3-4d5a-b063-317d98aaa440
	Changelog {
		2017-01-14: ""
	}
}

Question MMSERequired {
	"Perform MMSE in clinical notes and enter score here"
	
	Number required
	Category "neurology"
	Id bc64fe95-1ef9-695c-9829-a7741e68fa7b
	Changelog {
		2017-01-14: ""
	}
}


Question GeriatricDepressionScore {
	"Perform Geriatric Depression score in clinical notes and enter score here"
	
	Check optional
	Category "psychology"
	Id b5de8891-3fdd-ff5c-8c0a-a69717f9eac4
	Changelog {
		2017-01-14: ""
	}
}


Question RecordVisualAcuity {
	"Record visual acuity with and without glasses in the clinical notes"
	
	Check required
	Category "general"
	Id 3bb7acf9-9400-3250-b538-c5eaa27119ae
	Changelog {
		2017-01-14: ""
	}
}


Question RecordUrineTest {
	"Record urine test in clinical notes"
	
	Check required
	Category "kidneys"
	Id 3a1dce34-129b-345f-a4f6-6876fcb84091
	Changelog {
		2017-01-14: ""
	}
}


Question ADLScore {
	"Record Katz ADL scale score"
	
	Number required
	Category "social"
	Id b3161059-f950-d154-bf1c-1b347158b871
	Changelog {
		2017-01-14: ""
	}
}


Question MedicationHomeReviewNeeded {
	"A Home Medication Review ( #HMR# ) may be indicated if 
	
	* Polypharmacy 
	* Patient confusion or concern 
	
	Do you want to refer for a HMR?"
	
	YesNo
	Category "medications"
	
	Triggers {
		Yes: problem MedicationHomeReviewNeeded
	}
	Id bf2f6a46-4eed-485b-b452-ed7cb5d03b02
	Changelog {
		2017-01-14: ""
	}
}


Question ImmunisationNeeded {
	"Recommended immunisations for 65 and over are 
	
	* Pneumococcal age 65 (repeated in 5 years for high risk) 
	* Influenza yearly from age 65 
	* Tetanus (ideally DTPa) age 65 
	* Shingles age 70
	
	Do you need to recall for #any immunisations# ?"
	
	YesNo
	Category "prevention"
	
	Triggers {
		Yes: problem ImmunisationStatusNotUpToDate
	}
	Id 9635bb8e-0300-4911-9fb0-b41fbb5cfc07
	Changelog {
		2017-01-14: ""
	}
}


Question EarWaxRemovalNeeded {
	"Do they have ear wax that needs removal?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem EarWaxRemovalNeeded
	}
	Id 7b43761c-e758-4ca4-8ebe-e81c084fb0ec
	Changelog {
		2017-01-14: ""
	}
}

Question FallsAssessmentNeeded {
	"Do they require a falls prevention #assessment#?"
	
	YesNoNA
	Category "general"
	
	Triggers {
		Yes: problem FallsAssessmentNeeded
	}
	Id 4d2ef17b-a6bb-4982-b69a-72c104627cf8
	Changelog {
		2017-01-14: ""
	}
}


Question PhysicalExaminationProblemsPresent {
	"Are there any significant problems on physical examination?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem PhysicalExaminationAbnormal
	}
	Links {
		ADLScore
	}
	Id 6923d153-6458-4c19-8b36-e322550d9eff
	Changelog {
		2017-01-14: ""
	}
}


Question HealthAssessmentEditing {
	"The #art# of medicine is to #tailor# the treatment to the patient before you. 
	
	Press Finish to complete this section once all questions are done."
	
	Finish
	Category "general"
	Education "Tips for better patient recall and adherence" {"
     
	* #Alter the generic advice# in the plan to a personalised form (e.g. 'Contact XYZ physio for falls assessment' instead of 'start physio') 

	* Keep the number of recommended changes achievable. \"#Less is more#\". 

	* Clicking #'completed'# for a problem will remove it from the printed out plan. You can always see these again by clicking on Show completed box. 

	* Deface the plan once it is printed by drawing on it with #highlighters#.

	* Suggest patient photograph their Health Summary and save it on their mobile phone.
	  "}
	Id c3d9075f-beb4-44ae-929a-2ee10136219a
	Changelog {
		2017-01-14: ""
	}
}

Problem MedicationManagementRequired {
	Goal "Avoid errors and aid adherence"
	Plan "Check need for dossette and medication review ? HMR"
	Id 7b9580f7-978f-45d0-837c-4a488e646ee6
	Changelog {
		2017-01-14: ""
	}
}


Problem MedicationHomeReviewNeeded {
	Goal "Avoid errors and assist adherence"
	Plan "Home Medication Review required."
	Id b1265af7-ed80-441a-bcbf-daf6a527d62f
	Changelog {
		2017-01-14: ""
	}
}


Problem ImmunisationStatusNotUpToDate {
	Goal "Up to date immunisation"
	Plan "Arrange catch up immunisation with pneumococcal +/- influenza +/- tetanus."
	Id d64c764c-e849-4f7d-b792-82483079a9ff
	Changelog {
		2017-01-14: ""
	}
}


Problem NutritionAdviceNeeded {
	Goal "Healthy nutrition"
	Plan "Advise 3 meals daily with fruit, vegetables and dairy most days."
	Id 8be1fc14-7519-4003-a40e-03406a485078
	Changelog {
		2017-01-14: ""
	}
}


Problem HomeMealPreparationProblem {
	Goal "Healthy nutrition"
	Plan "Arrange assistance with cooking such as Meals On Wheels."
	Id f41ffa57-0037-4407-af9b-99b1d1b4d6cc
	Changelog {
		2017-01-14: ""
	}
}


Problem DentalHealthProblemPresent {
	Goal "Good oral/dental care"
	Plan "Refer for dental care."
	Id bfe3b779-9b4f-415b-a6df-542acc813f28
	Changelog {
		2017-01-14: ""
	}
}


Problem IncontinenceBowelPresent {
	Goal "Determine cause"
	Plan "Determine cause and arrange supply incontinence pads. Continence Aides Payment Scheme (CAPS) 1800 807 487."
	Id 223a7306-91a8-4769-89b2-3af389001dc9
	Changelog {
		2017-01-14: ""
	}
}


Problem MobilityProblem {
	Goal "Improve mobility"
	Plan "Assess diagnosis and arrange physio assessment including provision of aid if required."
	Id f89dd831-ccbf-43a8-8e4e-9d9771181f72
	Changelog {
		2017-01-14: ""
	}
}

Problem SocialIsolationProblem {
	Goal "Assist social support"
	Plan "Assess adequacy of support and social network."
	Id cf554d27-5206-4a61-aba3-99378728288a
	Changelog {
		2017-01-14: ""
	}
}


Problem OutsideHomeMaintenanceNeeded {
	Goal "Assist outside maintenance."
	Plan "Arrange assessment with HACC or similar."
	Id c45a146b-8909-4711-88fe-e8f958630c21
	Changelog {
		2017-01-14: ""
	}
}


Problem InsideHomeAssistanceNeeded {
	Goal "Assist"
	Plan "Assess and arrange assistance."
	Id 7c658e27-3e20-43d6-acce-a50f188635db
	Changelog {
		2017-01-14: ""
	}
}


Problem EarWaxRemovalNeeded {
	Goal "Improve hearing"
	Plan "Arrange removal."
	Id 89eb980e-8b50-46af-8d45-eacca9c12885
	Changelog {
		2017-01-14: ""
	}
}

Problem MedicalPowerOfAttorneyRequired {
	Goal "Enable health care decisions."
	Plan "Arrange Medical Power of Attorney. Check Financial Power of Attorney provision."
	Id f1278631-8723-4751-a664-a1358e2304a7
	Changelog {
		2017-01-14: ""
	}
}


Problem TransportAssistanceNeeded {
	Goal "Assist"
	Plan "Assess need for assistance such as Taxi concession card."
	Id 297d26f2-88bb-442f-9f0e-4d3194344ba0
	Changelog {
		2017-01-14: ""
	}
}


Problem TaxiTransportNeeded {
	Goal "Taxi"
	Plan "Taxi transport concession card in place"
	Id 08cdadf7-7800-4bab-9c51-389e46cc7721
	Changelog {
		2017-01-14: ""
	}
}


Problem InsideHomeSafetyProblem {
	Goal "Reduce falls risk"
	Plan "OT home assessment required"
	Id ebcd3e38-0d5b-484f-a2a2-0f775a291b96
	Changelog {
		2017-01-14: ""
	}
}


Problem HomeOutsideSafetyProblem {
	Goal "Prevent falls"
	Plan "Needs OT assessment."
	Id ac89a08c-e504-45af-b6ee-92f16dde801e
	Changelog {
		2017-01-14: ""
	}
}


Problem BathroomSafetyProblem {
	Goal "Prevent falls"
	Plan "OT assessment. Grab rail, flexible shower hose, wide access shower door, non slip mat, and? bath chair."
	Id 44023ef5-0dc0-49e0-a4d4-8be61b7bc81b
	Changelog {
		2017-01-14: ""
	}
}


Problem ToiletSafetyProblem {
	Goal "Prevent falls"
	Plan "OT assessment, Grab rail provision and ? altered toilet seat height."
	Id 615529f3-99d9-42e1-82f1-61abd4ffaba8
	Changelog {
		2017-01-14: ""
	}
}


Problem VisionProblem {
	Goal "Improve"
	Plan "Refer to optometrist or ophthalmologist."
	Id 18d9f6b4-24db-472e-826d-446e991fd766
	Changelog {
		2017-01-14: ""
	}
}


Problem HearingProblem {
	Goal "Improve"
	Plan "Check ears for wax and refer to audiologist."
	Id c705ea50-0446-4166-a149-1fd5b4c00f41
	Changelog {
		2017-01-14: ""
	}
}

