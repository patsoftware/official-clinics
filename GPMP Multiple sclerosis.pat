/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with MS */
Clinic GPMPMultipleSclerosis {
	Id 816d6fee-4b7c-4591-a267-658d44a0daca
	Filter MultipleSclerosis
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			MultipleSclerosisPresent
		}

	}
}

Filter MultipleSclerosis {
	Match {
		keyword "MS"
		keyword "multiple sclerosis"
	}
	Tests matching only this {
		"MS"
		"multiple sclerosis"
		"relapsing remitting multiple sclerosis"
	}
}

Question MultipleSclerosisPresent {
	"There are a range of services to assist people with Multiple Sclerosis to manage at home. 

	MS Australia has good symptom management resources.

    Do you want to bring back for a review?"
    
    YesNoNA
	NAText "MS not present"

	Education "Range of services" {"
     
	 * http://www.msnetwork.org/roadmap/homecare.htm[Home Care and Related Support - Multiple Sclerosis Network of Care Australia]
 
	"}

	Education "Symptom management" {"
     
	 * https://www.msaustralia.org.au/about-ms/ms-practice[Symptom Management - Multiple Sclerosis Australia]
 
	"}
	Category "neurology"
    Triggers {
		Yes: problem MultipleSclerosisPresent
	}
    Id e7024a82-940c-4439-825d-4448b33368ca
}

Problem MultipleSclerosisPresent {
	Goal "Optimise care"
    Plan "Access services"
    Id e39fc586-aa4d-47a8-93e6-3f5945588a4f
}
