/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with AF */
Clinic GPMPAF {
	Id 4428b4f9-b323-4389-9116-695486c0fef3
	Filter AF
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			AtrialFibrillationAndCHA2DS2VAScore
			AnticoagulantIndicated
            DualTherapyIndication
		}

	}
}

Clinic GPMPAFCore {
	Id 4428b4f9-b323-4389-9116-695486c0fef4
	Filter AF
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			AtrialFibrillationAndCHA2DS2VAScore
		}

	}
}
Filter AF {
	Match {
		keyword "AF"
		keyword "Atrial fibrillation"
		keyword "Atrial flutter"
	}
	Tests matching only this {
		"AF"
		"af"
		"Atrial fibrillation"
		"AF on atenolol"
		"AF on digoxin"
		"controlled atrial fibrillation"
		"rapid AF"
		"rapid atrial fibrillation"
	}
}




Question DualTherapyIndication {
	"
    Some patients have indications for both #DAPT (dual antiplatelet therapy)# for CHD and #oral anticoagulation (OAC)# for AF.

    The duration of triple therapy should be as short as possible to mitigate bleeding risk.
    
    Maximum duration is 3 months for ACS with/without stenting and 1 month for elective stenting.

    After 12 months most patients just require OAC monotherapy.



    Do you wish to #re-evaluate use#?"
    YesNoNA
	NAText "Decision already made"
    Category "Heart / Brain"
    Triggers {
		Yes: problem DualTherapyIndication
	}
    Id 7f66e241-3b5b-4c92-a01f-dc0738eeca2c
    Changelog {
        2020-03-05: ""
    }
}

Problem DualTherapyIndication {
	Goal "Reduce bleeding risk"
    Plan "Cease antiplatet agent as indicated"
    Id 6a4d2efe-7a56-46c7-8813-06f7c99e7c1f
    Changelog {
        2020-03-05: ""
    }
}

Question AtrialFibrillationAndCHA2DS2VAScore {
	"If atrial fibrillation is present, the sexless CHA2DS2-VA score is recommended to assess stroke risk and oral anticoagulant use.
	
	Note paroxysmal and permanent AF have similar risk profiles and should be treated equally. 

	Score interpretation:

	,===
	[blue]*Low risk*,[blue]*0*,[blue]*no anticoagulant*
	[blue]*Low-moderate risk*,[blue]*1*,[blue]*antiplatelet or anticoagulant* 
	[blue]*Moderate-high risk*,[blue]*2 or over*,[blue]*anticoagulant* 
	,===

	Should the patient be on an anticoagulant?"
	
	YesNoNA
	NAText "N/A - in sinus rhythm"
	Category "Heart / Brain"

	
	Education "References" {"
		* https://www.heartfoundation.org.au/images/uploads/publications/AF_Figure_6_Stroke_prevention_in_atrial_fibrillation.jpg[NHF: CHA₂DS₂-VA score interpretation]
	"}
	
	Triggers {
		Yes: education optional CHA2DS2-VA
		Yes: problem AtrialFibrillationPresent
	}
	Id 4ac5c46c-022c-495b-a6b1-4c39a63ff56c
	Changelog {
		2017-01-14: ""
	}
}

Education CHA2DS2-VA {
	Message "CHA2DS2-VA score"
    Link "https://www.heartfoundation.org.au/images/uploads/publications/Table_3_Definitions_and_points_in_the_CHA2DS2-VA_score.pdf"

    Id 3c5ffa20-f175-48ba-96c8-07a4211c924a
    Changelog {
        2017-10-22: ""
    }
}



Question AnticoagulantIndicated {
	"If the new sexless CHA2DS2-VA score indicates the patient should be on an anticoagulant establish if:

	 * Valvular: presence of #mechanical# prosthetic valves or moderate to severe mitral stenosis. You must use warfarin in these cases.
	 
	 OR
	 
	 * Non-valvular: NOAC (Non Vitamin K Oral Anticoagulant) is recommended instead of warfarin (unless CKD 4-5 is present in which case use warfarin.)
	 
	 Then use #HAS-BLED# to check for major bleeding risk
	 
	Do you want to start an anticoagulant?"
	
	YesNoNA
	NAText "N/A - absolute contraindication"
	Category "Heart / Brain", "medications"
	
	Education "HAS-BLED score for major bleeding risk" {"
      * People with AF should not be excluded from anticoagulant therapies solely on the basis of a high risk of bleeding determined using the HAS-BLED tool.

	  * Use the tool to identify and correct risk factors for bleeding, not just to assess bleeding risk.

	  * People at high risk of bleeding (people with three or more HAS-BLED risk factors) may need increased monitoring. 

	  * Having a risk of falls is not a direct contraindication to anticoagulant therapy. A patient would have to fall 295 times in one year for the risk of subdural haemorrhage to outweight the benefits of anticoagulation.(NPS Feb 2013).

	  * However the risk of falls should be reduced where possible by a Falls Prevention programme.  

	   * https://www.mdcalc.com/has-bled-score-major-bleeding-risk[HAS-BLED Score for major bleeding risk]

	"}

	Education "References" {"
		* https://www.mja.com.au/journal/2018/209/10/national-heart-foundation-australia-and-cardiac-society-australia-and-new[National Heart Foundation of Australia and Cardiac Society of Australia and New Zealand: Australian clinical guidelines for the diagnosis and management of atrial fibrillation (2018)]
	"}
	
	Triggers {
		Yes: problem WarfarinOrOtherNOACIndicated
		
	}
	Id 08f2a24c-9e70-42bc-9593-ffa79dfd9cad
	Changelog {
		2017-01-14: ""
	}
}


Problem AtrialFibrillationPresent {
	Goal "Protect from stroke"
	Plan "Review any contraindications to anticoagulant medication and decide if absolute or relative."
	Id 0876058b-b537-4e3a-8c01-4a0e11ab16fe
	Changelog {
		2017-01-14: ""
	}
}


Problem WarfarinOrOtherNOACIndicated {
	Goal "Prevent stroke"
	Plan "Start NOAC unless contraindicated (CKD 4-5 or valvular AF)"
	Id f0870639-611e-43b7-b934-94e2a42bb7fc
	Changelog {
		2017-01-14: ""
	}
}
