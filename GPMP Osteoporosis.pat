/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with osteoporosis */
Clinic GPMPOsteoporosis {
	Id 1c523200-0776-47d2-8896-489671cccc94
	Filter Osteoporosis
	Questions {
		Patient {
			OsteoporoticFractureBackPainPresent
			NationalSupportOrganisationReferralNeeded
		}

		Checklist {
		}

		Staff {
			OsteoporosisSpecificTreatmentNeeded
			BisphosphonateDurationReviewNeeded
		}

	}
}

Clinic GPMPOsteoporosisCore {
	Id 1c523200-0776-47d2-8896-489671cccc95
	Filter Osteoporosis
	Questions {
		Patient {
			OsteoporoticFractureBackPainPresent
			NationalSupportOrganisationReferralNeeded
		}

		Checklist {
		}

		Staff {
			OsteoporosisSpecificTreatmentNeeded
		}

	}
}
Filter Osteoporosis {
	Match {
		keyword "Crush  fracture"
		keyword "Fractured hip"
		keyword "Fractured neck of femur"
		keyword "Fractured vertebrae"
		keyword "Fragility fracture"
		keyword "Hip fracture"
		keyword "Osteoporosis"
		keyword "Osteoporosis with fracture"
		keyword "Osteoporotic fracture"
		keyword "Vertebral fracture"
	}
	Tests matching only this {
		"Osteoporosis"
	}

}

Question OsteoporoticFractureBackPainPresent {
	"Do you have #persistent back pain# from an osteoporotic crush fracture in your back?"
	
	YesNoNA
	NAText "N/A - no crush fracture "
	Category "osteoporosis"
	
	Triggers {
		Yes: problem BackPainPresent
	}
	Id f7dc50f4-e898-4891-a6e2-6b11fe504c74
	Changelog {
		2017-01-14: ""
	}
}


Question ArthritisOrOsteoporosisAustraliaReferralNeeded {
	"Would you like to know how to join #Arthritis Australia# or #Osteoporosis Australia# so you regularly receive more information?"
	
	YesNo
	Category "osteoporosis"
	
	Triggers {
		Yes: problem ArthritisAustraliaOrOsteoporosisAustraliaMembershipNeeded
	}
	Id a6d65df3-741d-430a-adfa-a6b7cbaa9f86
	Changelog {
		2017-01-14: ""
	}
}


Question OsteoporosisSpecificTreatmentNeeded {
	"Anti-osteoporosis medication is #definitely# indicated for any #one# of these: 
	
	* minimal trauma fracture history (BMD desirable but not compulsory) #or# 
	* age over 70 years and BMD T score < -3.0 (< -2.5 if using Alendronate or Denosumab) #or# 
	* corticosteroid use of at least 7.5mg and >3 months duration and BMD T-score < -1.5. 
	
	(Press the education button for the list of specific treatment options) 
	
	Do you wish to initiate specific treatment?"
	
	YesNoNA
	NAText "N/A - already on treatment"
	Category "osteoporosis"

	Education "References" {"
		* https://www.racgp.org.au/download/Documents/Guidelines/Musculoskeletal/osteoporosis-guidelines.pdf[Osteoporosis prevention diagnosis and treatment in postmenopausal women and men over 50 years of age, 2nd Eedition, RACGP, 2017]
	"}
	
	Triggers {
		Yes: problem OsteoporosisSpecificTreatmentNeeded
		Yes: education optional OsteoporosisMedications
	}
	Id 8e56fc64-e198-44ff-a89c-63bf0a35168c
	Changelog {
		2017-01-14: ""
	}
}


Question BisphosphonateDurationReviewNeeded {
	"#Review the duration after 5 years# of therapy if they have been on bisphosphonate therapy and now judged to be low risk of fracture. 
	
	Do you want to reconsider use?"
	
	YesNoNA
	NAText "N/A - treatment not required "
	Category "osteoporosis"
	
	Education "The International Osteoporosis Foundation recommended approach" {"
      * T score worse then -2.5 at the femoral neck: continue treatment 
	  * T score worse than - 2.0 at the femoral neck *with* a previous vertebral or hip #: likely to benefit from continuing treatment 
	  * T score better than -2.5 at the femoral neck *without* prior vertebral or hip #: unlikely to benefit from continuing treatment 
	
	Note: 

	   * Withdrawal needs monitoring with bone density second yearly & treatment should be restarted if BMD declines by >5% or another fracture occurs. 	
	   * Bisphosphonates persist in bone for several years, but the broad suggestion is that 2 years after stopping them patients are 'treatment naive'.
	   * Rapid bone loss can occur on cessation of denosumab. However commencing a bisphosphonate can mitigate this.

	== References
	* https://www.racgp.org.au/download/Documents/Guidelines/Musculoskeletal/osteoporosis-guidelines.pdf[Osteoporosis prevention diagnosis and treatment in postmenopausal women and men over 50 years of age, 2nd Eedition, RACGP, 2017]

	"}
	Triggers {
		Yes: problem BisphosphonateDurationReviewNeeded
	}
	Id a94cc335-c201-4f92-bd3d-a1e26c14734d
	Changelog {
		2017-01-14: ""
	}
}


Problem BackPainPresent {
	Goal "Clarify diagnosis and degree of concern"
	Plan "Arrange a follow-up consultation to specifically discuss the back pain."
	Id f178760d-4a6e-4ced-af1a-115dd8afb5a1
	Changelog {
		2017-01-14: ""
	}
}


Problem ArthritisAustraliaOrOsteoporosisAustraliaMembershipNeeded {
	Goal "Offer to all"
	Plan "Website is www.arthritisaustralia.com.au or www.osteoporosis.org.au."
	Id fa60277d-2d6e-4fd2-9b0c-8d04a3174411
	Changelog {
		2017-01-14: ""
	}
}


Problem BisphosphonateDurationReviewNeeded {
	Goal "Clarify response over time"
	Plan "Review the BMD and risk calculation. If ceasing arrange BMD recall in 1 to 2 years. Restart then if significant decrease in BMD."
	Id 2dc534c4-c2d2-4028-b531-5fdf3724cd12
	Changelog {
		2017-01-14: ""
	}
}


Education OsteoporosisMedications {
	File "rtf"
	Id 717521a8-305f-47f4-bb62-b2ceb2f9610b
	Changelog {
		2017-01-14: ""
	}
}


