/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with inflammatory bowel disease */
Clinic GPMPIBD {
	Id 452da5c0-41a5-41b8-b5bd-428b5de72007
	Filter IBD
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			IBDPresent
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter IBD {
	Match {
		keyword "Crohn"
		keyword "Crohn's"
		keyword "Inflammatory bowel"
		keyword "Regional enteritis"
		keyword "Ulcerative colitis"
	}
	Tests matching only this {
		"Crohn"
		"Crohn's"
		"crohn's disease"
		"regional enteritis"
		"Inflammatory bowel"
		"Inflammatory bowel disease"
		"Ulcerative colitis"
	}
}


Question IBDPresent {
	"If they have #inflammatory bowel disease# (Crohn's or U. colitis) check if they on the 
	
	* Appropriate preventer agent 
	* With appropriate pathology monitoring 
	* And appropriate follow-up with specialist and GP. 
	
	Do you wish to #arrange monitoring# and/or specialist follow up?"
	
	YesNoNA
	NAText "N/A - no IBD"
	Category "Colitis / Crohn's"

	Education "Inflammatory Bowel Disease GESA 2018 guidelines" {"
	
	* https://www.gesa.org.au/resources/inflammatory-bowel-disease-ibd/[Australian guidelines for Inflammatory Bowel Disease 4th edition (GESA)]
    ** https://www.gesa.org.au/public/13/files/Professional/2018_IBD_Clinical_Update_May_update.pdf[IBD 2018 Clinical Update]
	*** Routine follow up recommendations are on Pages 23-27
	
"}

	Triggers {
		Yes: problem IBDPresent
	

		
	}
	Id 247fb537-5cf8-45e0-a1a7-d40ac83db071
	Changelog {
		2017-01-14: ""
	}
}


Problem IBDPresent {
	Goal "Remission and monitoring"
	Plan "Add an appropriate reminder for IBD check up. If on immunosuppression check rule 3 pathology test provision. Prior to check up pre-order annual FBC, iron, B12, folate and Vitamin D. Bring back for preconception planning if relevant. Check vaccination status. Check Gastroenterologist referral requirement."
	Id 3078a927-4afe-4745-b864-79a463195f1d
	Changelog {
		2017-01-14: ""
	}
}


Education IBDComplicationPrevention {
	File "rtf"
	Id 5fcce561-32e8-47b9-acc6-5cced9ab1603
	Changelog {
		2017-01-14: ""
	}
}


