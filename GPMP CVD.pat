/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with CVD */
Clinic GPMPCVD {
	Id 62a172cd-b787-40c7-b996-4a56ad57a7e2
	Filter CVD
	Questions {
		Patient {
			BPMonitorOwnershipNeeded
			LipidLoweringAgentUseNeeded
			StatinConcern
			SaltIntakeHigh
			NationalSupportOrganisationReferralNeeded
			
		}

		Checklist {
		}

		Staff {
			Stroke3DrugsNeeded
			TIAOrStrokeRecently
			LipidResultsNotWithinRangeOnMedication
			StatinInitiationBaselineTestsNeeded
			FamilialHypercholesterolaemiaRisk
		}

	}
}

Clinic GPMPCVDCore {
	Id 62a172cd-b787-40c7-b996-4a56ad57a7e3
	Filter CVD
	Questions {
		Patient {
			BPMonitorOwnershipNeeded
			LipidLoweringAgentUseNeeded
			StatinConcern
			SaltIntakeHigh
			NationalSupportOrganisationReferralNeeded
			
		}

		Checklist {
		}

		Staff {
			Stroke3DrugsNeeded
		}

	}
}

Filter CVD {
	Match {
		keyword "Basal ganglia infarct"
		keyword "Basal ganglion infarct"
		keyword "Cerebral circulation infarct"
		keyword "Cerebral infarct"
		keyword "Cerebrovascular accident"
		keyword "Cerebrovascular disease"
		keyword "CVD"
		keyword "Infarct dementia"
		keyword "Infarction of basal ganglia"
		keyword "Lacunar infarct"
		keyword "Lacunar syndrome"
		keyword "Stroke"
		keyword "Thalamic infarct"
		keyword "TIA"
		keyword "Transient ischaemic attack"
		keyword "Vascular dementia"
		regex "infarct.*visual"
		regex "visual.*infarct"
	}
	Tests matching only this {
		"Cerebrovascular disease"
		"Cerebrovascular accident"
		"TIA"
		"Transient Ischaemic Attack"
		"CVD"
		"Stroke"
		"cerebral infarction"
		"Anterior cerebral circulation infarction"
		"cerebral infarction"
		"cerebral circulation infarction"
		"cerebral circulation infarct"
		"haemorrhagic cerebral infarction"
		"Basal ganglion infarct"
		"Basal ganglion stroke"
		"embolic stroke"
		"Infarction of basal ganglia"
		"Lacunar infarction"
		"multiple Lacunar infarcts"
		"lacunar infarcts"
		"lacunar stroke"
		"Pure motor lacunar infarction"
		"Pure motor lacunar syndrome"
		"Infarction of visual cortex"
		"infarct of visual cortex"
		"Thalamic stroke"
		"thalamic infarction"
		"Occipital cerebral infarction"
	}
}


Question Stroke3DrugsNeeded {
	"Stroke Foundation guidelines recommend all cerebrovascular disease patients with a Stroke or TIA should receive: 
	
	* Statin (Statins should not be routinely used for haemorrhagic stroke) 
	* Antihypertensive should be used in all patients with systolic BP >140 mmHg with long term target of <130 mmHg. (ACEI, ARB, Calcium blockers & thiazides are acceptable with best evidence for dual ACEI/thiazide) 
	* Antiplatelet agent 
	
	Are they on the #Ischaemic Stroke 3# regimen? 
	
	image:Stroke3Drugs.jpg[width=641]"
	
	YesNoNA
	NAText "N/A - Haemorrhagic stroke"
	Category "Heart / Brain", "medications"

	Education "Clinical guidelines for stoke managment 2017" {"
	* https://informme.org.au/en/Guidelines/Clinical-Guidelines-for-Stroke-Management-2017
	"}
	Triggers {
		No: problem CerebrovascularDiseasePresent
		Yes: education optional StrokeGuidelineAntiplatelet
	}
	Id 5253cb5d-3100-42b6-ab74-9ce7b0459877
	Changelog {
		2017-01-14: ""
	}
}


Question TIAOrStrokeRecently {
	"The Stroke Foundation 2017 guidelines have a recommended work-up for stroke and TIA.    
	
	If the patient has had #either a TIA or stroke# within the last 12 months have they been #fully investigated?#"
	
	YesNoNA
	NAText "N/A - no recent stroke/TIA"
	Category "Heart / Brain"
    
    Education "Stroke/TIA work-up" {"
     * All TIA patients with anterior circulation symptoms should undergo early carotid imaging with CT angiography (aortic arch to cerebral vertex), carotid Doppler ultrasound or MR angiography. Carotid imaging should preferably be done during the initial assessment but should not be delayed more than 2 days  
    * The administration of intravenous iodinated contrast for CT angiography/perfusion, when clinically indicated, should not be delayed by concerns regarding renal function. Post-hydration with intravenous 0.9% saline is advisable. (RANZCR guidelines 2016 [64]; Ang et al. 2015 [63]).
	* All patients with TIA should be investigated for atrial fibrillation with ECG during initial assessment and referred for possible prolonged cardiac monitoring as required (see Cardiac Investigations).
	* Blood tests for diabetes / dyslipidaemia. (In younger patients without clear alternative pathology check for thrombophilic states.) 
	* In patients with ischaemic stroke, echocardiography should be considered based on individual patient factors. (especially in younger patients without clear alternative pathology.) 	

    From https://app.magicapp.org/app#/guideline/2279[Clinical guidelines for TIA 2017]
	"}
    Education "Clinical guidelines for TIA 2017" {"
    	* https://app.magicapp.org/app#/guideline/2279
	"}
	
    
	Triggers {
		No: problem TIAOrStrokeRecently
	}
	Id 00b88607-f7d8-4667-8616-5120c645dc17
	Changelog {
		2017-01-14: ""
	}
}


Problem CerebrovascularDiseasePresent {
	Goal "Protect from further episodes"
	Plan "Ensure on all 3 therapies for secondary prevention. If female check for any kind of hormonal therapy."
	Id c9140771-455a-480e-9295-39ada4eaa0bb
	Changelog {
		2017-01-14: ""
	}
}


Problem TIAOrStrokeRecently {
	Goal "Ensure full work up"
	Plan "Check records for CT and carotid duplex scans and list results in health summary next to diagnosis. If female check for any kind of hormonal therapy."
	Id 24196abd-b3e5-4976-8e15-eae912ada5c1
	Changelog {
		2017-01-14: ""
	}
}


Education StrokeGuidelineAntiplatelet {
	Message "Stroke guidelines - antiplatelets"
	File "rtf"
	Id 1e1f63a0-c4f3-4771-802b-60698547fbb1
	Changelog {
		2017-01-14: ""
	}
}


