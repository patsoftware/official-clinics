/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with gout */
Clinic GPMPGout {
	Id 0f5b4a9a-0ed0-409e-a324-085b2a9a1945
	Filter Gout
	Questions {
		Patient {
		}


		Checklist {
		}

		Staff {
			GoutyArthritisPresent
		}

	}
}

Filter Gout {
	Match {
		keyword "Gout"
		keyword "Gouty"
		keyword "Tophi"
		keyword "Tophaceous"
	}
	Tests matching only this {
		"gout"
		"gouty arthritis"
		"tophi"
		"tophaceous"
		"tophaceous gout"
	}
}


Question GoutyArthritisPresent {
	"The uric acid #target level# for all gout patients is < 0.36 mmol/L or < 0.30 if tophi or severe gout.

	Do you want to alter or start #treatment#?"
   
    YesNoNA
    NAText "No gout"
    Category "arthritis"
    Triggers {
		Yes: problem GoutyArthritisPresent
	}
    Id 7fcffbb1-b5cd-491e-a01f-f038bff82cb5
    Changelog {
        2018-10-30: ""
    }
}

Problem GoutyArthritisPresent {
	Goal "Reduce attacks"
    Plan "Slowly increase allopurinol with colchicine low dose 0.5mg bd prophyllaxis (elderly or mild CKD use 0.5mg)."
    Id 271ca740-52c2-4d20-8c1f-1fe9e02b5deb
    Changelog {
        2018-10-30: ""
    }
}

