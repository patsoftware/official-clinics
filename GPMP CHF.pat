/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with CHF */
Clinic GPMPCHF {
	Id ba563885-8c6e-4ee4-8bdc-b683a751f32f
	Filter CHF
	Questions {
		Patient {
			BPMonitorOwnershipNeeded
			LipidLoweringAgentUseNeeded
			StatinConcern
			SaltIntakeHigh
			NationalSupportOrganisationReferralNeeded
			
		}

		Checklist {
		}

		Staff {
			CHF3DrugsNeeded
			CHFDiagnosisNeeded
			CHFClassificationNeeded
			ARNIForCHFNeeded
			CHFAggravatingDrugsPresent
			CHFFluidManagementNeeded
			AnaemiaCheckInChronicDiseaseNeeded
			LipidResultsNotWithinRangeOnMedication
		}

	}
}

Clinic GPMPCHFCore {
    Id 290b33c1-d4fe-42b7-bb8e-99982fd783b3
	Filter CHF
	Questions {
		Patient {
			BPMonitorOwnershipNeeded
			LipidLoweringAgentUseNeeded
			StatinConcern
			SaltIntakeHigh
			NationalSupportOrganisationReferralNeeded
			
		}

		Checklist {
		}

		Staff {
			CHF3DrugsNeeded
		}

	}
}

Filter CHF {
	Match {
		keyword "Cardiomyopathy"
		keyword "CCF"
		keyword "CHF"
		keyword "Congestic heart failure"
		keyword "Congestive cardiac"
		keyword "Congestive heart"
		keyword "Heart failure"
		keyword "HF"
	}
	Tests matching only this {
		"Congestive heart failure"
		"Congestive cardiac failure"
		"Congestive heart disease"
		"Heart failure"
		"HF"
		"CHF"
		"CCF"
		"CCF - Congestive cardiac failure"
	}
}

Question ARNIForCHFNeeded {
	"
    #Entresto# (sacubitril/valsartan) is now PBS authority for CHF (NYHA Class 2-4) with #LVEF <40% despite receiving maximally tolerated dose ACE or ARB# for 3 to 6 months. It combines an angiotensin receptor blocker with a nephrilysin inhibitor. RCT showed superior results to ACEI.

	Do you want to substitute #Entresto# for ACEI or ARB?
    "
    
    YesNoNA
    NAText "Entresto already started, or LVEF >40%"
    Category "Heart / Brain"

	Education "References" {"
	* Australian clinical guidelines for management of heart failure 2018, National Heart Foundation of Australia and Cardiac Society of Australia and New Zealand

		** https://www.heartfoundation.org.au/for-professionals/clinical-information/heart-failure [Resources for health professionals]
	"}
	

    Triggers {
		Yes: problem ARNIForCHFNeeded
    }
    Id 24db4d3d-6f66-4dda-813d-fb7c67a69b33
    Changelog {
        2017-07-12: ""
    }
}

Problem ARNIForCHFNeeded {
    Goal "Improve function and reduce hospitalisations"
    Plan "Allow ACEI 36 hours to wash out. Start ARNI Entresto one tab 49mg/51mg bd for 2 weeks then increase to 97mg/103mg bd as tolerated."
    Id f3296d8c-43f0-4169-8510-c1d450e2b0b9
    Changelog {
        2017-07-12: ""
    }
}




Question CHF3DrugsNeeded {
	" If HFrEF is present 2018 NHF guideline recommends they should be on the \"#CHF 3#\" medications. 
	
	* ACEI or ARB in maximum tolerated dose 
	* Specific beta blocker in maximum tolerated dose 
	* MRA (mineralocorticoid receptor antagonist)
	
	Are they on the CHF 3 regimen? 
	
	image:CHF2Drugs.jpg[width=563]"
	
	YesNoNA
	Category "Heart / Brain", "medications"
	NAText "N/A - not systolic CHF"

	Education "References" {"
	* Australian clinical guidelines for management of heart failure 2018, National Heart Foundation of Australia and Cardiac Society of Australia and New Zealand

		** https://www.heartfoundation.org.au/for-professionals/clinical-information/heart-failure-professionals[Resources for health professionals]
        ** https://www.heartfoundation.org.au/images/uploads/main/For_professionals/Clinical_Fact_Sheet_-_Pharmacological_Management.pdf[Clinical fact sheet: pharmacological management of HFrEF]
	"}
	
	
	Triggers {
		NA: AutoAnswer {
			ARNIForCHFNeeded NA
		}
		No: problem ChronicHeartFailureCHFPresent
	}
	Id 2ec905ea-2540-48c7-9e4d-d63334012886
	Changelog {
		2017-01-14: ""
	}
}


Question CHFDiagnosisNeeded {
	"If chronic heart failure (CHF) is a diagnosis, has this ever been confirmed by an #echocardiogram?#
	"
	
	YesNoNA
	NAText "N/A - no CHF"
	Category "Heart / Brain"

	Education "References" {"
	* Australian clinical guidelines for management of heart failure 2018, National Heart Foundation of Australia and Cardiac Society of Australia and New Zealand

		**  https://www.heartfoundation.org.au/for-professionals/clinical-information/heart-failure [Resources for health professionals]
	"}
	
	Triggers {
		No: problem CHFEchoDiagnosisNeeded
	}
	Id 6f1c34cc-d96e-4b61-9092-03a5484434bd
	Changelog {
		2017-01-14: ""
	}
}


Question CHFClassificationNeeded {
	"NHF recommends using the #echocardiogram to classify# CHF: 
	
	* #Systolic heart failure (HFrEF)# (LVEF <50%. Additional criteria required if LVEF 41-49% ) 
	* #Diastolic (HFpEF)# or HFPSF (heart failure preserved systolic function) LVEF >50% 
	
	Then list the #aetiology# of the CHF (e.g. CHD is commonest cause of LV systolic HF and hypertension is the commonest cause of LV diastolic HF). 
	
	The #treatment# depends on the aetiology #and# the type of CHF. 

	(See link below for indications for specialist referral). 
	
	Have you classified the CHF?"
	
	YesNoNA
	NAText "N/A - no CHF"
	Category "Heart / Brain"

	Education "Heart failure diagnostic criteria (MJA 2018)" {"

	*Heart failure with reduced ejection fraction (HFrEF)*
    
	    * Symptoms +/- signs of heart failure and
	    * LVEF < 50%.
	  (LVEF 41-49% requires additional criteria: signs of heart failure or diastolic dysfunction with high filling pressure demonstrated by invasive means or echo or BNP testing)
	  
	*Heart failure with preserved ejection fraction (HFpEF)*

	    * Symptoms +/- signs of heart failure
		and 
		* LVEF > 50% and 
		* Objective evidence of:
		- relevant structural heart disease (LV hypertrophy, left atrial enlargement)
		and/or
		- diastolic dysfunction, with high filling pressures demonstrated by *any* of the following:
		- invasive means (cardiac catheterisation)
		- echocardiography
		- biomarker (elevated BNP or NT pro BNP)
		- exercise test (invasive or echocardiography) 
	"}

	Education "References" {"
	* Australian clinical guidelines for management of heart failure 2018, National Heart Foundation of Australia and Cardiac Society of Australia and New Zealand

		** https://www.heartfoundation.org.au/for-professionals/clinical-information/heart-failure  [Resources for health professionals]
	"}
	
	Triggers {
		No: problem CHFClassificationNeeded
		Yes: education optional HeartFailureReferralIndicationsRtf
	}
	Id 02759f56-14a1-477b-8320-b87ed1af4f86
	Changelog {
		2017-01-14: ""
	}
}


Question CHFAggravatingDrugsPresent {
	"NHF recommends avoidance of #drugs that can exacerbate# CHF 
	
	Do you want to alter any of these medications?"
	
	YesNoNA
	NAText "N/A - No CHF"
	Category "Heart / Brain", "medications"
	
	Education "Medications exacerbating CHF" {"
      Common: 
	
	* Non-dihydropyridine calcium channel blockers (e.g. verapamil, diltiazem) 
	* Tricyclic antidepressants
	* Glitazones (rosiglitazone or pioglitazone)
	* Corticosteroids 
	* NSAIDs
	* Moxonidine 
	* Anti-arrhythmics other than amiodarone and beta blockers 
	
	Uncommon:
	
	* Clozapine 
	* TNF antagonists 
	* Dronedarone 
	* Trastuzumab 
	* Tyrosine kinase inhibitors 

	== References

	* Australian clinical guidelines for management of heart failure 2018, National Heart Foundation of Australia and Cardiac Society of Australia and New Zealand

		** https://www.heartfoundation.org.au/for-professionals/clinical-information/heart-failure [Resources for health professionals]
	"}
	
	Triggers {
		Yes: problem CHFAggravatingDrugsPresent
	}
	Id c3acc783-111c-421a-bf37-e1546dade575
	Changelog {
		2017-01-14: ""
	}
}


Question CHFFluidManagementNeeded {
	"NHF recommends that #symptomatic# CHF patients should: 
	
	* #Limit# their #fluid intake# to 1.5 litres daily in mild/moderate CHF, and 1 litre daily in severe CHF. 
	* #Weigh every day# after rising, emptying their bladder and before dressing. If their weight increases by #>2kg# over 48 hours they need to see their GP or self medicate with frusemide. The phrase is \"wake, wee, weigh\". 
	
	Do you want to discuss this fluid management plan with the patient?"
	
	YesNoNA
	NAText "N/A - not symptomatic"
	Category "Heart / Brain"
	
	Triggers {
		Yes: problem CHFFluidManagementPlanNeeded
	}
	Id 831beb4c-8c67-4ea2-87af-282af9855ca0
	Changelog {
		2017-01-14: ""
	}
}


Question AnaemiaCheckInChronicDiseaseNeeded {
	"NHF recommends checking for and treating #both# iron deficiency & anaemia in CHF patients. 
	
	Iron carboxymaltose infusions (but not oral replacement) have been shown to improve morbidity but not mortatlity in Systolic LV heart failure patients with #either# transferrin saturations <20% and ferritin 100 to 299 #or# ferritin <100mcg/l alone. This benefit is #irrespective# of the Haemoglobin concentration. 
	
	Do you want to check FBE and iron studies?
	"
	
	YesNoNA
	NAText "N/A - no CHF"
	Category "Heart / Brain"

	Education "References" {"
	* Australian clinical guidelines for management of heart failure 2018, National Heart Foundation of Australia and Cardiac Society of Australia and New Zealand

		** https://www.heartfoundation.org.au/for-professionals/clinical-information/heart-failure [Resources for health professionals]
	"}
	
	
	Triggers {
		Yes: problem AnaemiaCheckNeeded
	}
	Id 994e6c3f-535c-4343-903d-daddef2a7b75
	Changelog {
		2017-01-14: ""
	}
}


Problem ChronicHeartFailureCHFPresent {
	Goal "Protect from further episodes"
	Plan "With Systolic (HFrEF) ensure is on secondary prevention drugs in MAXIMAL tolerated dose. Note with diastolic (HFpEF) spironolactone may be considered as well."
	Id f4231f81-09c2-434b-8520-8cf1972fdcef
	Changelog {
		2017-01-14: ""
	}
}


Problem CHFEchoDiagnosisNeeded {
	Goal "Echocardiogram at least once in all CHF patients to determine type and severity."
	Plan "Order echocardiogram or refer to specialist. Ensure result of echocardiogram is appended to CHF in the health summary."
	Id 81e7aaf4-8d5f-45de-aa5c-9463a2ac3005
	Changelog {
		2017-01-14: ""
	}
}


Problem CHFClassificationNeeded {
	Goal "Classify both type and aetiology of CHF."
	Plan "Check for structural diagnosis such as valvular or myopathic heart disease. Then check for pathophysiologic diagnosis such as IHD which may be amenable to intervention such as stent or surgery. Refer to cardiologist if any uncertainty."
	Id 9baf2e90-0d6d-4fcb-886e-69d76627d090
	Changelog {
		2017-01-14: ""
	}
}


Problem CHFFluidManagementPlanNeeded {
	Goal "All CHF patients to have a fluid plan."
	Plan "Wake, wee, weight. If >2kg over 48 hours treat."
	Id 6ed7a8c9-46a4-4c56-bd9d-f729fd89f145
	Changelog {
		2017-01-14: ""
	}
}


Problem CHFAggravatingDrugsPresent {
	Goal "Clarify"
	Plan "Cease"
	Id 80d39065-2278-4c27-abb0-6b6ae1ea2985
	Changelog {
		2017-01-14: ""
	}
}


Problem AnaemiaCheckNeeded {
	Goal "Detect and treat anaemia in chronic disease."
	Plan "Order FBE and full iron studies if not done and arrange follow up."
	Id ca2983bc-6b9d-4bc3-b565-ec5b896e5e47
	Changelog {
		2017-01-14: ""
	}
}


Education HeartFailureReferralIndicationsRtf {
	Message "Heart failure referral indications"
	File "rtf"
	Id 30e535b7-78fa-4e11-ba8c-e989018e7194
	Changelog {
		2017-01-14: ""
	}
}


