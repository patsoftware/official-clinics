/* 
 * Copyright (c) 2017 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Problem AbsoluteCardiovascularRiskIsMediumOrHigh {
	Goal "Reduce risk calculation by addressing modifiable risk factors"
	Plan "Treat high absolute CV risk with lipid lowering and BP lowering medication unless contraindicated or clinically inappropriate. With moderate CV risk, drug treatment not routinely recommended. Aspirin not recommended for primary prevention just for secondary."
	Id 239867fd-b4c6-4981-ae04-b7c81d901200
	Changelog {
		2017-01-14: ""
	}
}

Problem AspirinOrNSAIDRiskFactors {
	Goal "Determine if relative or absolute contraindication. Reduce risk of NSAID or aspirin use with appropriate choice of medications."
	Plan "Review GIT and CV risk calculations. Discuss risks/benefits with patient."
	Id ea6882de-13b4-4cba-8167-ba458dcddfe2
	Changelog {
		2017-01-14: ""
	}
}

Question Aspirin-RiskFactors {
	"Aspirin is used in #secondary# prevention. 
	
	It is now not routinely used in primary prevention in medium or high absolute risk calculation patients. 
	
	The following need to be considered if aspirin is to be prescribed: . 
	
	* Allergy to aspirin in the past? 
	* Past history of peptic ulcer or bleeding from GIT? 
	* If PH of peptic ulcer has H pylori been tested &/or treated? 
	* Persistent or recurrent dyspepsia? 
	* On Warfarin or other oral anticoagulant? 
	* On another antiplatelet agent? 
	* On another NSAID? 
	* On Corticosteroids? 
	
	Are there #risk factors for aspirin# use?"
	
	YesNoNA
	NAText "N/A - use not indicated "
	Category "medications"
	
	Triggers {
		Yes: problem AspirinOrNSAIDRiskFactors
	}
	Id 352e7a86-f06d-416a-9130-917a85b8d34c
	Changelog {
		2017-01-14: ""
	}
}


Question BPMonitorOwnershipNeeded {
	"Blood pressure #varies all the time#. It is also, on average, 10 points higher in the morning than in the evening. 
	
	We like #12 to 14 readings#, half in the morning and half later in the afternoon, to work out an average each time we review you. 
	
	Therefore, it is very convenient for you to own a blood pressure monitor. 
	
	It is also important to use a wider BP cuff if your arm is larger. 
	
	#Do you own a BP monitor?#"
	
	YesNo
	Category "Heart / Brain"
	
	Triggers {
		No: problem HomeBPMonitorOwnershipNeeded
		No: education optional HomeBPMonitoringChartAverage
	}
	Id 57b189f1-33ff-495b-b524-3a03b28c648a
	Changelog {
		2017-01-14: ""
	}
}


Question Constipation {
	"Opioid-induced #constipation# (OIC) is very common and is unlikely to improve over time without treatment. 
	
	A simple way to assess is to ask the patient to rate 3 things over the last week:
	
	* ease of defaecation 
	* feeling of incomplete evacuation 
	* personal judgement of constipation 
	
	Is constipation a problem?"
	
	YesNoNA
	Category "medications"
	
	Triggers {
		Yes: problem Constipation
	}
	Id 79843054-7755-47bf-a324-7b30ec9e21ba
	Changelog {
		2017-01-14: ""
	}
}

Problem Constipation {
	Goal "Relieve constipation"
	Plan "Use laxatives or switch to opioid + opioid antagonist such as Targin."
	Id 187295e8-de64-4dc4-8f17-7aade99e7f9e
	Changelog {
		2017-01-14: ""
	}
}

Problem DementiaPresentOrSuspected {
	Goal "Determine presence & severity"
	Plan "Bring back for full evaluation if new diagnosis. Address vascular risk factors and physical inactivity. If established diagnosis check for extra needs for patient and carer. National Dementia Helpline 1800 100 500. Carers Australia for personal support 1800 242 636"
	Id 61b7d1d1-e8b3-44cf-9db3-289570a53298
	Changelog {
		2017-01-14: ""
	}
}

Question FamilialHypercholesterolaemiaRisk {
	"#Familial hypercholesterolaemia# is a fairly common (~1/500) dominantly inherited condition that greatly increases the risk of premature CVD.
	
	An https://www.athero.org.au/fh/calculator/[online calculator] can assess risk, especially given risk factors (see link below).



    Is there possible Familial Hypercholesterolaemia?
    "
	
	YesNo
	Category "prevention"

    /*
    Referring "Probable FH" (not "Possible FH") suggested by
    - https://www.racgp.org.au/afp/2012/december/familial-hypercholesterolaemia/["Detecting familial hypercholesterolaemia in general practice" (AFP, 2012)]
    - https://www.athero.org.au/fh/health-professionals/fh-webinars/[Australian Atherosclerosis Society webinars]

    However the https://www.athero.org.au/fh/calculator/[calculator] recommends referring "Possible FH"...

    Also https://www.csanz.edu.au/wp-content/uploads/2017/07/Familial-Hypercholesterolaemia_ratified_-25-Nov-2016.pdf
    states "We consider that all potential cases of FH should be referred to a lipid clinic / genetic service for confirmation of diagnosis, risk assessment, initial management and cascade screening or predictive testing of first-degree relatives"
    */

    Education "Factors requiring risk assessment" {"
        * untreated LDL above 5, or on a statin
        * premature CVD (< 60 years)
        * tendon xanthoma or arcus cornealis
        * first-degree relative with one of the above


	You can use an https://www.athero.org.au/fh/calculator/[online calculator] to assess the risk.

    "}
	
	Triggers {
		Yes: problem LipidClinicReferralNeeded
        Yes: problem StatinTherapyForFamilialHypercholesterolaemiaNeeded
	}
	Id 0d7bac2f-b46f-4ea1-b7ad-55227c2484d7
	Changelog {
		2017-01-14: ""
	}
}

Problem LipidClinicReferralNeeded {
    Goal "
    Possible genetic testing to advise on management and screen family members.
    Shared care for non-low-complexity patients.
    "
    Plan "Referral to lipid clinic"
    Id a0ad4b6c-5443-4097-b9d4-3dcbca528b92
}

Problem StatinTherapyForFamilialHypercholesterolaemiaNeeded {
    Goal "
    Lower LDL cholesterol to targets:
        no evidence of CVD: < 2.5 mmol/L
        evidence of CVD: < 1.8 mmol/L
    "
	Plan "Start or intensify statin ± ezetimibe ± fibrates ± niacin.
    Arrange baseline LFT and CK.
    Repeat cholesterol level in 6 weeks.  LFTs need only be checked if clinically indicated.
    "
    Id c217d278-d771-44f0-ad63-1863d764b10c
}

Problem FootStructuralAbnormalityCareNeeded {
	Goal "Educate more on foot care."
	Plan "If intermediate or high risk feet offer a foot protection program including podiatry referral. If low risk feet provide handout on foot care."
	Id 78bcfc78-7d84-4a63-8b95-450ea80b9af6
	Changelog {
		2017-01-14: ""
	}
}


Question FootStructureCheckNeeded {
	"Examine feet for deformities: skin, such as corns or calluses, toe nail dystrophy or bony/joint deformity. 
	
	Are the #feet structurally abnormal#?"
	
	YesNo
	Category "feet"
	
	Triggers {
		Yes: problem FootStructuralAbnormalityCareNeeded
	}
	Id 862f6ed7-c9e5-4c38-b088-60ece112bdae
	Changelog {
		2017-01-14: ""
	}
}


Question FoundationMembershipNeeded {
	"Would you like to know how to join: 
	
	#The Heart Foundation# (if you have any heart or blood pressure problems), 
	
	#or Kidney Health Australia# (if you have any renal/ kidney problems), 
	
	#or The Stroke Foundation# (if you have had a stroke or TIA), 
	
	so you regularly receive more information?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem FoundationMembershipNeeded
	}
	Id 5c9cedac-e041-42d9-a663-4dbab46e3bb1
	Changelog {
		2017-01-14: ""
	}
}


Problem FoundationMembershipNeeded {
	Goal "Encourage membership."
	Plan "www.heartfoundation.org.au   www.kidney.org.au   www.strokefoundation.com.au"
	Id 5171c6be-ee84-4764-8c3e-fdc88790f8d0
	Changelog {
		2017-01-14: ""
	}
}


Education GIRiskCalculator {
	Message "GI Risk Calculator"
	File "rtf"
	Id ff2577b2-2874-4b3a-8944-3a0fa2c30d42
	Changelog {
		2017-01-14: ""
	}
}


Education HomeBPMonitoringChartAverage {
	Message "Home BP"
	File "rtf"
	Id 9deb5d09-0489-4e30-ba32-47dc50bc2ba1
	Changelog {
		2017-01-14: ""
	}
}


Problem HomeBPMonitorOwnershipNeeded {
	Goal "Ensure accurate control by the averaging of 14 home readings"
	Plan "Purchase a home BP monitor with appropriate size cuff."
	Id 0b4e7e47-0de0-412b-8701-78ba16bd369d
	Changelog {
		2017-01-14: ""
	}
}


Question IncontinenceUrinePresent {
	"Do you have a problem with urine incontinence ( #wetting yourself# )?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem IncontinenceUrinePresent
	}
	Id 40e4a7df-476d-4ffb-afc1-adbaae4f2ecc
	Changelog {
		2017-01-14: ""
	}
}


Problem IncontinenceUrinePresent {
	Goal "Improve continence"
	Plan "Assess if stress or urgency. Assess severity. Continence Foundation Australia Helpline 1800 330 066"
	Id 3fbb1213-a498-4c90-94bc-fa8e8298e96d
	Changelog {
		2017-01-14: ""
	}
}


Problem LipidLevelsNotInRangeDespiteMedication {
	Goal "Secondary prevention requires the \"4, 2, 1 rule\". Total <4.0, LDL <2.0, HDL >1.0"
	Plan "Statins remain the clear first choice. Titrate or add medications. Check dietary adherence. Repeat cholesterol level in 6 weeks. LFTs need only be checked if clinically indicated."
	Id a99ac39a-e5ec-4e97-ae83-0862ad07f7b5
	Changelog {
		2017-01-14: ""
	}
}


Question LipidLoweringAgentUseNeeded {
	"High blood cholesterol gradually #clogs up# your arteries. 
	
	Only #30%# of this cholesterol comes #from your diet# so it often not possible to control it with diet alone. #70%# is made by #your liver enzymes.# The tablets we use have to compete with these enzymes #every day#. 
	
	Do you want to discuss more with your doctor how cholesterol lowering drugs #work#? 
	
	image:LipidLoweringAgentUse.jpg[width=940]"
	
	YesNoUnsure
	Category "Heart / Brain"
	
	Triggers {
		Yes Unsure: problem LipidLoweringAgentMechanism
		
	}
	Id b01ddf6a-5f29-4cf8-b046-53a8d16520ab
	Changelog {
		2017-01-14: ""
	}
}

Problem LipidLoweringAgentMechanism {
	Goal "Explain mechanism of action to improve adherence"
    Plan "Explain that statins compete with liver enzymes every day so should be taken every day - they are like blood pressure tablets - control not cure."
    Id 7b8dc323-e41e-4688-8994-6d88aa59b5c6
    Changelog {
        2017-10-22: ""
    }
}



Question LipidResultsNotWithinRangeOnMedication {
	"*Review the pathology results for total cholesterol (TC), HDL, LDL, triglycerides.* 
	
	If they are #on lipid lowering medication#, recommended targets are: 
	
	* total TC <4.0 
	* LDL <2.0 (Note new LDL target for acute coronary syndrome (ACS) subgroup is <1.4). 
	* HDL >1.0) 
	* Triglycerides <2.0 
	
	Do you wish to alter treatment?"
	
	YesNoNA
	NAText "N/A - no medication"
	Category "Heart / Brain"
	
	Triggers {
		No: AutoAnswer {
			QualifyForLipidLoweringAgent NA
		}
		Yes: problem LipidLevelsNotInRangeDespiteMedication
	}
	Links {
		HDLCholesterol
		LDLCholesterol
		TotalCholesterol
		Triglycerides
	}
	Id cd5d8a0f-7c02-4bac-9f95-cd96d97442c2
	Changelog {
		2017-01-14: ""
	}
}


Question MentalHealthProblemPresent {
	"If the patient has a #mental health problem# would a Better Access Mental Health Assessment be useful?"
	
	YesNoNA
	NAText "N/A - no mental health problems "
	Category "psychology"
	
	Triggers {
		Yes: problem MentalHealthProblemPresent
	}
	Id e43693a7-5c82-4b01-8c84-e28e688547ed
	Changelog {
		2017-01-14: ""
	}
}


Problem MentalHealthProblemPresent {
	Goal "Improve mental health outcome"
	Plan "Bring back for a full Better Access Assessment and Plan"
	Id 956cee4d-8060-4b10-af0a-839fb8907f8f
	Changelog {
		2017-01-14: ""
	}
}


Question MobilityAidNeeded {
	"Do you #use any mobility aid#: 
	
	* walking stick? 
	* frame? 
	* wheeled walker? 
	* gopher?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem MobilityAidNeeded
	}
	Id 54102d7f-5e88-4f79-ab5d-f29c53d7f196
	Changelog {
		2017-01-14: ""
	}
}


Problem MobilityAidNeeded {
	Goal "Improve mobility."
	Plan "Check if current aid is sufficient. Arrange Disabled Parking Sticker if appropriate."
	Id 93151ab5-214b-47f3-abbd-3663c8c62605
	Changelog {
		2017-01-14: ""
	}
}


Problem NSAIDRiskPresent {
	Goal "Clarify if risk outweighs benefit"
	Plan "Explain the risk benefit decision process and involve the patient."
	Id 36610c98-a620-4962-8228-e5142d2f8fc4
	Changelog {
		2017-01-14: ""
	}
}

Question NSAIDInUse {
	"Is the patient taking any NSAIDs (prescribed or OTC) OR are you considering prescribing a NSAID?"
	
	YesNo
	Triggers {
		No: AutoAnswer {
			NSAIDRisksPresent NA
			NSAIDRiskBenefitAnalysisNeeded NA
		}
	}
	Category "medications"
	Id b9e7e065-fa1b-4f47-88dd-6a8b972ba622
	AppliesTo "PATv3"
}
Question NSAIDRiskBenefitAnalysisNeeded {
	"#Prevention# of complications requires assessment of NSAID #risk factors#.
	
	 You can use the link below for a GI risk calculator. 
	 
	 Practice Software usually includes an absolute CV risk calculator.

	[red]#If prescribing, always use NSAIDs at the lowest effective dose for the shortest period of time to avoid GIT and CVS adverse effects.#
	
	Do you want to use NSAIDs because benefits outweigh risks (CV and GIT)?"
	
	YesNoNA
	NAText "Not on NSAIDs"
	Triggers {
		No: problem DeprescribeNSAID
		Yes: problem ManageNSAIDRisks
		Blank: education optional NSAIDRiskFactors
		Blank: education optional GIRiskCalculator
		Blank: education optional NSAIDPrescribingRecommendations

	}
	Category "medications"
	Id 5a38073b-eaea-477f-9a8c-e3a69cf32df4
	AppliesTo "PATv3"
}
Education NSAIDRiskFactors {
	Message "NSAID Risk Factors"
	File "pdf"
	Id b3ce013a-3c57-4bf9-8a5d-4db945fef45f
}
Education NSAIDPrescribingRecommendations {
	Message "NSAID Prescribing Recommendations"
	File "rtf"
	Id 2996e6f0-2b47-452c-86fc-7491198c23ee
}
Problem ManageNSAIDRisks {
	Goal "Minimise risks of aspirin or NSAIDs and ensure they are outweighed by benefits"
	Plan "Review GIT and CV risk calculations. Discuss risks/benefits with patient."
	Id cd6266f1-b566-4603-9b45-af6e27e3a8a7
	Changelog {
		2017-01-14: ""
	}
}
Problem DeprescribeNSAID {
	Goal "Deprescribe NSAIDs"
	Plan "Adopt alternative pain management methods. Minimise NSAID use with a view to cease. Monitor over-the-counter NSAID use."
	Id 6b64d909-8f8a-4967-bc1f-306d97c2b3e3
	Changelog {
		2017-01-14: ""
	}
}


Question NSAIDRisksPresent {
	"#Prevention# of complications requires assessment of the NSAID #risk factors# below (RACGP Osteoarthritis Guideline) 
	
	* elderly 
	* hypertension (will elevate BP) 
	* moderate to high cardiovascular risk calculation score 
	* aspirin induced asthma 
	* CKD or CHF or liver disease 
	* oral steroids 
	* concurrent low dose aspirin use 
	* anticoagulants 
	
	Do the #NSAID risks outweigh the benefit# in this patient? 
	
	[red]#If prescribing, always use NSAIDs at the lowest effective dose for the shortest period of time to avoid GIT and CVS adverse effects.#"
	
	YesNoNA
	NAText "N/A - NSAID not needed"
	Category "medications"
	
	Triggers {
		No: AutoAnswer {
			NSAIDUseRequiredDespiteRisk No
		}
		Yes: problem NSAIDRiskPresent
	}
	AppliesTo "PATv2"
	Id 6b22bbd8-b12c-4076-b4ef-ae097a4f81c6
	Changelog {
		2017-01-14: ""
	}
}

Question OrthopaedicReferralNeeded {
	"#Extending# management may include referral for joint replacement surgery. 

	Note knee arthroscopic lavage and debridement is NOT recommended in older patients with OA unless the knee is locked. 
	
	The #two most useful points# in assessing the need for orthopaedic referral for hip or knee replacement are: 
	
	- The level of pain. 
	- The amount of interference with daily function. 
	
	Do you want to refer for surgery #or# another reason?"
	
	YesNoNA
	Category "arthritis"
	
	Triggers {
		Yes: problem OrthopaedicReferralNeeded
	}
	AppliesTo "PATv2"
	Id c9940eda-5657-43e1-ad0e-369a343f22b0
	Changelog {
		2017-01-14: ""
	}
}


Problem OrthopaedicReferralNeeded {
	Goal "Appropriate referral"
	Plan "Arrange appropriate weight bearing x-rays."
	Id fd2adb3f-323a-49f3-89b7-507e4b8bdd5f
	Changelog {
		2017-01-14: ""
	}
}


Problem OsteoporosisPreventionNeeded {
	Goal "Ensure adequate calcium + vitamin intake"
	Plan "Perform an osteoporosis risk calculation via 'qfracture' consider bone density. Institute appropriate osteoporosis treatment if detected."
	Id f8e9f098-1d0f-4fd3-aaeb-89a79322d24c
	Changelog {
		2017-01-14: ""
	}
}


Problem OsteoporosisSpecificTreatmentNeeded {
	Goal "Clarify indications"
	Plan "Choose specific agent. Perform full work up for secondary causes and include a baseline bone density if this is initial prescription. Add reminder for bone density in 2 years."
	Id 8359fb83-e7d9-44af-8fe0-120e732b2da2
	Changelog {
		2017-01-14: ""
	}
}

Problem PBSGuidelinesAllowLipidLoweringAgent {
	Goal "Aim for targets of \"4, 2.0, 1.5, and 1\" (TC <4.0, LDL <2.0, Trig <1.5 and HDL >1.0). 
Subgroup of CHD require LDL <1.8."
	Plan "Print out cholesterol lowering diet sheet. Start Statin (first line NPS). Arrange baseline LFTs and CK and baseline record of musculoskeletal symptoms.  Repeat cholesterol level in 6 weeks.  LFTs need only be checked if clinically indicated. Escalate dose if not in range in 6 weeks. Consider screen for asymptomatic atherosclerosis e.g. carotids."
	Id 28a5a65c-dcdd-4836-be06-5ebe9ed200ff
	Changelog {
		2017-01-14: ""
	}
}


Question PhysioAssessmentNeeded {
	"Do you wish to refer to a #physio#?"
	
	YesNoNA
	Category "arthritis"

	Education "Physiotherapy indications" {"
	Extending the care team may be necessary to maintain #muscle strength# and #range# of movement, and improve physical activity generally. 
	
	For #hip and knee# arthritis generic exercises and stretches are not as effective as a personalised muscle strengthening exercise programme. 
	In addition manual therapy including stretching, mobilisation and manipulation may be useful.
	
	For #back pain# a structured supervised exercise programmes of up to 8 sessions tailored to the person are effective. 
	
	For #back pain# consider a course of manual spinal therapy of up to 9 sessions.

	== References

	* https://www.racgp.org.au/your-practice/guidelines/musculoskeletal/hipandkneeosteoarthritis/[Guideline for the management of knee and hip osteoarthritis 2nd edition (2018), RACGP]

	"}
	Triggers {
		Yes: problem PhysioAssessmentNeeded
	}
	Id 3a368d78-2b20-41fb-9296-2a9deec76329
	Changelog {
		2017-01-14: ""
	}
}


Problem PhysioAssessmentNeeded {
	Goal "Design of a personalized home exercise program."
	Plan "Strongly consider referral under EPC plan."
	Id 016470aa-d8fe-4e52-99e5-ea2a6723540f
	Changelog {
		2017-01-14: ""
	}
}

Problem PodiatryCareNeeded {
	Goal "Confirm indication for podiatry"
	Plan "Refer for podiatry. Arrange EPC plan if necessary."
	Id a77f054e-d516-4608-9aef-326ba8024c57
	Changelog {
		2017-01-14: ""
	}
}

Question PodiatryIndicationsDespiteNormalFeet {
	"Even if you have normal feet #podiatry# care will be necessary for you if:
	
	- You can't #reach your toe nails# to cut them. 
	- You can't #see# well enough to cut them. 
	
	Do #either# of these apply to you?"
	
	YesNoUnsure
	Category "feet"
	
	Triggers {
		Yes: problem PodiatryCareNeeded
	}
	Id 36a17bf7-f6cd-489b-9e0d-fe77073d32ad
	Changelog {
		2017-01-14: ""
	}
}

Question PrintHealthSummary {
	"Print out Health Summary and ask patient to review while they wait"
	
	Check required
	Category "general"
	Id 61773d22-d6ff-825d-acc4-80841b42bd75
	Changelog {
		2017-01-14: ""
	}
}

Question RecordSmokingAlcohol {
	"Enter smoking & alcohol details in the clinical notes"
	
	Check required
	Category "lifestyle"
	Id 37362d14-7556-7959-b84d-39b40b9b2ac4
	Changelog {
		2017-01-14: ""
	}
}

Question ResistantBPScreenNeeded {
	"If the averaged home BP is not within limits and they are #already on 3 or more classes# of antihypertensives at appropriate dose, including a diuretic, and adherent it is considered #resistant hypertension.# 
	
		
	Do you want to order extra tests?"
	
	YesNoNA
	NAText "N/A - Controlled on less than 4 medications"
	Category "Heart / Brain"
	
	Education "Common things to check in Resistant BP" {"
	  * Medication adherence
	  * Prohypertensive medications esp. NSAIDs 
	  * Ambulatory home BP study to confirm high BP and exclude whitecoat effect (possible with both clinic and home BP readings)
      * Fluid overload with CKD and high salt intake 
	  * Sleep study for sleep apnoea 	  
	  * Renal artery duplex scan 
	  * Renin aldosterone ratio (mane and upright) but remember ACE inhibitors and Beta blockers stimulate renin release so must be ceased temporarily before test.

      == References

		* https://www.heartfoundation.org.au/for-professionals/clinical-information/hypertension[NHF Guide to management of Hypertension in Adults, 2016 ]
	"}
	Triggers {
		Yes: problem ResistantBPScreenNeeded
	}
	Id f5634f7a-481e-4cbd-a7a9-288b1f22a2c4
	Changelog {
		2017-01-14: ""
	}
}


Problem ResistantBPScreenNeeded {
	Goal "Optimum control of blood pressure"
	Plan "Check for common causes of resistance and specialist referral if these are negative."
	Id 0ec2bb79-4cbf-471e-bc4a-26fbda353931
	Changelog {
		2017-01-14: ""
	}
}


Problem SaltIntakeHigh {
	Goal "Reduce both added and packaged salt"
	Plan "Recommend DASH website (Dietary Approaches to Stop Hypertension).
    Explain low salt diet reduces BP by 4/2mmHg."
	Id 61395c8f-1e0f-4766-9d22-f22352874cc1
	Changelog {
		2017-01-14: ""
	}
}


Question SaltIntakeHigh {
	"#Salt# intake is an important #contributor# to high blood pressure. 
	
	Avoiding adding salt in cooking and at the table only causes your salt intake to #reduce by 30%#. 
	
	#70%# of salt in our diet comes in #pre-packaged food# 	So if you cook most of your food from #fresh products# you reduce your salt intake dramatically. 
	
	As well it is useful to #buy low salt# labeled food. 
	
	So do you have a High salt diet because you #mainly# have pre packaged food?"
	
	YesNoUnsure
	Category "lifestyle"
	
	Triggers {
		Yes Unsure: problem SaltIntakeHigh
	}
	Id 520a0fcc-556f-4eed-8585-eacd2cc1a9bb
	Changelog {
		2017-01-14: ""
	}
}


Question SelfHelpGroupReferralNeeded {
	"People who take appropriate responsibility for their own management tend to have better outcomes. 
	
	#Self-help groups# can help some people manage their chronic medical conditions better. 
	
	Would a referral to a local self-help group suit #you#?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem SelfHelpGroupReferralNeeded
	}
	Id 013c4e5e-8028-4b79-8719-40bfc6b2fc71
	Changelog {
		2017-01-14: ""
	}
}


Problem SelfHelpGroupReferralNeeded {
	Goal "Improve self management"
	Plan "Refer to a local group. Check national body website if unsure."
	Id 935b723a-38c8-42ae-9118-cf51c9361b70
	Changelog {
		2017-01-14: ""
	}
}

Question StatinConcern {
	"#Statins# are the most commonly used medications. 
	
	They not only #prevent# more cholesterol being #deposited#, but #reduce the size# of the plaques already there. 
	
	Do you have #any concerns# about taking cholesterol lowering medications (statins)? 
	
	image:StatinConcern.jpg[width=939]"
	
	YesNoNA
	NAText "N/A - Not on cholesterol lowering medication"
	Category "Heart / Brain"
	
	Triggers {
		Yes: problem StatinConcern
		Yes: education optional StatinConcern
	}
	Id 2127a2ca-4a30-4cc9-a74a-74561c10ec73
	Changelog {
		2017-01-14: ""
	}
}

Problem StatinConcern {
	Goal "Address concerns."
	Plan "Reports of amnesia are rare, and cause and effect has not been established. FDA states patients on statins may have a small increased risk of elevated blood sugar and of being diagnosed with diabetes."
	Id 29d2dc9f-86ca-447d-9487-61617189a8ba
	Changelog {
		2017-01-14: ""
	}
}


Education StatinConcern {
	File "jpg"
	Id 20d26187-b79a-4dcc-a765-2d3818015609
	Changelog {
		2017-01-14: ""
	}
}


Problem WeightLossDesired {
	Goal "
    Healthy eating and pleasurable regular exercise.
    Initial aim of 5kg weight loss.
    "
	Plan "
    Review PAT Weight Questionnaire results
    Recommend smaller plate size and drinking 500mL of water before meals
    Free coaching: NSW (gethealthynsw.com.au), QLD (gethealthyqld.com.au)
    Online info: healthier.qld.gov.au  livelighter.com.au  eatforhealth.gov.au
    Phone app: e.g. MyFitnessPal
    "
    // TODO: add shorter link for Vic Health https://www.vichealth.vic.gov.au/be-healthy
	Id 24d44a05-e16f-4fff-be53-27854b939452
	Changelog {
		2017-01-14: ""
	}
}


