/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with COPD */
Clinic GPMPCOPD {
	Id 63970668-dc88-4e39-ad49-14faf286d432
	Filter COPD
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			COPDPresent
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter COPD {
	Match {
		keyword "CAFL"
		keyword "Chronic airflow limitation"
		keyword "Chronic airflow obstruction"
		keyword "Chronic airway disease"
		keyword "Chronic airway obstruction"
		keyword "Chronic bronchitis"
		keyword "Chronic irreversible airway"
		keyword "Chronic obstructive"
		keyword "Chronic obstructive airways disease"
		keyword "Chronic obstructive pulmonary disease"
		keyword "COAD"
		keyword "COPD"
		keyword "Emphysema"
		keyword "Respiratory"
	}
	Tests matching only this {
		"COPD"
		"COAD"
		"CAFL"
		"Chronic obstructive"
		"Respiratory"
		"end-stage COPD"
		"Chronic obstructive pulmonary disease"
		"Chronic obstructive lung disease"
		"Chronic obstructive airway disease"
		"Chronic irreversible airway obstruction"
		"Chronic airway obstruction"
		"Chronic airway disease"
		"Chronic airflow obstruction"
		"Chronic airflow limitation"
		"chronic obstructive pulmonary disease with acute lower respiratory infection"
	}
}


Question COPDPresent {
	"The #COPD-X plan#, developed by the Australian Lung Foundation (ALF) and the Thoracic Society of Australia and New Zealand, forms the basis for management. 
	
	[blue]*C* *is to confirm* diagnosis using '#Post 70/80' rule.# 
	
	* Use the #post# bronchodilator readings: 
	* Obstruction ([blue]*O* of COPD) is confirmed by the FEV1/FVC ratio being less than #70#%. 
	* Pulmonary Disease ([blue]*PD* of COPD) is confirmed by loss of volume. That is, an FEV1 less than #80#%of predicted. 

	If COPD is confirmed, do want to add  [blue]*an annual cycle of COPD care* reminder? "
	
	YesNoNA
	NAText "N/A - no COPD"
	Category "lungs"
	
	Triggers {
		Yes: problem COPDCycleOfCare
	}
	Id d5b9c45e-8942-45be-84d7-ebc5bfb025c8
	Changelog {
		2017-01-14: ""
	}
}


