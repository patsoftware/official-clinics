/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Clinic DiabetesCycleOfCare {
	Id 269920e2-64df-4b80-8d97-2778f55613f4
	Questions {
		Patient {
			ConsentForDiabetesCycleOfCare
			ResearchConsent
			InsulinRole
			WaistSize
			GlycaemicIndex
			BGLTiming
			HbA1cTesting
			FootCare
			PodiatryIndicationsAbnormalFeet
			PodiatryIndicationsDespiteNormalFeet
            OralHealth
            InsulinUse
			InsulinConcern
			BruisingWithInjections
            InsulinInjectionTechnique
			HypoglycaemicEpisodes
			GlucagonAvailability
            DiabetesAbove5ToDrive
			DifficultyWithTabletsOrInjections
			Distressed
			DepressionPresent
			AnxietyPresent
			EnrollmentWithNDSS
			AusroadsNotification			
			ResearchConsent
		}

		Checklist {
			Consent2521
			Consent732
			ReminderDiabetesCyclePlan12monthsRequired
			ReminderDiabetesVisit3or6months
			RecordHomeBP
			RecordHeightWeightWaist
			RemindersCVPlan
			RecordDiabeticCycleCare
		}

		Staff {
			DiabetesType1Present
			HbA1CDietControlOnly
			HbA1CControlledOnMedications
			MetforminDoseAdjustmentForCKDNeeded
			MetforminEducationNeeded
			MetforminUseOptimal
            DiabetesTreatmentIntensificationNeeded
            ComorbiditiesInfluencingMedicationChoicePresent
			SulphonylureaUseOptimal
			DiabetesDPP-4InhibitorUseNeeded
			DiabetesSGLT2InhibitorUseNeeded
			DiabetesGLP-1RANeeded
			DiabetesGlitazoneUseNeeded
			DiabetesAcarboseUseNeeded
			IntensificationInsulinNeeded
			InsulinSelfTitrationNeeded
			InsulinSecondDoseNeeded
			BPControlNeeded
			MicroalbuminuriaPresent
			ACEIOrARBTherapyNeeded
			LipidResultsNotWithinRangeOnMedication
			QualifyForLipidLoweringAgent
			HaemochromatosisCheckNeeded
			ArteriesAbnormal
			NeuropathyCheckNeeded
			FootStructureCheckNeeded
			EyeCheckNeeded
			RetinopthyPresent
			GPMPReviewNeeded
			RepeatScriptNeeded
			DataEntryNeeded
			ThreeMonthlyReminderNeeded
			Diabetic-NewlyDiagnosed
			Diabetic-UncomplicatedAndControlled
			Diabetic-UncomplicatedAndNotControlled
			Diabetic-ComplicatedAndControlled
			Diabetic-ComplicatedAndNotControlled
			EPCNeeded
			PersonalisingThePlan

			MainGoals
			PatientSaysAlreadyDoing
			DoctorSaysGoingWell
			PatientImportance0to10
			PatientConfidence0to10
			ChecklistCopyToPatient
			ChecklistCopiesToTeamWithConsent
			ReviewDate
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Question OralHealth {
	"
    Gum disease around your teeth is more common as you age, especially in diabetes, and can lead to tooth loss.
    
    Do you have an annual dental check up?"
    
    YesNoNA
	NAText "Dentures"
    Category "blood sugar"
    Triggers {
		No: problem OralHealth
	}
    Id 97573f68-db1a-43e7-83df-da5e3389cb1a
    Changelog {
        2020-03-05: ""
    }
}


Problem OralHealth {
	Goal "Protect teeth"
    Plan "Encourage dental check annually for periodontal disease to prevent tooth loss"
    Id 010c5ba1-d265-4783-9e20-1af17c72c913
    Changelog {
        2020-03-05: ""
    }
}


Question ComorbiditiesInfluencingMedicationChoicePresent {
	"
    #Comorbidities# can influence medication choice especially:
    
    - Any established cardiovascular disease

    - CKD
    
    - CHF

    
    Are any #significant comorbities# present?"    
    YesNoNA
	NAText "Already considered"
    Category "blood sugar"

	Education "Comorbidites influencing medication choice" {"
		== References

		* https://diabetologia-journal.org/2018/10/05/new-easd-ada-consensus-guidelines-on-managing-hyperglycaemia-in-type-2-diabetes-launched-at-easd-meeting-new-recommendations-include-specific-drug-classes-for-some-patients-and-enhancing-medication-a/[EASD European Society for the study of Diabetes guidelines 2018]   
	"}
    Triggers {
		Yes: problem ComorbiditiesInfluencingMedicationPresent
	}
    Id bccf1fa8-9e12-404c-a7e2-95aec9362725
    Changelog {
        2019-09-21: ""
    }
}

Problem ComorbiditiesInfluencingMedicationPresent {
	Goal "Choose medications appropriate for comorbidity"
    Plan "Consider SGLT-2 inhibitors or GLP-1 agonist if established cadriovascular disease is present"
    Id 0651976c-9d44-4fd0-a330-67315605c17f
    Changelog {
        2019-09-21: ""
    }
}



Question InsulinInjectionTechnique {
	"
    Here are some #good tips# for injecting insulin:

    - Use the new #shorter# 4 to 5 mm needles

    - Always use a #new needle# each time 

    - #Change# your injection site each time using a 'Calendar' or 'Clock' pattern 

    - #Count to eight# after injecting before pulling out the needle to ensure all the insulin gets out of the needle 

    Do want more advice on using insulin?"
  
    YesNoNA
	NAText "Not using insulin"
	Category "blood sugar"
    Triggers {
		Yes: problem InsulinUse
	}
    Id c755d22f-41f5-4ec4-8232-c71cfc3dd525
    Changelog {
        2019-01-11: ""
    }
}


Question InsulinUse {
	"
    Are you using #insulin#?
    "
    YesNo
	Category "blood sugar"
    Triggers {
		No: AutoAnswer {
			DiabetesAbove5ToDrive NA
		}
		No: AutoAnswer {
			GlucagonAvailability NA
		}
		No: AutoAnswer {
			HypoglycaemicEpisodes NA
		}
		No: AutoAnswer {
			InsulinInjectionTechnique NA
		}
		No: AutoAnswer {
			BruisingWithInjections NA
		}
		No: AutoAnswer {
			InsulinConcern NA
		}
		Yes: problem InsulinUse
	}
    Id 191c6602-267f-4517-8591-39fc9f1aa507
    Changelog {
        2019-01-11: ""
    }
}

Problem InsulinUse {
	Goal "Check for optimum usage"
    Plan "Check for injection technique"
    Id 2e6449bb-a46e-4182-9d9f-b68110d3a639
    Changelog {
        2019-01-11: ""
    }
}



Question ConsentForDiabetesCycleOfCare {
	"
We would like to give you a #full diabetes check-up# with the help of this computer application, the Patient Assistance Tool (PAT), which is designed to help patients and their healthcare team create thorough care plans, securely share them and keep them up to date. To enable this PAT Pty Ltd needs your consent to securely handle and store your data in accordance with its https://www.patsoftware.com.au/privacy[Privacy Policy] - a summary of it is below.

== Collection of personal information

PAT collects personal information about you through the PAT application from information provided by you and your healthcare practitioner. If you do not provide all the information requested by the PAT application, it may compromise the value of the information and services provided to you.

Our purposes for collecting your personal information

The PAT application collects personal information to:

* customise your care plan to your medical conditions and to give your healthcare practitioners access to relevant information as they work on your care plan
* allow users to, view, comment on and share their care plan
* send users reminders regarding certain goals in their care plan
* enable data to be used by your medical practice for internal audits to ensure good care
* if you provide additional consent on the next screen, to enable data to be used for medical research that is approved by an external ethics committee
* if you provide additional consent, to enable care plans to be shared with health practitioners that you are referred to
* provide the PAT application, including troubleshooting and handling complaints
* maintain, protect and improve the PAT application, and to develop new products or services;
* provide users with information about new products and services that may be of interest to them
* for any purpose required or authorised by Australian law

== Disclosure of your personal information

Data loaded into the PAT application could be seen by authorised personnel at the medical practice using the PAT system, healthcare practitioners that your doctor may refer you to (with your additional consent), trusted medical research organisations (if you consent on the next screen), yourself & the people you authorise, third parties by court order and a limited number of authorised personnel at PAT. We may disclose the information we collect about you to our service providers, our consultants, advisors and auditors, regulatory bodies and government authorities as required or authorised by law in a way that ensures these parties will safeguard your privacy and for purposes consistent with this Privacy Policy and the Privacy Act.

We will never sell your personal information. We will not share your information with the MyHealthRecord system without your explicit consent.

== Access, correction and complaints
	
Information on how to access or correct the personal information we hold about you, and about our privacy complaint management procedures can be found in our Privacy Policy at  https://www.patsoftware.com.au/privacy
	
Do you give consent to PAT handling your personal information as set out above?
	"
	
	Consent required
	Category "admin"
	
	Id ecdcde6d-8550-4ab7-9efd-578060008c01
	Changelog {
		2017-01-14: ""
	}
}


Question InsulinRole {
	"Insulin attaches to #insulin receptors# on cells in the body and this #causes glucose uptake# into the cells for energy. 
	
	- In type 2 diabetes there is #insulin resistance.# This means a higher level of insulin is required for uptake of glucose into cells. 
	- In type 2 diabetes there is also a gradual #loss of insulin production# in the pancreas. 
	
	Both of these cause high blood glucose levels. 
	
	Do you understand the #role# of insulin?"
	
	YesNoUnsure
	Category "blood sugar"
	
	Triggers {
		No: problem InsulinRoleNotUnderstood
		Yes: education optional InsulinActionJpg
	}
	Id d6045f71-dfd1-478c-b706-90a9bd8b94b4
	Changelog {
		2017-01-14: ""
	}
}


Question WaistSize {
	"Your waist is measured to determine how much #fat# is #inside your abdomen.# 
	
	Fat breakdown products #damage# the insulin producing cells in your pancreas, as well as causing #insulin resistance# in many cells in your body. 
	
	Do you understand how this fat makes your diabetes worse? 
	
	image:WaistSize.png[width=684]"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		No: problem WaistSizeLinkToAmountOfVisceralFatIsNotUnderstood
	}
	Id 422be4b8-b8f2-4326-8bae-e24ad5a7a65d
	Changelog {
		2017-01-14: ""
	}
}


Question GlycaemicIndex {
	"The #Glycaemic Index# (GI) approach to your diabetes enables you to choose #carbohydrate# containing foods based on how rapidly they release glucose inside your body: 
	
	- rapid release = #high GI# (this can be too quick for your insulin to manage). 
	- slower release = #medium or low GI# (more manageable for your insulin production). 
	
	[.big]
	Do you need more education on how to use #GI# to choose food?"
	
	YesNoUnsure
	Category "blood sugar"
	
	Triggers {
		Yes Unsure: problem GlycaemicIndexEducationNeeded
		Yes Unsure: education optional CarbohydrateExamples
	}
	Id 869d4746-aaf7-47e1-bf1f-f061286a64a7
	Changelog {
		2017-01-14: ""
	}
}


Question BGLTiming {
	"#BGL# (Blood glucose level) is now not routinely used if only on oral medications unless at risk of hypoglycaemia.
	 
	* With #insulin#, BGL is usually tested before each meal and before bed. 
	
	Do you need more information on when to test?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		Yes: problem BGLNotBeingTested
	}
	Id dcd5a109-edee-4fce-bd84-460550ef03b4
	Changelog {
		2017-01-14: ""
	}
}


Question HbA1cTesting {
	"
	[.big]
	#HbA1c# is the #main blood test# to monitor diabetes. 
	
	Red blood cells live for 120 days. 
	
	Glucose molecules bind with the red blood cell haemoglobin to form glycated haemoglobin (HbA1c). 
	
	So the higher the average blood glucose level the higher the HbA1c. 
	
	In other words, it gives us a very good idea of what the #average blood glucose level over 4 months# (120 days) has been. This is why we test it every 3 to 6 months. 
	
	[.big]
	Do you need more education on why we test the HbA1c regularly?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		Yes: problem HbA1cTestingNotUnderstood
	}
	Id 03dc1c9e-7bc8-4d82-ab80-d86e0d766990
	Changelog {
		2017-01-14: ""
	}
}


Question FootCare {
	"#Prevention# of foot problems requires a #simple daily routine:# 
	
	Washing: 
	
	* Check for any skin breaks on your feet every day especially between toes. 
	* Dry thoroughly especially between toes. 
	* If there are heel cracks see your GP. 
	* Avoid very hot water when washing. 
	
	Cutting nails 
	
	* Cut toenails regularly before they get too long. 
	* Cut nails to follow the curve of the toe without cutting into the corners. 
	
	Footwear 
	
	* Do not walk barefoot especially out of doors. 
	* Make sure shoes fit well and do not rub. 
	* Wear socks and change socks every day. 
	
	[.big]
	Do you #need more education# on foot care?"
	
	YesNo
	Category "blood sugar", "feet"
	
	Triggers {
		Yes: problem FootStructuralAbnormalityCareNeeded
	}
	Id d1977266-9954-40d7-a491-0d38538f6dfd
	Changelog {
		2017-01-14: ""
	}
}


Question PodiatryIndicationsAbnormalFeet {
	"Many diabetics can look after their own feet. However, #podiatry# care will be necessary for you if: 
	
	- The #nerves# to your feet are affected (especially touch sensation). 
	- The #arteries# to your feet are affected (poor circulation). 
	- You have #structural problems# (corns, calluses, pressure areas or abnormally thick or deformed toe nails). 
	
	Do #any# of these apply to you?"
	
	YesNoNA
    NAText "Already seeing a podiatrist"
	Category "blood sugar", "feet"
	
	Triggers {
		Yes: AutoAnswer {
			PodiatryIndicationsDespiteNormalFeet No
		}
		Yes Unsure: problem PodiatryCareNeeded
	}
	Id b8a00096-eeb2-4db5-a6d2-993d25a1a99b
	Changelog {
		2017-01-14: ""
	}
}

Question InsulinConcern {
	"The idea of using insulin injections can cause some patients concern #.# 
	
	If your doctor has #said you may need insulin injections soon,# or if you have recently started insulin, this question applies to you. 
	
	*Otherwise choose 'Not applicable'.* 
	
	Do you feel #comfortable with the idea# of using insulin?"
	
	YesNoNA
	NAText "Not using insulin"
	Category "blood sugar", "medications"
	
	Triggers {

		No: problem InsulinConcernPresent
		No: education required InsulinConcernsEducation
	}
	Id 937eced5-1b25-4410-8b01-9f0320633a6e
	Changelog {
		2017-01-14: ""
	}
}


Question BruisingWithInjections {
	"If you are using insulin, are there any 
	
	* #bruises# 
	* or #lumps# at your #injection sites#?"
	
	YesNoNA
	NAText "Not using insulin"
	Category "blood sugar", "medications"
	
	Triggers {
		Yes: problem BruisingWithInjectionsPresent
	}
	Id b3edf872-8b37-4090-adfb-379871a482ee
	Changelog {
		2017-01-14: ""
	}
}


Question HypoglycaemicEpisodes {
	"If you are on insulin have you had any hypoglycaemic episodes (#lows#) in the last 12 months?"
	
	YesNoNA
	NAText "Not using insulin"
	Category "blood sugar", "medications"
	
	Triggers {
		Yes: problem HypoglycaemicEpisodesPresent
	}
	Id 8f4a6bab-458d-426c-b5b6-4dffaf12d23d
	Changelog {
		2017-01-14: ""
	}
}


Question GlucagonAvailability {
	"If you are on insulin have you got: 
	
	* An #in date# glucagon hypokit at home #and# 
	* does someone know how to give it to you?"
	
	YesNoNA
	NAText "Not using insulin"
	Category "blood sugar", "medications"
	
	Triggers {
		No: problem GlucagonHypokitNotAvailable
	}
	Id a3a18a8c-42d9-4733-a603-1ac59e5b9ee9
	Changelog {
		2017-01-14: ""
	}
}


Question DifficultyWithTabletsOrInjections {
	"One of the best ways of managing your diabetes and other risk factors is to take your medications #regularly#. 
	
	We are very interested in helping you in this area. 
	
	Is there #anything# about taking your tablets, or insulin devices if you are on them, that you have #difficulty# with?"
	
	YesNo
	Category "blood sugar", "medications"
	
	Triggers {
		Yes: problem DifficultyWithInjectionsOrTablets
	}
	Id a9a40903-3d29-49d8-9991-d63d1ef420bf
	Changelog {
		2017-01-14: ""
	}
}


Question Distressed {
	"Sometimes people with diabetes are '#distressed#' rather than 'depressed'. 
	
	- Do you feel angry when you think about living with diabetes? 
	
	#Or# 
	
	- Do you feel burned out by the constant effort to manage your diabetes?"
	
	YesNo
	Category "blood sugar", "psychology"
	
	Triggers {
		Yes: problem DistressPresent
	}
	Id da01a386-581d-4d14-b1ff-aac081802f77
	References {
		"From PAID (Problem Areas in Diabetes Questionnaire)"
	}
	Changelog {
		2017-01-14: ""
	}
}


Question EnrollmentWithNDSS {
	"Are you #enrolled# with both: 
	
	- National Diabetes Services Scheme (NDSS) 
	
	#and# 
	
	- Diabetes Australia?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		No: problem NDSSEnrolmentNeeded
	}
	Id 15c3a729-5938-467c-be22-150fea987f96
	Changelog {
		2017-01-14: ""
	}
}


Question AusroadsNotification {
	"National Ausroads Guidelines, for *a private car license,* require you to #notify# your state road traffic authority if you have #diabetes and# any #one# of the following: 
	
	- you are on insulin. 
	- have had a 'hypo'(hypoglycaemia) from your diabetic tablets. 
	- have diabetes complications such as loss of sensation in your feet or vision problems. 
	
	Note there are #more requirements for private licence in some states, and for commercial drivers#) 
	
	Have you done this?"
	
	YesNoUnsure
	Category "blood sugar"
	
	Triggers {
		No Unsure: problem AusroadsNotificationNeeded
	}
	Id d6a4aae1-24ed-4a51-afc9-96a9eff9d829
	Changelog {
		2017-01-14: ""
	}
}


Question DiabetesAbove5ToDrive {
	"If you are #on insulin# the '#Above 5 to Drive'# rule apply to you: 
	
	* take your blood glucose meter with you when you drive. 
	* check your BGL is above 5mmol/l before you start and every 2 hours while driving. 
	* carry fast acting carbohydrate with you (e.g. jelly beans). 
	* if you feel a hypo developing stop driving as soon as it is safe to do so. 
	
	Do you need more information on this rule?"
	
	YesNoNA
	NAText "Not using insulin"
	Category "blood sugar"
	
	Triggers {
		Yes: problem Above5ToDriveRuleNeeded
	}
	Id 989c0dc3-84d4-4328-8e47-e5e0961409c5
	Changelog {
		2017-01-14: ""
	}
}


Question Consent2521 {
	"Record consent to proceed with  diabetic cycle of care (level C=2521)"
	
	Check required
	Category "admin"
	Id 32bac326-4d20-6852-9dfc-18e6b1c1e5ca
	Changelog {
		2017-01-14: ""
	}
}


Question Consent732 {
	"Record consent to proceed with Review of GP management plan (732) as well"
	
	Check optional
	Category "admin"
	Id e860a3ae-9740-005e-8985-a201462782f0
	Changelog {
		2017-01-14: ""
	}
}


Question ReminderDiabetesCyclePlan12monthsRequired {
	"All patients: enter reminder for \"Diabetes Cycle Plan\" for 12 months"
	
	Check required
	Category "admin"
	Id 0c8faf8b-5645-aa5f-9f68-4da4745f2222
	Changelog {
		2017-01-14: ""
	}
}


Question ReminderDiabetesVisit3or6months {
	"All patients: enter reminder for 3 or 6 months \"Diabetes Control\" visit"
	
	Check required
	Category "admin"
	Id 931044a3-afca-c75c-9644-3b88f5259db4
	Changelog {
		2017-01-14: ""
	}
}


Question RecordHomeBP {
	"Record the home BP average systolic and diastolic as today's sitting BP in the clinical notes."
	
	Check optional
	Category "admin"
	Id 7e9def8e-a575-5555-b9af-5cda800ab906
	Changelog {
		2017-01-14: ""
	}
}


Question RemindersCVPlan {
	"STRONGLY recommend PAT Cardiovascular Management Plan in 6/12 & add reminder"
	
	Check required
	Category "admin"
	Id f75a004e-2acf-d351-bd28-19f677513825
	Changelog {
		2017-01-14: ""
	}
}


Question RecordDiabeticCycleCare {
	"Record all available data in diabetic cycle of care but leave it to the doctor to mark as complete"
	
	Check required
	Category "admin"
	Id 17926b67-d818-b45d-b8ea-9d3972647f4c
	Changelog {
		2017-01-14: ""
	}
}


Question DiabetesType1Present {
	"Does the patient have #Type 1# diabetes mellitus (Diabetes type 1)?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		Yes: AutoAnswer {
			MetforminDoseAdjustmentForCKDNeeded NA
			MetforminUseOptimal NA
			SulphonylureaUseOptimal NA
			DiabetesDPP-4InhibitorUseNeeded NA
			DiabetesAcarboseUseNeeded NA
			IntensificationInsulinNeeded NA
			HbA1CDietControlOnly NA
			HbA1CControlledOnMedications NA
			DiabetesGLP-1RANeeded NA
			DiabetesTreatmentIntensificationNeeded NA
			DiabetesGlitazoneUseNeeded NA
			MetforminEducationNeeded NA
		}
		Yes: problem InsulinNeeded
	}
	Id 936e1584-484b-4301-b13f-bf2ca611c0cf
	Changelog {
		2017-01-14: ""
	}
}


Question HbA1CDietControlOnly {
	"Microvascular complications are minimized with an HbA1c less than 7.0 in the old units, or less than 53 in the new units (IFCC). 
	
	This is good control. 
	
	Is the patient in #control good# on #diet only# management?"
	
	YesNoNA
	NAText "N/A - on medication"
	Category "blood sugar"
	
	Triggers {
		Yes: AutoAnswer {
			HbA1CControlledOnMedications NA
			MetforminDoseAdjustmentForCKDNeeded NA
			MetforminUseOptimal NA
			SulphonylureaUseOptimal NA
			DiabetesDPP-4InhibitorUseNeeded NA
			DiabetesAcarboseUseNeeded NA
			IntensificationInsulinNeeded NA
			InsulinSelfTitrationNeeded NA
			InsulinSecondDoseNeeded NA
			DiabetesGLP-1RANeeded NA
			DiabetesTreatmentIntensificationNeeded NA
			DiabetesGlitazoneUseNeeded NA
			DiabetesSGLT2InhibitorUseNeeded NA
			MetforminEducationNeeded NA
		}
		No: problem DiabetesNotControlled
	}
	Links {
		HbA1C
	}
	Id 5c1f75f7-c83f-49b1-b157-ef435f767393
	Changelog {
		2017-01-14: ""
	}
}


Question HbA1CControlledOnMedications {
	"Microvascular complications are minimized with good control of HbA1c.
    
    This will commonly be less than 7.0 in the old units, or less than 53 in the new units (IFCC). 
	
		
	If the patient is a #Type 2  on medications +/- insulin#, are they in #good control#?"
	
	YesNoNA
	Category "blood sugar"
	
	Triggers {
		Yes: AutoAnswer {
			MetforminUseOptimal NA
			SulphonylureaUseOptimal NA
			DiabetesDPP-4InhibitorUseNeeded NA
			DiabetesAcarboseUseNeeded NA
			IntensificationInsulinNeeded NA
			InsulinSelfTitrationNeeded NA
			InsulinSecondDoseNeeded NA
			DiabetesGLP-1RANeeded NA
			DiabetesTreatmentIntensificationNeeded NA
			DiabetesGlitazoneUseNeeded NA
			DiabetesSGLT2InhibitorUseNeeded NA
		}
		No: problem DiabetesNotControlled
	}
	Links {
		HbA1C
	}
	Id edac8644-7c81-4de2-ac72-542839e3d1d8
	Changelog {
		2017-01-14: ""
	}
}


Question MetforminDoseAdjustmentForCKDNeeded {
	"When using metformin: 
	
	- if eGFR between 30 and 50 reduce to 1000mg. 
	- if eGFR below 30 only use with specialist review. 
	
	If they are on #metformin# is the #dose appropriate for eGFR#?"
	
	YesNoNA
	NAText "N/A - intolerant of metformin"
	Category "blood sugar", "medications"
	
	Triggers {
		NA: AutoAnswer {
			MetforminUseOptimal NA
		}
		No: problem MetforminDoseAlterationForCKDNeeded
	}
	Links {
		EGFR
	}
	Id 46ebe6b7-5de2-4e83-8de4-7143c6ea73cf
	Changelog {
		2017-01-14: ""
	}
}


Question MetforminEducationNeeded {
	"When using metformin, patients should be educated: 
	
	- to suspend use during acute conditions with potential to alter renal function such as dehydration, severe infections or administration of IV contrast media for scans. 
	- get Vitamin B12 level checked yearly, as it causes malabsortion of B12 in almost 10% after 4 years of use.
	
	Do you wish to inform them of these #precautions#?"
	
	YesNoNA
	NAText "N/A - not on metformin"
	Category "blood sugar", "medications"

	
	Triggers {
		Yes: problem MetforminEducationNeeded
	}
	Id a6233c39-a8bf-4c13-9c7a-ad152c191e17
	References {
		"Australian Prescriber 37:1: 2014"
	}
	Changelog {
		2017-01-14: ""
	}
}


Question MetforminUseOptimal {
	"Diabetes Australia recommends metformin as first choice in #Type 2# diabetes #with# 
	
	* HbA1c greater than 7.0 (old units) or 53 (new units) #and# 
	* if there are no medical contraindications to intensifying treatment, 
		 
	
	Remember maximum dose is 1000mg between eGFR 30 and 50. 
	
	[.big]
	Are they on #optimum dose# of #metformin#?"
	
	YesNoNA
	Category "blood sugar", "medications"

	Education "Metformin mode of action" {"

		* Metformin reduces hepatic glucose output so helps to control morning fasting BSL. 
		
		* It reduces insulin resistance 
		
		* It helps to reduce weight. 
	
	    * Maximum dose is 3000mg per day but most of the effect is achieved at a dose of 2000mg per day and GIT side effects are more common over 2000mg.
		"}
	Triggers {
		No: AutoAnswer {
			SulphonylureaUseOptimal NA
			DiabetesDPP-4InhibitorUseNeeded NA
			DiabetesAcarboseUseNeeded NA
			IntensificationInsulinNeeded NA
			InsulinSelfTitrationNeeded NA
			InsulinSecondDoseNeeded NA
			DiabetesGLP-1RANeeded NA
			DiabetesTreatmentIntensificationNeeded NA
			DiabetesGlitazoneUseNeeded NA
			DiabetesSGLT2InhibitorUseNeeded NA
		}
		No: problem MetforminDoseSuboptimal
	}
	Id a72ecc4c-8f60-4dcc-9da8-cb7e2b585653
	Changelog {
		2017-01-14: ""
	}
}


Question SulphonylureaUseOptimal {
	"Diabetes Australia lists Sulphonylureas (SU) as #one# of the second line options in addition to metformin in #Type 2# diabetes #with# 
	
	* HbA1c greater than 7.0 (old units) or 53 (new units) #and# 
	* if there are no medical contraindications to intensifying treatment 
		
	Do you want to use a #Sulphonylurea#?"
	
	YesNoNA
	Category "blood sugar", "medications"

	Education "SU mode of action" {"

      * Sulphonylureas increase insulin secretion, including that in response to food intake and this helps to reduce post prandial BGLs. 
	
	  * Short acting Sulphonylureas (glicazide, glipizide) are preferred if renal impairment is present. 

	  * All sulphonylureas can cause hypoglycaemia and special care is needed in the elderly.

		* http://t2d.diabetessociety.com.au/plan/
		"}

	Triggers {
		Yes: AutoAnswer {
			DiabetesDPP-4InhibitorUseNeeded NA
			DiabetesAcarboseUseNeeded NA
			IntensificationInsulinNeeded NA
			InsulinSelfTitrationNeeded NA
			InsulinSecondDoseNeeded NA
			DiabetesGLP-1RANeeded NA
			DiabetesGlitazoneUseNeeded NA
			DiabetesSGLT2InhibitorUseNeeded NA
		}
		Yes: problem SulphonylureaUseSuboptimal
	}
	Id 56779fd4-993b-4e9e-8f0f-dae9abbc02ae
	Changelog {
		2017-01-14: ""
	}
}


Question DiabetesTreatmentIntensificationNeeded {
	"#Intensification of therapy# for type 2 diabetes is required if: 
	
	* HbA1c greater than 7.0 (old units) or 53 (new units) 
	* And on maximal dose metformin 
	
	And no medical contraindication to intensifying treatment (if life expectancy < 5 years one can accept up to 8.0) 
	
		
	Do you want to #intensify# treatment?"
	
	YesNoNA
	NAText "N/A - controlled "
	Category "blood sugar"
	Education "Australian Blood Glucose Treatment Algorithm for type 2 diabetes.ADS" {"
		*  http://t2d.diabetessociety.com.au/plan/
		"}

	Triggers {
		No: AutoAnswer {
			DiabetesDPP-4InhibitorUseNeeded NA
			DiabetesSGLT2InhibitorUseNeeded NA
			DiabetesGLP-1RANeeded NA
			DiabetesAcarboseUseNeeded NA
			DiabetesGlitazoneUseNeeded NA
			IntensificationInsulinNeeded NA
		}
		Yes: problem DiabetesNotControlled
		Yes: education optional DiabetesSocietyAlgorithm
	}
	Links {
		HbA1C
	}
	Id c9023c2f-2769-49e9-a283-38a49f19f2a7
	Changelog {
		2017-01-14: ""
	}
}

Education DiabetesSocietyAlgorithm {
    Link "http://t2d.diabetessociety.com.au/plan/"
    Id 8a8eb55f-ba94-498c-a021-eea54e8bcc8d
    Changelog {
        2017-11-05: ""
    }
}



Question DiabetesDPP-4InhibitorUseNeeded {
	"An oral #DPP-4- inhibitor# may be used either: 
	
	- as #dual therapy# with metformin or sulphonylurea (SU). 
	- as #triple oral therapy# in combination with metformin + SU or metformin + SGLT2 if HbA1C >7.0. 
	- as #add on to insulin therapy# (Linagliptin and Sitagliptin)
	
	 Use of these agents as solo agent is *not allowed under the PBS.* 
	
	Do you want to use a #DPP-4 inhibitor#?"
	
	YesNoNA
	Category "blood sugar"

	Education "Gliptin mode of action" {"
		Gliptins all slow the breakdown of endogenous glucagon like peptide (GLP-1). 
	
	   GLP-1 enhances insulin production, inhibits glucagon secretion and GIP delays gastric emptying and improves satiety.

	   * http://t2d.diabetessociety.com.au/plan/
	   
		"}
	
	Triggers {
		Yes: AutoAnswer {
			DiabetesAcarboseUseNeeded NA
			IntensificationInsulinNeeded NA
			InsulinSelfTitrationNeeded NA
			InsulinSecondDoseNeeded NA
			DiabetesGLP-1RANeeded NA
			DiabetesGlitazoneUseNeeded NA
		}
		Yes: problem DiabetesDPP-4InhibitorNeeded
	}
	Id 92ed0c95-a0b4-408a-9f23-26b41c9af5e9
	Changelog {
		2017-01-14: ""
	}
}


Question DiabetesSGLT2InhibitorUseNeeded {
	" EASD 2018 guidelines recommend use of #SGLT-2 inbibitors# if Cardiovascular Disease, CHF or CKD ( depending on eGFR) are present.

      They may be used either: 
	
	- as #dual therapy# with metformin or sulphonylurea (SU). 
	- as #triple therapy# in combination with either metformin + SU or metformin + DPP4 if HbA1C >7.0 . 
	- as #add-on with insulin#. 
	
	Use of these agents as solo agent is *not allowed under the PBS.* 
	
	
	
	Do you want to use a #SGLT2 inhibitor#?"
	
	YesNoNA
	Category "blood sugar", "medications"

	Education "Important indications and precautions with SGLT2 use" {"
		* #Diabetic ketoacidosis (DKA)# has been reported with us of SGLT2 inhibitors. Inform patients of the signs and symptoms of DKA and to stop during serious illnesses, especially causing dehydration, and #routinely# before surgery.
	    * SGLT2 are contra-indicated #in marked renal impairment# (eGFR<45).
		* Acute kidney injury has been reported during serious illness. 
	    * Exercise care and check for volume depletion in people when commencing in patients already on diuretics. 
        * EASD 2018 guidelines recommend use of SGLT-2 inbibitors if ASCVD, CHF or CKD are present (if eGFR is appropriate for that glifozin).
		* 2018 NHF CHF guidelines recommend adding SGLT2 in #type 2 patients associated with cardiovascular(ASCVD) disease# and insufficient control despite metformin to decrease risk of CV events and risk of HF hospitalisation.
       * http://t2d.diabetessociety.com.au/plan/

        * https://diabetologia-journal.org/2018/10/05/new-easd-ada-consensus-guidelines-on-managing-hyperglycaemia-in-type-2-diabetes-launched-at-easd-meeting-new-recommendations-include-specific-drug-classes-for-some-patients-and-enhancing-medication-a/[EASD European Society for the study of Diabetes guidelines 2018]             
		"}
	
	Triggers {
		Yes: AutoAnswer {
			DiabetesGLP-1RANeeded NA
			DiabetesAcarboseUseNeeded NA
			DiabetesGlitazoneUseNeeded NA
			IntensificationInsulinNeeded NA
			InsulinSelfTitrationNeeded NA
			InsulinSecondDoseNeeded NA
		}
		Yes: problem DiabetesSGLT2InhibitorNeeded
		Yes: education optional SGLT2
	}
	Id 1b229ee1-16aa-471d-97fe-a90b2d629411
	Changelog {
		2017-01-14: ""
	}
}


Question DiabetesGLP-1RANeeded {
	"An injectible #GLP-1 receptor agonist# may be used either: 
	
	- as #dual therapy# with metformin or sulphonylurea (SU). 
	- as #triple therapy# in combination with metformin and sulphonylurea (SU) if HbA1C >7.0 . 
	- as #triple therapy# in combination with metformin and insulin. 
	
	Use as solo agent, or dual therapy with glitazone or DPP-4 inhibitor or SGLT2 inhibitor is *not allowed under the PBS.* 
		
	Do you want to use a  #GLP-1 RA# injection?"
	
	YesNoNA
	Category "blood sugar", "medications"

	Education "GLP-1 receptor agonist mode of action" {"
		
		It stimulates insulin secretion in a glucose dependent manner, so switches off when not needed so potentially minimising hypo risk.

	   GLP-1 enhances insulin production, inhibits glucagon secretion and GIP delays gastric emptying and improves satiety.

       GLP-1 agonists with proven CV benefit should be considered for use in with atherosclerotic cardiovascular disease.

       Available as daily (Byetta) or weekly injections (Trulicity, Bydureon).

	   * http://t2d.diabetessociety.com.au/plan/
	   
		"}
	

	Triggers {
		Yes: AutoAnswer {
			DiabetesAcarboseUseNeeded NA
			IntensificationInsulinNeeded NA
			InsulinSelfTitrationNeeded NA
			InsulinSecondDoseNeeded NA
			DiabetesGlitazoneUseNeeded NA
		}
		Yes: problem DiabetesGLP-1AnalogueNeeded
	}
	Id 56f5f1b2-55a8-4759-ac66-999793cae163
	Changelog {
		2017-01-14: ""
	}
}


Question DiabetesGlitazoneUseNeeded {
	"#Pioglitazone# may be used either: 
	
	- as #dual therapy with# metformin or sulphonylurea (SU). 
	- as #dual therapy# in combination with insulin. 
	- as #triple therapy# in combination with metformin and sulphonylurea (SU) if HbA1C >7.0 
	
	Use as solo agent is *not allowed under the PBS.* 
	
	Due to safety concerns glitazone use is generally limited to patients with severe insulin resistance or unable to tolerate other medications.(NPS Radar). 
	
	Do you want to use #pioglitazone#?
    "
	
	YesNoNA
	NAText "N/A - already on triple therapy"
	Category "blood sugar", "medications"

	Education "Glitazone precautions" {"

    With glitazone triple therapy care is necessary in:

    * CHF class 2-4 (increased CHF risk)
    * Liver dysfunction
    * Reduced bone density (fracture risk increase)
    * History bladder cancer (possible increased risk)
    * Morbid obesity (weight gain)
	"}
	
	Triggers {
		Yes: AutoAnswer {
			IntensificationInsulinNeeded NA
			InsulinSelfTitrationNeeded NA
			DiabetesAcarboseUseNeeded NA
		}
		Yes: problem DiabetesPioglitazoneNeeded
	}
	Id 8f3bd53d-8f67-40c6-a81c-260b9d69d287
	Changelog {
		2017-01-14: ""
	}
}


Question DiabetesAcarboseUseNeeded {
	"#Acarbose# has an #unrestricted# PBS listing but is usually used either: 
	
	- as #dual therapy with# metformin or sulphonylurea (SU). 
	- as #triple therapy# in combination with metformin and sulphonylurea (SU) if HbA1C >7.0 . 
	* It inhibits the digestion of carbohydrate and thus slows the rate of glucose release; useful if high post prandial BSLs. 
	* Flatulence & abdominal discomfort are side effects. 
	
	Do you want to use # acarbose#? 
	
	(Mark as #NA# if already on triple therapy) ."
	
	YesNoNA
	NAText "N/A - already on triple therapy"
	Category "blood sugar", "medications"
	
	Triggers {
		Yes: AutoAnswer {
			InsulinSelfTitrationNeeded NA
			IntensificationInsulinNeeded NA
			InsulinSecondDoseNeeded NA
		}
		Yes: problem DiabetesNotControlled
	}
	Id 9405420c-0a44-4c21-bdb0-e73ef5aa7c70
	Changelog {
		2017-01-14: ""
	}
}


Question IntensificationInsulinNeeded {
	"Intensification option: 
	
	Adding Insulin therapy to oral hypoglycaemic agents may be an option: 
	
	* if HbA1c #greater than 7.0# (old units) or #53# (new units) for more than 3 months #consider# 
	* if HbA1c #greater than 8.0# (old units) or #64# (new units) for more than 3 months #strongly consider# 
	
	Do you want to start #insulin#?
    "
	
	YesNoNA
	Category "blood sugar", "medications"

	Education "Flowchart to starting and adjusting basal insulin" {"
				
       * https://www.racgp.org.au/FSDEDEV/media/documents/Clinical%20Resources/Guidelines/Diabetes/Appendix-H.pdf[Examples for insulin initiation and titration, General practice management of type 2 diabetes, RACGP, 2016–2018]
	   
		"}
	
	Triggers {
		No: AutoAnswer {
			InsulinSelfTitrationNeeded NA
			InsulinSecondDoseNeeded NA
		}
		Yes: problem InsulinNeeded
		Yes: education optional InsulinTherapyFlexible
	}
	Id e919af37-8fa5-496b-8870-7df079ca3c0d
	Changelog {
		2017-01-14: ""
	}
}


Question InsulinSelfTitrationNeeded {
	"Does the patient understand the simple #Type 2# diabetes patient #insulin titration rules#?"
	
	YesNoNA
	Category "blood sugar", "medications"
	Education "Guide to insulin self titration" {"
	   * self adjust the dose twice weekly (unless on Insulin Degludec/Insulin Aspart (IDegAsp) 70/30, which is adjusted once weekly). 

	   * until the target BGL is less than 6 mmol/L based on the previous 3 consecutive day averaged readings or 7 days if on IDegAsp	

       * https://www.racgp.org.au/FSDEDEV/media/documents/Clinical%20Resources/Guidelines/Diabetes/Appendix-H.pdf[Examples for insulin initiation and titration, General practice management of type 2 diabetes, RACGP, 2016–2018]
	   
		"}
	Triggers {
		No: problem InsulinSelfTitrationNotUnderstood
		Yes: education optional StartingInsulin2018
	}
	Id 6e32a916-0435-4e7b-9293-582ae54532d2
	Changelog {
		2017-01-14: ""
	}
}

Education StartingInsulin2018 {
    File "rtf"
    Id b21b1ce3-fe3b-41cc-b37f-784df863e1e7
    Changelog {
        2018-08-21: ""
    }
}



Question InsulinSecondDoseNeeded {
	"In #Type 2 diabetics on insulin# once the fasting blood glucose (FBG) is fixed, check the evening pre and post prandial BGL. 
	
	* If fasting and evening blood glucose levels are similar, the evening insulin dose is probably ok. 
	* If evening blood glucose level is higher than fasting blood glucose level, it may be time to add a second insulin dose. 
	
	Do you want to add a #second insulin dose#?"
	
	YesNoNA
	Category "blood sugar", "medications"

	Education "Insulin intensification flowchart" {"
	   
       * https://www.racgp.org.au/FSDEDEV/media/documents/Clinical%20Resources/Guidelines/Diabetes/Appendix-H.pdf[Examples for insulin initiation and titration, General practice management of type 2 diabetes, RACGP, 2016–2018]
	   
		"}
	Triggers {
		Yes: problem InsulinNeeded
		
	}
	Id b25740c4-da18-4b80-97ec-6adcfb180ac9
	Changelog {
		2017-01-14: ""
	}
}


Question BPControlNeeded {
	"*The averaged HOME BP in the new Diabetes Australia guidelines * 
	
	Below #135/85# (equivalent to 140/90 office BP) for uncomplicated diabetes. 
	
	Below #125/75# (equivalent to 130/80 office BP) if #complicated# diabetes (e.g. microalbuminuria).
    
    Do you want to change BP medication?"
	
	YesNo
	Category "blood sugar", "Heart / Brain"
	
	Triggers {
		Yes: problem BPOutsideRange
	}
	Id 78f08e4a-9288-4361-b06f-bb6baf29f4a7
	Changelog {
		2017-01-14: ""
	}
}


Question MicroalbuminuriaPresent {
	"Is #microalbumin# checked by SPOT urine ACR in #abnormal range#?"
	
	YesNo
	Category "blood sugar", "kidneys"
	
	Triggers {
		No: AutoAnswer {
			ACEIOrARBTherapyNeeded NA
		}
		Yes: problem MicroalbuminuriaPresent
	}
	Links {
		ACR
		EGFR
	}
	Id fcfebaf4-311a-41f8-8503-24acf8f04c7c
	Changelog {
		2017-01-14: ""
	}
}

Question ACEIOrARBTherapyNeeded {
	"If #microalbumin# is persistently present, are they on #maximum tolerated dose of ACEI or ARB# to protect the kidneys?
	
	(ACEI or ARB are used #even if normotensive#)"
	
	YesNoNA
	Category "blood sugar", "kidneys", "medications"
	
	Triggers {
		No: problem CKD-SoACEIOrARBNeeded
	}
	Links {
		ACR
		EGFR
	}
	Id 778a4c7c-a776-461c-a251-4d040f2a4deb
	Changelog {
		2017-01-14: ""
	}
}


Question QualifyForLipidLoweringAgent {
	"Diabetics qualify for lipid lowering agents at [red]#any level# of cholesterol if any #one# of the following applies:
	
	- over 60 years 
	- ATSI ethnicity 
	- microalbuminuria 
	- any associated clinical conditions (ACC) 
	
	Other diabetics qualify if #total cholesterol greater than 5.5 mmol/l.# 
	
	If not already on lipid lowering agents should they be according to these PBS 2006 guidelines?"
	
	YesNoNA
	Category "blood sugar", "Heart / Brain"
	
	Triggers {
		Yes: problem PBSGuidelinesAllowLipidLoweringAgent
	}
	Links {
		HDLCholesterol
		LDLCholesterol
		TotalCholesterol
		Triglycerides
	}
	Id 72f474f4-6e64-4be8-ae68-a7e9f6d4ab49
	Changelog {
		2017-01-14: ""
	}
}


Question HaemochromatosisCheckNeeded {
	"Has a baseline #ferritin# level been done #once# to exclude haemochromatosis as a cause of the Type 2 diabetes?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		No: problem HaemochromatosisNotExcluded
	}
	Id 12732467-4943-40f8-a9d6-e1f0bf00d7a4
	Changelog {
		2017-01-14: ""
	}
}


Question ArteriesAbnormal {
	"Check foot pulses. Check ABI result from cardiovascular clinic if available (greater than 0.9) 
	
	Are the #foot pulses abnormal#?"
	
	YesNo
	Category "blood sugar", "Heart / Brain"
	
	Triggers {
		Yes: problem ABISuggestiveOfPVDSoAssessmentNeeded
	}
	Id 53426bbd-8ee7-44a3-a1e1-ad9c6fc6222a
	Changelog {
		2017-01-14: ""
	}
}


Question EyeCheckNeeded {
	"#KeepSight# is a national patient reminder program designed to ensure regular eye checks (launched October 2018).
    
    Check last ophthalmology or optometry letter. 
	
	Have their #eyes# been checked #within the last 24 months#?"
	
	YesNo
	Category "blood sugar"

	Education "KeepSight national diabetes eye screening program overview" {"
	   The KeepSight program is coordinated by Diabetes Australia and is designed for all patients registered with the NDSS (National Diabetes Services Scheme).
       Currently 50% of those patients are not having regular eye checks.

      - #Patients# log onto the website and get a KeepSight card and present it to the eye health professional at the time of their check.
      Once a patient’s eye examination results have been recorded in the KeepSight portal, this will trigger recall and reminder notices to encourage them to have future eye examinations within the timeframe recommended by their eye care provider. This will supplement the existing recall and reminder systems within eye care practices. 

      - #Eye care providers# (Optometrists, ophthalmologists, health workers, and GPs who do Diabetic Retinopathy (DR) screening) need to register as #eye care providers.#

      - #GPs# will need to register but as as a #provider of diabetes care.#
       When a GP sees a person with diabetes ask them if they’d like to be registered with KeepSight.
       If they consent, register your patient’s details with the secure https://www.keepsight.org.au/[KeepSight web portal]. 
       You’ll need to record the person’s name and their NDSS number.
       
       If they don’t have their NDSS number, or aren’t registered with the NDSS, please call 1300 136 588. 

	   * https://www.keepsight.org.au/health_professional[KeepSight registration website]
	   
		"}
	Triggers {
        Yes No: education optional KeepSightWebsite
		No: problem EyeCheckNotDoneWithin24Months
	}
	Id 75f58769-2299-4569-be9b-c2a79c81bfbe
	Changelog {
		2017-01-14: ""
	}
}

Education KeepSightWebsite {
    Link "https://www.keepsight.org.au/"
    Message "KeepSight website"
    Id 58bde0e6-b992-436f-ba9d-2176db4da767
}

Question RetinopthyPresent {
	"Are the #fundi abnormal#?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		Yes: problem RetinopathyPresent
	}
	Id c11c9235-f139-47a6-9186-a01edf0c7efd
	Changelog {
		2017-01-14: ""
	}
}


Question GPMPReviewNeeded {
	"PAT automatically adds this Diabetic Cycle of Care to the last GPMP/TCA. 
	
	So if you  #review and edit the problem list# from the last GPMP/TCA it will satisfy item 732.  
	

	Do you want more information about item 732?"
	
	YesNoNA
	Category "general"
	
	Education "Tips" {"
	#Tips:# 
	
	* Mark finished GPMP problems as #completed# to reduce the length of the combined plan.
    * However all long term problems that require monitoring should me marked as#commenced/continuing#.
	* Item 732 can be #claimed twice# on the same day providing an item 732 for reviewing a GP Management Plan and another 732 for reviewing Team Care Arrangements (TCAs) are both delivered on the same day as per the MBS item descriptors and explanatory notes.
	"}	

	Triggers {
		Yes: problem GPMPReviewNeeded
	}
	Id 190d30f3-2caa-4c7e-94cf-b9cc4d2f318b
	Changelog {
		2017-01-14: ""
	}
}


Question DataEntryNeeded {
	"Has all data been entered into the annual cycle of care section of your clinical records and marked as completed?"
	
	YesNo
	Category "admin"
	
	Triggers {
		No: problem DiabeticCycleOfCareNeeded
	}
	Id 4030bfff-02a7-47d1-993e-bffb12c39313
	Changelog {
		2017-01-14: ""
	}
}


Question ThreeMonthlyReminderNeeded {
	"Do they need a reminder for 3 monthly diabetic control visits? 
	
	(add a #6 monthly reminder# only if #good control Type 2# on diet or tablets, otherwise 3 monthly)"
	
	YesNo
	Category "admin"
	
	Triggers {
		Yes: problem ThirdMonthlyDiabeticVisitsNeeded
	}
	Id 2ba6784b-be6b-4f45-954e-51939fa2fd16
	Changelog {
		2017-01-14: ""
	}
}


Question Diabetic-NewlyDiagnosed {
	"Have they been #newly# diagnosed as a diabetic (Type 1 or 2) within the last 12 months?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		Yes: problem DiabetesNewlyDiagnosed
	}
	Id d6f9856e-ae24-4e3d-bc08-0f1db1cb9092
	Changelog {
		2017-01-14: ""
	}
}


Question Diabetic-UncomplicatedAndControlled {
	"Classifying patients (Type 1 or 2) into one of #four categories# can help determine the level of allied health services required: 

	,===
	#Controlled & uncomplicated#,Uncontrolled & uncomplicated 
	Controlled & complicated,[red]#Uncontrolled & and complicated# 
	,===

	Are they an #uncomplicated diabetic# who is controlled?"
	
	YesNoNA
	Category "blood sugar"
	
	Triggers {
		Yes: AutoAnswer {
			Diabetic-UncomplicatedAndNotControlled No
			Diabetic-ComplicatedAndControlled No
			Diabetic-ComplicatedAndNotControlled No
		}
		Yes: problem Diabetes-UncomplicatedAndControlled
		Yes: education optional DiabetesControlAndComplication
	}
	Id 42af400d-97a6-4287-8289-b6ec03ac35b5
	Changelog {
		2017-01-14: ""
	}
}


Question Diabetic-UncomplicatedAndNotControlled {
	"Are they an #uncomplicated# diabetic who is not controlled?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		Yes: AutoAnswer {
			Diabetic-ComplicatedAndControlled No
			Diabetic-ComplicatedAndNotControlled No
		}
		Yes: problem Diabetes-UncomplicatedAndNotControlled
	}
	Id cc415d7a-4246-4370-8b1d-e9c3d0e2c101
	Changelog {
		2017-01-14: ""
	}
}


Question Diabetic-ComplicatedAndControlled {
	"Are they a #complicated diabetic# who is controlled?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		Yes: AutoAnswer {
			Diabetic-ComplicatedAndNotControlled No
		}
		Yes: problem Diabetic-ComplicatedAndControlled
	}
	Id d3869ef6-7ab6-4adb-9a58-74a62dfda2cc
	Changelog {
		2017-01-14: ""
	}
}


Question Diabetic-ComplicatedAndNotControlled {
	"Are they a #complicated# diabetic who is not controlled?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		Yes: problem Diabetic-ComplicatedAndNotControlled
	}
	Id 68886de2-4ef3-4397-ac2f-c58015049deb
	Changelog {
		2017-01-14: ""
	}
}


Problem GlycaemicIndexEducationNeeded {
	Goal "Understand how to choose low and medium GI foods instead of high GI"
	Plan "Handout on GI foods. Advise purchase of \"Low GI Diet Shopper's Guide\" at bookstore. Refer to dietitian if moderate/ severe lack GI knowledge."
	Id ffe4c6d2-a5ff-41ea-8842-562b3297c8ab
	Changelog {
		2017-01-14: ""
	}
}


Problem WaistSizeLinkToAmountOfVisceralFatIsNotUnderstood {
	Goal "Achieve patient understanding of role of visceral fat in progressive destruction of insulin production"
	Plan "Explanation in simple terms that pro-inflammatory cytokines released by fat tissue destroy the beta cell function in the pancreas over time."
	Id 4b25eb98-3805-4b7f-9081-dbc319194c3a
	Changelog {
		2017-01-14: ""
	}
}


Problem BGLNotBeingTested {
	Goal "BGL testing at home as appropriate to the insulin regimen."
	Plan "With insulin, 'First fix fasting BSL' requires testing before breakfast (BB). 'Then tackle tea' requires testing before the evening meal (BT). 'Find the hidden hypers' may require testing 2 hours after breakfast/ before lunch/ 2 hours after the evening meal/ before bed."
	Id 2074347c-54a5-4cbf-a196-b111c59990fd
	Changelog {
		2017-01-14: ""
	}
}

Problem BruisingWithInjectionsPresent {
	Goal "Reduce bruising and scarring."
	Plan "Explain how to rotate injection sites (calendar pattern or clock). New needle each time. Do not inject long acting and short acting into the same site."
	Id 865bd85d-9c3c-4490-8a7f-4cbab6fa46b9
	Changelog {
		2017-01-14: ""
	}
}


Problem HypoglycaemicEpisodesPresent {
	Goal "Prevent episodes"
	Plan "Analyse pattern to determine cause. Ensure awareness of early hypoglycaemic symptoms to enable early glucose intake. Make sure glucagon kit is available."
	Id 20c38691-7c9f-42b2-976a-2f6238da532b
	Changelog {
		2017-01-14: ""
	}
}


Problem GlucagonHypokitNotAvailable {
	Goal "All insulin-using diabetics to have a hypokit."
	Plan "Provide script. Provide instruction in use."
	Id b201830a-5347-47d9-ac4e-d0b0525af194
	Changelog {
		2017-01-14: ""
	}
}


Problem AusroadsNotificationNeeded {
	Goal "Appropriate state notification"
	Plan "Advise of rules and arrange another consult to issue a medical certificate re fitness to drive.."
	Id 7f1e0f5c-c75c-40a1-b36d-fcfffe9e754f
	Changelog {
		2017-01-14: ""
	}
}


Problem NDSSEnrolmentNeeded {
	Goal "NDSS and Diabetes Australia enrolment"
	Plan "Advise and supply appropriate forms."
	Id d13300a2-8986-401e-8e90-a9cb320bf456
	Changelog {
		2017-01-14: ""
	}
}


Problem HbA1cTestingNotUnderstood {
	Goal "Understand importance of 3 to 6 monthly HbA1c testing"
	Plan "Check current understanding of HbA1c then fill in the missing education."
	Id 95ff6b5a-835c-4c5a-b9a7-2f65f553e14b
	Changelog {
		2017-01-14: ""
	}
}


Problem DifficultyWithInjectionsOrTablets {
	Goal "Adherence"
	Plan "Explore the problems and find a solution."
	Id dbd62b34-d41c-41d5-bb4e-430dda9df9f1
	Changelog {
		2017-01-14: ""
	}
}


Problem DiabetesNotControlled {
	Goal "HbA1c <7.0 unless medical complications limit intensification"
	Plan "Determine cause and intensify treatment."
	Id 5de29245-e2cd-4ed1-bb52-9db3b1bfa711
	Changelog {
		2017-01-14: ""
	}
}


Problem MicroalbuminuriaPresent {
	Goal "Determine if persistent and exclude other causes."
	Plan "Repeat spot ACR twice in 2 months. If persistent investigate with USS KUB and UMCS for microscopic haematuria."
	Id ffb67c99-c906-469d-90fa-0b3116d3a4bd
	Changelog {
		2017-01-14: ""
	}
}


Problem MetforminDoseAlterationForCKDNeeded {
	Goal "Avoid complications with metformin in CKD"
	Plan "Reduce dose or cease depending on eGFR."
	Id c5d7c34b-0415-41fe-87ee-f2eb224a7271
	Changelog {
		2017-01-14: ""
	}
}


Problem EyeCheckNotDoneWithin24Months {
	Goal "Second yearly eye checks"
	Plan "Refer today."
	Id 533020ee-9aa0-4b26-9ff4-c591b418e675
	Changelog {
		2017-01-14: ""
	}
}


Problem RetinopathyPresent {
	Goal "Prevent further complications"
	Plan "Arrange more frequent eye checks and control HbA1c <7.0. Strongly consider adding fenofibrate to statin to reduce progression."
	Id ca788cdf-3745-4f68-b97a-6821555b35b3
	Changelog {
		2017-01-14: ""
	}
}


Problem HaemochromatosisNotExcluded {
	Goal "Baseline ferritin in all diabetics"
	Plan "Arrange test and put result in the free box \"further detail'."
	Id 2156c6fc-fe31-453a-948a-67ea10380e1d
	Changelog {
		2017-01-14: ""
	}
}


Problem ThirdMonthlyDiabeticVisitsNeeded {
	Goal "3 or 6 monthly visits depending on control"
	Plan "Add reminder for 3 months."
	Id 7579e7e4-517e-40c2-8dde-7ad695096516
	Changelog {
		2017-01-14: ""
	}
}


Problem GPMPReviewNeeded {
	Goal "Reduce the number of visits required by synchronization of 732 with cycle of care."
	Plan "Review the last GPMP."
	Id 4b04e88f-25f4-41b4-8a53-b5a586ab1483
	Changelog {
		2017-01-14: ""
	}
}


Problem MetforminDoseSuboptimal {
	Goal "Maximize dose."
	Plan "Start at 500mg per day and increase every 2 weeks by 500mg (allow GIT tolerance to build up) until using at least 2000mg per day or side effects."
	Id 0af621e8-f9b6-42f5-83cb-f5d8b83e8c00
	Changelog {
		2017-01-14: ""
	}
}


Problem SulphonylureaUseSuboptimal {
	Goal "Maximize dose."
	Plan "Increase dose progressively at 2 weekly intervals in response to BGLs until control or maximum dose reached."
	Id 5679c04e-8007-48a6-8205-7aa20e30efdc
	Changelog {
		2017-01-14: ""
	}
}


Problem DiabetesPioglitazoneNeeded {
	Goal "Minimize side effects and control HbA1c."
	Plan "Start pioglitazone while monitoring for side effects especially fluid retention."
	Id d859fd4c-e8fe-4ef1-8d10-93254b94db0d
	Changelog {
		2017-01-14: ""
	}
}


Problem DiabetesDPP-4InhibitorNeeded {
	Goal "HbA1c control."
	Plan "Add oral gliptin. If fails, consider GLP-1 analogue by injection because it is more powerful. ."
	Id a7c51490-3f85-4de1-924f-182752099e70
	Changelog {
		2017-01-14: ""
	}
}


Problem InsulinNeeded {
	Goal "Appropriate insulin regimen."
	Plan "Analyse pattern of BGL readings to determine dose and timing. Usually leave oral agents unchanged when basal insulin is started. Stop Sulphonylureas if prandial insulin is added."
	Id 4b135814-ed7b-4487-ab3c-b016576bc161
	Changelog {
		2017-01-14: ""
	}
}


Problem InsulinSelfTitrationNotUnderstood {
	Goal "Institute self titration"
	Plan "Can use either a basal (e.g.Lantus) or premixed insulin. If average target BGL is 6.0 to 7.8 increase by 2 units. If average BGL over 7.9 increase by 4 units. Review regularly initially to reassure the patient about their dose decisions."
	Id 9906c1b3-b27f-41cc-9cfc-9c77fc0bd7b7
	Changelog {
		2017-01-14: ""
	}
}


Problem InsulinConcernPresent {
	Goal "Clarify concerns"
	Plan "Guide through the insulin use wizard."
	Id cfc81c33-af67-4381-9ffd-29cba757de89
	Changelog {
		2017-01-14: ""
	}
}


Problem Diabetes-UncomplicatedAndControlled {
	Goal "Maintain control."
	Plan "If well educated during the initial diagnosis year, may not need any allied health referrals under an EPC."
	Id e21181d3-9be1-47b8-adec-9733747f26ce
	Changelog {
		2017-01-14: ""
	}
}


Problem DiabetesNewlyDiagnosed {
	Goal "Ensure appropriate education."
	Plan "Strongly consider referral to allied health such as diabetic educator and dietitian under an EPC."
	Id 617990a2-1258-4b32-968d-182dc7b796d4
	Changelog {
		2017-01-14: ""
	}
}


Problem Diabetes-UncomplicatedAndNotControlled {
	Goal "Gain control"
	Plan "If problem is BP or cholesterol intensify medications. If HbA1c is the problem consider dietitian or diabetic educator referral under an EPC as well as intensifying medications."
	Id ec7e2866-7ace-4fd0-8c23-824b2ea64d31
	Changelog {
		2017-01-14: ""
	}
}


Problem Diabetic-ComplicatedAndControlled {
	Goal "Ensure complications are managed appropriately."
	Plan "Probably needs allied health referral under EPC depending on the complications."
	Id cceeb5dc-cc00-4b48-9fe1-de62e1d401f7
	Changelog {
		2017-01-14: ""
	}
}


Problem Diabetic-ComplicatedAndNotControlled {
	Goal "Gain control and manage complications appropriately."
	Plan "Strongly consider allied health referral under EPC. Strongly consider endocrinology review."
	Id 7f7eb2a0-0992-433d-a81f-baff6aeb5167
	Changelog {
		2017-01-14: ""
	}
}


Problem InsulinRoleNotUnderstood {
	Goal "Clarify role"
	Plan "Explain any gaps in knowledge."
	Id 1c4d6b06-31f7-46ab-bae2-852e1dc4cafc
	Changelog {
		2017-01-14: ""
	}
}


Problem DistressPresent {
	Goal "Reduce distress levels."
	Plan "Acknowledge problem and bring back specifically to explore issues."
	Id f59432d0-274a-407d-84db-4508d07773c9
	Changelog {
		2017-01-14: ""
	}
}


Problem DiabetesGLP-1AnalogueNeeded {
	Goal "Control HbA1c"
	Plan "Arrange coaching for injections of GLP1RA. Start at lower dose of Byetta BD. Warn that nausea usually settles. Alternative is once weekly either Bydureon 2mg or Trulicity 1.5mg injection but these are not on PBS to use with insulin."
	Id ab665b8f-8290-4ee2-a5cb-990f78dcfdba
	Changelog {
		2017-01-14: ""
	}
}


Problem Above5ToDriveRuleNeeded {
	Goal "Ensure safe driving."
	Plan "Check understanding of which medications cause hypos and the 'above 5 to drive' rules."
	Id fd8a13eb-a44d-4d2c-aa7e-f83e1dafb3de
	Changelog {
		2017-01-14: ""
	}
}


Problem DiabetesSGLT2InhibitorNeeded {
	Goal "Ensure safe use and better HbA1c control."
	Plan "Check eGFR is > 60 for dapaglifozin or >45 for empagliflozin and check for diuretic use. Warn about risk of genital yeast infections and UTIs. Warn about signs of DKA and to seek urgent attention if occurs ."
	Id a7455478-242d-43fc-a2d2-63a7217fb5dd
	Changelog {
		2017-01-14: ""
	}
}


Problem MetforminEducationNeeded {
	Goal "Ensure safety in acute alteration of renal function."
	Plan "Suspend use if acute renal function alteration. Order Vit B12 annually."
	Id 0af9d1d3-0830-48b3-bac9-edfcb6559f83
	Changelog {
		2017-01-14: ""
	}
}


Education InsulinActionJpg {
	Message "Insulin action"
	File "jpg"
	Id d0fcbac8-3fd0-45d4-b9a1-9dcb41119763
	Changelog {
		2017-01-14: ""
	}
}


Education CarbohydrateExamples {
	File "jpg"
	Id 251853ab-2f85-4eb4-8b83-125f6e4facae
	Changelog {
		2017-01-14: ""
	}
}

Education InsulinTherapyFlexible {
	Message "Insulin therapy for Type 2 diabetes"
	File "rtf"
	Id dd2c7106-11bd-4421-a1ae-1aa968b42a38
	Changelog {
		2017-01-14: ""
	}
}


Education DiabetesControlAndComplication {
	Message "Definitions of controlled and complicated diabetes"
	File "rtf"
	Id f6471270-4eb9-470f-8ff4-1341f0b9534d
	Changelog {
		2017-01-14: ""
	}
}


Education SGLT2 {
	File "rtf"
	Id 45dc0f66-0657-4e0f-a5a7-84ee56e33851
	Changelog {
		2017-01-14: ""
	}
}




