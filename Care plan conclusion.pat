Question MainGoals {
	"After looking at your health today, what would you like to see happen in the next 6 months?"

	Text
	DisplaySettings "location: 'conclusion'"
	Category "plan"
    AppliesTo "PATv3"
	Id dd2b5f9a-d8a7-455a-accb-1306d6044612
}

Question PatientSaysAlreadyDoing {

	"Patient - Tell me some things you have already done to either improve or manage your health?"

	Text
	DisplaySettings "location: 'conclusion', heading: 'Good News'"
	Category "plan"
    AppliesTo "PATv3"
	Id 992952f7-8846-4624-a152-0008414dd82e
}

Question DoctorSaysGoingWell {
	"Doctor - Here are some things I think are going well"

	Text
	DisplaySettings "location: 'conclusion', heading: 'Good News'"
	Category "plan"
    AppliesTo "PATv3"
	Id 988018cf-cccf-4cd5-87a4-476805ab6d5e
}

Question DoctorSaysThingsThatWillHelp {
    "Doctor - Here are some things I think have helped or will help you:"

	Text
	DisplaySettings "location: 'conclusion', heading: 'Good News'"
	Category "plan"
    AppliesTo "PATv3"
	Id 988018cf-cccf-4cd5-87a4-476805ab6d5f
}

Question PatientImportance0to10 {
	"How IMPORTANT is the above plan to you?"

	Number required
	DisplaySettings "location: 'conclusion'"
	Category "plan"
    AppliesTo "PATv3"
	Id aff339b2-5d3f-4b32-8251-96158c019815
}

Question PatientConfidence0to10 {
	"How CONFIDENT are you that you can do it?"

	Number required
	DisplaySettings "location: 'conclusion'"
	Category "plan"
    AppliesTo "PATv3"
	Id ada63df5-7868-4437-a650-13e58e96240d
}

Question ChecklistCopyToPatient {
	"Copy of GPMP and TCA offered to patient?"

	YesNo
	DisplaySettings "location: 'conclusion-checklist'"
	Category "plan"
    AppliesTo "PATv3"
	Id 9cf43c2d-e125-4b36-8a98-eb4a440c0745
}

Question ChecklistCopiesToTeamWithConsent {
	"Consent obtained and copy/or relevant parts of GPMP and TCA supplied to other providers?"
	
	YesNo
	DisplaySettings "location: 'conclusion-checklist'"
	Category "plan"
    AppliesTo "PATv3"
	Id 39a5bf21-a8a8-44b7-9934-34cbf253a39f
}

Question ChecklistReferralFormsCompleted {
	"Referral forms for allied health services completed?"
	
	YesNo
	DisplaySettings "location: 'conclusion-checklist'"
	Category "plan"
    AppliesTo "PATv3"
	Id 927dce53-02cc-4578-945e-e7eeb410cfce
}

Question ReviewDate {
	"Review date (6 months)"
	
	Month
	DisplaySettings "location: 'conclusion-checklist'"
	Category "plan"
    AppliesTo "PATv3"
	Id 93edc961-35b5-43b5-8d1a-0feb2b054356
}