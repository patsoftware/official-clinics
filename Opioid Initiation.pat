/* 
 * Copyright (c) 2017 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Clinic OpioidInitiation {
	Id 29c257cc-482e-43b1-8a52-18a327ba00ab
	Questions {
		Patient {
			ChronicPainPresent
		}

		Checklist {
			RecordSmokingAlcohol
		}

		Staff {
			PainMedicine10UniversalPrecautionsNeeded
			PainAppropriateDiagnosis
			PainComorbidConditionsPresent
			PainYellowFlagsPresent
			RiskForAbuseOrAddictionModerateOrHigh
			PainInformedConsentNeeded
			PainTreatmentAgreementNeeded
			PainAssessment
			PainOpioidTrialNeeded
			PainTrialAssessmentNeeded
			PainMedicine6AsCheckNeeded
			PainPeriodicReviewNeeded
			PainMedicationDocumentationNeeded
			Constipation
			PainSpecialistReferralNeeded

            PainGoalReduceLevel
            PainGoalIncreaseActivities
            PainGoalIncreaseExercise
			
            PatientSaysAlreadyDoing
            DoctorSaysThingsThatWillHelp

            PatientImportance0to10
			PatientConfidence0to10
			ChecklistCopyToPatient
			ChecklistCopiesToTeamWithConsent
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Question PainGoalReduceLevel {
    "After looking at things today, #what specific things# would you like to see happen in the next 6 weeks in the following 3 areas?

    Pain level reduction by"

	Text
	DisplaySettings "location: 'conclusion', heading: 'Goals'"
	Category "pain"
	Id 5ce8ce0a-1013-4e2b-b493-5a52666d8440
}

Question PainGoalIncreaseActivities {
    "Restart or increase these normal daily or social activities"

	Text
	DisplaySettings "location: 'conclusion', heading: 'Goals'"
	Category "pain"
	Id 71361266-064f-47af-b53d-933a0e02e2a2
}


Question PainGoalIncreaseExercise {
    "Start or increase exercise by doing"

	Text
	DisplaySettings "location: 'conclusion', heading: 'Goals'"
	Category "pain"
	Id 7a28e9ab-6056-4834-a838-429aac69238e
}
