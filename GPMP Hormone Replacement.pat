/* 
 * Copyright (c) 2019 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with hormone replacement therapy*/
Clinic GPMPHormoneReplacementTherapy {
	Id 37ae842a-9f83-4d9a-b055-651e886cd3de
	Filter HormoneReplacement
	Questions {
		Patient {
			
		}

		Checklist {
			
		}

		Staff {
			HormoneReplacementMonitoringNeeded
			
			
			
		}

	}
}

Question HormoneReplacementMonitoringNeeded {
	"
    Guidelines recommend evaluating the patient #after Hormome Replacement initiation# to assess whether the patient has responded to treatment, is suffering any adverse effects, and is complying with the treatment regimen.

    Do you want to bring them back for #review#?"
    
    YesNoNA
	Gender female
	NAText "Not on HRT"
    Category "medications"

Education
     "Hormone replacement monitoring summary" {"

	   A regular annual review should include the following:
       
       * Check for side-effects - eg, breast tenderness or enlargement, nausea, headaches or bleeding - and manage appropriately.
       
       * Check blood pressure and weight.
       
       * Encourage breast awareness and participation in screening mammography and also cervical screening if appropriate for age.
       
       * A review and discussion of an individual's risk:benefit ratio concerning HRT should occur at least annually.
       
       * If appropriate, consider switching from cyclical HRT to continuous combined HRT.
       
       * The decision on whether to advise continuation of HRT should be based on symptoms and ongoing risks and benefits, rather than a set minimum or maximum duration of therapy.
       
       * Cessation of HRT leads to recurring symptoms for up to 50% of women. Consider the potential impact of these symptoms on quality of life.When stopping HRT, it is generally recommended that the dose of HRT should be gradually reduced over three to six months, to minimise the chance of oestrogen deficiency symptoms returning.
       
       * The merits of long-term HRT use should be assessed for each individual woman and the lowest dose of HRT which controls symptoms should be used.
	"}


Education 
"RANZCOG Menopausal Hormone Therapy Advice July 2018" {"

		* https://www.ranzcog.edu.au/RANZCOG_SITE/media/RANZCOG-MEDIA/Women%27s%20Health/Statement%20and%20guidelines/Clinical-Obstetrics/Menopausal-Hormone-Therapy-Advice-(C-Gyn-16)-Re-write-July-2015.pdf?ext=.pdf[ RANZCOG Menopausal Hormone Therapy Advice July 2018]
		"}

    Triggers {
		Yes: problem HormoneReplacementMonitoringNeeded
	}
    Id 7e4a2c52-1e21-493c-9641-6b3c0a6a5c1e
    Changelog {
        2019-01-18: ""
    }
}

Problem HormoneReplacementMonitoringNeeded {
	Goal "Assess and optimise therapy"
    Plan "Modify HRT as needed"
    Id 81f92b99-2c66-49cf-a552-b3901d2c5c28
    Changelog {
        2019-01-18: ""
     }
}

    


Filter HormoneReplacement {
	Match {
		keyword "HRT"
		keyword "Hormone replacement therapy"
		keyword "Hormone implant"
        keyword "Oestrogen replacement therapy"
        keyword "Estrogen replacement therapy"
		keyword "Oestrogen implant"
		keyword "Estrogen implant"
	}
	Tests matching only this {
		"Hormone replacement therapy"
		"Oestrogen replacement therapy"
	}
}











