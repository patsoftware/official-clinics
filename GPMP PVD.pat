/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with peripheral vascular disease */
Clinic GPMPPVD {
	Id 59017466-1ec7-4f35-8199-daeff7bf22ad
	Filter PVD
	Questions {
		Patient {
			LipidLoweringAgentUseNeeded
			StatinConcern
			NationalSupportOrganisationReferralNeeded
			
		}

		Checklist {
		}

		Staff {
			PADMedicationReviewNeeded
			LipidResultsNotWithinRangeOnMedication
		}

	}
}

Clinic GPMPPVDCore {
	Id 59017466-1ec7-4f35-8199-daeff7bf22ae
	Filter PVD
	Questions {
		Patient {
			LipidLoweringAgentUseNeeded
			StatinConcern
			NationalSupportOrganisationReferralNeeded
			
		}

		Checklist {
		}

		Staff {
			PADMedicationReviewNeeded
		}

	}
}

Filter PVD {
	Match {
		keyword "PAD"
		keyword "Peripheral arterial disease"
		keyword "Peripheral vascular disease"
		keyword "PVD"
	}
	Tests matching only this {
		"Peripheral vascular disease"
		"PVD"
	}
}


Question PADMedicationReviewNeeded {
	"If clinically significant peripheral arterial disease (PAD) is present the following #2 groups# of medications are useful:
	
	* Statin 
	* Antiplatelet agent 
	
	Remember 60% of PAD patients will have either CVD or CHD as well so control of BP (preferably with ACEI) and if they have diabetes tight control of HbA1c is recommended.
	
	Do you wish to make any changes?
	"

	Education "References" {"
	* https://emedicine.medscape.com/article/761556-guidelines#g2[AHA/ACC Guideline on Lower-Extremity Peripheral Artery Disease, 2016]	
	"}
	
	YesNoNA
	NAText "N/A - no PAD"
	Category "Heart / Brain", "medications"
	
	Triggers {
		Yes: problem PeripheralArteryDiseasePADPresent
	}
	Id 2b1e02e1-f259-4590-93e0-55888a73d10d
	Changelog {
		2017-01-14: ""
	}
}


Problem PeripheralArteryDiseasePADPresent {
	Goal "Prevent progression."
	Plan "Ensure is on all secondary drugs and refer for exercise rehabilitation walking program. Refer to vascular surgeon if ABI < 0.5, or life style limiting claudication, or signs of critical limb ischaemia."
	Id 02f7d717-29a8-4237-9e73-4c1c39018a57
	Changelog {
		2017-01-14: ""
	}
}


