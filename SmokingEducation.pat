/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Education SmokingEducation {
    Questions {
        SmokingEducationIntro
        SmokingLikesAndDislikes
        SmokingBodyConcerns
        SmokingQuitStage
    }
    AppliesTo "PATv3"
    Id ddaa3be9-6dd3-489b-9e83-b0693c4d5689
}

Question SmokingEducationIntro {
	"You have answered yes to the smoking question.
    In general people continue doing things when they get some reward from the behaviour.
    
    So we are really interested in what you like and don't like about smoking.
    Please #think carefully# of how you feel about smoking before touching Next.
    "
    
    Display
    
    Category "smoking"
    Id 9764e2c4-35a5-4c58-b4d1-a0271ae7c5bb
    Changelog {
        2017-06-22: ""
    }
}

Question SmokingLikesAndDislikes {
	"*Here are common reasons why people like or don't like smoking.*
    
    Please select Yes or No for each one and then press Next."
    
    SubQuestions {
        SmokingLikes
        SmokingDislikes
    }
    
    Category "smoking"
    Id 077095ec-e6a0-411d-a87b-4af68d645f4c
}

Question SmokingLikes {
    "Things I like about smoking" 
    
    SubQuestions {
        SmokingForEnjoyment
        SmokingAsHabit
        SmokingForRelaxation
        SmokingOtherReason
    }
    Category "smoking"
    Id 06d5ec9b-c450-40fc-91ab-927d91a282cc
}

Question SmokingForEnjoyment {
    "I just enjoy it" 
    
	YesNo
    Category "smoking"
    Id 72907f49-461b-4bdb-b2a1-fd528460634e
}

Question SmokingAsHabit {
    "It is just a habit" 
    
    YesNo
    Category "smoking"
    Id 20db9bd1-5349-4587-a0ef-9502495c40ce
}

Question SmokingForRelaxation {
    "It is a stress reliever for me / it relaxes me" 
    
    YesNo
    Category "smoking"
    Id 5c526076-df53-48e6-a884-214e47d1a3fe
}

Question SmokingOtherReason {
    "Other reason" 
    
    YesNo
    Category "smoking"
    Id 066acd01-4fd1-48e9-9fb3-e37944c9db3d
}


Question SmokingDislikes {
    "Things I don't like about smoking"
    
    SubQuestions {
        SmokingBadFinancially
        SmokingBadSocially
        SmokingBadForHealth
        SmokingFamilyConcerned
    }
    Category "smoking"
    Id 09f4af57-11d8-4e7a-8985-1d026c66dc5a
}


Question SmokingBadFinancially {
    "Cost / money" 
    
    YesNo
    Category "smoking"
    Id 549e5b6a-7600-49eb-8972-eaf436e3ad91
}

Question SmokingBadSocially {
    "Social aspects such as people noticing the cigarette smell or me having to leave the room to smoke" 
    
    YesNo
    Category "smoking"
    Id 7bad5bed-ea74-47b1-a78f-47b184c2202e
}

Question SmokingBadForHealth {
    "My health in general (e.g. physical fitness, smoking shortening the length of my life)" 
    
    YesNo
    Category "smoking"
    Id b5d34d8b-907f-401b-8965-098f40b8ef65
}

Question SmokingFamilyConcerned {
    "Family is concerned (e.g. spouse or children)" 
    
    YesNo
    Category "smoking"
    Id 59a252e9-f232-44bf-86c3-0d48a02feb5b
}

Question SmokingBodyConcerns {
    "These pictures are specific body parts that can be affected from smoking.
    
    Please touch *any box* that you want the doctor to explain about in more detail
    image:SmokingBody.png[]
    "
    
    Category "smoking"
    SubQuestions {
        ConcernedSmokingAffectsEyes
        ConcernedSmokingAffectsHair
        ConcernedSmokingAffectsSkin
        ConcernedSmokingAffectsBrain
        ConcernedSmokingAffectsMouth
        ConcernedSmokingAffectsLungs
        ConcernedSmokingAffectsHeart
        ConcernedSmokingAffectsStomach

        ConcernedSmokingAffectsPancreas
        ConcernedSmokingAffectsBladder
        ConcernedSmokingAffectsGynaecology
        ConcernedSmokingAffectsImpotence
        ConcernedSmokingAffectsPeripheralArteries
        ConcernedSmokingAffectsBones
    }
    Id 779e8dc4-b787-4f73-afd1-2bd2abd6b41e
    DisplaySettings "allow-next-when-answered: 0"
}



Question ConcernedSmokingAffectsHeart {
    "Heart: coronary artery disease image:bodyparts/heart.png[]"
    
    YesNo
    Category "smoking"
    Id d6d53403-e36d-4355-baa3-5392055de5d4
}
Question ConcernedSmokingAffectsLungs {
    "Lungs: cancer, emphysema, pneumonia image:bodyparts/lungs.png[]"
    
    YesNo
    Category "smoking"
    Id 128974d0-9035-4926-8de5-7bbfbf7fecf6
}
Question ConcernedSmokingAffectsMouth {
    "Mouth and Pharynx: cancer, gum disease image:bodyparts/mouth.png[]"
    
    YesNo
    Category "smoking"
    Id e2e79969-f727-443e-bb35-8938868b9810
}
Question ConcernedSmokingAffectsBrain {
    "Brain: stroke image:bodyparts/brain.png[]"
    
    YesNo
    Category "smoking"
    Id c2f353c6-bc3b-4cf8-bb89-6f583f4d6f9e
}
Question ConcernedSmokingAffectsSkin {
    "Skin: ageing, wrinkles, wound infection image:bodyparts/dermatology.png[]"
    
    YesNo
    Category "smoking"
    Id b85989d0-bd8f-443c-954d-5a5a9a4ba6c5
}
Question ConcernedSmokingAffectsHair {
    "Hair loss image:bodyparts/hair.png[]"
    
    YesNo
    Category "smoking"
    Id d41b6058-fc22-4d6f-8a08-115bf419bf3d
}
Question ConcernedSmokingAffectsEyes {
    "Eyes: macular degeneration image:bodyparts/eyes.png[]"
    
    YesNo
    Category "smoking"
    Id e01142b1-b9e1-43f7-8326-d1233fb1ed53
}
Question ConcernedSmokingAffectsBladder {
    "Bladder: cancer image:bodyparts/bladder.png[]"
    
    YesNo
    Category "smoking"
    Id f94f46a4-fd4f-49eb-aa01-85f7465a82dd
}
Question ConcernedSmokingAffectsPancreas {
    "Pancreas: cancer image:bodyparts/pancreas.png[]"
    
    YesNo
    Category "smoking"
    Id 7b65226e-132c-4b16-9206-7a05e3122b97
}
Question ConcernedSmokingAffectsStomach {
    "Stomach: cancer, ulcer image:bodyparts/stomach.png[]"
    
    YesNo
    Category "smoking"
    Id c7115ce1-5597-470f-a460-3245c0fd09d1
}
Question ConcernedSmokingAffectsImpotence {
    "Men: impotence image:bodyparts/penis.png[]"
    
    YesNo
    Category "smoking"
    Id 21fb67af-c227-4067-8680-22bb4bc289f5
}
Question ConcernedSmokingAffectsGynaecology {
    "Women: cervical cancer, early menopause, irregular and painful periods image:bodyparts/gynaecology.png[]"
    
    YesNo
    Category "smoking"
    Id 395f247d-a0d7-45a7-ab50-20399b3c6170
}
Question ConcernedSmokingAffectsPeripheralArteries {
    "Arteries: peripheral vascular disease image:bodyparts/arteries.png[]"
    
    YesNo
    Category "smoking"
    Id 28f91490-55ba-46fa-ad6b-a4269b090997
}
Question ConcernedSmokingAffectsBones {
    "Bone: osteoporosis image:bodyparts/bone.png[]"
    
    YesNo
    Category "smoking"
    Id 8fee8552-bd32-4836-8ff2-f542bf0c8bf3
}

Question SmokingQuitStage {
    "People take different amounts of time to decide to change. So where are you now?

    Touch one of the 4 answers."
    
    Select one SmokingQuitStage
    DisplaySettings "selection-background-colour: '#FFFF80'"
    Category "smoking"
    Id 8331c6eb-5687-464c-8663-c3aa9718fe9f
}

Selection SmokingQuitStage {
    Option precontemplative { "Not ready to think about it at all yet"}
    Option contemplative    { "I will think about it over the next few months"}
    Option ready            { "I will be ready to quit within a month" }
    Option action           { "I am ready to quit today or I have" }
    Id 0d52f013-6061-4f90-93c2-27d2434d8efe
}