/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Clinic GPMPBackPain {
	Id afe4432a-8d9d-4164-99d0-b5009fd20ce6
	Filter BackPain
	Questions {
		Patient {
			ParacetamolNotControllingPain
			ArthritisOrBackPainExtraIdeas
		}

		Checklist {
		}

		Staff {
			PainPharmacotherapyChangeNeeded
		}

	}
	Changelog {
		2017-01-14: ""
	}
}
Filter BackPain {
	Match {
		keyword "Back pain"
		keyword "Backache"
		keyword "LBP"
		keyword "Spondylolisthesis"
		keyword "Spinal canal stenosis"
		regex ". *spondylosis"
		regex ".*disc prolapse"
		regex ".*radiculopathy"
	}
	Tests matching only this {
		"Back pain"
		"LBP"
		"low back pain"
		"backache"
		"thoracic back pain"
		"cervical back pain"
	}
}