From https://commons.wikimedia.org/wiki/File:Diagram_showing_the_chest_wall_CRUK_298.svg
Cancer Research UK / Wikimedia Commons


This image has been released as part of an open knowledge project by Cancer Research UK. If re-used, attribute to Cancer Research UK / Wikimedia Commons 


This file is licensed under the Creative Commons Attribution-Share Alike 4.0 International license. 	

    You are free:

        to share  to copy, distribute and transmit the work
        to remix  to adapt the work

    Under the following conditions:

        attribution  You must attribute the work in the manner specified by the author or licensor (but not in any way that suggests that they endorse you or your use of the work).
        share alike  If you alter, transform, or build upon this work, you may distribute the resulting work only under the same or similar license to this one.