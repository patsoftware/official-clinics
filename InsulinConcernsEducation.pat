/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Education InsulinConcernsEducation {
    Questions {
        InsulinEducationIntro

        InsulinNegativeFeelings1
        InsulinNegativeFeelings2

        InsulinNobodysFaultInfo
        InsulinCarefulStartInfo
        InsulinCounterAnxietyInfo1
        InsulinCounterAnxietyInfo2

        InsulinEducationImpact
    }
	
    AppliesTo "PATv3"
    Id 5cc0c5f8-a322-4da7-9248-8862d215cad8
}

Question InsulinEducationIntro {
    "*PAT Insulin Education*

    Copyright PAT Pty Ltd and inspired by articles written by Dr Pat Phillips.

    Please press Next to start.
    "
    
    Display
    Category "blood sugar"
    Id dfa07de6-2388-47d6-bb61-210555b85ddb
}


Question InsulinNegativeFeelings1 {
    "*Common _feelings_ about having to use insulin.*

    Press *any* or *all* of the boxes that match your feelings about using insulin.
    "
    
    SubQuestions {
        AnnoyedWithMyselfAboutInsulin
        SadAboutInsulin
        AnnoyedAboutInsulin
        AnxiousAboutInsulin
        HappyEnoughAboutInsulin
    }
    Category "blood sugar"
    DisplaySettings "allow-next-when-answered: 1"
    Id 977b3595-420e-42b0-b5c8-f2c82d4fc6b5
}
Question AnnoyedWithMyselfAboutInsulin {
    "I feel a bit annoyed
    
    with myself"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#F14501'"
    Category "blood sugar"
    Id ff0fba38-747f-4778-829c-88b4cfa8dd09
}
Question SadAboutInsulin {
    "I feel a bit sad"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#463D3C'"
    Category "blood sugar"
    Id 26625ebb-b97a-483b-a4ac-b7869089e5a1
}
Question AnnoyedAboutInsulin {
    "I just feel annoyed"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#F10101'"
    Category "blood sugar"
    Id cc9c8488-726b-480a-95c9-9f2a1383ab57
}
Question AnxiousAboutInsulin {
    "I feel worried
    
    or anxious"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#941A70'"
    Category "blood sugar"
    Id 3b1545cc-dcc9-44e7-94ae-734d1a844825
}
Question HappyEnoughAboutInsulin {
    "I feel happy enough"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#4977E3'"
    Category "blood sugar"
    Id dc4c9ef8-6647-45af-92a7-760fcb2cd36e
}


Question InsulinNegativeFeelings2 {
    "*Common _feelings_ about having to use insulin.*

    Press *any* or *all* of the boxes that match your feelings about using insulin.
    "
    
    SubQuestions {
        ShouldHaveTriedHarderManagingDiabetes
        InsulinWillBeDifficultToUnderstandOrCopeWith
        UnfairIHaveToUseInsulinNotHadEnoughHelp
        ConcernedInsulinPainfulDangerousOrLimiting
        ManagingWellReInsulin
    }
    Category "blood sugar"
    DisplaySettings "allow-next-when-answered: 1"
    Id a3f5ba65-bdfa-498f-bb89-edd55c9a7358
}
Question ShouldHaveTriedHarderManagingDiabetes {
    "I should have tried harder
    
    to look after myself.

    I have failed to follow the

    treatment properly.
    "
    
    YesNo
    Triggers {
		Yes: show InsulinNobodysFaultInfo
    }
    DisplaySettings "text-colour: 'white', background-colour: '#F14501'"
    Category "blood sugar"
    Id f807f256-a86f-4187-aeba-7c32f7a618c5
}
Question InsulinWillBeDifficultToUnderstandOrCopeWith {
    "It will be too difficult

    to understand.
    
    I will not be able to cope.
    "
    
    YesNo
    Triggers {
		Yes: show InsulinCarefulStartInfo
    }
    DisplaySettings "text-colour: 'white', background-colour: '#F10101'"
    Category "blood sugar"
    Id f9ced0fd-f6b0-4689-9a6c-613ea0764d23
}

Question UnfairIHaveToUseInsulinNotHadEnoughHelp {
    "I should have been

    helped more or earlier.
    
    It is just unfair that I
    
    have to use insulin.
    "
    
    YesNo
    Triggers {
		Yes: show InsulinNobodysFaultInfo
    }
    DisplaySettings "text-colour: 'white', background-colour: '#463D3C'"
    Category "blood sugar"
    Id 6cd72f27-619f-4fcd-90ca-2aae82275453
}

Question ConcernedInsulinPainfulDangerousOrLimiting {
    "It will be painful to inject.

    I may lose consciousness.

    It will limit my driving

    or activities.
    "
    
    YesNo
    Triggers {
		Yes: show InsulinCounterAnxietyInfo1
		Yes: show InsulinCounterAnxietyInfo2
    }
    DisplaySettings "text-colour: 'white', background-colour: '#941A70'"
    Category "blood sugar"
    Id 30e23ac9-50b6-4178-b0ae-e7bb3e8886a0
}
Question ManagingWellReInsulin {
    "I am managing well"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#4977E3'"
    Category "blood sugar"
    Id 6dac3165-b6ae-4e08-a3f6-e827133dd9f2
}

Question InsulinNobodysFaultInfo {
    "*Here is some extra information that may change your thinking about insulin.*

    [cols=\"^.^1,6\",grid=\"all\"]
    |=====
    |
    {set:cellbgcolor: #E0E0E0} 
    |
    {set:cellbgcolor:#374679} 
    [white]#Lifestyle change often controls newly diagnosed diabetes,
but only for a limited time.#
    |=====

    [cols=\"^.^1,6\",grid=\"all\"]
    |=====
    |
    {set:cellbgcolor: #E0E0E0} 
    |
    {set:cellbgcolor:#374679} 
    [white]#Similarly tablets that reduce insulin resistance and
increase insulin production work initially, but the effect gradually lessens.#
    |=====

    [cols=\"^.^1,6\",grid=\"all\"]
    |=====
    |
    {set:cellbgcolor: #E0E0E0} 
    |
    {set:cellbgcolor:#374679} 
    [white]#It is not anyone's fault that you need insulin.
It is just the natural progression of the disease.#
    |=====

    "
    
    Display
    Category "blood sugar"
    Id 86fcc25e-d901-457d-9788-34f57ad2174d
}

Question InsulinCarefulStartInfo {
    "
    *Here is some extra information that may change your thinking about insulin.*

    [cols=\"^.^1,6\",grid=\"all\"]
    |=====
    |
    {set:cellbgcolor: #E0E0E0} 
    |
    {set:cellbgcolor:#F5F567} 
    [black]#We will see you frequently at the start
so that we can help you with any problems.#
    |=====

    [cols=\"^.^1,6\",grid=\"all\"]
    |=====
    |
    {set:cellbgcolor: #E0E0E0} 
    |
    {set:cellbgcolor:#F5F567} 
    [black]#Using insulin sounds complicated but
we start with a very simple plan.#
    |=====

    [cols=\"^.^1,6\",grid=\"all\"]
    |=====
    |
    {set:cellbgcolor: #E0E0E0} 
    |
    {set:cellbgcolor:#F5F567} 
    [black]#Either the doctor or the diabetic educator will
educate you very carefully on how to use the insulin device.#
    |=====

    "
    
    Display
    Category "blood sugar"
    Id 01298dea-b50a-4080-b1c3-2961f8a4afeb
}

Question InsulinCounterAnxietyInfo1 {
    "
    *Here is some extra information that may change your thinking about insulin.*

    |====
    | {set:cellbgcolor:#941A70} 
    [white]#*Concern*: Starting insulin means my diabetes is really bad and major complications will occur soon.#
    | {set:cellbgcolor:#3A7AE4}
    [white]#*Fact*: Insulin improves diabetes control and thus reduces the risk of complications.#
    |====


    |====
    | {set:cellbgcolor:#941A70} 
    [white]#*Concern*: Insulin will cause loss of consciousness.#
    | {set:cellbgcolor:#3A7AE4}
    [white]#*Fact*: Because type 2 diabetics still produce some of their own insulin, the body reduces insulin production if the injection dose is a bit high. So control is more stable.#
    |====


    |====
    | {set:cellbgcolor:#941A70} 
    [white]#*Concern*: Injection will be painful.#
    | {set:cellbgcolor:#3A7AE4}
    [white]#*Fact*: The doctor will show you that the ultrafine needles do not hurt.#
    |====
    "
    
    Display
    Category "blood sugar"
    Id c685b388-487f-4357-8964-07557c712eea
}

Question InsulinCounterAnxietyInfo2 {
    "
    *Here is some extra information that may change your thinking about insulin.*

    |====
    | {set:cellbgcolor:#941A70} 
    [white]#*Concern*: Insulin treatment will lead to an \"addiction\".#
    | {set:cellbgcolor:#3A7AE4}
    [white]#*Fact*: The insulin injected is identical to and just replaces the shortage of insulin in your body.#
    |====

    |====
    | {set:cellbgcolor:#941A70} 
    [white]#*Concern*: Insulin treatment will limit my driving, employment or recreation.#
    | {set:cellbgcolor:#3A7AE4}
    [white]#*Fact*: It is true that some recreational activities are not possible with insulin.
Otherwise well controlled diabetics with no hypos have very little restriction.( e.g. they can scuba dive)#
    |====
    "
    
    Display
    Category "blood sugar"
    Id 523ca04d-3213-4db9-b689-dc8361f53062
}


Question InsulinEducationImpact {
    "Press the box on how you *_now_* feel about the idea of using insulin?"
    
    Select one InsulinEducationImpact
    DisplaySettings "selection-text-colour: 'white', selection-background-colour-1: '#941A70', selection-background-colour-2: '#808080', selection-background-colour-3: '#008000'"
    Category "blood sugar"
    Id 01f03d68-7485-49f5-8cd6-321ce28390aa
}

Selection InsulinEducationImpact {
    Option less-comfortable { "I am less comfortable with the idea." }
    Option unchanged { "I feel the same as before." }
    Option more-comfortable { "I feel more comfortable with the idea." }
    Id 1b7408f7-0369-45e7-9c3f-99ff4e798a15
}
