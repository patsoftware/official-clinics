/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with headaches */
Clinic GPMPHeadaches {
	Id b699770b-31a9-4062-80c9-6e0ad34bec15
	Filter Headaches
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			HeadachesPresent
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter Headaches {
	Match {
		keyword "Cephalalgia"
		keyword "Cephalgia"
		keyword "Headache"
		keyword "Hemicrania"
		keyword "Migraine"
	}
	Tests matching only this {
		"Headache"
		"headaches"
		"Migraine"
		"hemicrania continua"
		"paroxysmal hemicrania"
		"cluster headache"
		"cephalalgia"
		"cephalgia"
	}
}


Question HeadachesPresent {
	"The #EASE# approach can help patients #manage headache# triggers: 
	
		
	Do you want to #bring them back# for either a long term headache management plan or for an acute episode treatment plan?"
	
	YesNoNA
	NAText "N/A - no headaches "
	Category "neurology"

	Education "EASE approach" {"
		* *Experiment* – some factors that they think are headache triggers may not be. If there is any doubt whether a trigger really can lead to headaches test whether it can by exposing to a mild version of the trigger to see what happens. 
	    * *Avoid* – however there are some triggers that are best avoided. These tend to be the triggers that not only precipitate headaches but are bad for your health/well-being. 
	    * *Stress* – stress is the most common trigger of headaches The approach here should be to learn to cope with stress (stress management). 
	    * *Exposure* – it is possible to reduce the capacity of some triggers to precipitate headaches by 'graduated exposure'. 

		*
		https://headacheaustralia.org.au/headache-management/the-ease-approach/[Headache Australia Ease approach]
		"}
	
	Triggers {
		Yes: problem HeadachePresent
	}
	Id 7288707a-fa47-4b0c-add0-eba2700b6f3c
	Changelog {
		2017-01-14: ""
	}
}


Problem HeadachePresent {
	Goal "Control headaches"
	Plan "Add an appropriate reminder for check up. Promote website www.headacheaustralia.org.au.
	Request keeping a Headache Diary prior to returning for a specific consultation on headache. Useful app is 'iManage Migraine'."
	Id b0df3560-d61a-4931-a127-cf1735c4634f
	Changelog {
		2017-01-14: ""
	}
}


