/* 
 * Copyright (c) 2017 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Clinic CarePlanReview {
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			MainGoals
			PatientSaysAlreadyDoing
			DoctorSaysGoingWell
			PatientImportance0to10
			PatientConfidence0to10
			ChecklistCopyToPatient
			ChecklistCopiesToTeamWithConsent
			ChecklistReferralFormsCompleted
			ReviewDate
		}

	}
	Changelog {
		2017-01-14: ""
	}
    AppliesTo "PATv3"
	Id 2d11a221-87c4-4d01-a060-4d8c21b1c803
}

