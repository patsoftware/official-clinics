/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with carotid stenosis */
Clinic GPMPCarotidStenosis {
	Id 9a74254a-9ba8-4631-ac34-42b368b964bd
	Filter CarotidStenosis
	Questions {
		Patient {
			LipidLoweringAgentUseNeeded
			StatinConcern
		}

		Checklist {
		}

		Staff {
			LipidResultsNotWithinRangeOnMedication
			Aspirin-RiskFactors
		}

	}
	Changelog {
		2017-01-14: ""
	}
}
Clinic GPMPCarotidStenosisCore {
	Id 9a74254a-9ba8-4631-ac34-42b368b964be
	Filter CarotidStenosis
	Questions {
		Patient {
			LipidLoweringAgentUseNeeded
			StatinConcern
		}

		Checklist {
		}

		Staff {
			LipidResultsNotWithinRangeOnMedication
		}

	}
	Changelog {
		2017-01-14: ""
	}
}
Filter CarotidStenosis {
	Match {
		keyword "Carotid artery narrowing"
		keyword "Carotid artery stenosis"
		keyword "Carotid stenosis"
	}
	Tests matching only this {
		"Carotid stenosis"
		"carotid artery stenosis"
		"carotid artery narrowing"
	}
}
