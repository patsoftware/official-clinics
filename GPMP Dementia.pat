/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with dementia */
Clinic GPMPDementia {
	Id 6549603b-211b-4183-a2a9-db3d8b93d4db
	Filter Dementia
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			DementiaPresent
			DementiaInvestigationsNeeded
		}

	}
}

Clinic GPMPDementiaCore {
	Id 6549603b-211b-4183-a2a9-db3d8b93d4dc
	Filter Dementia
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			DementiaPresent
		}

	}
}

Filter Dementia {
	Match {
		keyword "Alzheimers"
		keyword "Alzheimer's"
		keyword "Dementia"
		keyword "Lewy body"
	}
	Tests matching only this {
		"Alzheimers"
		"Alzheimer's"
		"Alzheimer disease"
		"Alzheimer's disease"
		"Dementia"
		"Lewy body"
	}
}


Question DementiaInvestigationsNeeded {
	"The appropriate investigations for diagnosis of #dementia# are: 
	
	* FBC 
	* Calcium, glucose, renal and liver function 
	* Thyroid function tests 
	* Serum vitamin B12 and folate levels 
	* CT scan brain (non contrast)
    
    Do you want to order investigations?"
	
	YesNoNA
	NAText "N/A - no dementia "
	Category "neurology"

	Triggers {
		Yes: problem DementiaPresentOrSuspected
	}
	Id 05bc4723-f493-49b8-8796-5935c632369e
	Changelog {
		2017-01-14: ""
	}
}


