/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */


/** Tests for sub-clinic keywords that should match multiple clinics, or no clinics.
	Tests that match only a single sub-clinic can go into that sub-clinic's file. */
SubclinicMatchingTests {
	Expect {
		ArthritisInflammatory
		ArthritisOsteoarthritis
		"Osteoarthritis of ankle secondary to inflammatory arthritis"
		"arthritis"
	}
	Expect {
		Anxiety
		Depression
		"depression / anxiety"
		"depression/anxiety"
		"depression, anxiety"
	}
	Expect {
		Bipolar
		Depression
		"manic-depressive illness"
	}
	Expect {
		Dementia
		CVD
		"multi-infarct dementia"
		"vascular dementia"
	}
	Expect {
		BackPain
		ChronicPain
		"chronic back pain"
	}

	Expect {
		/* should match no clinics */
		"enteritis"
		"ischaemic"
		"head"
		"bla"
		"bla bla"
		"bla gla dla"
		"disorder"
		"disease"
		"some disorder"
		"a disease"
		"non-malignant"
		"non-malignant effusion"
	}

}