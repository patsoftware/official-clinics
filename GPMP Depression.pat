/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with depression */
Clinic GPMPDepression {
	Id d74fcf3d-1ae1-4d2a-bd73-f97b319edd25
	Filter Depression
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			MentalHealthProblemPresent
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter Depression {
	Match {
		keyword "Depression"
		keyword "Dysthymia"
		keyword "MDD"
	}
	Tests matching only this {
		"Depression"
		"depressive disorder"
		"depressed"
		"depressed mood"
		"Schizoaffective disorder, depressive type"
		"MDD"
		"dysthymia"
	}
}


