/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with bipolar affective disorder */
Clinic GPMPBipolar {
	Id a822cf66-c906-45ab-8d50-ac360af47dde
	Filter Bipolar
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			BipolarLithiumMonitoringNeeded
		}

	}
	Changelog {
		2017-01-14: ""
	}
}
Filter Bipolar {
	Match {
		keyword "Bipolar"
		keyword "BPAD"
		keyword "Manic"
	}
	Tests matching only this {
		"Bipolar"
		"bipolar I"
		"BPAD"
		"bipolar affective disorder"
		"bipolar disorder"
		"Manic"
		"manic episode"
	}
}

Question BipolarLithiumMonitoringNeeded {
	"If the patient has #bipolar disorder on lithium# do you want to arrange a review and checks of thyroid and renal function and lithium levels?"
	
	YesNoNA
	Category "medications", "psychology"
	
	Triggers {
		Yes: problem BipolarLithiumMonitoringNeeded
	}
	Id 0bdfdd81-876d-4204-9937-3c1bcd5b1f8e
	Changelog {
		2017-01-14: ""
	}
}


Problem BipolarLithiumMonitoringNeeded {
	Goal "Safe use"
	Plan "Add reminder for check up. Lithium level every three months for the first year, and after the first year every 12 months. However measure every three months for people in any of the following groups: 
	1. older
	2. taking drugs that interact with lithium 
	3. at risk of impaired renal or thyroid function, raised calcium levels or other complications
	4. poor symptom control or poor adherence
	5. last level > 0.8mmol/l

	Annually TSH, E&LFTs and weight."
	Id efcf2983-4408-42ee-ac71-e1815b3bede3
	Changelog {
		2017-01-14: ""
	}
}


