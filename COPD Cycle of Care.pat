/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Clinic COPDCycleOfCare {
	Id a702cc85-2be5-4de7-a50d-fabe9a1f14ae
	Questions {
		Patient {
			COPDInflammatoryNature
			AdditionalAgentToShortActingBronchodilators
			CombinationOrPreventerInhaler
			ForgetToTakeMedication
			CarelessSometimesWithTaking
			StopTakingWhenWell
			SideEffectOfMedicationSuspected
			Exercise2HoursPerWeek
			InfluenzaImmunization1
			SmokingInCOPD
			AnnualReviewOK
			SelfHelpGroupReferralNeeded
			AnxietyCheck
			DepressionPresent
			AdvanceHealthDirectiveNeeded
			FirstAid4by4by4
			DEMTreatment
			HospitalAdmission
			COPDActionPlanPatient
			ExtraHelpNeeded
			AustralianLungFoundation
			ResearchConsent
		}

		Checklist {
			ConsentCOPDitems44and11506
			SpirometryBestOf3
			AdministerSalbutamol4puffs
			PulseOximetryRoomAir
			RecordSmoking
			PackYears
			CATScore
			ReminderCOPDReview12months
			InformCOPDProgressCheckAtGPMP
			RecordHeightWeight
			Spirometry15MinsPostSalbutamol3
			PrintHealthSummary
			RecordCarerDetails
			CheckTiming721and723
		}

		Staff {
			HealthSummaryAndMedicationListReviewNeeded
			COPDSpirometryDiagnosisConfirmed
			AsthmaPlusCOPDPresent
			COPDWithSomeReversibilityPresent
			COPDMildSeverity
			COPDModerateSeverity
			COPDSevere
			CATScoreDiscrepancyPresent
			ShortActingBetaAgonistsNeeded
			LongActingBronchodilatorsNeeded
			COPDAndRoleOfICSExplanationNeeded
			SpacerMDIUse
			PulmonaryRehabilitationNeeded
			PhysiotherapyNeeded
			SputumTestSupplyNeeded
			COPDNutritionAdviceNeeded
			SignificantCo-morbiditiesPresent
			COPDAndBetaBlockersNeeded
			COPDOsteoporosisPreventionAndTreatmentNeeded
			HypoxemiaPresent
			PneumococcalImmunisationNeeded
			FormulationOfTCAItem723Needed
			COPDSpecialistReferralNeeded
			COPDActionPlanNeeded
			ScriptsForCOPDNeeded
			PersonalisingThePlan

			MainGoals
			PatientSaysAlreadyDoing
			DoctorSaysGoingWell
			PatientImportance0to10
			PatientConfidence0to10
			ChecklistCopyToPatient
			ChecklistCopiesToTeamWithConsent
			ChecklistReferralFormsCompleted
			ReviewDate
		}

	}
	Changelog {
		2017-01-14: ""
	}
}


Question COPDInflammatoryNature {
	"
	[.big]
	COPD is an #inflammatory# disease. 
	
	Irritating gases and particles in the air cause redness and swelling (inflammation) of the #breathing tubes# (bronchi) which results in scarring (fibrosis) and narrowing. 
	
	The #tiny air sacks# (alveoli) are also #damaged,# so it is more difficult to absorb oxygen into the blood and to get rid of waste carbon dioxide. 
	
	#Elastic tissue# in the lungs is also #lost.# 
	
	All of these things mean you have to #work harder to breathe# and so feel short of breath. 
	
	[.big]
	Would you like to see a diagram of what these COPD changes in the lung look like?"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem COPDInflammatoryNatureExplanationNeeded
		Yes Unsure: education optional COPDProcess
	}
	Id fc61f0ab-9ba8-4742-aa4d-e2aa491b0987
	Changelog {
		2017-01-14: ""
	}
}


Question AdditionalAgentToShortActingBronchodilators {
	"[big]#Medications that open the airways are called bronchodilators and are our first line of treatment.#

	#Short acting bronchodilators# open your airways for a short time (quick onset and last 4 to 6 hours). 

	There are #2 main types:# 

	- Beta agonists such as #Ventolin# and #Bricanyl#.
	- Anticholinergics such as #Atrovent#.

	You can use these #separately# or #together.# 

	They do not treat inflammation. They are called #relievers# because you can #use them as necessary# t o relieve feelings of shortness of breath. 

	[big]#Would you like more information on this type of inhaler?#"
	
	YesNoUnsure
	Category "lungs", "medications"
	
	Triggers {
		Yes: AutoAnswer {
			CombinationOrPreventerInhaler Yes
		}
		Yes Unsure: problem ShortActingBronchodilatorsNeeded
	}
	Id 0d895540-38a3-484f-8dad-57c0b63cb1aa
	Changelog {
		2017-01-14: ""
	}
}


Question CombinationOrPreventerInhaler {
	"
	[.big]
	The inflammation can be partially settled down by #preventer# medications. 
	
	These are all inhaled steroids and must be taken #regularly.# 
	
	They are used for severe COPD, #or# if moderate COPD frequently flares up. 
	
	Preventers can be used alone, but when combined with a long acting bronchodilator, they give a #better# outcome #.# 
	
	So #combination# medications are often used: Brands include 
	
	* Seretide 
	* Symbicort 
	* Breo Ellipta 
	* Flutiform 
	
	[.big]
	Would you like to see a diagram of how your preventer or combination inhaler works?"
	
	YesNoUnsure
	Category "lungs", "medications"
	
	Triggers {
		No: problem PreventerUse
		Yes: education required COPDCombinationInhalerAction
	}
	Id a842a255-92f0-42a3-81d3-c202ff422b78
	Changelog {
		2017-01-14: ""
	}
}


Question Exercise2HoursPerWeek {
	"
	[.big]
	A large study showed #2 hours# walking or cycling per week reduced hospital admissions in patients with COPD by 30 to 40%. 
	
	It is extremely useful in controlling COPD to exercise daily to a #mild / moderate shortness of breath.# This means you feel puffy and can only talk in short sentences. 
	
	This can be #accumulated in 10 minute# episodes. 
	
	[.big]
	Are you exercising 120 minutes #(2 hours)# per week?"
	
	YesNo
	Category "lifestyle"
	
	Triggers {
		No: problem PhysicalActivityNeeded
		Yes: education optional ExerciseEducation
	}
	Id 0cce41cf-4cbb-4393-a3ed-8f8b91273def
	Changelog {
		2017-01-14: ""
	}
}


Question InfluenzaImmunization1 {
	"Influenza (flu) vaccination reduces your risk of hospitalization or death by 50%. 
	
	The ideal time to have an flu immunization is before winter starts. 
	
	Do have an annual flu immunization?"
	
	YesNo
	Category "prevention"
	
	Triggers {
		No: problem InfluenzaAtRiskGroup
	}
	Id fe8ffd17-9f71-4dde-b830-437b5a58f00a
	Changelog {
		2017-01-14: ""
	}
}


Question SmokingInCOPD {
	"Smoking is the #single biggest cause# of COPD (80%) and the single most important thing you can do in COPD is to stop smoking. 
	
	Are you a smoker?"
	
	YesNo
	Category "smoking"
	
	Triggers {
		Yes: problem Smoking
		Yes: education optional SmokingEducation
	}
	Id 0b957bdf-73d1-46e4-a3aa-546a622ca024
	Changelog {
		2017-01-14: ""
	}
}


Question AnxietyCheck {
	"Attacks of breathlessness can make you upset or frightened. 
	
	If these episodes occur in public it can also cause embarrassment. 
	
	- Are you #reluctant to go out#? 
	- Have you #often been worried# about breathlessness in the last month? 
	- In the past month has this worry #often# made you feel so fidgety or restless that you could not sit still? 
	
	Do #any# of the above 3 questions apply to you? 
	
	[.small]
	(Adapted from Bruce Arroll BMJ 2003; 327:1144-6)"
	
	YesNoUnsure
	Category "lungs", "psychology"
	
	Triggers {
		Yes Unsure: problem DepressionOrAnxietyPresent
	}
	Id 7dac8cf3-8d99-49fc-8b7a-b176781c9795
	Changelog {
		2017-01-14: ""
	}
}


Question COPDActionPlanPatient {
	"The final step is to #design a 'COPD Action Plan'#, which helps you detect, and then #manage worsening symptoms# (an exacerbation or flare up) . 
	
	A plan allows you to recognise a flare up #early# and treat #promptly.# This can reduce both the risk of getting sicker, and of having to go to hospital for admission. 
	
	Do you have a COPD action plan that #you are confident# you can follow?"
	
	YesNoUnsure
	Category "lungs"

	Triggers {
		Yes Unsure: problem COPDCycleOfCare
	}
	Id 1b4f5c22-1653-4a6d-bc71-96de5c35010d
	Changelog {
		2017-01-14: ""
	}
}


Question AustralianLungFoundation {
	"Would you like to know how to join the #Australian Lung Foundation# so you regularly receive more information?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem AustralianLungFoundationReferralNeeded
	}
	Id f05f9f4e-0b27-4864-9af0-245dd089d8dc
	Changelog {
		2017-01-14: ""
	}
}


Question ConsentCOPDitems44and11506 {
	"Obtain consent for item 44 + 11506 (spiro) for annual COPD cycle of care."
	
	Check required
	Category "admin"
	Id e26382f3-8785-7255-9c81-70fa5594131e
	Changelog {
		2017-01-14: ""
	}
}


Question PulseOximetryRoomAir {
	"Record the pulse oximetry reading on room air."
	
	Number required
	Category "lungs"
	Id a4673150-7b71-655e-8029-ed57cfba98f2
	Changelog {
		2017-01-14: ""
	}
}


Question PackYears {
	"Calculate the pack years (= number of cigarettes per day x number of years divided by 20)."
	
	Check required
	Category "smoking"
	Id c5423a6e-ea74-275b-afd1-58402774ab15
	Changelog {
		2017-01-14: ""
	}
}


Question CATScore {
	"Record the CAT score from patient's home score sheet. (If it has not been done, administer)"
	
	Number required
	Category "lungs"
	Id 2cf42f1d-e0f9-e355-9912-03fcbbc53d0e
	Changelog {
		2017-01-14: ""
	}
}


Question ReminderCOPDReview12months {
	"Enter reminder for COPD Cycle of Care in 12 months."
	
	Check required
	Category "admin"
	Id f24d2874-a3c0-be5c-8e61-1c342394b189
	Changelog {
		2017-01-14: ""
	}
}


Question InformCOPDProgressCheckAtGPMP {
	"Tell patient COPD progress will be checked at time of next General GPMP."
	
	Check required
	Category "lungs"
	Id f5958a85-e583-8e54-8359-1a54dc80ba0a
	Changelog {
		2017-01-14: ""
	}
}


Question Spirometry15MinsPostSalbutamol3 {
	"15 minutes after the salbutamol perform 3 post spirometry readings, record in the clinical notes."
	
	Check required
	Category "lungs"
	Id 162b0dcd-9d29-9354-b117-a49cf378dbab
	Changelog {
		2017-01-14: ""
	}
}


Question CheckTiming721and723 {
	"Check timing last item 721 (General GPMP) and 723(TCA). Add reminder for General GPMP."
	
	Check required
	Category "admin"
	Id 6b80e3da-7225-4052-8ef1-de36644f2a98
	Changelog {
		2017-01-14: ""
	}
}


Question COPDSpirometryDiagnosisConfirmed {
	"The #COPD-X plan#, developed by the Australian Lung Foundation (ALF) and the Thoracic Society of Australia and New Zealand, forms the basis for this doctor section. 
	
	(This section is designed for ongoing maintenance of COPD, [red]#NOT as a comprehensive initial workup)#.
	
	[blue]**C***onfirm* diagnosis by spirometry (as required by #PBS# authority rule) using the '#Post 70/80' rule# so use the *post bronchodilator* readings: 
	
	- Obstruction ([blue]*O* of COPD) is confirmed by the FEV1/FVC ratio being less than #70#%. 
	- Pulmonary Disease ([blue]*PD* of COPD) is confirmed by loss of volume. That is, an FEV1 less than #80#% of predicted.

	*Is COPD confirmed?*"
	
	YesNo
	Category "lungs"
	
	Triggers {
		No: problem COPDSpirometryDiagnosisConfirmed
	}
	Id 27551701-920e-45d5-848e-bf53d7037894
	Changelog {
		2017-01-14: ""
	}
}


Question AsthmaPlusCOPDPresent {
	"
	[.big]
	#C# COPD may coexist with asthma. 
	
	#If airflow limitation is fully or substantially reversible the patient should be treated for asthma. (ALF).# 
	
	Reversibility is calculated by using the formula: 
	
	* (FEVI post bronchodilator minus FEV1 pre bronchodilator) divided by FEV1 pre bronchodilator multiplied by 100. 
	
	If it is >12% #and# 200ml then asthma may be present as well as COPD and the patient should be treated as asthmatic. 
	
	If spirometry returns fully to normal it is asthma alone. 
	
	[.big]
	Is there asthma present?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: AutoAnswer {
			COPDWithSomeReversibilityPresent No
		}
		Yes: problem AsthmaPlusCOPD
	}
	Id 1a4ee845-d822-4783-982f-72564e7f408b
	Changelog {
		2017-01-14: ""
	}
}


Question COPDWithSomeReversibilityPresent {
	"
	[.big]
	#C# COPD with some reversibility may benefit from inhaled corticosteroids (ICS). 

	Also presence of either sputum or #blood eosinophilia# is predictive of a response to ICS.
	
	Is there #partial reversibility# (less than 12% and 200mL?)"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem COPDWithSomeReversibilityPresent
	}
	Id beeb4c2d-ff7d-4519-bd54-558e9f3888bc
	Changelog {
		2017-01-14: ""
	}
}


Question CATScoreDiscrepancyPresent {
	"
	[.big]
	#C# Review the #CAT# (COPD assessment test) score for clinical severity. 
	
	* If the clinical severity via CAT is #more severe than spirometry# it is important to check for comorbidities which also cause SOB such as:
	- cardiac disease 
	- lack of fitness 
	- obesity 
	* CAT is also useful for assessing response to a trial of medication with an improvement of 2 or more points being significant. 
	
	
	Is there a #discrepancy# between the CAT score severity and the spirometry severity rating from the previous questions?"
	
	YesNo
	Category "lungs"
	Education "CAT score interpretation" {"
	* https://www.catestonline.org/content/dam/global/catestonline/documents/CAT_HCP%20User%20Guide.pdf[CAT score interpretation]
	"}
	Triggers {
		Yes: problem CATSeverityDiscrepancyPresent
		Yes: education optional CATScoreOnline
	}
	Links {
		CATScore
	}
	Id 2bdac0e6-6c83-49e8-90d4-db9035f88895
	Changelog {
		2017-01-14: ""
	}
}


Question ShortActingBetaAgonistsNeeded {
	"#Optimising function with relievers# 
	
	Short acting bronchodilators provide symptom relief and may increase exercise capacity. 
	
	
	[.big]
	Do you want to supply a short acting bronchodilator to be #used as necessary#?"
	
	YesNoNA
	NAText "N/A - on LAMA already"
	Category "lungs", "medications"
	
	Education "Information on short acting bronchodilators" {"
     
	 There are #2 main types# of short acting bronchodilators or #relievers:# 
	
	- #Short acting beta agonists (SABA)# such as salbutamol and terbutaline with a duration of about 4 hours. 
	- #Short acting muscarinic antagonists (SAMA)# ipratropium (atrovent) with a duration 6 hours. 
	
	They can be used separately or together in mild COPD. 
	
	Ipratropium (SAMA) has a significantly greater effect on lung function than a SABA. 
	
	SABA can be used in all grades of COPD including with both a LAMA and a LABA.

    During an acute attack salbutamol can be used in doses of 400 to 800mcg evry 3 to 4 hours.
	
	However [red]#SAMA must not# be used once LAMA is commenced. 

     https://lungfoundation.com.au/wp-content/uploads/2018/09/Book-COPD-X-Concise-Guide-for-Primary-Care-Jul2017.pdf
    
	"}
	Triggers {
		Yes: problem ShortActingBetaAgonistNeeded
	}
	Id cc3c4c2e-9308-4bb6-8fae-8a556fc07acd
	Changelog {
		2017-01-14: ""
	}
}


Question LongActingBronchodilatorsNeeded {
	"#Long acting bronchodilators# are the most important medications in COPD management. ICS are only added as triple therapy for certain indications. 

    PBS has removed the criteria to stabilise on both individual monotherapy inhalers before commencing a combination inhaler. 

	[.big]
	Do you want to start a LAMA or a LABA?"
	
	YesNoNA
	NAText "N/A - Already on both"
	Category "lungs", "medications"
	
	Education "Information for starting LAMA or LABA" {"
	#2 main types# of bronchodilators:
	
	- Long acting muscarinic antagonists (LAMA)
	- Long acting beta agonists (LABA) 
		
	Start with either type and re-assess at 6 weeks re need to use both. 
	
	- Recent evidence is that LAMA/LABA combination may reduce the frequency of exacerbations compared with (1) LAMA alone or (2) LABA/ICS alone (FLAME study). 
	- 1/8/18 PBS removed the requirement to stabilise on both individual monotherapy inhalers before commencing a fixed dose combination LAMA/LABA.
	- 2017 GOLD COPD Guidelines advocate optimising bronchodilation with dual LAMA/LABA before introducing ICS except in patients with concomitant asthma. 
	
	For moderate COPD and definitely for severe COPD you will often need both LAMA and LABA.

	== References

	* https://lungfoundation.com.au/wp-content/uploads/2018/09/Information-paper-Stepwise-Management-of-Stable-COPD-Jan19.pdf[Stepwise Management of COPD (2019), Lung Foundation of Australia]
	"}
	Triggers {
		Yes: problem COPDModerateSeverity
		Yes: education optional COPDStepwiseManagement
	}
	Id 0d74b90e-6977-4a42-ae68-f37ab98413ac
	Changelog {
		2017-01-14: ""
	}
}


Question COPDAndRoleOfICSExplanationNeeded {
	"
	[.big]
	#O = optimise function# 
	
	Inhaled corticosteroids (ICS) are #only indicated in certain# COPD patients. #PBS# authority for ICS now requires a spirometry diagnosis. 
			
	- (click below for list of PBS inhalers) 

	[.big]
	Do want to initiate triple therapy by starting an ICS? 
	
	image:COPDAndRoleOfICS.jpg[width=1136]"
	
	YesNoNA
	NAText "N/A - on ICS already"
	Category "lungs", "medications"
	
	Education "Indications for ICS in COPD" {"

     * severe COPD = <50% FEV1 with 2 or more exacerbations per year 
	* or * 
	* asthma/COPD overlap syndrome (PH asthma or bronchodilator reversibility or blood eosinophilia). 
	
	If FEV1>50% and no exacerbations in the last 12 months ICS should be judiciously back titrated and, if no problems, ceased because there is *increased risk of pneumonia with ICS use*. (WISDOM trial)
	"}
	Triggers {
		Yes: problem COPDAndRoleICSExplanationNeeded
		Yes: education optional COPDStepwiseManagement
	}
	Id 2ee9329c-d709-4c17-b8f9-3c83ae0f42df
	Changelog {
		2017-01-14: ""
	}
}


Question COPDTripleTherapyNeeded {
	"
	[.big]
	#O = optimise function# 
	
	#Triple therapy# refers to using: 
	
	- LAMA (long acting muscarinic antagonist) 
	
	plus 
	
	- ICS (inhaled corticosteroid)
	
	plus 
	
	- LABA (long acting beta agonist) 
	
	Triple therapy can be either LAMA + combination ICS/LABA or single inhaler (Trelegy Ellipta). It is used if there are frequent exacerbations (2 or more in last 12 months) + severe COPD (FEV1 <50%). 
	
	[small]#(click below for PBS inhalers)#
	
	Is triple therapy #indicated#?"
	
	YesNoNA
	NAText "N/A - not appropriate "
	Category "lungs", "medications"
	
	Triggers {
		Yes: problem COPDSevere
		Yes: education optional COPDStepwiseManagement
	}
	Id 0121ff9e-b0fd-485d-9899-f1f868f9c36a
	Changelog {
		2017-01-14: ""
	}
}


Question PulmonaryRehabilitationNeeded {
	"
	[.big]
	#O# #Pulmonary rehabilitation# reduces dyspnoea, fatigue, anxiety and depression as well as reducing hospitalization (AFL). 

    In patients with recent COPD hospitalisation, the NNT is 5-6 to prevent another hospitalisation.
	
	Based on their #severity do you want to refer# for pulmonary rehabilitation?"
	
	YesNo
	Category "lungs", "medications"
	
	Triggers {
		Yes: problem PulmonaryRehabilitationNeeded
		Yes: education optional CATScoreInterpretation
	}
	Id dc4331f3-5337-499f-964e-209f65c2d267
	Changelog {
		2017-01-14: ""
	}
}


Question PhysiotherapyNeeded {
	"#O# COPD patients #with sputum production# (chronic bronchitis and bronchiectasis and some patients in acute flare ups) may benefit from #airways clearance instruction# (ALF). 
	
	Do they need #referral for physiotherapy# instruction?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem PhysiotherapyNeeded
	}
	Id 581e105d-2cda-4a91-9735-cd3a789ce896
	Changelog {
		2017-01-14: ""
	}
}


Question SputumTestSupplyNeeded {
	"
	[.big]
	#X# In acute flare ups COPD patients do not routinely require  #sputum C/S# unless lack of response, or repeated infections, or with co-existent bronchiectasis. 
	
	If you decide to do this, a collection jar with a pathology form should be provided for their next exacerbation. 
	
	Do you want to provide a sputum test jar and path slip?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem SputumTestSupplyNeeded
	}
	Id 57e957e1-f8db-4374-b306-72ab81f6ffaa
	Changelog {
		2017-01-14: ""
	}
}


Question COPDNutritionAdviceNeeded {
	"
	[.big]
	#O# Both #low# and #excess weight# is associated with increased morbidity. 
	
	* Obesity increases the work of breathing and predisposes to sleep apnoea. 
	* Progressive weight loss or BMI < 20 are both important prognostic factors for poor survival. 
	
	Do they want to #refer to a dietitian#?"
	
	YesNo
	Category "lungs", "lifestyle"
	
	Triggers {
		Yes: problem COPDNutritionAdviceNeeded
	}
	Id 5062b48b-56c4-4bef-a95e-50b987ab5dbb
	Changelog {
		2017-01-14: ""
	}
}


Question SignificantCo-morbiditiesPresent {
	"
	[.big]
	#O# #Co-morbidities# causing increased risk are common in COPD patients: 
		
	Are any significant co-morbidities present?"
	
	YesNo
	Category "lungs"
	Education "Increased risk from comorbidities in the presence of COPD" {"
    Comorbidites causing increased risk: 
    
    - Cardiovascular disease (CHD, CHF, Stroke, Arrhythmias). 

	- Lung Disease ( Lung cancer, Bronchiectasis, Pulmonary Fibrosis and emphysema). 

	- Fragility and Falls. 

    - Aspiration from disordered swallowing.

    - Sleep related breathing disorders.

    - GORD.

    - Alcohol and sedatives.

    - Testosterone deficiencies and supplementation. 

     * https://copdx.org.au/copd-x-plan/o-optimise-function/o7-comorbidities/o7-1-increased-risks-from-comorbidities-in-the-presence-of-copd/
          	"}
	Triggers {
		Yes: problem SignificantCo-morbiditiesPresent
	}
	Id d0261f6c-5d4e-46cb-bb85-2452a58d2d8a
	Changelog {
		2017-01-14: ""
	}
}


Question COPDAndBetaBlockersNeeded {
	"
	[.big]
	#O# Drug safety. A recent Cochrane review indicated that #cardio selective beta blockers are safe# in COPD (ALF). 
	
	Heart disease is a major comorbidity in many COPD patients. 
	
	Does this patient have an indication for beta blockers to continue?"
	
	YesNo
	Category "lungs", "medications"
	
	Triggers {
		Yes: problem COPDAndBetaBlockersNeeded
	}
	Id 4b9500ae-96e6-4a0b-b20a-6077b7c0dec3
	Changelog {
		2017-01-14: ""
	}
}


Question COPDOsteoporosisPreventionAndTreatmentNeeded {
	"
	[.big]
	#O# Prevent or treat #osteoporosis#.
	
	Increased risk in COPD due to the disease itself, as well as oral steroids and reduced activity. 
	
	Calcium + vitamin D is relevant if in hostel care or poor outdoor activity. 

	High risk = risk calculator >3% hip or >20% any fracture, or BMD T score <2.5.
	
	Do you want to perform a #calculation of fracture risk# and/or order a #bone density#?"
	
	YesNo
	Category "lungs", "osteoporosis"
	
	Triggers {
		Yes: problem OsteoporosisPreventionNeeded
		Yes: education optional QfractureRiskCalculator
	}
	Id 9923b2c6-5e72-4781-8df7-e694dc35909d
	Changelog {
		2017-01-14: ""
	}
}


Question HypoxemiaPresent {
	"
	[.big]
	#O# #Hypoxemia# in COPD patients (ALF): 
	
	Should be identified and corrected with long term oxygen therapy as this improves survival and quality of life. 
	
	
	[.big]
	Do you want to refer for arterial blood gas measurement and consideration of oxygen treatment?"
	
	YesNo
	Category "lungs"
	
	Education "Diagnosis and treatment of hypoxemia" {"
	Long term oxygen therapy is >15 hours per day and prolongs life in hypoxemic patients. 
	
	Best #screening# is via pulse oximetry on room air. Positive if SaO2 <92 %. 
	
	It must be confirmed by ABG at pathology with a Pa02 <55mmhg (or <60mmhg if pulmonary hypertension). 

	Then refer to Respiratory physician for oxygen therapy initiation.
	"}
	Triggers {
		Yes: problem HypoxemiaPresent
	}
	Links {
		PulseOximetryRoomAir
	}
	Id aa703cc4-f20e-4755-8e84-edfd960798e1
	Changelog {
		2017-01-14: ""
	}
}


Question PneumococcalVaccinationCOPDNeeded {
	"
	[.big]
	#P# Prevent deterioration with the appropriate schedule of pneumococcal immunisations. 
	
	Is pneumococcal immunisation needed?"
	
	YesNo
	Category "prevention"
	
	Triggers {
		Yes: problem PneumococcalImmunisationNeeded
        Yes: education optional PneumococcalVaccinationNIP
	}
	Id 5bd74873-a4cb-474f-80c1-724b816f4b7c
	Changelog {
		2017-01-14: ""
	}
}


Question FormulationOfTCAItem723Needed {
	"*A* [blue]*support network* *can be very important in people with chronic medical conditions.*

	* PAT generates an item 721(GPMP) + item 723(TCA) plan = old EPC PLAN. 
	* Medicare requires communication with the allied health care providers when you do the 723, so it is #often more convenient to arrange another appointment for the 723.#

	*Does the patient need formulation of a TCA item 723 with 2 or more health care providers involved?*"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem CareFrom2OrMoreOtherHealthCareProvidersOtherThanYourself
	}
	Id 9ab03237-e56f-4cef-9ec9-faafda4569a1
	Changelog {
		2017-01-14: ""
	}
}


Question COPDSpecialistReferralNeeded {
	"
	[.big]
	#D# Do they need referral for specialist review?"
	
	YesNo
	Category "lungs"
	Education "Indications for specialist referral" {"
    
     * https://copdx.org.au/copd-x-plan/confirm-diagnosis/c5-specialist-referral/?highlight=specialist%20referral
          	"}
	Triggers {
		Yes: problem COPDSpecialistReferralNeeded
		
	}
	Id 5f8b560a-b13b-4868-b699-0930bacc7dc8
	Changelog {
		2017-01-14: ""
	}
}


Question COPDActionPlanNeeded {
	"
	[.big]
	#X# Early diagnosis and treatment of exacerbations #within 24 hours# of onset may prevent hospitalization. 
	
	Do you want to develop a self management #COPD Action Plan# with the patient #AND print# it out?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem COPDCycleOfCare
		Yes: education optional COPDActionPlan2017
	}
	Id c90f1e4b-5764-4268-ad67-10e1be89b6c4
	Changelog {
		2017-01-14: ""
	}
}


Question ScriptsForCOPDNeeded {
	"
	[.big]
	#X# #Systemic steroids# shorten the duration and lessen the severity of #exacerbations#. (ALF)
	
	A recent study found 40mg prednisolone for 5 days (no tapering) non-inferior to 14 days of 30 to 50 mg prednisolone. 
	
	#Blood eosinophil count >2% is a useful biomarker# to determine which patients will benefit. 
	
	Exacerbations with clinical signs of infection #benefit from antibiotic therapy# (ALF). Antibiotic guidelines recommend doxycycline or amoxycillin for 7 to 10 days. 
	
	Have you ensured that they have #adequate scripts#, both for maintenance therapy, and prednisolone and antibiotics for flare ups?"
	
	YesNo
	Category "lungs", "medications"
	
	Triggers {
		No: problem ScriptsForCOPDNeeded
	}
	Id 89019809-15b7-4b91-a802-f89f0acbe1e5
	Changelog {
		2017-01-14: ""
	}
}


Problem CATSeverityDiscrepancyPresent {
	Goal "Reconcile discrepancy."
	Plan "Assign a severity rating to the COPD and treat."
	Id 01af3d5c-2eac-473e-8551-4cdff276a20b
	Changelog {
		2017-01-14: ""
	}
}


Problem COPDInflammatoryNatureExplanationNeeded {
	Goal "Understand disease process."
	Plan "Clarify any lack of knowledge. Emphasize that smoking is the most common irritating agent and the single most important step in COPD is to stop smoking."
	Id 02e961a8-c64e-4c89-a7ba-5ae20ac25a06
	Changelog {
		2017-01-14: ""
	}
}


Problem ShortActingBronchodilatorsNeeded {
	Goal "Understand action and first line role in mild COPD."
	Plan "Clarify any lack of knowledge."
	Id 0b1efc84-73fd-465e-bfde-cfde2d0554e6
	Changelog {
		2017-01-14: ""
	}
}


Problem COPDSpirometryDiagnosisConfirmed {
	Goal "Confirm COPD diagnosis"
	Plan "If diagnosis is not confirmed review history."
	Id a7011ef5-c3cc-4371-831f-5a150cb5ae07
	Changelog {
		2017-01-14: ""
	}
}


Problem COPDWithSomeReversibilityPresent {
	Goal "Clarify if benefit from ICS."
	Plan "Lower doses of ICS than used previously may provide equivalent benefit. Monitor response with both a patient questionnaire e.g. CAT, and spirometry. Note ICS monotherapy is not approved for COPD long term."
	Id cde749cf-1f63-4b0a-99ee-3c6399faf027
	Changelog {
		2017-01-14: ""
	}
}


Problem COPDAndRoleICSExplanationNeeded {
	Goal "Clarify if benefit from ICS."
	Plan "ICS should be added to LAMA and LABA and not used as monotherpy."
	Id 49c3b471-1fea-45b1-bd39-4162937e5afa
	Changelog {
		2017-01-14: ""
	}
}


Problem ShortActingBetaAgonistNeeded {
	Goal "Understand action and first line role"
	Plan "Use beta agonist first line and add ipratropium if needed but not for regular use."
	Id d3a56e3f-54a5-4a2f-b2aa-9d80f8c2c43c
	Changelog {
		2017-01-14: ""
	}
}


Problem PulmonaryRehabilitationNeeded {
	Goal "Improve physical capacity in all severe COPD"
	Plan "Refer to nearest pulmonary rehabilitation unit using appropriate referral form."
	Id 4aa82e57-b5b8-4702-8d48-bcacedec4655
	Changelog {
		2017-01-14: ""
	}
}


Problem PhysiotherapyNeeded {
	Goal "Sputum clearance"
	Plan "Strongly consider referral under EPC."
	Id 69254646-cad2-4920-8a6a-84c86b9f210b
	Changelog {
		2017-01-14: ""
	}
}


Problem COPDNutritionAdviceNeeded {
	Goal "Optimize weight."
	Plan "Strongly consider referral to a dietitian under EPC."
	Id 17d0c3d5-b10b-4e76-9694-24187f8e2641
	Changelog {
		2017-01-14: ""
	}
}


Problem SignificantCo-morbiditiesPresent {
	Goal "Clarify if present"
	Plan "Add to health summary and institute appropriate follow-up for treatment."
	Id 911c132f-44c9-45d5-8da9-5fdfc5c6474a
	Changelog {
		2017-01-14: ""
	}
}


Problem COPDAndBetaBlockersNeeded {
	Goal "Appropriate use beta blockers"
	Plan "Institute beta blocker use if clinically indicated for cardiovascular disease."
	Id 75f05d4d-971b-47f9-896e-2536333ada90
	Changelog {
		2017-01-14: ""
	}
}


Problem HypoxemiaPresent {
	Goal "Detect and treat"
	Plan "Confirm with ABG and refer for specialist review if present."
	Id 144734f8-dbc8-43c5-b66b-dbd65fdf13e4
	Changelog {
		2017-01-14: ""
	}
}


Problem COPDSpecialistReferralNeeded {
	Goal "Appropriate specialist referral."
	Plan "Check the indications via the educate button."
	Id 8065a4a4-0834-467b-874d-fca0afc536bb
	Changelog {
		2017-01-14: ""
	}
}


Problem ScriptsForCOPDNeeded {
	Goal "Ensure adequate scripts for both long term and flare up medications."
	Plan "Prednisolone course is 30 to 50 mg for less than 2 weeks & no tapering. Antibiotic guidelines recommend doxycycline or amoxycillin for 7 to 10 days."
	Id 8f53a765-1845-41f5-945b-90ead860dbfc
	Changelog {
		2017-01-14: ""
	}
}


Problem SputumTestSupplyNeeded {
	Goal "Collect before commencement of antibiotics."
	Plan "Supply pathology form and collection jar. Inform that fever or colour change in sputum is an indication for collection."
	Id 1b6c6122-76fc-4d69-949e-876515cb04a0
	Changelog {
		2017-01-14: ""
	}
}


Problem AustralianLungFoundationReferralNeeded {
	Goal "Encourage all COPD to join."
	Plan "Website is www.lungfoundation.com.au."
	Id 869babc4-1db2-415f-888f-8df595c7338d
	Changelog {
		2017-01-14: ""
	}
}


Education COPDSpecialistReviewIndicationsRtf {
	Message "COPD specialist review indications "
	File "rtf"
	Id 1d3a3bd7-e7a6-4b57-b60b-00baf10c35f6
	Changelog {
		2017-01-14: ""
	}
}


Education COPDProcess {
	File "jpg"
	Id e12371da-9b12-49af-9e74-ef079d1cdc44
	Changelog {
		2017-01-14: ""
	}
}


Education QfractureRiskCalculator {
	Message "QFracture risk calculator"
	Link "https://qfracture.org"
	Id d859dcf6-cbdd-4210-9cc0-abd271f487f5
	Changelog {
		2017-01-14: ""
	}
}


Education COPDCombinationInhalerAction {
	File "jpg"
	Id 9b9efa56-e39e-4b3f-b08a-c0946af9eb52
	Changelog {
		2017-01-14: ""
	}
}


Education CATScoreOnline {
	Message "CAT score online"
	Link "https://www.catestonline.org/patient-site-test-page-english.html"
	Id 6245d23c-a874-4595-bfc4-7cd026bd739d
	Changelog {
		2017-01-14: ""
	}
}


Education COPDActionPlan2017 {
	Message "COPD Action Plan"
	Link "http://lungfoundation.com.au/wp-content/uploads/2014/02/LFA-COPD-Action-Plan-Traffic-Lights-Steps-for-writting_0316_print-2_editable.pdf"
	Id 0a6c93c6-7bfb-428d-9344-f99ac17ab4a7
	Changelog {
		2017-01-14: ""
	}
}


Education CATScoreInterpretation {
	Message "CAT score user guide"
	Link "https://www.catestonline.org/content/dam/global/catestonline/documents/CAT_HCP%20User%20Guide.pdf"
	Id 8f512bdb-9d98-4e9a-bdca-6c99d725503b
	Changelog {
		2017-01-14: ""
	}
}


