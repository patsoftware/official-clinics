/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with inflammatory arthritis (except for gout) */
Clinic GPMPArthritisInflammatory {
	Id 95e892ed-696a-4ea4-92dd-0b960ab76406
	Filter ArthritisInflammatory
	Questions {
		Patient {
			PainChartNeeded
			ParacetamolNotControllingPain
			OTCMedicationsNotProvenUseful
			OsteoporosisPreventionCalcium
			MobilityAidNeeded		
			FootProblem
		}


		Checklist {
		}

		Staff {
			ArthritisPatternAnalysisNeeded
			GoutyArthritisPresent
			DMARDUnderstandingNeeded
			SteroidAdverseEffectsAnalysisNeeded
			CardiovascularFactorsPresent
			Omega3UseNeeded
			OrthopaedicReferralNeeded
			PhysioAssessmentNeeded
		}

	}
}

Clinic GPMPArthritisInflammatoryCore {

	Id 95e892ed-696a-4ea4-92dd-0b960ab76407
	Filter ArthritisInflammatory
	Questions {
		Patient {
			PainChartNeeded
			ParacetamolNotControllingPain
			OTCMedicationsNotProvenUseful
			OsteoporosisPreventionCalcium
			MobilityAidNeeded		
			FootProblem
		}


		Checklist {
		}

		Staff {
			DMARDUnderstandingNeeded
		}

	}
}
Filter ArthritisInflammatory {
	Match {
		keyword "Ankylosing spondylitis"
		// TODO: PAT should raise an error if PMH simply has "arthritis"
		// keyword "Arthritis"
		keyword "Arthropathy in Behcet"
		keyword "Connective tissue disease"
		keyword "Enteropathic arthritis"
		keyword "Inflammatory arthritis"
		keyword "Inflammatory arthropathy"
		keyword "Inflammatory monoarthropathy"
		keyword "Inflammatory polyarthropathy"
		keyword "Juvenile arthritis"
		keyword "RA"
		keyword "Rheumatoid arthritis"
		keyword "Seronegative"
		keyword "Still's disease"
	}
	Tests matching only this {
		"Inflammatory arthritis"
		"Inflammatory polyarthropathy"
		"Inflammatory monoarthropathy"
		"Inflammatory arthropathy"
		"seronegative arthritis"
		"seronegative Rheumatoid arthritis"
		"seronegative"
		"RA"
		"ankylosing spondylitis"
		"Rheumatoid arthritis"
		"Still's disease"
		"juvenile arthritis"
		"arthropathy in Behcet's syndrome of the shoulder region"
		"enteropathic arthritis"
	}
}

Question FootProblem {
	"Common foot problems that need podiatry care include:

	* Corns or calluses on the skin that are thick or painful

	* Thick or deformed toe nails that are difficult to cut 

	* Bone or joint deformity that causes rubbing on your shoes
	
	Have you got any foot problems that may need care by a podiatrist?
    
    "
    
    YesNo
    Category "feet"
    Triggers {
		Yes: problem FootStructuralAbnormalityCareNeeded
	}
    Id b7e35d1c-ffa0-4f8d-a3d4-a9a8d27119fc
    Changelog {
        2018-08-17: ""
    }
}



Question OTCMedicationsNotProvenUseful {
	"There are some #OTC# (over the counter) #medications# for which there is #no evidence# of usefulness in osteoarthritis: 

	- Fish oil
	- Vitamins, e.g. vitamin C
	- Herbal remedies e.g. ginger, Salix (willow)
	- Mineral supplements
	- Dietary supplements e.g. New Zealand mussel powder
	- Aromatherapy

	Are you using any product that you #wish to discuss# with your doctor? 

	[small]#(RACGP Osteoarthritis Guideline)#"
	
	YesNoUnsure
	Category "arthritis"
	
	Triggers {
		Yes Unsure: problem OTCMedicationsNotUseful
	}
	Id f51f2431-8515-44b8-a552-bd9d4b595e51
	Changelog {
		2017-01-14: ""
	}
}


Question OsteoporosisPreventionCalcium {
	"Prevention of #osteoporosis# (weak bones) is important. 
	
	Enough #calcium# from dairy products and #sun exposure# for vitamin D production is the ideal way. 
	
	Tablets with calcium and vitamin D are used #only# if diet or sun exposure is not ideal (e.g. nursing home) 
	
	Your doctor will discuss measuring bone density and whether you need any other medication for your bones. 
	
	Do you wish to discuss taking calcium and/or vitamin D tablets?
	
	[small]#(RACGP Osteoporosis Guideline)#"
	
	YesNo
	Category "osteoporosis"
	
	Triggers {
		Yes: problem OsteoporosisPreventionNeeded
	}
	Id 10558e84-d2d0-40ee-86ae-05f802531cb8
	Changelog {
		2017-01-14: ""
	}
}


Question ArthritisPatternAnalysisNeeded {
	"There are over 100 types of arthritis, but the #two main groups# are 
	
	* #Inflammatory:# e.g. rheumatoid, lupus, sero-negative, psoriatic, ankylosing spondylitis, other connective tissue
	* #Degenerative:# primary polyarticular osteoarthritis and secondary (post damage) osteoarthritis. 
	
	Would you #classify# the patient's arthritis as inflammatory arthritis?"
	
	YesNoNA
	NAText "N/A - no arthritis any type"
	Category "arthritis"
	
	Triggers {
		No NA: AutoAnswer {
			Omega3UseNeeded NA
			CardiovascularFactorsPresent NA
			SteroidAdverseEffectsAnalysisNeeded NA
			DMARDUnderstandingNeeded NA
		}
		Yes: problem ArthritisPatternInflammatoryPresent
	}
	Id e63ef78d-ba26-479f-876a-4e54cdb66ab7
	Changelog {
		2017-01-14: ""
	}
}


Question DMARDUnderstandingNeeded {
	"To #optimise# management of an #inflammatory arthritis# Disease Modifying Anti Rheumatic Drugs (#DMARDs#) are prescribed. 
	
	They are powerful agents designed to induce a remission and require regular checks. 
	
	Click below for more information on the use of DMARDS. 
	
	Do you want to bring them back for review to ensure the patient #understands# which medication is their DMARD and what #monitoring# is required. 
	"
	
	YesNoNA
	Category "arthritis"

	Education "References" {"
		* https://www.nps.org.au/news/shared-care-approaches-to-rheumatoid-arthritis-supporting-early-and-sustained-methotrexate[Shared care approaches to rheumatoid arthritis: supporting early and sustained methotrexate, Medicinewise News, 28 February 2018]
	"}
	
	Triggers {
		Yes: problem DMARDNeeded
		Yes: education optional DMARDS
	}
	Id 60071c36-e144-4a56-92a7-7b1be8d3d8af
	Changelog {
		2017-01-14: ""
	}
}


Question SteroidAdverseEffectsAnalysisNeeded {
	"#Glucocorticoid adverse effects# are related to average and cumulative oral dose. 
	
	Do you want to bring them back for review?"
	
	YesNoNA
	NAText "N/A - not on oral steroids"
	Category "medications"
	
	Triggers {
		Yes: problem SteroidsAdverseEffectsPossible
		Yes: education optional SteroidToxicity
	}
	Id ac932969-de11-46ec-ace0-ba9a239eb9bf
	Changelog {
		2017-01-14: ""
	}
}


Question CardiovascularFactorsPresent {
	"Because rheumatoid arthritis #increases risk# of cardiovascular #(CV) disease:# 
	
	* perform an Absolute Cardiovascular Risk calculation (and, for those who are sero positive, or who have had the disease for 10 years, multiply risk by a factor of 1.5). 
	
	* check for occult symptoms of CV disease. 
	
	Do you want to modify treatment because of CV factors?"
	
	YesNoNA
	Category "arthritis", "Heart / Brain"
	
	Triggers {
		Yes: problem AbsoluteCardiovascularRiskIsMediumOrHigh
	}
	Id 3608a640-89c6-4cda-a737-e654c1a04f82
	Changelog {
		2017-01-14: ""
	}
}


Question Omega3UseNeeded {
	"#Omega 3# polyunsaturated fatty acid supplementation should be recommended for pain and stiffness in all #RA# patients.
	
	* Grade A recommendation (RACGP RA Guidelines)
	* Higher doses are likely to be of greatest benefit in RA (up to 12gm daily) 
	* Benefit has not been confirmed in OA patients yet. 
	
	Do you want to start omega 3 supplements?"
	
	YesNoNA
	NAText "N/A - no Rheumatoid"
	Category "arthritis"
	
	Triggers {
		Yes: problem Omega3Needed
	}
	Id af4616a8-b9d1-4f9b-b439-19c14c4d3b6c
	Changelog {
		2017-01-14: ""
	}
}


Problem ArthritisPatternInflammatoryPresent {
	Goal "Classify for appropriate treatment"
	Plan "Ensure appropriate teamwork with early rheumatologist referral. Ensure appropriate monitoring is being done for any DMARD in use."
	Id 0e141f71-22aa-48be-a936-971b3639dd18
	Changelog {
		2017-01-14: ""
	}
}


Problem DMARDNeeded {
	Goal "Understand DMARD action and monitoring"
	Plan "Arrange reminder for review to explain action, common side effects, and monitoring frequency. At review ensure appropriate team work with a rheumatologist. Consider HMR with pharmacist. Ensure contraception for fertile female on DMARD. Arrange annual skin cancer check reminder because of the immunosuppression."
	Id 5768b46a-cae8-4f6d-b50e-c2d5590ae7d8
	Changelog {
		2017-01-14: ""
	}
}


Problem OTCMedicationsNotUseful {
	Goal "Reduce use"
	Plan "Explain. Consider involving pharmacist with HMR if complex."
	Id d47a5b4a-7090-4362-b77c-7de0124bb64f
	Changelog {
		2017-01-14: ""
	}
}




Problem Omega3Needed {
	Goal "Offer Omega 3 to all RA patients"
	Plan "Prescribe high dose omega 3 (12gm daily)."
	Id d76936ba-1dba-497f-b472-b96839b83a86
	Changelog {
		2017-01-14: ""
	}
}


Problem SteroidsAdverseEffectsPossible {
	Goal "Minimise development or progression of adverse effects"
	Plan "Arrange reminder for review: referral for annual eye check and pre-order lipids & BGL if not done. Check BP. If adrenal suppression suspected, check mane cortisol level after withholding mane steroid dose. Educate to present early if sick because of the immunosuppression."
	Id d0b9c5aa-946e-41ed-a3a9-0f6801eb9442
	Changelog {
		2017-01-14: ""
	}
}


Education DMARDS {
	File "rtf"
	Id 99320d92-0e1b-4650-a386-54f6573e9649
	Changelog {
		2017-01-14: ""
	}
}


Education SteroidToxicity {
    Message "Glucocorticoid adverse effects"
	File "rtf"
	Id 7dfc8caa-17a0-4eb0-a932-205193dab611
	Changelog {
		2017-01-14: ""
	}
}


