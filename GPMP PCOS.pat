/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with PCOS */
Clinic GPMPPCOS {
	Id 85f6a6e5-8797-4411-8fe1-6340786f7bbf
	Filter PCOS
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			PCOSPresent
		}

	}
}

Filter PCOS {
	Match {
		keyword "PCOS"
		keyword "polycystic ovaries"
	}
	Tests matching only this {
		"pcos"
		"polycystic ovaries"
		"Polycystic ovary syndrome"
	}
}

Question PCOSPresent {
	"There is an international evidence-based guideline for polycystic ovary syndrome (PCOS) incorporating resources for GPs developed by Monash University.

	Do you want to add PCOS to the care plan?"
    
    YesNoNA
	NAText "PCOS not present (check active problems)"
	Category "Women's health"

	Education "Resources" {"
     
	 * https://www.monash.edu/__data/assets/pdf_file/0010/1459243/PCOS-GP-Tool-20180815.pdf[Monash PCOS GP Tool]
	 * https://www.monash.edu/__data/assets/pdf_file/0004/1412644/PCOS-Evidence-Based-Guideline.pdf[International evidence-based guideline for the assessment and management of polycystic ovary syndrome 2018]
	"}

    Triggers {
		Yes: problem PCOSEducationNeeded
		Yes: problem PCOSSymptomsPresent
		Yes: problem PCOSFertilityAdviceNeeded
		Yes: problem PCOSEndometrialProtectionNeeded
	}
    Id bc2e2600-385c-4691-9ae8-7166dc1dd55f
}

Problem PCOSEducationNeeded {
	Goal "Be informed about PCOS"
    Plan "
	Excellent resources are available online by searching for 'Monash Resources for Women with PCOS'.
	You can also install the Monash AskPCOS app.
	"
    Id 743b9bc9-9d18-4662-a51f-cba9f4e87d57
}

Problem PCOSEndometrialProtectionNeeded {
	Goal "Regulate periods and protect  endometrium"
    Plan "Aim for ≥4 cycles per year. If not achieving this, first line use combined oral contraceptives (COCP). Lifestyle factors ± metformin."
    Id 743b9bc9-9d18-4662-a51f-cba9f4e87d58
}

Problem PCOSSymptomsPresent {
	Goal "Assess impact on quality of life, reduce adverse impact, meet patient expectations"
    Plan "Treat symptoms (e.g. hirsutism, alopecia, acne) cosmetically (e.g. laser hair removal), and/or with a combined oral contraceptives for at least 6-12 months."
    Id 743b9bc9-9d18-4662-a51f-cba9f4e87d59
}

Problem PCOSFertilityAdviceNeeded {
	Goal "Improve fertility chances"
    Plan "
	Reassure most PCOS achieve desired family size but many need oral medication.
	Letrozole is first line for ovulation induction.
	Prevent weight gain ± weight loss.
	Consider trying to conceive prior to 35.
	Refer to fertility specialist after 12 months (<35) or 6 months (>35).
	Preconception care."
    Id 743b9bc9-9d18-4662-a51f-cba9f4e87d50
}
