/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with GORD */
Clinic GPMPGORD {
	Id b70b43c3-e0c4-475c-acba-b9c8282752fe
	Filter GORD
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			GORDPresent
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter GORD {
	Match {
		keyword "GERD"
		keyword "GORD"
		keyword "Oesophagitis"
		keyword "Reflux"
	}
	Tests matching only this {
		"GERD"
		"GORD"
		"Reflux"
		"Gastro-oesophageal reflux"
	}
}


Question GORDPresent {
	"With #gastro oesophageal reflux disease# data is accumulating re possible long term effects of PPIs.

    Physiological studies show acid rebound hypersecretion after cessation PPIs, so if withdrawing, tapering over 4-8 weeks is recommended. 
	
	So, if they are both 
	
	* Well controlled on medication 

	And 

	* Have no complications requiring follow-up with a specialist (such as Barrett's oesophagus, past history of stricture or dilatation). 
	
	Do to wish to #step down use#?"
	
	YesNoNA
	NAText "N/A - No GORD present"
	Category "medications"

	Education "Stepping the appropriate path with GORD medicines" {"
		
	*
	https://nps.org.au/news/stepping-the-appropriate-path-with-gord-medicines

	"}
	

	Triggers {
		Yes: problem GORDPresent
		Yes: education optional ReviewPPIUse
	}
	Id 05b1f7ab-eb03-4fb5-9f3c-8835b216d947
	Changelog {
		2017-01-14: ""
	}
}


Problem GORDPresent {
	Goal "Control symptoms and prevention complications"
	Plan "If controlled review use."
	Id 6727f854-7067-4a07-a12d-35a0bad40543
	Changelog {
		2017-01-14: ""
	}
}


Education ReviewPPIUse {
	File "rtf"
	Id 40b185b1-ee45-4690-b087-57e079edfe0b
	Changelog {
		2017-01-14: ""
	}
}


