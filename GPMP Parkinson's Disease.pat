/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with Parkinson's Disease */
Clinic GPMPParkinsonsDisease {
	Id b9000e53-d4d2-409c-9162-875a6fd004d5
	Filter ParkinsonsDisease
	Questions {
		Patient {
			ParkisonsWebsiteReferralNeeded
		}

		Checklist {
		}

		Staff {
			ParkinsonsAdverseSymptomsPresent
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter ParkinsonsDisease {
	Match {
		keyword "Parkinsonism"
		keyword "Parkinson's disease"
	}
	Tests matching only this {
		"Parkinsonism"
		"Parkinson's disease"
		"Parkinson's"
	}
}


Question ParkisonsWebsiteReferralNeeded {
	"Would you like to get #extra information# about Parkinson's Disease from the national website?"
	
	YesNoNA
	NAText "N/A - No Parkinson's Disease"
	Category "neurology"
	
	Triggers {
		Yes: problem ParkinsonsAustraliaWebsiteReferralNeeded
	}
	Id ea875737-995c-4bf9-a774-ef343023d482
	Changelog {
		2017-01-14: ""
	}
}


Question ParkinsonsAdverseSymptomsPresent {
	"Once stable on Parkinson's treatment, long term monitoring is recommended including for non motor symptoms: 
	
	Do you want to bring them back for any such symptoms?"
	
	YesNoNA
	NAText "N/A - No Parkinson's Disease"
	Category "neurology"
	
	Education "Non-motor symptoms of Parkinson's to monitor" {"
		#Adverse events# such as 

	* somnolence 
	* sudden onset sleep 
	* impulse control disorder 

	#Cognitive / psychiatric# non-motor symptoms such as

	* dementia 
	* mood disorders especially depression 
	* psychosis 

	#Autonomic# non-motor symptoms such as

	* orthostatic hypotension 
	* constipation 

	== References
	* https://www.nps.org.au/australian-prescriber/articles/management-of-parkinson-s-disease[Management of Parkinson's disease, Australian Prescriber, 2012, DOI 10.18773/austprescr.2012.084]
	"}
	
	Triggers {
		Yes: problem ParkinsonsAdverseSymptomsPresent
	}
	Id b24883fa-aacc-435f-b978-1c4e47452d2c
	Changelog {
		2017-01-14: ""
	}
}


Problem ParkinsonsAustraliaWebsiteReferralNeeded {
	Goal "Latest information and connection with self-help groups"
	Plan "Visit Parkinson's Australia for information and support: https://www.parkinsons.org.au"
	Id a1464dd0-3f64-4303-9969-12c9142918b7
	Changelog {
		2017-01-14: ""
	}
}


Problem ParkinsonsAdverseSymptomsPresent {
	Goal "Reduce adverse and non-motor Parkinson's symptoms"
	Plan "Arrange appropriate reminder for check up. At check up: Viagra for impotence. Movicol for constipation. Modafinil for somnolence. ?Melatonin for insomnia. ?Methylphenidate for fatigue. SNRI may be more effective than SSRI for depression. Either can be used for anxiety. Exelon or Aricept for dementia."
	Id 560d2983-0fd8-4b38-b55b-a2f5520746b4
	Changelog {
		2017-01-14: ""
	}
}


