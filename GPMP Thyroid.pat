/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with thyroid disease */
Clinic GPMPThyroid {
	Id 405c9914-3653-49b0-8dd5-46b89d5a2887
	Filter Thyroid
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			ThyroidDiseasePresent
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter Thyroid {
	Match {
		keyword "Grave"
		keyword "Grave's"
		keyword "Hashimoto"
		keyword "Hashimoto's"
		keyword "Hyperthyroidism"
		keyword "Hypothyroidism"
		keyword "Thyroid"
		keyword "Thyroidism"
		regex ".*thyroid"
	}
	Tests matching only this {
		"Grave"
		"Grave's"
		"Grave's disease"
		"Hashimoto"
		"Hashimoto's"
		"Hashimoto's thyroiditis"
		"Thyroid"
		"thyroiditis"
		"hyperthyroid"
		"hyperthyroidism"
		"hyporthyroid"
		"hyporthyroidism"
	}
}


Question ThyroidDiseasePresent {
	"If they have #thyroid disease#: 
	
	* Ensure an accurate diagnosis in the health summary. 
	* Include significant treatment or investigations in the additional information box of the diagnosis. 
	* Thyroid disease is an indication for screening bone densitometry. 
	* Annual TSH is recommended to monitor replacement therapy. 
	* If they have active hyperthyroidism, care is usually coordinated between GP and specialist. 
	
	Do you wish to #take any action# for their thyroid disease?"
	
	YesNoNA
	NAText "N/A - No thyroid disease"
	Category "thyroid"
	
	Triggers {
		Yes: problem ThyroidDiseasePresent
	}
	Id a008a8a2-fb25-4685-8826-6e272f60412c
	Changelog {
		2017-01-14: ""
	}
}


Problem ThyroidDiseasePresent {
	Goal "Monitor for complications"
	Plan "Add appropriate reminder for annual thyroid check up. TSH annually if replacement therapy. Consider Bone density screening."
	Id d5721ada-1cc0-4012-b4ac-a1a28f444b96
	Changelog {
		2017-01-14: ""
	}
}


