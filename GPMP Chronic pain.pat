/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with chronic pain */
Clinic GPMPChronicPain {
	Id bedfe75a-61d4-413b-b5a8-869ed30b65d7
	Filter ChronicPain
	Questions {
		Patient {
			PainChartNeeded
		}

		Checklist {
		}

		Staff {
			ChronicPain
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter ChronicPain {
	Match {
		keyword "Chronic body aches"
		keyword "Chronic pain"
		keyword "Fibromyalgia"
		regex "chronic.*pain.?"
	}
	Tests matching only this {
		"Chronic pain"
		"chronic pelvic pain"
		"chronic pain due to injury"
		"chronic body pains"
		"chronic body aches"
		"chronic post-operative pain"
		"Fibromyalgia"
		"chronic abdominal pain"
		"chronic non-specific abdominal pain"
		"chronic neck pain"
		"chronic non-malignant pain"
		"chronic nonmalignant pain"
	}
}


Question ChronicPain {
	"#Chronic pain# is pain that continues beyond the usual time of healing (or expected time of recovery), defined as #longer than 3 months.# 
	
	Do they have #chronic pain#?"
	
	YesNo
	Category "pain"
	
	Triggers {
		Yes: problem ChronicPain
	}
	Id 914bcc07-2787-4dcd-bcb1-a831b98ac2c7
	Changelog {
		2017-01-14: ""
	}
}


Problem ChronicPain {
	Goal "Ensure comprehensive evaluation."
	Plan "Bring back for chronic pain clinic."
	Id 729be865-9fea-4edc-977b-6fe45f780836
	Changelog {
		2017-01-14: ""
	}
}


