/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for bariatric surgery follow-up */
Clinic GPMPBariatricSurgery {
	Id 0e0211fc-4edc-48c6-a5e1-7b9a857ebc76
	Filter BariatricSurgery
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			BipolarLithiumMonitoringNeeded
		}

	}
	Changelog {
		2017-01-14: ""
	}
}
Filter BariatricSurgery {
	Match {
		keyword "bariatric surgery"
        keyword "sleeve gastrectomy"
        keyword "gastric band"
        keyword "gastric bypass"
	}
	Tests matching only this {
		"bariatric surgery"
        "adjustable gastric band "
        "Roux-en-Y gastric bypass"
	}
}

Question BariatricSurgeryAnnualFollowup {

    "#Bariatric surgery# requires annual long term follow up (click below).
   
    Do you wish to add this to the care plan?"
   
    YesNoNA
    NAText "N/A  No bariatric surgery"
    Category "bariatrics"

    Education "Bariatric surgery monitoring recommendations" {"

    [%header,cols=4*] 
    |===
    | Nutritional marker 
    | SG (sleeve gastrectomy)
    | AGB (adjustable gastric band)
    | RYGB (Roux-en-Y gastric bypass)

    |Iron studies 
    |+
    |+
    |+

    |Thiamine
    |+
    |Optional
    |+

    |Vitamin B12
    |+
    |+
    |+

    |Folic acid and homocysteine
    |+
    |+
    |+

    |Vitamin D and Calcium
    |+
    |+
    |+

    |Parathyroid PTH
    |+
    |+
    |+

    |Zinc 
    |+
    |+
    |+

    
    |Copper
    |+
    |+
    |+

    |Vitamin A
    |-
    |-
    |+

    |Selenium
    |Optional
    |Optional
    |Optional

    |Vitamin E and K 
    |If symptomatic
    |If symptomatic
    |If symptomatic
    |===


    (Aus Doc October 2019)

    "}

	
	Triggers {
		Yes: problem BariatricSurgeryMonitoringNeeded
	}
	Id 232da927-69c8-4387-8b0f-e2d7c56cbe38
	Changelog {
		2017-01-14: ""
	}
}


Problem BariatricSurgeryMonitoringNeeded {
    Goal "Monitor for complications"
    Plan "Add appropriate reminder for check up. Order recommended tests annually. Consider dietitian and bone density."
	Id 96169763-61d4-4d15-9e75-e1af01c369af
}
