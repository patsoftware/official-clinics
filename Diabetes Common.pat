/* 
 * Copyright (c) 2017 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */


Question NeuropathyCheckNeeded {
	"Perform 10gm thread test on the 4 spots of each foot, test vibration, and elicit ankle tendon reflexes. 
	
	Are the feet #neurologically abnormal#?"
	
	YesNo
	Category "feet"
	
	Triggers {
		Yes: problem NeuropathyPresent
	}
	Id 0dc736c4-58e6-4450-a07e-be20276f8940
	Changelog {
		2017-01-14: ""
	}
}


Problem NeuropathyPresent {
	Goal "Prevent progression"
	Plan "Control HbA1c <7.0. Refer for podiatry. Educate patient intensively re foot care."
	Id 02054b2b-3586-4783-b2e0-8162f5019f57
	Changelog {
		2017-01-14: ""
	}
}


Problem DiabeticCycleOfCareNeeded {
	Goal "Complete an annual cycle"
	Plan "Enter data and arrange reminders for next part cycle."
	Id 5476b50c-7e5a-4e20-86e5-7db2d474c552
	Changelog {
		2017-01-14: ""
	}
}