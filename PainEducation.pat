Education ChronicPainEducation {
    Questions {
        PainEducationIntro
        PainEducationFacts1
        PainEducationFacts2
        PainEducationFacts3
        PainEducationFacts4
        PainEducationHealthyLifestyleAlreadyDoing
        PainEducationUnhelpfulThoughts

        PainEducationNoPainNoGain
        PainEducationCanDo
        PainEducationWellness
        PainEducationWhoIsDriving
        PainEducationFeelings
        PainEducationStressors
        PainEducationActions
    }
    AppliesTo "PATv3"
	Id 2f71cf7e-cf8d-4384-af26-541637923090
}

Question PainEducationIntro {
    "*PAT Chronic Pain Education*

    Copyright PAT Pty Ltd. Inspired by articles written by Hunter Integrated Pain Service.

    Please press 'Next' to start.
    "
    
    Display
    Category "pain"
    Id 72a2b583-d307-46af-adb5-839f7ef3a0b0
}

Question PainEducationFacts1 {
    "*Chronic pain, in medical terms, is pain that has lasted for at least 3 months.*

    When pain becomes chronic, many factors other than the original injury or cause can contribute to the pain.

    image:pain-education/pain_pg1.jpg[]

    This wizard will help you identify such factors so that you can make changes to help you manage the pain.
    "
    
    Display
    Category "pain"
    Id cc9ce3e1-4b49-4ebb-aae9-181ec2c70183
}


Question PainEducationFacts2 {
    "*Here are some important facts about chronic pain:*

    * All pain sensation (both acute and chronic) is produced in the brain.

    image:pain-education/pain_pg2.jpg[]

    * The pain is real, it is not ‘just in your mind’ or ‘made up’, and it is not your fault.
    "
    
    Display
    Category "pain"
    Id 15833a67-e952-4e6f-b264-a6e09353648a
}

Question PainEducationFacts3 {
    "*Some other facts:*

    * Not all pain can be cured.

    image:pain-education/pain_pg3.jpg[]

    * Medications are useful. 
    * However waiting passively for medication to work can be a trap – you need to help it work. 
    * The rest of this program concentrates on looking at things you can do to help.
    "
    
    Display
    Category "pain"
    Id 9e19f7f7-a722-435d-a56d-d7d97b06a244
}

Question PainEducationFacts4 {
    "*Now some good news:*

    * If you are doing this pain wizard, it means your doctor is sure that all major diseases or problems have been ruled out with tests.
    * Most injuries that can be healed are healed as best they can be by 3 to 6 months.
    * The biggest recent discovery is the concept of ‘neuroplasticity’.

    Put simply it means the brain can be retrained.

    image:pain-education/pain_pg4.jpg[]

    So, if we identify contributing factors, we can reverse some of the changes in the brain that contribute to the sensation of pain.
    "
    
    Display
    Category "pain"
    Id 7e56431f-1bc3-440e-9475-9f4b42dab608
}



Question PainEducationHealthyLifestyleAlreadyDoing {
    "A *healthy lifestyle*  means a healthier immune system and nervous system. The immune system helps our body repair itself, including the nerves that transmit pain signals to the brain.

    Below are steps for such a healthy lifestyle.  

    Please press *any box* that you are *already doing*.
    "
    
    SubQuestions {
        AlreadyEatingWell
        AlreadySleepingWell
        AlreadyExercising
        AlreadyNonsmokerAndCarefulDrinking
        AlreadyReducingStress
        HealthyLifestyleAlreadyDoingNA
    }
    Category "pain"
    DisplaySettings "allow-next-when-answered: 1"
    Id 05f8c680-82c4-48a7-b87c-19ad8d4e3552
}
Question AlreadyEatingWell {
    "I eat pretty sensibly with

    fresh fruit and vegetables

    and low saturated fat foods"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#008000'"
    Category "pain"
    Id 4566b35c-b72c-4f5a-aa3e-1d70a37e72b2
}
Question AlreadySleepingWell {
    "I usually get a

    good night's sleep"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#000080'"
    Category "pain"
    Id ff7803a9-052a-4bbc-9e64-d333bec6e274
}
Question AlreadyExercising {
    "I try to keep my

    physical fitness up

    with regular exercise"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#808000'"
    Category "pain"
    Id b43b49ea-8718-4622-9f77-fa70ae202b50
}
Question AlreadyNonsmokerAndCarefulDrinking {
    "I do not smoke and

    am careful in my

    alcohol intake"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#32CD32'"
    Category "pain"
    Id cf767cf6-e3ce-440b-9678-3225aa6c04fa
}
Question AlreadyReducingStress {
    "I have learned ways to

    reduce stress in my life

    such as Yoga or meditation"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#8080FF'"
    Category "pain"
    Id a3f1162f-e2f8-4550-91a7-41d490594919
}
Question HealthyLifestyleAlreadyDoingNA {
    "None of these apply to me"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#808080'"
    Category "pain"
    Id ee24dbc1-7c08-4cd8-a5ac-731377b5b317
}


Question PainEducationUnhelpfulThoughts {
    "Our *inner thoughts and beliefs* are very powerful in affecting our health and our healing processes. image:pain-education/pain_pg6.jpg[]

    Here are some thoughts that are common in chronic pain:

    Please press *any box* that you think may apply to you.
    "
    
    SubQuestions {
        PainIsHarmful
        PainIsDisabling
        PainIsIllnessNeedingCure
        PainHasTakenOverMyLife
        PainUnhelpfulThoughtsNA
    }
    Category "pain"
    DisplaySettings "allow-next-when-answered: 1"
    Id f9afbee2-d2e5-4ecb-99af-050a104b9916
}
Question PainIsHarmful {
    "This pain is harmful to me.

    The pain means more damage

    is being done to my body.
    "
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#941A70'"
    Category "pain"
    Id 8197237a-501c-4e49-855d-1ff8a46bb2bb
}
Question PainIsDisabling {
    "This pain is disabling me.

    I cannot do lots of things

    ever again.
    "
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#F14501'"
    Category "pain"
    Id df4e8848-97dc-485d-87f8-8b2f2d506e41
}

Question PainIsIllnessNeedingCure {
    "This pain means I have an

    illness and I will not get better

    until a cure is found.
    "
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#463C3C'"
    Category "pain"
    Id 49e69110-6134-4f0d-ac7c-4f9af9f0e4a6
}

Question PainHasTakenOverMyLife {
    "This pain has taken over

    most of my life"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#F10101'"
    Category "pain"
    Id c7d3f132-40ff-49ce-8bd2-af524b3f462f
}
Question PainUnhelpfulThoughtsNA {
    "None of these apply to me"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#808080'"
    Category "pain"
    Id bc0656c5-4ddf-4fde-b2d1-1008e9752686
}

Question PainEducationNoPainNoGain {
    "
    *Positive thoughts* lift our mood and help us feel more confident
    to *make changes* to help the pain.


    [cols=\"^.^1,6\",grid=\"all\"]
    |=====
    |
    *No pain*

    *No gain*
    |
    {set:cellbgcolor:#FFFFC0} 
    Think of pain as “hurt” from a scar rather than “harm”.

    Although the pain hurts when you exercise,
    it does not necessarily mean that something harmful
    is happening in your body. For example muscles often
    ache when they have not been used much.

    This will enable you to work to recover
    physical strength and ability to do things that you used to.
    |=====

    image:pain-education/pain_pg7.jpg[]
    "
    
    Display
    Category "pain"
    Id fd9c470d-0fef-4671-8e99-9af7b1d27eb7
}


Question PainEducationCanDo {
    "
    *Positive thoughts* lift our mood and help us feel more confident
    to *make changes* to help the pain.

    [cols=\"^.^1,6\",grid=\"all\"]
    |=====
    |
    *Think +
    “can do” +
    not +
    “can't do”*
    |
    {set:cellbgcolor:#FFFFC0} 
    If you can accept that you have the pain, you can
    concentrate on adapting to it.

    Concentrate on all the things you can still do
    despite having the pain.

    List the things you stopped doing that you
    could try to restart.
    |=====

    image:pain-education/pain_pg8.jpg[]
    "
    
    Display
    Category "pain"
    Id a0828525-d26d-419c-8c16-4d61881d3481
}


Question PainEducationWellness {
    "
    *Positive thoughts* lift our mood and help us feel more confident
    to *make changes* to help the pain.

    [cols=\"^.^1,6\",grid=\"all\"]
    |=====
    |
    *“Wellness” +
    not +
    “sickness”*
    |
    {set:cellbgcolor:#FFFFC0} 
    Thinking of pain as illness can put you into a downwards
    “sick” path and tends to becomes self perpetuating.

    Deciding to aim for wellness puts you on an
    upwards path which enables you to plan anew.
    |=====

    image:pain-education/pain_pg9.jpg[]
    "
    
    Display
    Category "pain"
    Id dde16459-76f7-4b73-aade-69055eb6039c
}




Question PainEducationWhoIsDriving {
    "
    *Positive thoughts* lift our mood and help us feel more confident
    to *make changes* to help the pain.

    [cols=\"^.^1,6\",grid=\"all\"]
    |=====
    |
    *Who is +
    driving +
    my car?*
    |
    {set:cellbgcolor:#FFFFC0} 
    Chronic pain can become so prominent in your life that it
    seems to control everything you do. “It is driving your car”.

    Thinking about controlling the pain puts you back in the
    driver’s seat. You can then make positive changes in your life.

    Then pain becomes just one part of your life.
    |=====

    image:pain-education/pain_pg10a.jpg[]
    "
    
    Display
    Category "pain"
    Id 4025d5d4-2ab1-437c-9165-a8e4bb3c3484
}


Question PainEducationFeelings {
    "Feelings (emotions) can influence chronic pain. Some of these feelings
    may be due to the pain, but some may be due to deeper issues.

    It is very useful to identify such feelings."
    
    SubQuestions {
        PainFeelingsAngry
        PainFeelingsSad
        PainFeelingsAnnoyed
        PainFeelingsAnxious
        PainFeelingsNA
    }
    Category "pain"
    DisplaySettings "allow-next-when-answered: 1"
    Id 7ae5f3b7-bcbe-4c3e-9558-f17b00d162d7
}

Question PainFeelingsAngry {
    "I feel angry with

    people or about things"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#F14501'"
    Category "pain"
    Id 26decd6e-7576-4093-8b71-63b5276842da
}
Question PainFeelingsSad {
    "I feel sad or down"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#941A70'"
    Category "pain"
    Id 583139ed-0cbe-4511-926f-a95d796f539b
}
Question PainFeelingsAnnoyed {
    "I just feel annoyed

    with myself"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#463C3C'"
    Category "pain"
    Id 92e618d7-fb43-4b10-83f1-15d4c1638fca
}
Question PainFeelingsAnxious {
    "I feel anxious

    or worried"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#F10101'"
    Category "pain"
    Id b8856ce6-6406-4a6c-aca7-de5384231eb0
}
Question PainFeelingsNA {
    "None of these apply to me"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#808080'"
    Category "pain"
    Id 5953b4a6-a870-4180-a635-2e521d890cd4
}

Question PainEducationStressors {
    "Life is full of stresses, and increased stress levels can increase both the level of pain and make it harder to manage it. image:pain-education/pain_pg12a.jpg[]

    Obviously, not all of life stresses are under our control. However, sometimes we can change things to reduce stress in some areas.

    Think about everything that is going on in your life. Then press *any* of the boxes below *where you think some stress* is coming from."
    
    SubQuestions {
        PainStressorsHomeRelationships
        PainStressorsWork
        PainStressorsFinancial
        PainStressorsSocialRelationships
        PainStressorsNA
    }
    Category "pain"
    DisplaySettings "allow-next-when-answered: 1"
    Id 3c3e3d54-a918-4ddf-92dd-f53b7d6adfa1
    
}
Question PainStressorsHomeRelationships {
    "*Home relationships*

    - Partner
    - Children
    - Others
    "
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#FF4500'"
    Category "pain"
    Id e4206229-9d9b-4422-ba7e-7589116c6e57
}
Question PainStressorsWork {
    "*Work*

    - Current work situation
    - Looking for work or off work
    - Relationships at work
    "
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#7B18A0'"
    Category "pain"
    Id 4df55144-3e1b-45e9-b4cb-d814d96c71ea
}
Question PainStressorsFinancial {
    "*Financial matters*

    - Money concerns
    - Centrelink
    - Compensation
    "
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#460963'"
    Category "pain"
    Id 964eebfe-2875-4cc5-b071-6635b33a36eb
}
Question PainStressorsSocialRelationships {
    "*Social relationships*

    - Relatives
    - Friends
    - Clubs
    "
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#CF3845'"
    Category "pain"
    Id 64e4185f-1999-4f6c-8f63-bb79d57d20c7
}
Question PainStressorsNA {
    "None of these apply to me"
    
    YesNo
    DisplaySettings "text-colour: 'white', background-colour: '#808080'"
    Category "pain"
    Id ea7427cd-60ab-4ff6-ab74-08906bc0d2c1
}


Question PainEducationActions {
    "After looking at things today what would you like to do?

    Press at least *one* of the boxes below (you can press more than one)."
    
    SubQuestions {
        PainActionMyself
        PainActionDoctorsHelp
        PainActionReferralToSpecialist
        PainActionReferralToCounselor
        PainActionNone
    }
    Category "pain"
    DisplaySettings "allow-next-when-answered: 1"
    Id d5a2fedd-dc16-42a3-9054-5536ee68a09a
}
Question PainActionMyself {
    "I will try to make some changes myself"
    
    YesNo
    DisplaySettings "text-colour: black, background-colour: '#FFFF80'"
    Category "pain"
    Id 70a85bcb-17ed-4fde-a336-93b0e29048ae
}
Question PainActionDoctorsHelp {
    "I would like my doctor to help me make changes"
    
    YesNo
    DisplaySettings "text-colour: black, background-colour: '#FFFF80'"
    Category "pain"
    Id a50cc216-766c-47d3-97ae-43a8c9ca4392
}
Question PainActionReferralToSpecialist {
    "I would like to discuss referral

    to a chronic pain specialist"
    
    YesNo
    DisplaySettings "text-colour: black, background-colour: '#FFFF80'"
    Category "pain"
    Id dc1bbac5-12ae-4401-aae8-0da7279deb9c
}
Question PainActionReferralToCounselor {
    "I would like to discuss referral to a counselor

    or psychologist for some help"
    
    YesNo
    DisplaySettings "text-colour: black, background-colour: '#FFFF80'"
    Category "pain"
    Id f4bd1b1a-9c59-4117-97a2-d79392abc3a3
}
Question PainActionNone {
    "None of these apply to me"
    
    YesNo
    DisplaySettings "text-colour: black, background-colour: '#FFFF80'"
    Category "pain"
    Id a5f721e9-bc42-4872-a273-f3f5ad80cbf6
}

