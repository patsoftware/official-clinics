/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Clinic Age45To49HealthAssessment {
	Id ab7173d4-5b79-4169-9288-353724551800
	Questions {
		Patient {
			BloodPressureFH
            FHOfPrematureCVDLessThan60Years
			StrokeFH
			DiabetesFH
			DepressionFH
			BowelCancerFHAnyAge
			BowelCancerFHEarly
			BreastOrOvarianCancerFHAnyAge
            BreastCancerFHEarly
			ProstateCancerFH

			SkinCheckNecessary
			GlaucomaFH
			FHOfOtherSignificantDisease
			ArthritisOrPHArthritis
			BackPainPresent
			AlcoholIntakeMoreThanRecommended
			Smoker
			PhysicalActivityNeeded
			SaltIntakeHigh
			DietaryRecommendationsForCardiovascularPatients
			WeightLossTipsDesired
			BreastScreenNeeded
			NationalBowelScreenNeeded
			RelationshipConcerns
			AnxietyPresent
			DepressionPresent
			ErectileProblem
			SexualHealthConcerns
			IncontinenceUrinePresent
			CervicalScreeningNeeded
			GestationalDiabetesOrHTInPH
			ResearchConsent
		}

		Checklist {
			HealthAssessmentConsent
			RecordHeightWeightWaist
			RecordSittingBP
			RecordSmokingAlcohol
			SmokerBasicSpirometry
			AsthmaScoreAdministered
			PrintHealthSummary
		}

		Staff {
			HealthSummaryAndMedicationListReviewNeeded
			FamilialHypercholesterolaemiaRisk
			AsthmaHistory
			WaistHipRatioOutsideRecommendedLimits
			BPMedicationChangeNeeded
			FHAbdominalAorticAneurysm
			BGLAbnormal-TestNeeded
			DiabetesRiskHigh
			AbsoluteCardiovascularRiskCalculatorMediumOrHigher
			OsteoporosisRiskMediumOrHigh
			LungAgeCalculationNeeded
			BreastOrOvarianCancerFH
            BowelCancerFH
			CancerPH

			MainGoals
			PatientSaysAlreadyDoing
			DoctorSaysGoingWell
			PatientImportance0to10
			PatientConfidence0to10
			ChecklistCopyToPatient
			ChecklistCopiesToTeamWithConsent
            ReviewDate
		}

	}
	Changelog { 
		2017-01-14: ""
	}
}

Question BloodPressureFH {
	"
    === High Blood Pressure in Family

    Have any of your first-degree relatives (mother, father, brother, sister) had high blood pressure (hypertension)?"
	
	YesNoUnsure
	Category "Heart / Brain"
	
	Triggers {
		Yes: problem FamilyHistoryPositiveForHypertension
	}
	Id 5cf41118-34c7-4677-a8b9-1d27aff0276c
	Changelog {
	}
}		 

Question FHOfPrematureCVDLessThan60Years {
    "
    === Early Heart Disease or Stroke in Family
    
    Has one of your relatives (such as your brother, sister, parent, grandparent, uncle, or aunt) had a heart attack, heart disease, heart surgery, or a stroke before 60 years of age?"
	
	YesNoUnsure
	Category "Heart / Brain"
	
	Id d69bdd91-e9fc-4dad-9299-0752141b4e0a
	Changelog {
		2017-01-14: ""
	}

    /*
    ================================================================================
    Guidelines for the management of Absolute cardiovascular disease risk (2012)
    ================================================================================

    Family history of CVD: A family history of premature cardiovascular disease refers to an event that occurs in relatives including parents, grandparents, uncles and/or aunts before the age of 55 years.
        https://www.heartfoundation.org.au/images/uploads/publications/Absolute-CVD-Risk-Full-Guidelines.pdf#page=111


    ================================================================================
    RACGP Red Book
    ================================================================================

    ..Patients with a family history of premature CVD (in a first-degree relative – men aged <55 years, women aged <65 years)...

        https://www.racgp.org.au/clinical-resources/clinical-guidelines/key-racgp-guidelines/view-all-racgp-guidelines/red-book/prevention-of-vascular-and-metabolic-disease/assessment-of-absolute-cardiovascular-risk

    Appendix 2A. Family history screening questionnaire:
    Have any of your close relatives had heart disease before 60 years of age?
    ‘Heart disease’ includes cardiovascular disease, heart attack, angina and bypass surgery

        https://www.racgp.org.au/FSDEDEV/media/documents/Clinical%20Resources/Guidelines/Red%20Book/Appendix-2A.pdf


    ================================================================================
    Guideline for the diagnosis  and management of  hypertension in adults - 2016
    National Heart Foundation
    ================================================================================
        ...family history of premature CVD (immediate relative before 55 years of age for men and before 65 years of age for women).

        https://www.heartfoundation.org.au/images/uploads/publications/PRO-167_Hypertension-guideline-2016_WEB.pdf#page=21

    */
}


Question StrokeFH {
	"
    === Stroke in Family
    
    Have any of your first-degree relatives (mother, father, brother, sister) had a stroke or a TIA (mini-stroke)?"
	
	YesNoUnsure
	Category "Heart / Brain"
	
	Triggers {
		Yes Unsure: problem FamilyHistoryPositiveForStroke
	}
	Id 1a06edc7-668e-420e-b78f-386392874457
	Changelog {
		2017-01-14: ""
	}
}




Question BowelCancerFHAnyAge {
	"#Bowel cancer# is also known as colon cancer, rectal cancer or colorectal cancer.

    Did any of your:
	
	- first-degree relatives (mother, father, brother, sister), or
	- second-degree-relatives (grandparents, uncles, aunts)

	have bowel cancer?
    "
    
    YesNoUnsure
	Category "prevention"
    
    Triggers {
		No: AutoAnswer {
			BowelCancerFHEarly No
		}
		Yes Unsure: show BowelCancerFH
	}
    Id a534be0e-5c2f-4912-be2a-55ec8f8c07c2
    Changelog {
        2017-02-23: ""
	}
}


Question BowelCancerFHEarly {
	"#Bowel cancer# is also known as colon cancer, rectal cancer or colorectal cancer.
    
    Did any of your parents, brothers, sisters or children have bowel cancer before 55 years of age?
    "
	
	YesNoUnsure
	Category "prevention"
	
	Triggers {
		Yes Unsure: show BowelCancerFH
	}
	Id 114e8c7a-f4d6-4168-9914-d311e100cd10
	Changelog {
		2017-01-14: ""
	}
}

Question BowelCancerFH {
    "The patient reports a family history of bowel cancer.

    If there are significant risk factors, risk can be fully assessed using the https://www.racgp.org.au/clinical-resources/clinical-guidelines/key-racgp-guidelines/view-all-racgp-guidelines/red-book/early-detection-of-cancers/colorectal-cancer[RACGP Red Book]

    What is the risk?
    "

    Select one BowelCancerRiskCategory
    NAText "No family history"
	Category "prevention"

    Triggers {
        moderate: problem BowelCancerModerateRisk
        high: problem BowelCancerHighRisk
        high: problem CancerInFamily
    }

    Education "Significant risk factor summary" {"
        - First-degree relative diagnosed before 55
        - Multiple relatives with bowel or Lynch Syndrome associated cancer (https://premm.dfci.harvard.edu/[online risk calculator])
        - Relative with large number of adenomas (familial polyposis syndromes)
        - Known gene mutation in family
    "}

    Links {
        BowelCancerFHAnyAge
        BowelCancerFHEarly
    }

	Changelog {
		2019-05-13: "doctor question to assess risk rather than adding to care plan directly"
	}
    References {
        "https://wiki.cancer.org.au/australia/Guidelines:Colorectal_cancer/Lynch_syndrome"
    }
    Id 15f62203-9505-434d-974d-c9d09a90eb53
}
Selection BowelCancerRiskCategory {
    Option average   { "Average or slightly increased"}
    Option moderate { "Moderate"}
    Option high   { "High"}
    Id 373c85b0-ae03-492f-8d18-12466c4753d1
}


Question DiabetesFH {
	"
    === Diabetes in Family
    
    Have any of your first-degree relatives (mother, father, brother, sister) had diabetes?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		Yes: problem DiabetesFH
	}
	Id bddbbd16-4af6-453e-9903-b00e7eca11dd
	Changelog {
		2017-01-14: ""
	}
}


Question DepressionFH {
	"
    === Depression in Family

    Have any of your first-degree relatives (mother, father, brother, sister) had depression or bipolar disorder?"
	
	YesNoUnsure
	Category "psychology"
	
	Triggers {
		Yes Unsure: problem DepressionFH
	}
	Id 815bf983-f66c-4510-8950-f05847d1ed76
	Changelog {
		2017-01-14: ""
	}
}


Question BreastOrOvarianCancerFHAnyAge {
	"
    === Breast Cancer in Family
        
    Did any of your
	
	- first-degree relatives (parent, sister, child), or
	- second-degree relatives (aunt, grandmother) on either side
	
	have breast or ovarian cancer?"
	
	YesNoUnsure
	Category "prevention"
	
	Triggers {
		No: AutoAnswer {
            BreastCancerFHEarly No
        }
        Yes Unsure: show BreastOrOvarianCancerFH
	}
	Changelog {
		2019-05-13: "combined with ovarian cancer question to trigger a doctor question rather than adding to care plan directly"
	}
	Id 442e6a1a-d06d-4527-810e-9abec25869c8
}				

Question BreastCancerFHEarly {
	"
    === Early Breast Cancer in Family
    
    Did any of your parents, sisters or children have breast cancer #before 50 years of age#?"
	
	YesNoUnsure
	Category "prevention"
	
	Triggers {
        Yes Unsure: show BreastOrOvarianCancerFH
	}
	Changelog {
		2019-05-15: "separate question for early breast cancer like before"
		2019-05-13: "combined with ovarian cancer question to trigger a doctor question rather than adding to care plan directly"
	}
	Id cbcce87c-8f2f-45ad-ad4f-063a526f34d8
}				
		
		
Question BreastOrOvarianCancerFH {
    "The patient reports a family history of breast or ovarian cancer.

    This is potentially significant if

    - First-degree relative diagnosed before 50
    - Two first or second-degree relatives on the same side of the family
    - A relative with sarcoma diagnosed before 45 on the same side of the family
    - Known gene mutation in family

    Risk can be assessed using

    - https://canceraustralia.gov.au/clinical-best-practice/gynaecological-cancers/fra-boc/evaluate[FRA-BOC online tool]
    - https://www.racgp.org.au/clinical-resources/clinical-guidelines/key-racgp-guidelines/view-all-racgp-guidelines/red-book/early-detection-of-cancers/breast-cancer[RACGP Red Book]

    Is there potentially moderate or high risk?
    "

    /*
        eviQ rReferral guidelines: https://www.eviq.org.au/cancer-genetics/referral-guidelines/1620-referral-guidelines-for-breast-cancer-risk-as#guideline
    */

    YesNoNA
    NAText "No family history"
	Category "prevention"

    Triggers {
        Yes: problem CancerInFamily
        Yes: problem BreastOrOvarianCancerIncreasedRisk
    }

    Links {
        BreastOrOvarianCancerFHAnyAge
        BreastCancerFHEarly
    }

	Changelog {
		2019-05-13: "doctor question to assess risk rather than adding to care plan directly"
	}
	Id 6b3c116f-8927-4f19-95cc-74f5f0611ab3
}


Question ProstateCancerFH {
	"
    === Prostate Cancer in Family

    Did any of your first-degree relatives (father, brother) have prostate cancer before 60 years of age?"
	
	YesNo
	Gender male
	Category "prevention"

	Triggers {
		Yes: problem ProstateCancerFH
	}
	Id 2555aac8-23d5-4c69-880f-f9d2ede75c01
	Changelog {
		2017-01-14: ""
	}
}


Question SkinCheckNecessary {
	"A #skin check# is recommended every year for *any* of the following groups: 
	
	* Past skin cancer removal
	* Past freezes for sun spots or presence of sun spots today 
	* Multiple Dysplastic naevi (multiple abnormally coloured moles) on your body 
	* High levels of sun exposure in childhood or at work 
	* Fair complexion, tendency to burn, freckles, light or red hair colour 
	* Melanoma in close relatives (mother, father, brother, sister, child)
	
	Do you require a skin check?"
	
	YesNo
	Category "prevention"
	
	Triggers {
		Yes: problem SkinCheckNecessary
	}
	Id 26e4e557-2594-41af-b10c-33198d6c3c0a
	Changelog {
		2017-01-14: ""
	}
}


Question GlaucomaFH {
	"
    === Glaucoma in Family

    Have any of your first-degree relatives (parent, brother, sister) had #glaucoma# (increased eye pressure)?"
	
	YesNo
	Category "prevention"
	
	Triggers {
		Yes: problem GlaucomaFH
	}
	Id d6688533-edfa-4688-bb15-61f83640e0ba
	Changelog {
		2017-01-14: ""
	}
}


Question FHOfOtherSignificantDisease {
	"
    === Other Diseases in Family

    Do you have any first-degree relatives (mother, father, brother, sister) with any other disease you would like to tell the doctor about to see if they may be inheritable and so affect you?"
	
	YesNo
	Category "prevention"
	
	Triggers {
		Yes: problem FamilyHistoryOfOtherSignificantDisease
	}
	Id 0728b1af-a424-4f65-80dc-97070032448a
	Changelog {
		2017-01-14: ""
	}
}


Question ArthritisOrPHArthritis {
	"Have you got joint pain or a past history of arthritis?"
	
	YesNo
	Category "arthritis"
	
	Triggers {
		Yes: problem ArthritisAndOrBackPainPH
	}
	Id b52f00f9-5258-4e57-9763-4747211ce4ae
	Changelog {
		2017-01-14: ""
	}
}


Question BackPainPresent {
	"Do you have problems with chronic or recurrent back pain?"
	
	YesNo
	Category "pain"
	
	Triggers {
		Yes: problem BackPainPresent
	}
	Id df392497-b800-4eb5-8cbe-68fef4732580
	Changelog {
		2017-01-14: ""
	}
}

Question DietaryRecommendationsForCardiovascularPatients {
	"The #healthy heart diet# is low in saturated fat, high in fruit and vegetables. Is this how you #usually# eat? 
	
	image:DietaryRecommendationsForCardiovascularPatients.jpg[width=832]"
	
	YesNoUnsure
	Category "Heart / Brain"
	
	Triggers {
		No Unsure: problem WeightLossDesired
	}
	Id 4bcabe20-8d84-42f2-b8e8-a8d08c030243
	Changelog {
		2017-01-14: ""
	}
}

Question RelationshipConcerns {
	"Sometimes relationships in families can cause stress. 
	
	For example, it is common at this age to be taking over a carer role for aging parents. Counselling can help. 
	
	Would you like to discuss relationship issues?"
	
	YesNo
	Category "general"
	
	Triggers {
		Yes: problem RelationshipConcerns
	}
	Id a29baa75-89b7-4d19-8ad9-be19ad981095
	Changelog {
		2017-01-14: ""
	}
}

Question GestationalDiabetesOrHTInPH {
	"Both diabetes and hypertension occurring during pregnancy mean you are at higher risk than the normal population of recurrence. 
	
	It is recommended to screen for both each year. 
	
	Have you had either #diabetes or high blood pressure during pregnancy#?"
	
	YesNoUnsure
	Gender female
	Category "blood sugar"

	Triggers {
		Yes: problem GestationalDiabetesOrHT
	}
	Id 4c08c44c-06a3-419c-9f25-2d09bf74508d
	Changelog {
		2017-01-14: ""
	}
}


Question HealthAssessmentConsent {
	"Explain purpose and record consent to proceed with Health Assessment"
	
	Check required
	Category "admin"
	Id cf9fb8f4-0f71-745c-98dd-b2f5fd60bfd4
	Changelog {
		2017-01-14: ""
	}
}

Question RecordSittingBP {
	"Take BP reading and list as the sitting BP in the clinical notes"
	
	Check required
	Category "Heart / Brain"
	Id 0023109c-8c33-ce5d-b677-424924c0ffc7
	Changelog {
		2017-01-14: ""
	}
}

Question WaistHipRatioOutsideRecommendedLimits {
	"Is waist/hip ratio outside recommended limits of #< 0.90# for males and #< 0.80# for females?"
	
	YesNo
	Category "Heart / Brain"
	
	Triggers {
		Yes: problem WeightLossDesired
	}
	Id a6361653-2ad2-4b85-816e-eecf7e453096
	Changelog {
		2017-01-14: ""
	}
}

Question DiabetesRiskHigh {
	"Use the #Australian Type 2 Diabetes Risk Assessment# Tool in your clinical notes to calculate level of risk of developing type 2 diabetes. 
	
	If they are #at high risk# they are #eligible for referral to a lifestyle# modification program and require follow-up relating to the management of any risk factors identified. 
	
	Are they at high risk?"
	
	YesNo
	Category "blood sugar"
	
	Triggers {
		Yes: problem DiabetesRiskHigh
	}
	Id c30b6967-be71-4237-96d9-66f43ed85826
	Changelog {
		2017-01-14: ""
	}
}

Question OsteoporosisRiskMediumOrHigh {
	"Is the patient at intermediate or high risk for osteoporosis using the #Garvan Know Your Bones calculator?# 
	
	* https://www.garvan.org.au/promotions/bone-fracture-risk/calculator/[Garvan Know Your Bones calculator]"
		
	YesNo
	Gender female
	Category "osteoporosis"

	Triggers {
		Yes: problem OsteoporosisRiskIntermediateOrHigh
		
	}
	Id b06e1cfb-a786-4722-8366-42259cad129b
	Changelog {
		2017-01-14: ""
	}
}


Question LungAgeCalculationNeeded {
	"Smoking effectively makes the lungs age. 

	* https://lungfoundation.com.au/primary-care-respiratory-toolkit/[Lung Age Estimator]
	
	Would you like to use spirometry to measure the loss?"
	
	YesNoNA
	Category "lungs"
	
	Triggers {
		Yes: problem Smoking
		
	}
	Id d2ba9620-7e45-4470-8fef-78af058d6da9
	Changelog {
		2017-01-14: ""
	}
}


Question CervicalSmearPast5Years {
	"The #National Cervical Screening Program# starts at age 25 and continues regularly to age 75. 

	If there are no abnormalities it should be done 2 years after a Pap Smear test, and then every 5 years.
	
	Do you need to #arrange an appointment# for a Cervical Smear?"
	
	YesNo
	Gender female
	Category "prevention"

	Triggers {
		Yes: problem CervicalScreeningNeeded
	}
	Id 4beb7a70-6c19-47eb-ac2f-a72281de3b18
	Changelog {
		2017-01-14: ""
	}
}

Problem DiabetesRiskHigh {
	Goal "Reduce risk of progression to diabetes."
	Plan "Refer for lifestyle modification plan. http://www.health.gov.au/preventionoftype2diabetes"
	Id 4603c6a6-0fd7-4fca-b96f-f5a1d0bfba69
	Changelog {
		2017-01-14: ""
	}
}


Problem CancerInFamily {
	Goal "Possible genetic testing to advise on management and screen family members"
	Plan "Referral to Familial Cancer Clinic"
	Id 4179344b-89e5-419a-bf40-d95e2af6e42d
    Education "Genetics Clinics" {"
        https://www.genetics.edu.au/genetic-services/general-genetics-clinics
    "}
    Education "Genetics Pamphlets" {"
        https://www.genetics.edu.au/publications-and-resources/booklets-and-pamphlets/individuals-and-families-cancer-resources
    "}

    References {
        "https://www.eviq.org.au/cancer-genetics/referral-guidelines/1147-general-practitioner-referral-guidelines-for#guideline"
        "https://www.racgp.org.au/FSDEDEV/media/documents/Clinical%20Resources/Guidelines/Genomics-in-general-practice.pdf"
    }
	Changelog {
		2019-05-15: "assessment moved to a doctor question"
	}
}
Problem BowelCancerModerateRisk {
	Goal "Appropriate management and screening"
	Plan "
    Occult blood test every two years from 40 to 49
    Colonoscopy every five years from 50 to 74
    "
	Id 4179344b-89e5-419a-bf40-d95e2af6e42e
	Changelog {
		2019-05-15: "assessment moved to a doctor question"
	}
    References {
        "https://www.racgp.org.au/clinical-resources/clinical-guidelines/key-racgp-guidelines/view-all-racgp-guidelines/red-book/early-detection-of-cancers/colorectal-cancer"
    }
}
Problem BowelCancerHighRisk {
	Goal "Appropriate management and screening"
	Plan "Referral to bowel cancer specialist to further assess risk and plan screening"
	Id 4179344b-89e5-419a-bf40-d95e2af6e42f
	Changelog {
		2019-05-15: "assessment moved to a doctor question"
	}
    References {
        "https://www.racgp.org.au/clinical-resources/clinical-guidelines/key-racgp-guidelines/view-all-racgp-guidelines/red-book/early-detection-of-cancers/colorectal-cancer"
    }
}


Problem BreastOrOvarianCancerIncreasedRisk {
	Goal "Appropriate management and screening"
	Plan "
    Mammogram at least every 2 years from age 50 to 74
    If first-degree relative diagnosed with breast cancer before 50: Mammogram anually from age 40
    If high risk: custom surveillance program 
    "
    /* TODO: Link: 
    https://breast-cancer.canceraustralia.gov.au/awareness
    https://canceraustralia.gov.au/system/tdf/publications/breast-cancer-risk-factors-glance/pdf/breast_cancer_risk_factors_at_a_glance_0.pdf?file=1&type=node&id=6426
    http://healthyweight.health.gov.au/wps/portal/Home/get-informed/physical-activity-and-sedentary-behaviour/levels-of-physical-activity-intensity
    */

	Id a8675580-b56e-43d6-ae5a-6a6f2499bf77
	Changelog {
		2019-05-13: "assessment moved to a doctor question"
	}

    References {
        "https://breastcancerriskfactors.gov.au/what-you-can-do" // accessed 2019-05-13
    }
}


Problem DepressionFH {
	Goal "Determine level of risk"
	Plan "Take history to see if any personal past history of depression/ mood changes. Determine number of relatives involved."
	Id 32996780-4ecd-414d-8e87-f57762fa5991
	Changelog {
		2017-01-14: ""
	}
}


Problem FamilyHistoryPositiveForHypertension {
	Goal "Determine risk."
	Plan "Add to FH section. Advise yearly BP level check."
	Id e9b547b2-90e1-4ef0-944e-1eca96f4e6c9
	Changelog {
		2017-01-14: ""
	}
}


Problem FamilyHistoryPositiveForStroke {
	Goal "Determine risk"
	Plan "Arrange appropriate screening: BP, lipids."
	Id d0cf320e-488f-47dd-8716-cfc827d08631
	Changelog {
		2017-01-14: ""
	}
}


Problem FamilyHistoryOfOtherSignificantDisease {
	Goal "Risk assessment."
	Plan "Determine if hereditary aspect to the stated disease."
	Id b5b1fda5-dae9-4d12-81a3-f4641adbfa9e
	Changelog {
		2017-01-14: ""
	}
}


Problem DiabetesFH {
	Goal "Determine risk"
	Plan "Perform Diabetes Risk calculation. Advise annual BSL."
	Id f60f4f38-5cc4-4f4f-8761-e8e64bcadd23
	Changelog {
		2017-01-14: ""
	}
}

Problem OsteoporosisRiskIntermediateOrHigh {
	Goal "Confirm and treat."
	Plan "Intermediate- measure BMD and recalculate risk. High - measure BMD and treat."
	Id 22cf2477-cb86-4253-90db-f92b7480d3ae
	Changelog {
		2017-01-14: ""
	}
}


Problem SkinCheckNecessary {
	Goal "Examine skin"
	Plan "Check all of skin from scalp to soles of feet."
	Id be0811c9-32e5-4e8d-bc3a-ebc0f083d635
	Changelog {
		2017-01-14: ""
	}
}

Problem ArthritisAndOrBackPainPH {
	Goal "Clarify arthritis &/or back problem."
	Plan "Identify type and degree of problem. Bring back for a follow-up consultation if concern over aetiology of either the back pain or the arthritis."
	Id 771e2a51-8dd7-4668-b0ee-ad0714d88277
	Changelog {
		2017-01-14: ""
	}
}

Problem RelationshipConcerns {
	Goal "Clarify"
	Plan "Acknowledge the concern and arrange a follow-up consult to perform an assessment."
	Id 0722f8fa-8c76-4c43-98ba-b493062cc846
	Changelog {
		2017-01-14: ""
	}
}

Problem GestationalDiabetesOrHT {
	Goal "Early detection"
	Plan "Enroll in yearly recall system."
	Id 25f5f16f-ade6-4ed7-94cb-ca6b009eb3ef
	Changelog {
		2017-01-14: ""
	}
}


Problem ProstateCancerFH {
	Goal "Screening"
	Plan "Offer initial PSA at 40 to 45 years age and further testing is determined by PSA level.
	If PSA <1.0 no further testing until 50 years age. If PSA 1.0 to 2.0 test every 2 years. If PSA > 2.0  free to total PSA ratio needed."
	Id e8838fd7-7942-4b3b-bb5d-b0c28196a8c5
	Changelog {
		2017-01-14: ""
	}
}


Problem GlaucomaFH {
	Goal "Prevent vision loss."
	Plan "Refer for eye pressure check."
	Id 7c3570aa-0747-457b-9e0f-f8966465b0a8
	Changelog {
		2017-01-14: ""
	}
}

Education PrimaryCareRespiratoryToolkit {
	Message "Respiratory Toolkit "
	Link "http://lungfoundation.com.au/health-professionals/clinical-resources/copd/primary-care-respiratory-toolkit/"
	Id 004862db-1154-4987-855c-d34f6397dea0
	Changelog {
		2017-01-14: ""
	}
}

Education FractureRisk {
	Message "Fracture Risk"
	Link "http://osteoporosisdtc.azurewebsites.net/home"
	Id a4917157-1537-4a65-a064-8f3befba6826
	Changelog {
		2017-01-14: ""
	}
}


