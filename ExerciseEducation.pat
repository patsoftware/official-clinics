/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Education ExerciseEducation {
    Questions {
        ExerciseEducationIntro
        ExerciseKnowledge
        ExerciseRelevant
        ExerciseBenefit
        ExerciseOpportunity
        ExerciseBodyConcerns
        ExerciseAction
    }
    AppliesTo "PATv3"
	Id de616f5c-fb4b-402c-a24f-de8b0f622176
}

Question ExerciseEducationIntro {
    "*Exercise*

    Moderate physical activity is an activity that causes a #*slight but noticeable*# increase in breathing, heart rate and in some, light sweating.
    
    The aim is #30 minutes at least 5 days per week#. The 30 minutes can be achieved by 3 sessions of 10 minutes duration.

    There are #*four* common reasons# why people do not exercise.
    
    We would like you to #*think carefully*# of why you are currently not exercising enough *before* you press \"Next\".
    
    There may be more than one reason relevant to you."
    
    Display
    Category "lifestyle"
    Id fd2829f0-aeb8-4d59-8f28-67bf3faea0ab
}

Question ExerciseKnowledge {
    "image:exercise-education/exercise-knowledge.jpg[]

    #Knowledge#: Some people do not know that exercise has multiple health benefits including a 50% reduction of the risk of cardiovascular disease (heart attack and stroke).
    
    Do you think exercise helps some health problems?"
    
    YesNo
    Category "lifestyle"
    Id affe452b-5b9f-40b1-bbc2-27464696b81f
}


Question ExerciseRelevant {
    "image:exercise-education/exercise-relevance.jpg[]

    #Personal relevance#: An example is: ‘I am pretty healthy and only have a bit of blood pressure so I don't need exercise’. So some people understand the theoretical benefits, but think it is not personally relevant at this stage in their life.
    
    Do you think exercise will be helpful for you at this time?"
    
    YesNo
    Category "lifestyle"
    Id 0c004164-6412-4ac7-991d-e59d75ec723e
}


Question ExerciseBenefit {
    "image:exercise-education/exercise-netBenefit.jpg[]

    #Lack of net benefit#:  An example is: ‘I know I will live longer if I exercise, but it's so hard to get out of bed in the morning’. So for some people the benefits of exercise are outweighed by the immediate disadvantages.
    
    Is exercising worth the effort for you at this time?"
    
    YesNo
    Category "lifestyle"
    Id f7d8a4b9-d359-46c5-89c7-990ecfa8ce14
}

Question ExerciseOpportunity {
    "image:exercise-education/exercise-opportunity.jpg[]

    #Lack of opportunity#: For some people who really want to exercise there are barriers, such as an arthritic knee or lack of time with long work hours.

    Is there something making it difficult for you to exercise?"
    
    YesNo
    Category "lifestyle"
    Id 30202272-48c8-449f-8a24-181a7e8881a7
}


Question ExerciseBodyConcerns {
    "
    :linkattrs:
    [.big]
    These are pictures representing beneficial effects of exercise.(link:https://www.gov.uk/government/publications/health-matters-getting-every-adult-active-every-day/health-matters-getting-every-adult-active-every-day[^1^,title='NHS Guidance: Health matters: getting every adult active every day (2016)',window='_blank']  link:https://www.betterhealth.vic.gov.au/health/HealthyLiving/sports-and-physical-activity[^2^,title='State Government of Victoria BetterHealth website: Sports and physical activity',window='_blank']   link:https://www.mayoclinic.org/diseases-conditions/high-blood-pressure/in-depth/high-blood-pressure/art-20045206[^3^,title='Mayo Clinic: Exercise: A drug-free approach to lowering high blood pressure',window='_blank'])
    
    Please press *any box* containing a benefit of exercise that you think may be relevant to you.
    image:ExerciseBody.png[]
    "
    
    Category "lifestyle"
    SubQuestions {
        InterestedExerciseBenefitsPsyche
        InterestedExerciseBenefitsBrain
        InterestedExerciseBenefitsLungs
        InterestedExerciseBenefitsHeart
        InterestedExerciseBenefitsBloodPressure
        InterestedExerciseBenefitsArteries
        InterestedExerciseBenefitsPancreas
        InterestedExerciseBenefitsWaistSize
        InterestedExerciseBenefitsErections
        InterestedExerciseBenefitsMuscles
        InterestedExerciseBenefitsBones
        InterestedExerciseBenefitsLegArteries
        InterestedExerciseBenefitsBreastCancer
        InterestedExerciseBenefitsBowelCancer
    }
    DisplaySettings "allow-next-when-answered: 0"
    Id 2cc4434e-675b-48f2-87ff-d75ea2af38dc
}

Question InterestedExerciseBenefitsPsyche {
    "Psyche: release of endorphins improves mood image:bodyparts/masks.png[]"
    
    YesNo
    Category "lifestyle"
    Id dfaf09f8-a675-493f-a03e-34b8a2434bdd
}

Question InterestedExerciseBenefitsBrain {
    "Brain: reduces dementia by up to 30%, reduces stroke risk image:bodyparts/brain.png[]"
    
    YesNo
    Category "lifestyle"
    Id 99a27c97-0d68-4c53-b832-3923ec1a3552
}
Question InterestedExerciseBenefitsLegArteries {
    "Leg arteries: improves circulation in peripheral vascular disease image:bodyparts/LegArteries.png[]"
    
    YesNo
    Category "lifestyle"
    Id ac0e8dfe-3189-4ac1-b23b-75a17082f7e1
}
Question InterestedExerciseBenefitsLungs {
    "Lungs: improves exercise capacity (including in COPD) image:bodyparts/lungs.png[]"
    
    YesNo
    Category "lifestyle"
    Id 25726f53-26e8-4025-b2cb-309b63ac1dc1
}
Question InterestedExerciseBenefitsHeart {
    "Heart: reduces risk of heart attacks, improves function in heart failure image:bodyparts/heart.png[]"
    
    YesNo
    Category "lifestyle"
    Id 6db50020-2753-4977-8c68-a8ee88e670fc
}
Question InterestedExerciseBenefitsBloodPressure {
    "Blood pressure: lowers by 4 - 9 on average image:bodyparts/tape_measure.png[]"
    
    YesNo
    Category "lifestyle"
    Id d1e57532-4928-4e64-ae5d-2e0327ba48ce
}
Question InterestedExerciseBenefitsWaistSize {
    "Waist size: reduces visceral fat image:bodyparts/waist.png[]"
    
    YesNo
    Category "lifestyle"
    Id 57a8b470-7057-4e8f-861e-6f82e0f21d2c
}
Question InterestedExerciseBenefitsMuscles {
    "Muscles: enhances fitness image:bodyparts/muscles.png[]"
    
    YesNo
    Category "lifestyle"
    Id c0508229-b698-49f3-8e4f-1b139f6294a3
}
Question InterestedExerciseBenefitsArteries {
    "Arteries: reduces cholesterol and fat build-up image:bodyparts/artery.png[]"
    
    YesNo
    Category "lifestyle"
    Id 8e112ef7-d651-4fca-a05a-00e9deb86596
}
Question InterestedExerciseBenefitsErections {
    "Erections: improves erection strength image:bodyparts/penis.png[]"
    
    YesNo
    Category "lifestyle"
    Id b545c727-9080-4011-b163-5a47921ac047
}
Question InterestedExerciseBenefitsPancreas {
    "Pancreas: reduces diabetes by up to 40% image:bodyparts/pancreas.png[]"
    
    YesNo
    Category "lifestyle"
    Id f8a52fb6-131d-4f39-a74e-920be914d006
}
Question InterestedExerciseBenefitsBones {
    "Bones: protects from osteoporosis image:bodyparts/bone.png[]"
    
    YesNo
    Category "lifestyle"
    Id 627ce9b8-a2e8-45bb-a390-5d63c55ff17b
}

Question InterestedExerciseBenefitsBreastCancer {
    "Breasts: reduces cancer risk by up to 20% image:bodyparts/breast.png[title='Image thanks to Breast diagram - Cancer Research UK / Wikimedia Commons']"
    
    YesNo
    Category "lifestyle"
    Id 4909d43f-b073-4b7e-b29e-a2b87b99c2b6
}

Question InterestedExerciseBenefitsBowelCancer {
    "Bowels: reduces cancer risk by up to 30% image:bodyparts/bowels.png[]"
    
    YesNo
    Category "lifestyle"
    Id 8861fa36-907b-4fd1-b58a-4f79e1372372
}


Question ExerciseAction {
    "People take different amounts of time to decide to change.
    What stage are you at about starting to do more exercise?
    
    Touch one of the 4 answers."
    
    Select one ExerciseAction
    DisplaySettings "selection-background-colour: '#FFFF80'"
    Category "lifestyle"
    Id f32e4d7d-1485-42bf-a30d-3e022fb5d11f
}

Selection ExerciseAction {

    Option precontemplative { "Not ready to think about it at all yet"}
    Option contemplative    { "I will think about it over the next few months"}
    Option ready            { "I will be ready to start within a month" }
    Option action           { "I am ready to start today" }
    Id 09915d5b-f812-4095-9a81-b5af0ee2f2e2
}