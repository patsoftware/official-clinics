/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with osteoarthritis */
Clinic GPMPArthritisOsteoarthritis {
	Id 5eb844be-9437-4e92-b339-c2483aea64b2
	Filter ArthritisOsteoarthritis
	Questions {
		Patient {
			OAProcess
            ArthritisMisconceptions
			PainChartNeeded
			ParacetamolNotControllingPain
			ArthritisFlareUpControl
			ArthritisOrBackPainExtraIdeas
			MobilityAidNeeded
			WeightProblem
			NationalSupportOrganisationReferralNeeded			
		}

		Checklist {
		}

		Staff {
			OAPatternAnalysisNeeded
			OrthopaedicReferralNeeded
			PhysioAssessmentNeeded
		}

	}
	Changelog {
		2017-01-14: ""
	}
}
Clinic GPMPArthritisOsteoarthritisCore {
	Id 5eb844be-9437-4e92-b339-c2483aea64b3
	Filter ArthritisOsteoarthritis
	Questions {
		Patient {
			OAProcess
            
			PainChartNeeded
			ParacetamolNotControllingPain
			ArthritisFlareUpControl
			ArthritisOrBackPainExtraIdeas
			MobilityAidNeeded
			WeightProblem
			NationalSupportOrganisationReferralNeeded			
		}

		Checklist {
		}

		Staff {
			OAPatternAnalysisNeeded
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Question ArthritisMisconceptions {
	"There are two common #misconceptions# about osteoarthritis (OA):

    - Exercise will worsen my arthritis.

    - My arthritis will inevitabily get worse and there is nothing I can do about it.

    Both of these are #NOT TRUE#!

    Do you want to discuss this further with your doctor?
    "
    YesNoNA
	NAText "No arthritis"
    Category "arthritis"
    Triggers {
		Yes: problem ArthritisMisconceptions
	}
    Id 8ee7bcc8-b3c2-4062-82bc-652835c74a1e
    Changelog {
        2019-08-07: ""
    }
}

Problem ArthritisMisconceptions {
	Goal "To correct misconceptions"
    Plan "Think of pain with exercise as going to the gym. No pain no gain."
    Id b931b1c7-792d-49e8-a174-af129cae76d5
    Changelog {
        2019-08-07: ""
    }
}


Filter ArthritisOsteoarthritis {
	Match {
		keyword "OA"
		keyword "Osteoarthritis"
		regex "^arthritis$"
	}
	Tests matching only this {
		"OA"
		"Osteoarthritis"
	}
}

Question ArthritisAustraliaMembership {
	"
    #Arthritis Australia# is a not for profit organisation that provides support and information for all patients with arthritis.

	Would you like the website or contact phone number for #Arthritis Australia#?
    "
    
    YesNoNA
	NAText "No arthritis"
    Category "arthritis"
    Triggers {
		Yes: problem ArthritisAustraliaOrOsteoporosisAustraliaMembershipNeeded
    }
    Id e1b469f7-4354-4066-a1f7-4a777d108fd4
    Changelog {
        2017-10-22: ""
    }
}



Question OAProcess {
	"#Osteoarthritis# causes #thinning# and eventually breakdown, of the #cartilage layer# which acts like a cushion for the joint. 
	
	This loss of cushioning causes tiny stress fractures in the underlying bone. The cartilage breakdown products also cause inflammation (redness and swelling) of the joint lining (synovium), and fluid in the joint. 
	
	This thinning process leads to reduced ability of the joint to withstand #impact# #shock# such as occurs in the knee and hip when walking. 
	
	This is why the simple plan of always wearing footwear with #thick soft# #soles# and #heels#, which can #absorb impact shocks,# helps control pain. 
	
	#Do you wish to see an illustration of what happens inside the joint?#"
	
	YesNoUnsure
	Category "arthritis"
	
	Triggers {
		Yes Unsure: problem OAProcess
		Yes Unsure: education optional OsteoarthritisProcess
	}
	Id eb30408e-766d-4425-9d8b-ffe3d439ff9e
	Changelog {
		2017-01-14: ""
	}
}


Question ArthritisFlareUpControl {
	"For any type of knee or hand joint arthritis, #rub-in (topical) medications#, are the next step to add to paracetamol tablets. #NSAID rubs# (e.g. Voltaren gel) are useful for knee and hand arthritis. 
	
	These are ideally used for #flare-ups#.
	
	Do you wish to discuss rub-in medications for flare-ups with your doctor? 
	
	[small]#(RACGP Osteoarthritis Guideline)#"
	
	YesNoNA
	Category "arthritis"
	
	Triggers {
		Yes: problem ArthritisFlareUpNotControlled
	}
	Id a82871b8-baec-411a-8c58-1a0f9672987d
	Changelog {
		2017-01-14: ""
	}
}

Question WeightProblem {
	"Carrying some extra weight causes a ‘#double hit#’:
	
	* It puts a mechanical load on the arthritic joints and your back. 
	* Fat produces inflammatory substance (cytokines) that circulate in your blood and damage all your joints. 
	
	It has been estimated that if all overweight people with arthritis lost #just 5kg#, approximately 25 to 50% of all knee replacements could be avoided. 
	
	Do you need to lose weight?"
	
	YesNoUnsure
	Category "arthritis", "lifestyle"
	
	Triggers {
		Yes: problem WeightLossDesired
		Yes: education optional WeightEducation
	}
	Id cdd2c151-16b3-4d7f-8555-011e858eed92
	Changelog {
		2017-01-14: ""
	}
}


Question OAPatternAnalysisNeeded {
	"#Classification# of #primary# polyarticular osteoarthritis (POA) is into two main #phenotypes#. 
	
	#Type 1# = nodal generalized arthritis. Involving DIP joints of hands, hips, knees, great toe MTP. Heberden's nodes often present. 
	
	#Type 2# = MCP joints hands, atypical joints such as wrist and ankle. Often associated with heterozygote haemochromatosis carrier. 
	
	#Secondary# OA is due to joint damage from any cause. 
	
	Do they have any type of Osteoarthritis?"
	
	YesNoNA
	NAText "N/A - No arthritis "
	Category "arthritis"
	
	Triggers {
		Yes: problem OAPatternAssessmentNeeded
	}
	Id d4f57028-0766-4acb-ad8a-f436140fd6b8
	Changelog {
		2017-01-14: ""
	}
}


Problem OAProcess {
	Goal "Understand the process"
	Plan "Explain process."
	Id cd96d1d0-f3d4-4577-8f7a-975b4ec1c404
	Changelog {
		2017-01-14: ""
	}
}


Problem OAPatternAssessmentNeeded {
	Goal "Classify for appropriate treatment"
	Plan "Check for haemochromatosis if type 2. Check for degree of problem in typically involved joints. Check current analgesia regimen."
	Id d7ca9efc-f7dd-49f7-8d79-56b46c166a03
	Changelog {
		2017-01-14: ""
	}
}


Problem ArthritisFlareUpNotControlled {
	Goal "Control pain"
	Plan "Consider oral NSAID trial if no contraindications and monitor with pain chart."
	Id de82d9b1-1abc-48f3-b9e6-6daf0c1f504d
	Changelog {
		2017-01-14: ""
	}
}

Education OsteoarthritisProcess {
	Message "Osteoarthritis process"
	File "jpg"
	Id 4ffad618-8e0c-49f4-bf3c-d5d3fe2a14b9
	Changelog {
		2017-01-14: ""
	}
}


