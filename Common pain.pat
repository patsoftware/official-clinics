/* 
 * Copyright (c) 2017 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */


Question ArthritisOrBackPainExtraIdeas {
	"Here is a list of #extra ideas# that do have evidence for helping #some# people with arthritis or back pain.
	
	- Turmeric based compounds may be effective  
	- Hydrotherapy (exercise in a pool) 
	- Thermotherapy (heat packs, warm baths) 
	- Tai Chi (stretching exercise programme). Two sessions per week improve balance, flexibility and cardio fitness. 
	- Using a walking stick if you have knee arthritis helps take the weight off the knee. 
    - Using a knee brace from a physio or wedged insoles from a podiatrist if knee malalignment is present.
	- TENS (Transcutaneous electrical nerve stimulation) portable unit.
	
	[small]#(RACGP Osteoarthritis Guideline)#

	Do you wish to #discuss any# of those listed above?"
	
	YesNoUnsure
	Category "pain"
	
	Triggers {
		Yes Unsure: problem ArthritisOrBackPainExtraIdeas
	}
	Id 939b0cd3-a346-4b5b-9303-37d09a3b93e1
	Changelog {
		2017-01-14: ""
	}
}


Problem ArthritisOrBackPainExtraIdeas {
	Goal "Extend non-medication options"
	Plan "Discuss and refer as required. Strongly consider EPC. Manual therapy programme of up to 9 sessions over 12 weeks may be useful for back pain."
	Id 3ca68896-52e6-4e2a-8528-4a78b92d33a3
	Changelog {
		2017-01-14: ""
	}
}


Education BriefPainInventory {
	Message "Brief Pain Inventory "
	Link "http://www.aci.health.nsw.gov.au/__data/assets/pdf_file/0015/212910/Brief_Pain_Inventory_Final.pdf"
	Id 2f981515-9ca1-4db5-8524-515e68daa6bf
	Changelog {
		2017-01-14: ""
	}
}

Education DiscontinuingOpioids {
	Message "Discontinuing opioids"
	File "rtf"
	Id 817827e5-f830-4ab7-87a4-305a5f3bde2c
	Changelog {
		2017-01-14: ""
	}
}

Education OpioidRiskTool {
	Message "Opioid risk tool "
	File "rtf"
	Id 43e0fa29-0141-4d64-ba10-e502e3c10627
	Changelog {
		2017-01-14: ""
	}
}


Question PainAppropriateDiagnosis {
	"Step 1: Has an #appropriate diagnosis# been made? 
	
	* Review your records and external evaluations to make sure there is not a treatable cause. 

	* Make sure there are not any 'red flags' of possible serious underlying conditions requiring further medical intervention. 
    "
	
	YesNoNA
	Category "pain"
	Education "Red  Flags" {"
	* http://www.hnehealth.nsw.gov.au/Pain/Documents/red%20and%20yellow%20flags.pdf[Red and Yellow Flags. Hunter integrated pain service]
	
	"}
	Triggers {
		No: problem PainAppropriateDiagnosisNotApparentOrCo-morbidityPresent
		Yes: education optional RedFlags
	}
	Id 5eefab12-7749-401b-925a-14651d28d766
	Changelog {
		2017-01-14: ""
	}
}


Problem PainAppropriateDiagnosisNotApparentOrCo-morbidityPresent {
	Goal "Check all pain patients for appropriate diagnosis and significant co- morbid conditions."
	Plan "Investigate further."
	Id 7298710c-5454-42b6-8813-ca2c53bd2535
	Changelog {
		2017-01-14: ""
	}
}


Question PainAssessment {
	"Step 5: A formal #Pain Assessment# before and after the trial of opioids enables a decision on response to therapy. 
	
	Have you performed a baseline Brief pain inventory (BPI)?"
	
	YesNoNA
	Category "pain"
	Education "Brief Pain Inventory" {"
	* http://www.hnehealth.nsw.gov.au/Pain/Documents/BPI.dec06.pdf[Brief Pain Inventorty. Hunter integrated pain service]
	
	"}
	Triggers {
		No: problem PainMedicine10UniversalStepsNeeded
		Yes: education optional BriefPainInventory
	}
	Id eadc1234-50c0-4c6f-a316-ee56257afcc2
	Changelog {
		2017-01-14: ""
	}
}


Question PainChartNeeded {
	"It is really important to know #if a new medication works.# You can use a #pain score chart# for this: 
	
	- Score the amount of pain, from the problem area, experienced over the whole day from 0 (no pain) to 10 (worst possible pain) at the end of each day for 2 weeks. Then work out your average daily score (#baseline score#). 
	- Take the new pain medication and #keep all your other tablets the same#. 
	- Score your daily pain again for the next 2 weeks. Then work out your average daily score (#intervention score#). 
	- If the average score markedly decreases the new medication is effective. 
	
	Do you wish to discuss using a pain chart more with your doctor?"
	
	YesNoUnsure
	Category "pain"
	
	Triggers {
		Yes Unsure: problem PainChartNeeded
	}
	Id 93826b70-f76e-40c0-a9f8-fcf4f97e2580
	Changelog {
		2017-01-14: ""
	}
}


Problem PainChartNeeded {
	Goal "Make sure the new medication works for you"
	Plan "Add the extra agent to regular paracetamol and keep a pain chart."
	Id 555d7d1c-76ef-406b-814d-fc53090bcb42
	Changelog {
		2017-01-14: ""
	}
}


Problem ParacetamolNotControllingPain {
	Goal "Control pain"
	Plan "Add an extra agent to regular paracetamol such as a rub as needed. Anti-inflammatory NSAID gel rubs can be used. Using Gladwrap overnight allows NSAID gel to penetrate better."
	Id 109125b4-58f4-4311-b037-bd1056bd2c64
	Changelog {
		2017-01-14: ""
	}
}


Question ParacetamolNotControllingPain {
	"#Pain# is one of the main problems #with any type# of arthritis or back pain. 
	
	We may not be able to completely remove the pain but we can help reduce it significantly. 
	
	#Plan paracetamol# (e.g. Panadol, Panamax) is the #best to start with# as there is no data suggesting added benefit from slow release paracetamol. 
	
	If your pain is #mild,# just take 2 tablets #as needed# 20 minutes before doing an activity that usually causes pain (such as your daily walk). 
	
	Is your pain adequately controlled by paracetamol? 
	
	[small]#(RACGP Osteoarthritis Guideline)#"
	
	YesNoNA
	NAText "N/A - Do not have significant pain"
	Category "pain", "medications"
	
	Triggers {
		No: problem ParacetamolNotControllingPain
	}
	Id fc66e3e2-7d65-4718-97d5-58b4f5316653
	Changelog {
		2017-01-14: ""
	}
}

Question PainComorbidConditionsPresent {
	"As part of step 1 does the patient have #any of these comorbid conditions# already present? 

	* Substance abuse? 
	
	* Psychiatric illness?"
	
	YesNoNA
	Category "pain"
	
	Triggers {
		Yes: problem PainAppropriateDiagnosisNotApparentOrCo-morbidityPresent
	}
	Id 15e73e25-5cb6-4beb-ad38-0466ed4e3817
	Changelog {
		2017-01-14: ""
	}
}


Question PainMedicationDocumentationNeeded {
	"Step 10: Appropriate #documentation# of opioid use. 
	
		
	Have you performed such documentation?"
	
	YesNoNA
	Category "pain", "medication"
	Education "Opioid Documentation" {"

     * At each follow-up visit it is in the best clinical interest of both parties as well as important medico-legally (evidence of appropriateness of treatment) to document progress in progress notes. 

	* Legislative authority/approval/permit from your relevant State Pharmaceutical Services or Drugs of Dependence Unit (required if > 2months opioid use). 

	* Schedule 8 Medicare authority prescriptions.  
	"}
	Triggers {
		No: problem PainMedicine10UniversalStepsNeeded
	}
	Id 3ab8d440-8cc1-4507-96cb-7088d71c86e4
	Changelog {
		2017-01-14: ""
	}
}


Question PainInformedConsentNeeded {
	"Step 3: #Informed consent#. 
	
	
	Has the patient given informed consent?"
	
	YesNoNA
	Category "pain", "medication"

	Education "Informed consent checklist" {"

	#Potential Benefits# of opioid use:
	
	* Reduction in pain 

	* Improvements in activities of daily living 

	* Increased performance of agreed on exercises 
	
	#Potential risks# of opioid use:
	
	* Potential to develop tolerance, dependence and/or addiction to opiate. 

	* Potential to develop side effects, including mental clouding and sedation, constipation, nausea. 

	* Acceleraton of bone density loss, hypogonadism and increased risk of heart attack.   
	"}
	Triggers {
		No: problem PainMedicine10UniversalStepsNeeded
	}
	Id 66bb82ba-86f4-4b8e-af51-a68ca44d7fdd
	Changelog {
		2017-01-14: ""
	}
}


Education PainMedicine10Steps {
	File "rtf"
	Id 9a68b2d3-4e22-46fc-bad9-7f3306e6a716
	Changelog {
		2017-01-14: ""
	}
}


Question PainMedicine10UniversalPrecautionsNeeded {
	"The aim of opioid use in chronic pain is not to remove pain but to  #reduce pain# without causing distressing side-effects thus enabling patient function to improve. 

	* Opioid therapy for *longer than 90 days* is associated with continuing use and worse functional status.

	* The recommended maximal daily dose is *40mg of oral morphine equivalent*.
	
	* It is recommended that *all patients put on regular opioids* should work through the #10 steps of universal precautions# in pain medicine. 
	
	
	Do you wish to work through the 10 steps now with your patient?"
	
	YesNo
	Category "pain", "medication"
	Education "10 steps of universal precautions" {"

	Pain Medicine: 10 steps of universal precautions when prescribing opioids.

	* Step 1: Appropriate diagnosis

    *  Step 2: Psychological risk assessment for abuse or addiction

    *  Step 3: Informed consent

    *  Step 4: Treatment agreement

    *  Step 5: Pain Assessment

    *  Step 6: Opioid trial

    *  Step 7: Follow-up Assessment

    *  Step 8: The 6 As of Pain Medicine

    *  Step 9: Periodic review

    *  Step 10: Documentation

* https://www.medscape.org/viewarticle/503596_3[Universal Precautions in Pain Medicine]

	"}
	
	Triggers {
		No: AutoAnswer {
			PainAppropriateDiagnosis NA
			PainComorbidConditionsPresent NA
			RiskForAbuseOrAddictionModerateOrHigh NA
			PainYellowFlagsPresent NA
			PainInformedConsentNeeded NA
			PainTreatmentAgreementNeeded NA
			PainAssessment NA
			PainOpioidTrialNeeded NA
			PainTrialAssessmentNeeded NA
			PainMedicine6AsCheckNeeded NA
			PainPeriodicReviewNeeded NA
			PainMedicationDocumentationNeeded NA
		}
		Yes: problem PainMedicine10UniversalStepsNeeded
		Yes: education optional PainMedicine10Steps
	}
	Id baad12c5-b950-4e85-89f7-a46c017fb19f
	Changelog {
		2017-01-14: ""
	}
}


Problem PainMedicine10UniversalStepsNeeded {
	Goal "Ensure all long term opioid patients have the 10 steps done at least once."
	Plan "10  step plan."
	Id 85cd8d4a-0964-4c44-a5df-7b6ccd184f2e
	Changelog {
		2017-01-14: ""
	}
}


Question PainMedicine6AsCheckNeeded {
	"Step 8: The #6 As# of Pain Medicine need to be regularly checked (weekly in the trial and monthly thereafter): 
	
		
	Do you need to modify your treatment as a result of the 6As?"
	
	YesNoNA
	Category "pain", "medication"

	Education "6As of pain medicine monitoing tool" {"
     #6As of pain medicine#

     1 #Activity#

     * What progress has been made in the patients functional goals?

     2 #Analgesia#

     * How does the patient rate their average and worst pain over the last 24 hours?

     * How much relief have pain medications provided?

     3 #Adverse effects# 

     * Has the patient experienced any adverse effects from medication?

     4 #Aberrant behaviour#

     * Has the patient been taking medication as prescribed?

     * Has the patient exhibited any signs of medication misuse/behaviours?

     5 #Affect#

     * Have there been any changes to the way the patient has been feeling?

     * Is pain impacting on the patient’s mood? (Depressed or Anxious)

    6  #Accurate records#

     * Document the initial evaluation and each follow-up, including current pain medication and any changes to the management plan.   

	 * http://www.trmc.net.au/pdf/sixas-opioid-therapy-monitoring-tool.pdf[6 As – Opioid therapy monitoring tool]
	"}
	Triggers {
		Yes: problem PainMedicine10UniversalStepsNeeded
		Yes: education optional PainMedicine6As
	}
	Id 500d9ea4-55b9-4872-b50e-e524477f32aa
	Changelog {
		2017-01-14: ""
	}
}


Education PainMedicine6As {
	File "rtf"
	Id 3c9f7229-2203-46e7-aebb-4e38f3509991
	Changelog {
		2017-01-14: ""
	}
}


Question PainOpioidTrialNeeded {
	"Step 6:An #opioid trial# will establish whether the patient's chronic moderate to severe pain is responsive to opioid therapy 
	
	* Duration 4-6 weeks for first-time patients 
	
	* Start at a low dose and gradually titrate upwards if required 
	
	Have you arranged follow up for this trial?"
	
	YesNoNA
	Category "pain", "medication"
	
	Triggers {
		No: problem PainMedicine10UniversalStepsNeeded
	}
	Id 93f0422f-d046-46c1-a231-cc50693e592f
	Changelog {
		2017-01-14: ""
	}
}


Question PainPeriodicReviewNeeded {
	"Step 9: Arrange #Periodic review# 
	
	Have you arranged periodic review? 
    "
	
	YesNoNA
	Category "pain", "medication"
	Education "Periodic review tips" {"

	* If the patients pain is opioid responsive, consider longer-term opioid therapy for a duration of 3-6 months with monthly reviews.

	* Review interacting co morbid conditions at these periodic reviews.

	* Ensure the patient is #actively participating in all aspects# (including non-pharmacological) of their pain management plan.

	* Once improvement in function occurs reduce opioid dose by 10% at each regular monthly review. 	
	
	"}
	Education "Discontinuing opioids" {"
    #Cessation of opioid therapy requires gradual dose reductions over time.#
		
	The decision to discontinue opioid therapy may be made for a variety of reasons including:

	* Successful therapeutic outcomes

	* If the pain is unresponsive to opioid therapy

	* Ongoing adverse effects

	* Development of aberrant behaviours

	* Development of a predominance of psychological issues

	* Patients choice

	* Opioid-related side effects


	#Common opioid side effects#:

	* Dry mouth

	* Nausea and vomiting

	* Opioid-induced constipation (OIC)

	* Postural hypotension

	* Pruritus

	* Sedation

	#Other side effects#:

	* Hormonal effects

	* Hyperalgesia

	* Immunosuppression

	* Respiratory depression
	
	* Tolerance and addiction
	
	"}
	Triggers {
		No: problem PainMedicine10UniversalStepsNeeded
		Yes: education optional DiscontinuingOpioids
	}
	Id e14c363c-dff2-4953-8f3c-98a2dea5c4a4
	Changelog {
		2017-01-14: ""
	}
}


Question PainPharmacotherapyChangeNeeded {
	"#Pharmacotherapy# may involve both analgesic and adjuvant medications. 

	[cols=\"1,3\" width=70%]
	:===
	Analgesics:Adjuvant analgesics

	Paracetamol:Antidepressants
	NSAIDs:NMDA-receptor antagonists (e.g. ketamine)
	Opioids:Benzodiazepinnes
	:Corticosteroids 
	:Anti-epileptic drugs (e.g. pregabalin, gabapentin, carbamazepine)
	:===
	[small]#(Analgesic Expert Group. Therapeutic Guidelines: Analgesic. Version 6 Melbourne. Therapeutic Guidelines Limited. 2012.)#


	Do you need to change medications?

	[small]#(click below for starting doses)#"
	
	YesNoNA
	Category "pain", "medications"
	
	Triggers {
		NA: AutoAnswer {
			PainLadderOpioidPrescription NA
		}		
		Yes: problem PainPharmacotherapyChangeRequired
		Yes: education optional PainPharmacotherapyOptions
		Yes: education optional OpioidPatchAdvice
	}
	Id e477023c-a915-4ec1-ab9a-5f7e11cc4c0a
	Changelog {
		2017-01-14: ""
	}
}


Problem PainPharmacotherapyChangeRequired {
	Goal "Reduce pain"
	Plan "Move up the pain ladder +/- add adjuvant."
	Id 26168a0f-e6ed-43cd-9b76-c0d3817eee04
	Changelog {
		2017-01-14: ""
	}
}

Problem PainSpecialistAssessmentNeeded {
	Goal "Pain reduction"
	Plan "Review and implement plan."
	Id 39f803fc-44de-428a-862f-c7ae20003457
	Changelog {
		2017-01-14: ""
	}
}


Question PainSpecialistReferralNeeded {
	"Does this patient require #referral to a pain specialist/clinic#, or if back pain, an intensive back rehabilitation programme?"
	
	YesNoNA
	Category "pain"
	Education "Early referral indications" {"
	 #Early referral to a pain specialist or multidisciplinary pain clinic# is indicated for: 

     * Patients who are psychologically unstable (very distressed).

	 * Patients with a history of addiction to drugs or alcohol.

	 * When previous use of opioids has been problematic.

	 * Young patients with obscure pathology.
	 
	 * Patients with significant psychological issues e.g. somatisation, PTSD, mood disorder, borderline personality disorder.

	 * Complex compensation cases e.g. where response to treatment suggests factors other than nociception are playing a role.

	"}
	Triggers {
		Yes: problem PainSpecialistAssessmentNeeded
		Yes: education optional PainSpecialistReferral
	}
	Id 52286201-70f9-404a-bfbb-9461f417006d
	Changelog {
		2017-01-14: ""
	}
}


Education PainSpecialistReferral {
	File "rtf"
	Id 396e9a9d-6dc9-41f7-80c0-f5754667dee4
	Changelog {
		2017-01-14: ""
	}
}


Question PainTreatmentAgreementNeeded {
	"Step 4 Have you explained that a #treatment agreement# for initiation, continuation and termination of opioid treatment is incorporated in the PAT final plan? 
	
	[red]#Emphasise that either:# 
	
	* [red]#not meeting the goals of therapy# 
	* [red]#or development of aberrant behaviours.# 
	
	are grounds for discontinuing therapy"
	
	YesNoNA
	Category "pain", "medications"
	
	Triggers {
		No: problem PainMedicine10UniversalStepsNeeded
		Yes: education optional PainTreatmentAgreement
	}
	Id e2df6360-21b9-414b-922a-e4c3d2b26859
	Changelog {
		2017-01-14: ""
	}
}


Education PainTreatmentAgreement {
	Message "Pain treatment agreement"
	File "rtf"
	Id f07bdd18-8b4f-4438-a7a9-b09d569bf2f2
	Changelog {
		2017-01-14: ""
	}
}


Question PainTrialAssessmentNeeded {
	"Step 7: #Follow-up Assessment# 
	
	* Initially weekly during the trial period 
	* Assess pain and function using BPI or other scale 
	* Decide whether to continue, modify dose, or withdraw opioid 
	
	[red]#A valid outcome of an opioid trial is the decision NOT to proceed with treatment# 
	
	Have you arranged suitable follow up?"
	
	YesNoNA
	Category "pain", "medications"
	
	Triggers {
		No: problem PainMedicine10UniversalStepsNeeded
	}
	Id 7dfb2c0c-1177-40c5-891c-f7592e12c1ec
	Changelog {
		2017-01-14: ""
	}
}


Question PainYellowFlagsPresent {
	"* [yellow]#Psychosocial 'yellow flags'# may predict which patients will develop long-term disability and pain. 

	* These have already been screened for in the patient questions and via the pain wizard.

	* These results will automatically list in the summary.

	* (however click below if you wish to see a complete list of yellow flags) 
	
	Do you want to check for yellow flags in the summary?"
	
	YesNoNA
	Category "pain"
	Education "Yellow Flags" {"
	[yellow]#Yellow flags# are psychosocial indicators suggesting increased risk of progression to long term distress disability and potential drug misuse.

[yellow]#Workplace#:

	* Belief that all pain must be abolished before attempting to return to work or normal activity

	* Expectation or fear of increased pain with activity/work

	* Poor work history

	* Unsupportive work environment

[yellow]#Attitudes and beliefs#:

	* Belief that pain is harmful resulting in avoidance  and poor compliance with exercise

	* Catastrophising – thinking the worst

	* Misinterpreting bodily symptoms

	* Belief that pain is uncontrollable

	* Expectation of the techno –fix for pain

[yellow]#Social/ family#:

	* Over protective partner or spouse 

	* Socially punitive partner or spouse 

	* Lack of support to talk about problems

[yellow]#Behaviours#: 

	* Passive approach to rehabilitation

	* Use of extended rest 

	* Reduced activity with withdrawal from activities of daily living

	* Avoidance of normal activity

	* Impaired sleep because of pain

	* Increased intake of alcohol or other substances since the onset of the pain



[yellow]#Affective/emotions#:
	* Depression

	* Feeling useless

	* Irritability

	* Anxiety about heightened bodily sensation

	* Disinterest in social activity

* https://www.racgp.org.au/afpbackissues/2004/200406/20040601jensen.pdf[Back Pain Clinical Assessment. S Jensen]
	
	"}
	Triggers {
		Yes: problem PainYellowFlagsPresent
		Yes: education optional PainYellowFlags
	}
	Id 797dd2a2-5589-4453-b97b-f60b9e133a50
	Changelog {
		2017-01-14: ""
	}
}


Problem PainYellowFlagsPresent {
	Goal "Assess any yellow flags"
	Plan "Incorporate patient answers to yellow flag questions with the Pain Wizard summary."
	Id 69f0c473-564c-4d5d-bdcb-5f7908d93cf7
	Changelog {
		2017-01-14: ""
	}
}


Education PainYellowFlags {
	File "rtf"
	Id e9a2431c-d279-4065-a3a6-b3cedf56b44e
	Changelog {
		2017-01-14: ""
	}
}


Question ChronicPainPresent {
	"#Chronic pain# is pain that continues beyond the usual time of healing (or expected time of recovery), defined as #longer than 3 months.# 
	
	If you are going this clinic then your doctor thinks you have chronic pain. 
	
	Your doctor's goal is to 
	
	* #Reduce# your pain (by >20% is considered successful and by 50 % very successful) 
	* Improve your ability to function socially and physically 
	* Enhance your quality of life 
	* Minimise the risk of adverse effects 
	
	Press yes to proceed"
	
	YesNo
	Category "pain"
	
	Triggers {
		Yes: problem PainMedicine10UniversalStepsNeeded
		Yes: education required ChronicPainEducation
	}
	Id bd4ccbe1-b7f4-456b-b6f7-144edb37cfa8
	Changelog {
		2017-01-14: ""
	}
}

Education RedFlags {
	Message "Red flags"
	File "rtf"
	Id d805da87-2c82-46b4-8d19-2adf2c436d13
	Changelog {
		2017-01-14: ""
	}
}


Question RiskForAbuseOrAddictionModerateOrHigh {
	"It is recommended to perform a #psychological risk assessment# for abuse or addiction 
	
	* https://www.mdcalc.com/opioid-risk-tool-ort-narcotic-abuse[Opioid Risk Tool (ORT)]
	
	Are they at moderate or high risk?"
	
	YesNoNA
	Category "pain", "medications"
	
	Triggers {
		Yes: problem RiskForAbuseOrAddictionModerateOrHigh
		Yes: education optional OpioidRiskTool
	}
	Id ad1474c0-6600-46a2-a774-9d8f8c807e14
	Changelog {
		2017-01-14: ""
	}
}


Problem RiskForAbuseOrAddictionModerateOrHigh {
	Goal "Screen all opioid prescription patients for addiction risk."
	Plan "Written agreement (step 4), time limited opioid trial (step 6) and monitoring for aberrant behaviour (step 8) strongly recommended."
	Id 67fdfe5c-bede-4bb4-b39f-7a617e6fe3f9
	Changelog {
		2017-01-14: ""
	}
}


Education PainPharmacotherapyOptions {
	File "rtf"
	Id ad0435e8-e3ff-49ba-9284-a8f498e45eea
	Changelog {
		2017-01-14: ""
	}
}


Education OpioidPatchAdvice {
	File "rtf"
	Id ad0435e8-e3ff-49ba-9284-a8f498e11eea
	Changelog {
		2017-01-14: ""
	}
}