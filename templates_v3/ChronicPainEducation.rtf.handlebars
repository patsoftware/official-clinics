{\b\fs22 Lifestyle suggestions: must be SMART (specific, measurable, achievable, realistic, and timely)\b0\par
{{#IfNotYes "<AlreadyEatingWell>"}} \tab eat sensibly \par {{/IfNotYes}}
{{#IfNotYes "<AlreadySleepingWell>"}} \tab get a good sleep \par {{/IfNotYes}}
{{#IfNotYes "<AlreadyExercising>"}} \tab exercise \par {{/IfNotYes}}
{{#IfNotYes "<AlreadyNonsmokerAndCarefulDrinking>"}} \tab quit smoking and moderate alcohol \par {{/IfNotYes}}
{{#IfNotYes "<AlreadyReducingStress>"}} \tab learn stress reduction \par {{/IfNotYes}}

{{#IfYes "<AlreadyEatingWell>"}}
{{#IfYes "<AlreadySleepingWell>"}}
{{#IfYes "<AlreadyExercising>"}}
{{#IfYes "<AlreadyNonsmokerAndCarefulDrinking>"}}
{{#IfYes "<AlreadyReducingStress>"}}
  \tab already eating, sleeping, exercising and relaxing \par
  \tab not smoking and is careful with alcohol \par
{{/IfYes}}
{{/IfYes}}
{{/IfYes}}
{{/IfYes}}
{{/IfYes}}
}


{\b\fs22 Ideas for Cognitive Therapy (listen carefully for helplessness)\b0\par
{{#IfYes "<PainIsHarmful>"}} \tab pain is harmful --> no pain no gain \par {{/IfYes}}
{{#IfYes "<PainIsDisabling>"}} \tab pain is disabling --> concentrate on what you can do, not can't do \par {{/IfYes}}
{{#IfYes "<PainIsIllnessNeedingCure>"}} \tab pain is an illness --> aim for wellness and reject sickness \par {{/IfYes}}
{{#IfYes "<PainHasTakenOverMyLife>"}} \tab pain controls my life --> who is in control, me or the pain? \par {{/IfYes}}
{{#IfYes "<PainUnhelpfulThoughtsNA>"}} \tab none \par {{/IfYes}}
}


{\b\fs22 Emotions requiring therapy (strongly consider review for mental health plan if depressed)\b0\par
{{#IfYes "<PainFeelingsAngry>"}} \tab angry with people or about things \par {{/IfYes}}
{{#IfYes "<PainFeelingsSad>"}} \tab sad or depressed or in despair \par {{/IfYes}}
{{#IfYes "<PainFeelingsAnnoyed>"}} \tab annoyed with myself \par {{/IfYes}}
{{#IfYes "<PainFeelingsAnxious>"}} \tab anxious or worried \par {{/IfYes}}
{{#IfYes "<PainFeelingsNA>"}} \tab none \par {{/IfYes}}
}

{\b\fs22 Stresses requiring problem solving  \b0\par
{{#IfYes "<PainStressorsHomeRelationships>"}} \tab home relationships \par {{/IfYes}}
{{#IfYes "<PainStressorsWork>"}} \tab work \par {{/IfYes}}
{{#IfYes "<PainStressorsFinancial>"}} \tab financial \par {{/IfYes}}
{{#IfYes "<PainStressorsSocialRelationships>"}} \tab social \par {{/IfYes}}
{{#IfYes "<PainStressorsNA>"}} \tab none \par {{/IfYes}}
}


{\b\fs22 Actions \b0\par
{{#IfYes "<PainActionMyself>"}} \tab I will try to make some changes myself. \par {{/IfYes}}
{{#IfYes "<PainActionDoctorsHelp>"}} \tab I would like my doctor to help me make changes. \par {{/IfYes}}
{{#IfYes "<PainActionReferralToSpecialist>"}} \tab I would like to discuss referral to counselor or psychologist for some help. \par {{/IfYes}}
{{#IfYes "<PainActionReferralToCounselor>"}} \tab I would like to discuss referral to a chronic pain specialist. \par {{/IfYes}}
{{#IfYes "<PainActionNone>"}} \tab None of these apply to me. \par {{/IfYes}}
}

