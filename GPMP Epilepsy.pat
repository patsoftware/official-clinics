/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with epilepsy */
Clinic GPMPEpilepsy {
	Id 4c8d68b6-3079-4363-bc50-813f80571b90
	Filter Epilepsy
	Questions {
		Patient {
		}

		Checklist {
			NumerOfFits12months
		}

		Staff {
			EpilepsyCycleOfCareNeeded
			EpilepsyFitFree
		}

	}
}

Clinic GPMPEpilepsyCore {
	Id 4c8d68b6-3079-4363-bc50-813f80571b91
	Filter Epilepsy
	Questions {
		Patient {
		}

		Checklist {
			NumerOfFits12months
		}

		Staff {
			EpilepsyCycleOfCareNeeded
		}

	}
}

Filter Epilepsy {
	Match {
		keyword "Convulsion"
		keyword "Epilepsy"
		keyword "Epileptic fit"
		keyword "Epilepticus"
		keyword "Seizure"
	}
	Tests matching only this {
		"Epilepsy"
		"Seizure"
		"convulsion"
		"epileptic seizure"
		"status epilepticus"
		"epileptic fits"
	}
}


Question NumerOfFits12months {
	"If they have epilepsy enter number of fits in the last 12 months (estimate acceptable)"
	
	Number required
	Category "neurology"
	Id ab4a3847-1377-bd58-8799-45db75c0a2ab
	Changelog {
		2017-01-14: ""
	}
}


Question EpilepsyCycleOfCareNeeded {
	"With any type of #epilepsy# do you want to bring them back for another appointment to complete the annual epilepsy cycle of care? 
	
	(click below for recommendations on the annual cycle of epilepsy care)"
	
	YesNoNA
	NAText "N/A - No epilepsy "
	Category "neurology"
	
	Triggers {
		Yes: problem EpilepsyPresent
		Yes: education optional EpilepsyCycleOfCare
	}
	Id 3531050d-9b00-462e-8ae7-20079a407d59
	Changelog {
		2017-01-14: ""
	}
}


Question EpilepsyFitFree {
	"If they have any type of #epilepsy# have they been #fit free# for 12 or more months?"
	
	YesNoNA
	NAText "N/A - no epilepsy "
	Category "neurology"
	
	Triggers {
		No: problem EpilepsyPresent
	}
	Links {
		NumerOfFits12months
	}
	Id 473e13e6-3be2-4a69-94bc-5529c78fcafa
	Changelog {
		2017-01-14: ""
	}
}


Problem EpilepsyPresent {
	Goal "Fit free control and assess risk factors"
	Plan "Arrange an annual cycle of epilepsy care reminder."
	Id 274f3b63-8c60-4b09-8b42-3b8c3dabe15c
	Changelog {
		2017-01-14: ""
	}
}


Education EpilepsyCycleOfCare {
	File "rtf"
	Id 45598735-2c6a-48e8-9926-6b46926d8606
	Changelog {
		2017-01-14: ""
	}
}


