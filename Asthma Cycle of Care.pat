/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Clinic AsthmaCycleOfCare {
	Id 42a9ebcc-d799-4da2-8d5a-ff0773112442
	Questions {
		Patient {
			Asthma-Controlled
			Asthma-ControlLessThan20
			AsthmaICSConcern
			AllergicRhinitis
			InhaledAllergens
			HouseDust
			CatOrDogAllergy
			MouldAllergy
			PollenAllergy
			FoodAllergy
			FoodIntolerance
			PrescriptionMedicationAggravating
			OTCMedicationAggravatingAsthma
			RefluxAggravatingAsthma
			Smoker
			AirPollutantsAggravatingAsthma
			AirChemicals
			AtmosphericChangesAggravatingAsthma
			URTITrigger
            InfluenzaImmunizationAsthma
			ExerciseTrigger
			AsthmaExercise
			InflammatoryNatureAsthma
			RelieverUse
			PreventerRole
			AsthmaCombinationInhaler
			RinseMouth
			AnnualReviewOK
			PeakFlowMeter
			FirstAid4by4by4
			DEMTreatment
			HospitalAdmission
			MedicationAdherence
			CarelessSometimesWithTaking
			StopTakingWhenWell
			SideEffectOfMedicationSuspected
			DepressionPresent
			AnxietyPresent
			AsthmaAndPregnancy
			AsthmaCouncil
			ResearchConsent
		}

		Checklist {
			Consent2552and11506
			PFMBaseline
			SpirometryBestOf3
			AdministerSalbutamol4puffs
			RecordHeightWeight
			RecordSmoking
			AsthmaScoreFromHome
			AsthmaDaysMissed
			Spirometry10MinsPostSalbutamolBestOf3
			PrintHealthSummary
			ReminderAsthmaCycleCareNeeded
		}

		Staff {
			SpirometrySABAResponsePresent
			OralSteroidsRequiredInLastYear
			LifeThreateningAttackInPastHistory
			DaysOffWithAsthmaPresent
			AsthmaPreventerDiscrepancyPresent
			COPDCo-existingWithAsthmaPresent
			COPDMildSeverity
			COPDModerateSeverity
			COPDSevere
			AgingConditionsAffectingAsthmaPresent
			AsthmaMedicationChangeNeeded
			AsthmaPreventerDecreaseNeeded
			AsthmaLAMATripleTherapy
			SpacerMDIUse
			AssessmentOfDeviceAndPFMNeeded
            SevereAsthmaPresence
			SevereAsthmaEvaluation
			ThunderstormAsthmaRisk
			AsthmaPlanNeeded
			ScriptSupplyNeeded
			EczemaPresent
			AllergicConjunctivitisPresent
			AllergicRhinitisPresent
			AllergenTestingNeeded
			PersonalisingThePlan


			MainGoals
			PatientSaysAlreadyDoing
			DoctorSaysGoingWell
			PatientImportance0to10
			PatientConfidence0to10
			ChecklistCopyToPatient
			ChecklistAsthmaActionPlanNeeded
			ReviewDate
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Question InfluenzaImmunizationAsthma {
	"A yearly flu vaccine helps #protect you in winter# time. 
    
    It is especially important if you are pregnant.
    
    Do you have a #yearly flu vaccine#?"
   
    YesNo
	Category "prevention"
    Triggers {
		No: problem InfluenzaAtRiskGroup
	}
    Id 0f00c756-be6f-4ce0-9eb2-52e33aa5056b
    Changelog {
        2019-03-24: ""
    }
}


Question SevereAsthmaEvaluation {
	"#Severe asthma# requires specialist referral. Useful tests to consider ordering prior to, and then including in, your referral include:
	
	- Basic spirometry
	
	- CXR

    - FBC to detect the 50% with eosinophilia who have e-asthma
	
	- RAST for aeroallergens, total IGE,.
	
	Do you want to #order tests# before referrral?"
   
    YesNoNA
	NAText "Not severe"
	Category "lungs"
	Education "Severe Asthma Toolkit website" {"
		* https://toolkit.severeasthma.org.au/diagnosis-assessment/diagnosis-overview/[Severe Asthma Toolkit - diagnosis (University of Newcastle, Australia)]
	"}

	
    Triggers {
		Yes: problem SevereAsthmaEvaluation
	}
    Id 21ddf67e-b6e7-4579-b81b-a5050d0d9cb2
    Changelog {
        2019-01-01: ""
    }
}

Problem SevereAsthmaEvaluation {
	Goal "Refer to specialist and include results of basic workup"
    Plan "Order tests and send results to the specialist with referral"
    Id 8daf0b9d-47dc-4e59-ae9a-9c6a60c4efbb
    Changelog {
        2019-01-01: ""
    }
}



Question SevereAsthmaPresence {
	" Difficult uncontrolled asthma includes the subgroup of #Severe asthma# which is asthma that:
	
	- remains uncontrolled 
	
	- despite #all treatable factors# having been addressed 
	
	- and #maximal inhaled therapy# being taken #regularly# and correctly.
    
    Do they have severe asthma?"
    YesNo
	Category "lungs"
    Education "Severe Asthma Toolkit website" {"
		* https://toolkit.severeasthma.org.au/severe-asthma/overview/[Severe Asthma Toolkit website (The University of Newcastle, Australia)]
	"}

    Triggers {
		Yes: problem SevereAsthmaPresence
		No: AutoAnswer {
			SevereAsthmaEvaluation NA
		}
	}
    Id 995688d7-b795-45d4-8f0c-32f041e929b0
    Changelog {
        2019-01-01: ""
    }
}


Problem SevereAsthmaPresence {
	Goal "Distinguish true severe asthma from uncontrolled asthma"
    Plan "Refer severe asthma to respiratory specialist."
    Id 2264dd66-bf62-4115-b423-cefb419ba590
    Changelog {
        2019-01-01: ""
    }
}


Problem ThunderstormAsthma {
	Goal "Appropriate prevention upper and lower airways"
    Plan "Institute ICS to upper and lower airways as indicated."
    Id 3beaae48-1dbe-49c3-9b8a-19f20dd47019
    Changelog {
        2018-07-21: ""
    }
}

Question ThunderstormAsthmaRisk {
	"
    People at risk of thunderstorm asthma include:
	
	 (1) those with springtime allergic rhinitis (#with or without# asthma)

	 (2) those with current or PH asthma
	 
	 (3) and those with undiagnosed asthma
	 
	 who live in, or are travelling to, an area with high rye grass pollen levels. 
     
     Melbourne, Geelong, Newcastle and Wagga Wagga have had epidemics of thunderstorm asthma. 
	
	Do you want to institute treatment for thunderstorm asthma?"
    
	YesNo

	Education "Recommendations to prevent thunderstorm asthma" {"
     
	  * People with seasonal nose allergy who do not use nasal ICS all year round should use nasal ICS 6 weeks before and during the pollen season (from start September to end of December). 

	  * People with asthma should use continuous ICS if indicated (most), or if not indicated, use ICS 6 weeks before and during the pollen season (from start September to end of December). 
 	"}

	Education "References" {"
		* https://www.asthmahandbook.org.au/clinical-issues/thunderstorm-asthma[Australian Asthma Handbook v1.3: Preventing thunderstorm-triggered asthma]
	"}

    Category "lungs"
    Triggers {
		Yes: problem ThunderstormAsthma
	}
    Id 9527fb89-e292-4cf4-94fb-0864b922f030
    Changelog {
        2018-07-21: ""
    }
}



Question AsthmaLAMATripleTherapy {
	"#LAMA# use in #severe asthma# requires #three# criteria:

	(1) At least one severe exacerbation (with documented use of systemic corticosteroids) in the previous 12 months.

	(2) While receiving optimised asthma therapy (= at least 800 micrograms budesonide per day or equivalent + LABA) and despite formal assessment of and adherence to correct inhaler technique (documented). 

	(3) LAMA must be used in combination with a maintenance combination of ICS and a LABA. 

	Do you need to #add a LAMA#?
    "
    
    YesNoNA
	NAText "Not on optimised ICS dose"
    Category "lungs"
    
    Triggers {
		Yes: problem AsthmaLAMATripleTherapy
    }
    Id 92b242fe-7151-4a43-9e9b-f40b4b004d81
    Changelog {
        2017-10-18: ""
    }
}


Problem AsthmaLAMATripleTherapy {
	Goal "Asthma Control"
    Plan "Add LAMA to ICS and LABA."
    Id f4af1263-3863-4796-b81f-ce7cc4a1a9fa
    Changelog {
        2017-10-18: ""
    }
}



Question Asthma-Controlled {
	"Ongoing asthma care requires a #six step plan#. The #first step# is to work out how well you are controlled. 
	
	Even if you think your asthma is under control, knowing your score is important. 
	
	Use the #asthma score# you did at home to work out how well you are controlled. 
	
	Are you #on target# with a score of #20-25#?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: AutoAnswer {
			Asthma-ControlLessThan20 No
		}
		No: problem AsthmaControlScoreLessThan20
	}
	Id 994da0cb-2a0d-46f1-ace2-8e3bb9a22d24
	Changelog {
		2017-01-14: ""
	}
}


Question AsthmaICSConcern {
	"The #second step# is to achieve your best lung function. 
	
	To do this we often use steroid preventer inhalers. 
	
	Here is clarification on some common myths about them 
	
	* You will not become dependent on them. 
	* It is not better for children to fight off asthma 'naturally'. 
	* They do not interfere with the growth of children. 
	* They do not increase the risk of bone fracture in patients over 40 year's age. 
	
	Do you have any concerns about steroid preventers you wish to discuss with your doctor?"
	
	YesNoUnsure
	Category "lungs", "medications"
	
	Triggers {
		Yes: problem InflammatoryNatureOfAsthma
	}
	Id 57c68215-ef36-4dfe-b441-91b60c936278
	Changelog {
		2017-01-14: ""
	}
}


Question AllergicRhinitis {
	"As you breathe air in through your #nose and sinuses# it is warmed up, air particles are filtered out and it is humidified before going down to your lungs. The sinuses also produce nitric oxide which helps oxygenation of the lungs and protects against infection. 
	
	If the nose and sinuses are blocked it will influence your lungs. 
	
	Any of the following can indicate allergic rhinitis: nose blockage, post nasal drip, running nose or sneezing. 
	
	60 to 80% of asthmatics have allergic rhinitis or \"sinus problems\". 
	
	Do you have allergic rhinitis or \"sinus problems\"?"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem AllergicRhinitisPresent
		Yes: education optional UpperAirwayFunction
	}
	Id 20a2b00d-f72c-4a35-b8c9-7075f4d561ea
	Changelog {
		2017-01-14: ""
	}
}


Question InhaledAllergens {
	"The #third# step is to identity any factors that flare you up (#triggers#) so you can then avoid or reduce your exposure to them. 
	
	Triggers vary from patient to patient. They can flare up your nose (sneezing, itching, running nose) or your lungs (wheezing, short of breath), or both. 
	
	There are groups of triggers listed over the next pages. 
	
	You may find that only a few are relevant for you but #please read them all.# 
	
	[.big]
	Do any #things in the air# (inhaled allergens) flare up your asthma *or* your sinuses?"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem InhaledAllergenTriggerPresent
	}
	Id a33616f8-d294-43a9-afb2-8243af2dddf9
	Changelog {
		2017-01-14: ""
	}
}


Question HouseDust {
	"#House dust,# which contains house dust mite and cockroach allergens, is the #commonest# cause of all year round inhaled allergies. 
	
	Does house dust #flare up your symptoms#? 
	
	image:HouseDust.jpg[width=200]"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem InhaledAllergenTriggerPresent
	}
	Id bc47fedb-e73a-4717-894f-c0c2ceed92b9
	Changelog {
		2017-01-14: ""
	}
}


Question CatOrDogAllergy {
	"#Animal hair# gets shed from cats and dogs. It can cause all year round inhaled allergies as well. 
	
	Do you #flare up when around# cats or dogs? 
	
	image:CatOrDogAllergy.jpg[width=300]"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem InhaledAllergenTriggerPresent
	}
	Id 61176126-dab1-480a-b7c0-0c05467bbc64
	Changelog {
		2017-01-14: ""
	}
}


Question MouldAllergy {
	"#Moulds in wet areas# such as showers can cause all year round inhaled allergies. 
	
	Do mouldy areas #flare you up#? 
	
	image:MouldAllergy.jpg[width=200]"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem InhaledAllergenTriggerPresent
	}
	Id 392c2b62-b79e-42cc-9d73-e44067e99738
	Changelog {
		2017-01-14: ""
	}
}


Question PollenAllergy {
	"#Pollens# from grasses, flowers or trees are the #commonest cause of seasonal# inhaled allergies. 
	
	Do pollens #flare you up#? 
	
	image:PollenAllergy.jpg[width=200]"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem InhaledAllergenTriggerPresent
	}
	Id ef57a473-f5ac-45f6-80ac-932fbda752a1
	Changelog {
		2017-01-14: ""
	}
}


Question FoodAllergy {
	"Food allergens are an #uncommon# trigger for asthma in #any# age group so diet usually should not be restricted. 
	
	However, the #following food allergens# can cause an immediate severe asthma reaction #in some people#: peanuts, tree nuts, fish, shellfish, wheat, soy, milk (particularly in children) and eggs (particularly in children). 
	
	Do #any of the above# foods flare your asthma up?
	
	image:FoodAllergy.jpg[width=200] "
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem FoodAllergyPresent
	}
	Id 5936c4bf-95a4-4923-8d3a-7be1cecf9059
	Changelog {
		2017-01-14: ""
	}
}


Question FoodIntolerance {
	"Food #chemical intolerance# is #more# common . 
	
	#Sulphur dioxide#, used for preserving food such as wine and dried fruit, may affect up to 5 to 10% of asthmatics. 
	
	MSG (#monosodium glutamate)#, used in flavoring foods such as Chinese takeaways, is also a suspected cause. 
	
	[.big]
	Do you suspect that food chemicals flare your asthma up? 
	
	image:FoodIntolerance.jpg[width=676]"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem FoodIntolerancePresent
	}
	Id 05e3b207-db06-48b0-b3d3-6d02d3a6f464
	Changelog {
		2017-01-14: ""
	}
}


Question PrescriptionMedicationAggravating {
	"8% of asthmatics experience nose or lung symptoms after taking #aspirin or arthritis medications# (e.g.voltaren, nurofen).
    
    Other medications can #occasionally# make asthma worse in a few people. 
	
	Examples are: beta blockers (atenolol, metoprolol) used for blood pressure and the heart, and some eye drops for glaucoma such timoptol and pilocarpine. 
	
	Do you #suspect any# of your medications are making your asthma worse? 
	
	image:PrescriptionMedicationAggravating.jpg[width=200]"
	
	YesNoUnsure
	Category "lungs", "medications"

	Triggers {
		Yes Unsure: problem MedicationsAggravatingAsthmaPresent
	}
	Id 452f5218-5e02-4da3-bb0d-0351888649de
	Changelog {
		2017-01-14: ""
	}
}


Question OTCMedicationAggravatingAsthma {
	"#Over the counter medications# can #sometimes# make your asthma worse. 
	
	Examples include: Royal Jelly (bee pollen) and Echinacea. 
	
	[.big]
	Do you #suspect any# over the counter medications make your asthma worse? 
	
	image:OTCMedicationAggravatingAsthma.jpg[width=300]"
	
	YesNoUnsure
	Category "lungs", "medications"
	
	Triggers {
		Yes Unsure: problem MedicationsAggravatingAsthmaPresent
	}
	Id 19e268fe-36a6-4b4b-a22b-f2fa8d3440eb
	Changelog {
		2017-01-14: ""
	}
}


Question RefluxAggravatingAsthma {
	"Heartburn or acid reflux can cause asthma or make it worse. This can be the explanation for asthma appearing for the first time in middle age. 
	
	Does #reflux cause wheezing# or coughing fits in you?"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem RefluxAggravatingAsthmaPresent
	}
	Id a93a9b9b-722d-4573-9836-6151cdf63bec
	Changelog {
		2017-01-14: ""
	}
}


Question AirPollutantsAggravatingAsthma {
	"Exposure to #air pollutants# can make asthma worse in some patients. 

	- cigarette smoke 
	- city smog 
	- bushfire smoke 
	- perfumes 
	- household cleaners 

	Do #any# of the above make your asthma worse?

	image:AirPollutantsAggravatingAsthma.jpg[width=200]"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem AtmosphericChangesOrPollutantsOrURTIAggravatingAsthmaPresent
	}
	Id 32bb0bb8-e51f-4869-be11-930af402f9ec
	Changelog {
		2017-01-14: ""
	}
}


Question AirChemicals {
	"Sometimes exposure to #air# containing hobby #chemicals# or work chemicals or #dusts# can cause asthma or make it worse. Examples include: 

	- wood dusts (sawdust) 
	- baker's flour 
	- 2 pack paints 
	- 2 pack glues 

	Do #any# of these #flare up# asthma in you?

	image::AirChemicals.jpg[width=200]"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem AirChemicalsAggravatingAsthmaPresent
	}
	Id 8a7b60df-1e16-46c0-8965-1bafd55166c3
	Changelog {
		2017-01-14: ""
	}
}


Question AtmosphericChangesAggravatingAsthma {
	"#Atmospheric changes# can bring on asthma in some people. Do #any# of the below flare up your asthma? 

	- weather changes
	- humidity
	- air temperature (especially cold air) 

	image:AtmosphericChangesAggravatingAsthma.jpg[width=200]"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem AtmosphericChangesOrPollutantsOrURTIAggravatingAsthmaPresent
	}
	Id ae880adf-6ffc-4fa6-bc69-00c2288b1b07
	Changelog {
		2017-01-14: ""
	}
}


Question URTITrigger {
	"Do viral respiratory infections #(colds) *usually*# flare up your asthma? 
	
	image:URTITrigger.jpg[width=300]"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem AtmosphericChangesOrPollutantsOrURTIAggravatingAsthmaPresent
	}
	Id 14889684-f12d-4427-a737-3046bfd782f2
	Changelog {
		2017-01-14: ""
	}
}


Question ExerciseTrigger {
	"Does #exercise# often flare your asthma up? 
	
	image:ExerciseTrigger.jpg[width=200]"
	
	YesNoUnsure
	Category "lungs"
	
	Triggers {
		Yes Unsure: problem ExerciseTriggerPresent
	}
	Id 8d003f89-a550-4cf8-8815-d6ed48dcac21
	Changelog {
		2017-01-14: ""
	}
}


Question AsthmaExercise {
	"
	[.big]
	#Regular physical activity# improves fitness and quality of life in people with asthma. 
	
	* Exercise does not cause asthma but can 'trigger' symptoms. This can be prevented by using a short acting bronchodilator (e.g. ventolin) 5-20 minutes before exercise or a regular preventer. 
	* Exercise in a warm humid environment and try to avoid high levels of airborne allergens (e.g.pollens) and irritants (e.g. chlorine in swimming pools). 
	
	[.big]
	Are you exercising regularly?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		No: problem ExerciseTriggerPresent
	}
	Id 080c22a8-340e-41d3-9914-73fccb58eee6
	Changelog {
		2017-01-14: ""
	}
}


Question InflammatoryNatureAsthma {
	"Asthma is an #inflammatory# disease. 
	
	This inflammatory tendency #remains for up to 18 months after an attack.# You may have no symptoms. This is why you get episodes of asthma. 
	
	It causes #three# main narrowing #effects:# 
	
	Do you want to discuss this further with your doctor? 
	
	#image:InflammatoryNatureAsthma.jpg[width=1136]#"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem InflammatoryNatureOfAsthma
	}
	Id b1cd59c8-4703-40cf-b9b2-8af222661833
	Changelog {
		2017-01-14: ""
	}
}


Question RelieverUse {
	"Our first line of treatment is a short acting #bronchodilator or reliever# (e.g. Ventolin) which relieves spasm.  
	
	These work quickly within 5 minutes, last about 4 hours, but #do not treat the inflammation# and are just #used as needed#. 

	Do you know which medication to use as your #reliever#?
	
	image:RelieverUse.jpg[width=1072]
    "
	
	YesNoUnsure
	Category "lungs", "medications"
	
	Triggers {
		No Unsure: problem RelieverUseCheckNeeded
	}
	Id 73329964-4b64-4d04-882b-cf3ce555f67a
	Changelog {
		2017-01-14: ""
	}
}


Question PreventerRole {
	"#Preventer# medication #settles down# inflammation (#mucosal swelling# and #mucus#) if taken #regularly#. 
	
	Most preventers are inhaled but some are tablets (e.g. Singulair used in children) 
	
	Do you require more explanation on your preventer? 
	
	image:PreventerRole.jpg[width=1072]"
	
	YesNoNA
	Category "lungs", "medications"
	
	Triggers {
		Yes: problem PreventerUse
		Yes: education optional AsthmaMAActions
	}
	Id de04cd82-692a-4eed-a4bf-41bedbba518d
	Changelog {
		2017-01-14: ""
	}
}


Question AsthmaCombinationInhaler {
	"If the inhaled steroid preventer alone does not give control: 
	
	We use a #combination# inhaler with an #inhaled steroid + a long acting bronchodilator (# e.g. Breo Ellipta, Seretide, Symbicort, Flutiform). 
	
	Press *Yes* to proceed after viewing how they work. 
	
	image:AsthmaCombinationInhaler.jpg[width=1370]"
	
	YesNoNA
	Category "lungs", "medications"
	
	Triggers {
		No: problem AsthmaSymptomControllerUseNeeded
	}
	Id 2a5d227e-8219-46b3-aa85-f84db4bf3b3e
	Changelog {
		2017-01-14: ""
	}
}


Question RinseMouth {
	"Are you aware that after using the inhaled preventer you should #rinse# your mouth out with water and #gargle# to prevent both thrush and hoarse voice?"
	
	YesNo
	Category "lungs", "medications"
	
	Triggers {
		No: problem RinseAfterPreventerAdviceNeeded
	}
	Id a741bb10-3039-4b48-b9a8-9f0d8574221d
	Changelog {
		2017-01-14: ""
	}
}


Question PeakFlowMeter {
	"The sixth and final step is to design an #'Asthma Action Plan.'# 
	
	This helps you detect if your asthma is getting worse and tells you what to do. 
	
	It is based on your symptoms PLUS a peak flow meter reading . 
	
	Do you have a home peak flow meter?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		No: problem PeakFlowMeterNeeded
	}
	Id 2bd5b284-8843-45f0-be37-50723f06ce39
	Changelog {
		2017-01-14: ""
	}
}


Question MedicationAdherence {
	"One of the best ways of controlling asthma is to take your medications regularly. We are very interested in helping you to do this.
	
	Do you ever #forget# to take your medication?"
	
	YesNo
	Category "medications", "lungs"
	
	Triggers {
		Yes: problem AdherenceProblem
	}
	Id 34b00460-8e6a-43e2-a960-152d7f28667b
	References {
		"4 item Morisky Medication Adherence Questionnaire"
	}
	Changelog {
		2017-01-14: ""
	}
}


Question AsthmaAndPregnancy {
	"Asthma in pregnancy: 
	
	* Poorly controlled asthma puts both mother and baby at risk so good asthma control is important. 
	* Inhaled corticosteroid preventers have a good safety profile in pregnancy. 
	* So get your asthma plan updated at the start of pregnancy! 
	
	Do you wish to discuss #management of asthma in pregnancy# with the doctor?"
	
	YesNoNA
	NAText "N/A - Finished having children"
	Gender female
	Category "lungs"

	Triggers {
		Yes: problem AsthmaAndPregnancy
	}
	Id 984fb730-d5fe-4054-b597-58889b90b0f7
	Changelog {
		2017-01-14: ""
	}
}


Question AsthmaCouncil {
	"Would you like to know how to join the #National Asthma Council of Australia# so you regularly receive more information?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem AsthmaCouncilMembershipReferralNeeded
	}
	Id ec4117e0-ae9e-4824-baff-4a53f086345e
	Changelog {
		2017-01-14: ""
	}
}


Question Consent2552and11506 {
	"Explain cost (2552 + 11506 or 11505) and record patient's agreement to proceed."
	
	Check required
	Category "admin"
	Id 6b97908a-95e1-9355-8f1b-cfec07bf6011
	Changelog {
		2017-01-14: ""
	}
}


Question PFMBaseline {
	"Ask patient to perform 2 baseline PFM with their own meter and record the best."
	
	Check required
	Category "lungs"
	Id 418d3faa-a130-1055-a84e-9c1bd09808ba
	Changelog {
		2017-01-14: ""
	}
}

Question AsthmaDaysMissed {
	"Record the number of days missed from asthma in the last year"
	
	Number required
	Category "lungs"
	Id 82cda5c8-dfdc-9358-a1dd-35d62e5c99cc
	Changelog {
		2017-01-14: ""
	}
}


Question Spirometry10MinsPostSalbutamolBestOf3 {
	"10 minutes after ventolin perform 3 spirometry and 2 PFM readings.  Record  best in clinical notes."
	
	Check required
	Category "lungs"
	Id d7f8429d-3310-915b-844b-7a2e2ec30b1f
	Changelog {
		2017-01-14: ""
	}
}


Question ReminderAsthmaCycleCareNeeded {
	"Add reminder for Asthma cycle of Care Plan in 12 months."
	
	Check required
	Category "admin"
	Id 2667a85c-f122-4e53-b00c-1c99e21d632f
	Changelog {
		2017-01-14: ""
	}
}


Question SpirometrySABAResponsePresent {
	"Review the spirometry. 
	
	Because the patient has been instructed not to take their normal asthma medication today, the % change with salbutamol (ventolin) gives some information on control. 
	
	A #change post salbutamol# suggests control may not be optimal with current preventer, and a change ≥ 12% is definitely significant. 
	
	A low lung function on spirometry is an additional risk factor even if no symptoms. 
	
	Is there a change?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem SpirometrySABAResponsePresent
	}
	Id 70048f35-2d1a-41ea-ae0f-4a44342081ee
	Changelog {
		2017-01-14: ""
	}
}


Question OralSteroidsRequiredInLastYear {
	"Have they needed a course of oral steroids for their asthma in the last 12 months? 
	
	Either self administered according to action plan or doctor initiated ."
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem OralSteroidCourseRequired
	}
	Id 975cc217-56e2-41da-aacb-e0278eaf04fd
	Changelog {
		2017-01-14: ""
	}
}


Question LifeThreateningAttackInPastHistory {
	"Have they ever had a life threatening asthma attack (= requiring admission to ICU)?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem LifeThreateningAttackInPastHistory
	}
	Id 117fafd0-6fa6-4343-86e9-96e770e48fc5
	Changelog {
		2017-01-14: ""
	}
}


Question DaysOffWithAsthmaPresent {
	"Have they missed #days off# work or school because of their asthma in the last 12 months?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem AsthmaSymptomControllerUseNeeded
	}
	Links {
		AsthmaDaysMissed
	}
	Id aa238b55-d59e-4fe0-9cec-42992c347d40
	Changelog {
		2017-01-14: ""
	}
}


Question AsthmaPreventerDiscrepancyPresent {
	"Is there a #discrepancy# between what preventer dose they are actually taking and what is recorded in their chart and current asthma plan?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem AsthmaMedicationReviewNeeded
	}
	Id a88f7d55-16db-46fd-8378-252087e72773
	Changelog {
		2017-01-14: ""
	}
}


Question COPDCo-existingWithAsthmaPresent {
	"[blue]*COPD* *can co-exist with asthma.* 
	
	#Post# bronchodilator spirometry readings must be used to confirm the diagnosis by using the #“POST 70/80\"# rule. 
	
	Obstruction ([blue]*O* of COPD) is confirmed by the FEV1/FVC ratio being less than #70#%. 
	
	Pulmonary Disease ([blue]*PD* of COPD) is confirmed by loss of volume. That is by an FEV1 less than #80#% of predicted. 

	*Do they have COPD* [blue]*as well as asthma?*"
	
	YesNoNA
	Category "lungs"

	Education "References" {"
		* https://www.asthmahandbook.org.au/clinical-issues/copd/managing-asthma-copd-overlap[Australian Asthma Handbook v1.3: Managing asthma-COPD overlap]
	"}
	
	Triggers {
		No: AutoAnswer {
			COPDMildSeverity No
			COPDModerateSeverity No
			COPDSevere No
		}
		Yes: problem AsthmaPlusCOPD
	}
	Id fd37c965-b486-4772-8f77-627beaec5571
	Changelog {
		2017-01-14: ""
	}
}


Question AgingConditionsAffectingAsthmaPresent {
	"Common conditions in #older people# that may affect asthma control include:
	
	* Obesity 
	* Gastro-oesophageal reflux disease 
	* Obstructive sleep apnoea 
	* Osteoporotic crush fractures impairing respiratory capacity 
	* Cardiovascular disease causing dyspnoea or some medications (beta blockers) 
	* Diabetes can affect decision re use of systemic steroids 
	
	[big]#Do you need to alter your treatment to allow for any of these conditions?#"
	
	YesNoNA
	Category "lungs"

	
	Education "References" {"
		* https://www.asthmahandbook.org.au/clinical-issues/comorbidities[Australian Asthma Handbook v1.3: Conditions that may affect asthma symptom control, risk or management]
	"}
	
	Triggers {
		Yes: problem AgingConditionsAffectingAsthmaPresent
	}
	Id 445e0fa5-5a90-4835-a5aa-75d47d07894d
	Changelog {
		2017-01-14: ""
	}
}


Question AsthmaMedicationChangeNeeded {
	"There are a number of factors to take into account to decide if the current asthma medications need to be changed. 
	
	[.big]
	Do you need to change their medications (either increase or decrease)?"
	
	YesNo
	Category "lungs", "medications"
	
	Education "Factors influencing asthma medication choice." {"
	* Asthma Score - reflects current control.
	* Spirometry - low lung function is an additional risk
	and significant response to salbutamol today suggest lability.
	* Oral steroids course needed for asthma in the last 12 months. 
	* DEM visits in the last 12 months 
	* Hospitalisation in the last 12 months 
	* Missed days off work or school with asthma. 
	* PH life threatening attack = never cease preventer.
	* Discrepancy between prescribed dose and actual dose.
	* Continued exposure to triggers such as smoking.
	* Unavoidable allergen exposure to proven allergen.e.g. house dust mite. 

	== References
	* https://www.asthmahandbook.org.au/management/adults[Australian Asthma Handbook v1.3: Stepped treatment adjustment]
	"}
	Triggers {
		No: AutoAnswer {
			AsthmaLAMATripleTherapy No
		}
		No: AutoAnswer {
			AsthmaPreventerDecreaseNeeded NA
		}
		Yes: problem AsthmaMedicationReviewNeeded
		Yes: education optional AdultAndChildICSDoseTables
        }
	Links {
		AsthmaScoreFromHome
		AsthmaScoreAdministered
	}
	Id 95dab4f7-55f0-4c12-9684-c367b5cb9373
	Changelog {
		2017-01-14: ""
	}
}


Question AsthmaPreventerDecreaseNeeded {
	"If you have decided to change their preventer have you #decreased# the dose?"
	
	YesNoNA
	Category "lungs", "medications"

	Education "References" {"
	* https://www.asthmahandbook.org.au/management/adults/stepped-adjustment[Australian Asthma Handbook v1.3: Adjusting treatment in adults by stepping up or stepping down]
	"}
	
	Triggers {
		Yes: problem AsthmaMedicationReviewNeeded
		Yes: education optional AsthmaStepDown
	}
	Id cb35bf09-4cef-4ca9-9a75-05bd3f845013
	Changelog {
		2017-01-14: ""
	}
}


Question AssessmentOfDeviceAndPFMNeeded {
	"Incorrect inhalation technique is common. A 2008 Australian survey showed only 10% were using all devices correctly.
		
	Have you assessed and/or demonstrated both: 
	
	- the technique for using *all* their inhaler devices 

	 #and# 
	
	- the use of a home *peak flow meter* and its role in the Asthma Action Plan?
	"
	
	YesNo
	Category "lungs", "medications"

	Education "Inhaler Technique Checklists" {"
	* https://assets.nationalasthma.org.au/resources/InhalerTechniqueChecklist_2016.pdf[Inhaler Technique Checklists.NPS and National Asthma Council]
	
	"}
	Education "Common inhaler problems " {"

	* Reduced maximal inspiratory flow (esp. if COPD) can limit the use of dry powder devices. 

	* Reduced hand dexterity (esp. with OA) can limit use of pMDI without either a spacer or a Haleraid hand grip device. 

	* Reduced coordination of actuation and inhaling can limit the use of pMDI without a spacer. 

	* A breath activated inhaler (e.g. Autohaler) or a dry powder inhaler (e.g. Turbuhaler, Accuhaler) are other options. 
	
	"}
	Triggers {
		No: problem AssessmentOfMDIDevice
	}
	Id 4229d617-09e1-46d8-8ba2-8686819be94b
	Changelog {
		2017-01-14: ""
	}
}


Question AsthmaPlanNeeded {
	"According to the Cochrane review, doubling of steroid preventer in an acute flare up is ineffective. 
	
	Medicare #requires a printed out action plan# to satisfy the 2+ cycle of asthma care. 
	
	Have you printed out and explained an Asthma Action Plan for the patient?
	"

	Education "References" {"
		* https://www.asthmahandbook.org.au/management/action-plans[Australian Asthma Handbook v1.3: Asthma action plans]
	"}
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes No: problem AsthmaPlanNeeded
	}
	Id b728706c-22fb-438d-807f-d08f8ab2681f
	Changelog {
		2017-01-14: ""
	}
}


Question ScriptSupplyNeeded {
	"Have you ensured that they have adequate scripts, including for emergency prednisolone?"
	
	YesNo
	Category "lungs", "medications"
	
	Triggers {
		No: problem ScriptSupplyNeeded
	}
	Id 9f5e8330-5905-4848-95c6-bcf120145caa
	Changelog {
		2017-01-14: ""
	}
}


Question EczemaPresent {
	"Does the patient have eczema?"
	
	YesNo
	Category "lungs", "allergies"
	
	Triggers {
		Yes: problem EczemaPresent
		Yes: education optional SCORAD
	}
	Id 4721665e-8b95-41af-85a1-3fbc3f1a0d20
	Changelog {
		2017-01-14: ""
	}
}


Question AllergicConjunctivitisPresent {
	"Intranasal steroids, intranasal antihistamines or oral antihistamines will oftern relieve eye itching and redness of #allergic conjunctivitis# without the need for eye drops.
	
	Do you want to start treatment for #allergic conjunctivitis#?"
	
	YesNoNA
	NAText "N/A - no allergic conjunctivitis"
	Category "lungs", "allergies"
	Education "Total Ocular Symptom Score (TOSS) for severity" {"
    Ratings of nil (0), mild (1), moderate (2), severe (3) for:

	* itching/burning
	* watering
	* redness

	Total score out of 9
	"}
	
	Triggers {
		Yes: problem AllergicRhinitisPresent
	}
	Id 7e6959ec-54c1-42cc-bf1f-693b65a800d1
	Changelog {
		2017-01-14: ""
	}
}


Question AllergicRhinitisPresent {
	"75% of asthmatics have #allergic rhinitis (AR)#.
    
    Prescribe intranasal steroid (INCS) long term for persistent AR or moderate to severe intermittent AR. 

	Do you want to #use INCS# for allergic rhinitis?"
	
	YesNoNA
	NAText "N/A - no allergic rhinitis"
	Category "lungs", "allergies"

    Education "Total Nasal Symptom Score (TNSS) for severity" {"
    Ratings of nil (0), mild (1), moderate (2), severe (3) for:
	
	* congestion 
	* nasal itch 
	* rhinorrhoea 
	* sneezing 
	* sleep
	
	Total score out of 15
	"}
	
	
	Education "References" {"
		* https://www.nationalasthma.org.au/living-with-asthma/resources/health-professionals/information-paper/allergic-rhinitis[National Asthma Council of Australia: Managing allergic rhinitis in people with asthma]
	"}
	
	Triggers {
		Yes: problem AllergicRhinitisPresent
	}
	Id c4a3a553-d584-46e4-86cd-5e5c4916845d
	Changelog {
		2017-01-14: ""
	}
}


Question AllergenTestingNeeded {
	"Consider allergy testing for all patients on #preventers# if you suspect allergic triggers.	
	
	It may also be considered for any asthmatic who requests it, or in the presence of persistent moderate or severe allergic rhinitis. 
	
	Skin prick testing (SPT) is more accurate for inhaled allergens than RAST blood tests. 

    It offers the opportunity for #avoidance# or #immunotherapy#. 

	Do you want to arrange an #allergy test#?"
	
	YesNoNA
    NAText "Allergy test already done"
	Category "lungs", "allergies"

	Education "References" {"
		* https://www.asthmahandbook.org.au/clinical-issues/allergies[Australian Asthma Handbook v1.3: Allergies and asthma]
	"}
	
	Triggers {
		Yes: problem InhaledAllergenTriggerPresent
	}
	Id 7a50b3a7-a47a-431a-9e5c-f4bd9f08fb08
	Changelog {
		2017-01-14: ""
	}
}


Problem InhaledAllergenTriggerPresent {
	Goal "Identify by both history + allergy testing."
	Plan "SPT (skin prick testing) is more accurate than RAST blood test. Then avoid, or minimise e.g. by mite impermeable bed coverings. Use inhaled nasal steroid if chronic. Offer desensitisation if moderate/severe symptoms."
	Id bfd241b6-320d-4e97-8659-f15a2d323e69
	Changelog {
		2017-01-14: ""
	}
}


Problem FoodAllergyPresent {
	Goal "Confirm. Avoid."
	Plan "Arrange RAST testing to confirm. If severe reaction refer to allergy specialist."
	Id 598984e1-dbef-4b1b-96af-b5599ceb3505
	Changelog {
		2017-01-14: ""
	}
}


Problem FoodIntolerancePresent {
	Goal "Confirm. Avoid."
	Plan "Confirm with food diary (looking for flare up after a food chemical ingestion on at least 2 occasions). Advise use of a food additive code website or book to identify foods."
	Id 82a4c757-8ce4-4987-8e8d-d28ac8488127
	Changelog {
		2017-01-14: ""
	}
}


Problem MedicationsAggravatingAsthmaPresent {
	Goal "Clarify if OTC or prescribed."
	Plan "Clarify with history. Trial of ceasing medication may be needed to confirm."
	Id 7764c736-d509-4bae-b8db-81815146b82c
	Changelog {
		2017-01-14: ""
	}
}


Problem RefluxAggravatingAsthmaPresent {
	Goal "Clarify and treat"
	Plan "Clarify with history. Consider double dose proton pump treatment to confirm as for treatment of heartburn pain."
	Id 95d272b0-0047-4649-8a0f-3b5e58ff46f3
	Changelog {
		2017-01-14: ""
	}
}


Problem AirChemicalsAggravatingAsthmaPresent {
	Goal "Clarify. Avoid."
	Plan "Clarify with history. May require a diary of exposure with serial Peak Flow Meter readings to confirm a consistent drop on exposure."
	Id a1881216-2996-4031-9f01-c32032b963b3
	Changelog {
		2017-01-14: ""
	}
}


Problem AtmosphericChangesOrPollutantsOrURTIAggravatingAsthmaPresent {
	Goal "Clarify."
	Plan "Avoid if possible. Increase your monitoring of symptoms and PFM via Asthma Action Plan."
	Id 49a09155-90ef-481f-aaa0-a41071adc8f7
	Changelog {
		2017-01-14: ""
	}
}


Problem ExerciseTriggerPresent {
	Goal "Improve exercise and fitness"
	Plan "Either use a short acting beta agonist 5-10 minutes before exercise or a long acting beta agonist with the steroid preventer. Remember exercise induced asthma is one of the first symptoms to appear when control is sub optimal irrespective of their Asthma Score."
	Id 03d01b6b-17d5-4815-a2f7-5ea4b45045d1
	Changelog {
		2017-01-14: ""
	}
}


Problem InflammatoryNatureOfAsthma {
	Goal "Clarify"
	Plan "Clarify any concerns."
	Id b3a0ff25-0bb7-45e0-b5f8-716108800e3b
	Changelog {
		2017-01-14: ""
	}
}


Problem RelieverUseCheckNeeded {
	Goal "Clarify role"
	Plan "Clarify the function after discovering the deficient area of knowledge."
	Id c82356ef-15b3-43e1-b774-1127333df3d1
	Changelog {
		2017-01-14: ""
	}
}


Problem RinseAfterPreventerAdviceNeeded {
	Goal "Prevent thrush"
	Plan "Educate."
	Id bae773f1-1040-4c43-bd72-749e1671e93e
	Changelog {
		2017-01-14: ""
	}
}


Problem PeakFlowMeterNeeded {
	Goal "PFM at home"
	Plan "Educate re need. Advise purchase."
	Id 90742d35-75f7-4a03-bd3c-5a9a17b1da71
	Changelog {
		2017-01-14: ""
	}
}


Problem AllergicRhinitisPresent {
	Goal "Confirm diagnosis and treat."
	Plan "With UAD (united airways disease) treatment of the upper (nose) airways with INCS improves control of the lower (chest) airways.
    Oral or Intranasal antihistamine can be added for nose flare ups "
	Id 9214ff69-f165-4d16-bae3-d057638fde1b
	Changelog {
		2017-01-14: ""
	}
}


Problem SpirometrySABAResponsePresent {
	Goal "Clarify control"
	Plan "Take the SABA (short acting beta agonist) response into consideration when calculating degree of control."
	Id dbf13dab-99b1-4413-a44c-f7f05d4b033c
	Changelog {
		2017-01-14: ""
	}
}


Problem OralSteroidCourseRequired {
	Goal "Clarify control."
	Plan "Take need for oral steroids into consideration when deciding on preventer dose."
	Id 5d6a9a6a-b027-4f53-876a-a65327f0dc68
	Changelog {
		2017-01-14: ""
	}
}


Problem LifeThreateningAttackInPastHistory {
	Goal "Prevent recurrence"
	Plan "Emphasise importance of preventer therapy and understanding Asthma Action Plan."
	Id e586a0f7-c49b-4667-b8b9-b00db2067705
	Changelog {
		2017-01-14: ""
	}
}


Problem AsthmaMedicationReviewNeeded {
	Goal "Total control"
	Plan "Assess adherence first. Escalate preventer dose if not total control and consider reducing if controlled. Once moderate dose of ICS required use symptom controller for the LABA effect."
	Id 6de90890-f041-402c-a165-625939fa4061
	Changelog {
		2017-01-14: ""
	}
}


Problem AsthmaPlanNeeded {
	Goal "Provide every asthmatic with an Asthma Action Plan"
	Plan "Use your clinical package to generate an Action Plan."
	Id 807d0f2f-7588-42e8-9916-a5b6635dcc01
	Changelog {
		2017-01-14: ""
	}
}


Problem ScriptSupplyNeeded {
	Goal "Ensure script supply"
	Plan "Supply 6 months of scripts."
	Id f24ce3d4-60b6-4fab-ba48-6c9b13ac58b8
	Changelog {
		2017-01-14: ""
	}
}


Problem AsthmaCouncilMembershipReferralNeeded {
	Goal "Encourage membership."
	Plan "www.nationalasthma.org.au"
	Id 15c1cf9b-2fc4-491d-a1d3-8b83177bfd2b
	Changelog {
		2017-01-14: ""
	}
}


Problem AsthmaSymptomControllerUseNeeded {
	Goal "Appropriate symptom controller use."
	Plan "Clarify the problem with/indication for symptom controller use."
	Id b9fc254e-fa32-4525-991f-6c68077fc355
	Changelog {
		2017-01-14: ""
	}
}


Problem AgingConditionsAffectingAsthmaPresent {
	Goal "Clarify"
	Plan "Treat as appropriate."
	Id d45aec84-979b-4ea4-a931-9b7858e7271a
	Changelog {
		2017-01-14: ""
	}
}


Problem EczemaPresent {
	Goal "Identify triggers and educate re prevention"
	Plan "Arrange SPT. Eczema prevention measures sheet. Decrescendo topical steroid plan."
	Id a4949198-5a9f-4213-a060-584aa221ef78
	Changelog {
		2017-01-14: ""
	}
}


Problem AsthmaAndPregnancy {
	Goal "Good control in pregnancy"
	Plan "Assess control with score and update plan. Consider switching to budesonide which is  catgory A (rest are B3). During severe exacerbations oral prednisolone can be used (category A)"
	Id 6307f457-0667-44ce-988f-00ac9bc9b5a6
	Changelog {
		2017-01-14: ""
	}
}


Education UpperAirwayFunction {
	File "jpg"
	Id 1d93451b-c0ea-482a-b43c-8e3967f409c7
	Changelog {
		2017-01-14: ""
	}
}


Education AsthmaMAActions {
	Message "Asthma MA actions"
	File "jpg"
	Id af27224e-b90e-4c48-b936-63c0f7a6cb8b
	Changelog {
		2017-01-14: ""
	}
}


Education AsthmaStepDown {
	Message "Asthma step-down advice"
	File "rtf"
	Id 263e73ee-f408-4328-9f63-d5275b5ec6c9
	Changelog {
		2017-01-14: ""
	}
}


Education AdultAndChildICSDoseTables {
	Message "Adult and child ICS tables"
	File "rtf"
	Id 7589222f-8841-41f5-9159-c9841ed18b7f
	Changelog {
		2017-01-14: ""
	}
}


Education SCORAD {
    Message "SCORing Atopic Dermatitis (SCORAD) (DermNet NZ)"
    Link "https://www.dermnetnz.org/topics/scorad/"
	Id 63c8e38f-6607-4f07-b9dd-6cd53306c4c5
	Changelog {
		2017-01-14: ""
	}
}


Question ChecklistAsthmaActionPlanNeeded {
	"Copy of Asthma Action Plan supplied to patient?"

	YesNo
	DisplaySettings "location: 'conclusion-checklist'"
	Category "plan"
    AppliesTo "PATv3"
	Id b8536ae7-15be-4ec9-8fc5-3f3e9acd500e
}
