/* 
 * Copyright (c) 2017 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Clinic ChronicPainGPMP {
	Id e719f324-640c-4541-91b8-6d020c8cb6c2
	Questions {
		Patient {
			ChronicPainPresent
			PainChartNeeded
			Exercising
			ParacetamolUsage
			PainWorkConcerns
			PainHomeProblems
			PainBehaviours
			PainEmotionalReactions
			DepressionPresent
			ExtraHelpNeeded
		}

		Checklist {
			Consent721or723
			RecordHeightWeightWaist
			RecordSmokingAlcohol
			RecordBP
			RemindersChronicPain
		}

		Staff {
			PainChronic
			HealthSummaryAndMedicationListReviewNeeded
			PainMultimodalApproach
			PainSpecialistAssessement
			PainNon-pharmacologicalApproach
			PainPharmacotherapyChangeNeeded
			PainProceduralIntervention
			PainLadderOpioidPrescription
			PainMedicine10UniversalPrecautionsNeeded
			PainAppropriateDiagnosis
			PainComorbidConditionsPresent
			RiskForAbuseOrAddictionModerateOrHigh
			PainYellowFlagsPresent
			PainInformedConsentNeeded
			PainTreatmentAgreementNeeded
			PainAssessment
			PainOpioidTrialNeeded
			PainTrialAssessmentNeeded
			PainMedicine6AsCheckNeeded
			PainPeriodicReviewNeeded
			PainMedicationDocumentationNeeded
			PainADRBs
			Constipation
			PainSpecialistReferralNeeded


			MainGoals
			PatientSaysAlreadyDoing
			DoctorSaysGoingWell
			PatientImportance0to10
			PatientConfidence0to10
			ChecklistCopyToPatient
			ChecklistCopiesToTeamWithConsent
			ChecklistReferralFormsCompleted
			ReviewDate
		}

	}
	Changelog {
		2017-01-14: ""
	}
}


Question Exercising {
	"Some patients are wary of exercising. 
	
	When they experience some pain with exercise they think it may be damaging their joints more. This is #not true# for low impact exercise such as walking, biking, swimming. 
	
	#Exercise# is extremely important to help #muscle strength# around the joint and help overcome joint stiffness ( #flexibility# ). 
	
	If walking is a problem because of knee or hip arthritis there are #alternatives# such as #hydrotherapy# (exercising in a pool) or on an exercise bike. 
	
	Exercise can be accumulated in 10 minute sessions - it does not have to be done for 30 minutes at a time. 
	
	Are you exercising for 150 minutes (2.5 hours) per week? 
	
	(RACGP Osteoarthritis Guideline)"
	
	YesNoUnsure
	Category "lifestyle"
	
	Triggers {
		No Unsure: problem ExerciseAndArthritis
		No Unsure: education optional ExerciseEducation
	}
	Id 950149e1-3835-4397-8d8d-757da99f67d6
	Changelog {
		2017-01-14: ""
	}
}


Question ParacetamolUsage {
	"Taking  #paracetamol# is often useful even if you are on a strong analgesic as well. 
	
	* #Paracetamol# (e.g. Panadol, Panamax) is the best.

	* You can just take 2 tablets #as needed# 20 minutes before doing an activity that usually causes pain (such as your daily walk).

	* Or it can be used up to 2 tablets 4 times a day.

	* There is no data suggesting added benefit from slow release paracetamol. 
		
	
	*Are you taking paracetamol*?"
	
	YesNoUnsure
	Category "pain", "medications"
	
	Triggers {
		No Unsure: problem ParacetamolNotControllingPain
	}
	Id 547703d8-f513-4408-ac0c-741c168c5048
	Changelog {
		2017-01-14: ""
	}
}


Question PainWorkConcerns {
	"Here are two common #concerns about returning to work# with chronic pain: 
	
	* Believing that all pain should be gone before attempting to return to work or normal activity 
	* Expecting or worrying that pain will increase with activity/work

	*Do either of these apply to you?*"
	
	YesNoUnsure
	Category "pain"
	
	Triggers {
		Yes Unsure: problem PainWorkConcerns
	}
	Id 81b36909-59da-4282-a5b2-3caa535dbbbd
	Changelog {
		2017-01-14: ""
	}
}


Question PainHomeProblems {
	"Here are two common things that can occur at #home#: 
	
	* My partner (or spouse) is often critical of me or my efforts 
	* There is a lack of support from my family to talk about my problems 
	
	*Do either of these apply to you?*"
	
	YesNoUnsure
	Category "pain"
	
	Triggers {
		Yes Unsure: problem PainHomeProblems
	}
	Id d74ff146-6929-40ef-ba0a-c8fbc40c089e
	Changelog {
		2017-01-14: ""
	}
}


Question PainBehaviours {
	"Here are two #common behaviours# in people with chronic pain: 
	
	* Withdrawing from some of their normal daily activities 
	* or avoiding most of their normal activities 
	
	*Do either of these two apply to you?*"
	
	YesNoUnsure
	Category "pain"
	
	Triggers {
		Yes Unsure: problem PainBehaviours
	}
	Id ff5535f4-f1d7-4ce6-8008-20db71b15c08
	Changelog {
		2017-01-14: ""
	}
}


Question PainEmotionalReactions {
	"Here are two common #emotions# in people with chronic pain: 
	
	* Irritability 
	* Loss of interest in social activity 
	
	*Do either of these apply to you?*"
	
	YesNoUnsure
	Category "pain"
	
	Triggers {
		Yes Unsure: problem IrritabilityOrDisinterest
	}
	Id d32f9084-f0f2-4202-86cf-bffb0913b1b0
	Changelog {
		2017-01-14: ""
	}
}

Question Consent721or723 {
	"Check item if 721 (GPMP) and 723(TCA) in last12/12. If yes, use item 44. Get consent."
	
	Check required
	Category "admin"
	Id 0790803c-ea0c-a65d-8505-78e6547d809e
	Changelog {
		2017-01-14: ""
	}
}

Question RecordBP {
	"Record BP in clinical notes"
	
	Check required
	Category "Heart / Brain"
	Id db8e814e-71f3-4d5a-b063-317d98aaa555
	Changelog {
		2017-01-14: ""
	}
}


Question RemindersChronicPain {
	"Add reminder for 'Chronic pain review' and a 12 month 'General GPMP'"
	
	Check required
	Category "admin"
	Id 2e71db9c-ee5e-765f-b732-fff5b0411fd6
	Changelog {
		2017-01-14: ""
	}
}


Question PainChronic {
	"#Chronic pain# is pain that continues beyond the usual time of healing (or expected time of recovery), defined as #longer than 3 months# 
	
	Goals of pain management 
	
	* #Main focus# is on improving patient function socially and physically rather than seeking a cure 
	* #Reduce# pain (by >20% is considered a successful trial and > 50% very successful) 
	* Minimise risk of adverse effects 
	
	Have you explained these goals to the patient?"
	
	YesNoNA
	Category "pain"
	
	Triggers {
		No: problem PainMedicine10UniversalStepsNeeded
	}
	Id 9cd6513c-4c98-4f94-93af-9c9694c9d7d0
	Changelog {
		2017-01-14: ""
	}
}


Question PainMultimodalApproach {
	"Chronic pain is complex and requires a combination of management strategies- #a multimodal approach.# 
	
	
	Do you want to proceed with this #multimodal approach?#"
	
	YesNoNA
	Category "pain"
	Education "Multimodal approach 4 components" {"

	* Non-pharmacological approaches 

	* Pharmacotherapy 

	* Referral to other health care professionals 

	* Procedural intervention 
	
	"}
	Triggers {
		No: problem PainMedicine10UniversalStepsNeeded
	}
	Id 18da7787-ec44-4087-abee-15ac57b3edbe
	Changelog {
		2017-01-14: ""
	}
}


Question PainSpecialistAssessement {
	"Has the patient ever been assessed by a chronic pain specialist/clinic or a back pain rehabilitation unit?"
	
	YesNoNA
	Category "pain"
	
	Triggers {
		Yes: problem PainSpecialistAssessmentNeeded
	}
	Id 20b2cb3f-14e4-4de7-b3e4-79b7ab655c12
	Changelog {
		2017-01-14: ""
	}
}


Question PainNon-pharmacologicalApproach {
	"Non-pharmacological approaches include: 
	
	#Physical techniques# 
			
	#Psychological techniques# 
	
	#Occupational therapy# 
	
	#Social interventions# 
	
	
	
	Does the patient require referral for one or more of these?"
	
	YesNoNA
	Category "pain"
	Education "Physical techniques" {"

	* physiotherapy 

	* manual therapy 

	* hyperstimulation analgesia 
	
	"}
	Education "Social interventions" {"

	* community support groups 

	* self help groups 

	* work retraining/modification 
	"}
	Triggers {
		Yes: problem CareFrom2OrMoreOtherHealthCareProvidersOtherThanYourself
	}
	Id e418251a-35bb-4e63-a7d0-449d9ed7b3d0
	Changelog {
		2017-01-14: ""
	}
}

Question PainProceduralIntervention {
	"#Procedural interventions# 
	
		
	Is referral for a procedural intervention appropriate?"
	
	YesNoNA
	Category "pain"
	Education "Procedural interventions" {"

	* Local anaesthetic nerve blocks for diagnostic, prognosis or therapeutic purposes 

	* Spinal cord stimulation 

	* Intrathecal therapy 
	
	* Surgery 
	 
	"}
	Triggers {
		Yes: problem PainProceduralInterventionAppropriate
	}
	Id da3c67ce-9654-48fc-af46-5682f0182a97
	Changelog {
		2017-01-14: ""
	}
}


Question PainLadderOpioidPrescription {
	"#Pharmacotherapy changes#:

	#Precaution:# Opioids should only be used short term after an initiial trial with defined postive outcomes.

	 If pain is persisting or increasing do you want to consider moving up the WHO analgesic ladder to #regular opioid prescription?# 
			
	"
	
	YesNoNA
	Category "pain", "medications"

	Education "Commencing opioids" {"
	Only commence: 

	1. Where #all other# conservative pain management approaches have been tried and failed. 

    2. Do not use for headache, non-specific low back pain, fibromyalgia or chronic visceral pain.

	3. Use only in those at #low risk# of opioid misuse. 

	4. Aim for #a limited period# of use to enable improvement in patient function.
	
	5. As soon as treatment goal is reached  discontinue by gradually tapering the dose.

	6. #Atypical opioids# (tramadol, tapentadol, buprenorphine) are the preferred starting medications.
	"}
	Triggers {
		Yes: problem PainPharmacotherapyChangeRequired
		Yes: education optional PainPharmacotherapyOptions
		Yes: education optional OpioidPatchAdvice
	}
	Id 3f5bf02e-d59a-4233-a2f3-f7bf82972708
	Changelog {
		2017-01-14: ""
	}
}


Question PainADRBs {
	"#Aberrant drug-related behaviours# (ADRBs) occur frequently enough to be of concern during chronic opioid treatment. 
	
		
	Does the patient display any aberrant drug-related behaviours (ADRBs)?"
	
	YesNoNA
	Category "pain", "medications"

	Education "Abberrant drug related behaviours (ADRBs)" {" 

    * Borrowing another patients drugs

	* Obtaining prescription drugs from non-medical/other medical sources

    * Unsanctioned dose escalations
	
	* Aggressive complaining about the need for higher doses

	* Drug hoarding	

	* Requesting specific drugs

	* Prescription forgery

	* Recurring prescription losses

	* Injection of substances prescribed for oral use
	
	* Concurrent use of related illicit drugs
	
	"}
	Triggers {
		Yes: problem PainADRBsPresent
		Yes: education optional AberrantDrugRelatedBehaviours
	}
	Id 1ac3dfd3-7210-4d4c-b767-fad5d74de2b1
	Changelog {
		2017-01-14: ""
	}
}

Problem ExerciseAndArthritis {
	Goal "150 minutes of pleasurable exercise per week."
	Plan "Aerobic fitness exercise is best for hip OA. Quadriceps strengthening and resistance exercises best for OA knee. A personally designed program is more effective than generic exercise instruction."
	Id 2cdae820-3aee-4555-a936-73647a3938c5
	Changelog {
		2017-01-14: ""
	}
}


Problem PainWorkConcerns {
	Goal "Reduce concerns"
	Plan "Explore beliefs and offer a problem solving approach."
	Id 4387430b-1ce1-4b79-9b92-cf447d49ea43
	Changelog {
		2017-01-14: ""
	}
}


Problem PainHomeProblems {
	Goal "Reduce home problems"
	Plan "Problem solving approach including counselling for home problems."
	Id 57f480c1-02a7-455d-af5a-5403161d0d25
	Changelog {
		2017-01-14: ""
	}
}


Problem PainProceduralInterventionAppropriate {
	Goal "Reduce pain/improve function."
	Plan "Appropriate specialist referral."
	Id 0be4a8e5-196d-440f-b065-5a9f15553028
	Changelog {
		2017-01-14: ""
	}
}


Problem PainBehaviours {
	Goal "Improve activity"
	Plan "Explore beliefs about activity."
	Id 4437c19a-d350-4d06-a9bb-7f08003a37f9
	Changelog {
		2017-01-14: ""
	}
}


Problem IrritabilityOrDisinterest {
	Goal "Improve mood"
	Plan "Explore beliefs with CBT."
	Id c4068a49-4e68-48dd-a6b4-c0bbf791ecf4
	Changelog {
		2017-01-14: ""
	}
}


Problem PainADRBsPresent {
	Goal "Resolve cause of ADRBs."
	Plan "ADRBs may be indicative of risk of addiction or may arise from a number of factors, including under-treatment of pain."
	Id 10d2c880-96dc-4f8c-b284-d9f5d86bc0bc
	Changelog {
		2017-01-14: ""
	}
}

Education AberrantDrugRelatedBehaviours {
	File "rtf"
	Id b2172fec-9522-406b-b58a-a423476eae43
	Changelog {
		2017-01-14: ""
	}
}



