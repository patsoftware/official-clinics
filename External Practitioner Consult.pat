/* 
 * Copyright (c) 2017 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

Clinic ExternalPractitionerConsult {
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
		}

	}
	Changelog {
		2017-01-14: ""
	}
    AppliesTo "PATv3"
	Id e7ac50d9-a29e-4490-bef8-1321928d1312
}

