/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with hypertension */
Clinic GPMPHTN {
	Id 01cb1476-db89-4130-95c2-087168660763
	Filter HTN
	Questions {
		Patient {
			BPMonitorOwnershipNeeded
			BloodPressureElevatingSubstances
			LipidLoweringAgentUseNeeded
			StatinConcern
			SaltIntakeHigh
			NationalSupportOrganisationReferralNeeded
		}

		Checklist {
		}

		Staff {
			ResistantBPScreenNeeded
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter HTN {
	Match {
		keyword "Elevated blood pressure"
		keyword "High blood pressure"
		keyword "HTN"
		keyword "Hypertension"
	}
	Tests matching only this {
		"HTN"
		"Hypertension"
		"hypertensive disorder"
		"hypertensive"
		"high blood pressure"
		"elevated blood pressure"
	}
}


Question BloodPressureElevatingSubstances {
	"Some ordinary substances and over the counter medications can raise blood pressure if you take them every day. Do you take any of the following [red]#every# day? 
	
	* Appetite suppressants 
	* St John's Wort 
	* More than 3 cups of coffee 
	* Nasal or sinus decongestant tablets 
	* Liquorice
    
    == References

		* https://www.heartfoundation.org.au/for-professionals/clinical-information/hypertension[NHF Guide to management of Hypertension in Adults, 2016 ]
    
    "
	
	YesNoUnsure
	Category "Heart / Brain"
	
	Triggers {
		Yes Unsure: problem MedicationsThatCanIncreaseBPIdentified
	}
	Id 0f5083b7-a379-4c08-bddb-6f330bea40b1
	Changelog {
		2017-01-14: ""
	}
}


Question MedicationsOrOTCThatMightElevateBP {
	"Is the patient on #any# medications or OTC products that might #elevate BP?# 
	
	NHF guideline list. 
	
	#Common# 
	
	* Contraceptives or HRT 
	* Non steroidal anti-inflammatory drugs (NSAIDS) 
	* Corticosteroids (prednisolone etc) 
	* SNRI antidepressants (Efexor, Cymbalta)

    == References

		* https://www.heartfoundation.org.au/for-professionals/clinical-information/hypertension[NHF Guide to management of Hypertension in Adults, 2016 ]
    
    " 
	
	
	YesNoNA
	Category "Heart / Brain", "medications"
	
	Education "Less common medications elevating BP" {"
	  * Cyclosporin
	  * MAO inhibitor antidepressants (Aurorix, Parnate, Nardil)
	  * NRI antidepressants (Edronax)
	  * Sibutramine (Reductil)
	  * Bupropion (Zyban)
	  * Amphetamines
	  * Erythropoietin injections for CKD anaemia
	  * Ergotamine over use in migraine
== References

		* https://www.heartfoundation.org.au/for-professionals/clinical-information/hypertension[NHF Guide to management of Hypertension in Adults, 2016 ]

	"}
	Triggers {
		Yes: problem MedicationsThatCanIncreaseBPIdentified
	}
	Id fea96e5d-bae3-4a20-a98b-ca3e4bc6d479
	Changelog {
		2017-01-14: ""
	}
}


Question PosturalDrop {
	"Postural hypotension can be a problem especially in the elderly in summer. 
	
	A difference between sitting BP and standing BP > or equal to 20mmhg systolic or 10mmhg diastolic , either immediately or after standing for 2 minutes is significant 
	
	Is there any significant #postural drop?#"
	
	YesNo
	Category "Heart / Brain"
	
	Triggers {
		Yes: problem BPPosturalDrop
	}
	Id ff8c4d0b-04f5-4ed9-844a-b2333a2f36b5
	Changelog {
		2017-01-14: ""
	}
}


Problem MedicationsThatCanIncreaseBPIdentified {
	Goal "Optimum control of blood pressure"
	Plan "Reduce or cease."
	Id 2dbb5ff9-2f02-4e72-a70c-7eb651a767cb
	Changelog {
		2017-01-14: ""
	}
}


Problem BPPosturalDrop {
	Goal "Protect against falls"
	Plan "Alter BP medications. Diuretics are often the culprit."
	Id 8da0f4f1-e3b0-44d1-9514-bfa8c3b5a4fb
	Changelog {
		2017-01-14: ""
	}
}


