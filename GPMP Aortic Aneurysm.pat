/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with an aortic aneurysm */
Clinic GPMPAorticAneurysm {
	Id 23658671-0648-4a46-8acc-de2e7f53a7c5
	Filter AorticAneurysm
	Questions {
		Patient {
			LipidLoweringAgentUseNeeded
			StatinConcern
		}

		Checklist {
		}

		Staff {
			LipidResultsNotWithinRangeOnMedication
			Aspirin-RiskFactors
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Clinic GPMPAorticAneurysmCore {
	Id 23658671-0648-4a46-8acc-de2e7f53a7c6
	Filter AorticAneurysm
	Questions {
		Patient {
			LipidLoweringAgentUseNeeded
			StatinConcern
		}

		Checklist {
		}

		Staff {
			LipidResultsNotWithinRangeOnMedication
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter AorticAneurysm {
	Match {
		keyword "AAA"
		keyword "AAAA"
		keyword "Abdominal aortic aneurysm"
		keyword "Aneurysm of abdominal"
		keyword "Aneurysm of abdominal aorta"
		keyword "Aneurysm of aorta"
		keyword "Aneurysm of aortic"
		keyword "Aneurysm of ascending"
		keyword "Aneurysm of descending"
		keyword "Aneurysm of thoracic"
		keyword "Aorta aneurysm"
		keyword "Aortic aneurysm"
	}
	Tests matching only this {
		"AAA"
		"AAA 5.6cm"
		"AAA 5.6 cm"
		"AAA 5.6 cm 2009"
		"AAA 2.6 cm (USS 2009 - little change from 2007)"
		"AAA repaired"
		"repair of AAAA"
		"Abdominal aortic aneurysm"
		"Aneurysm of abdominal aorta"
		"Aneurysm of aorta"
		"Aneurysm of aortic"
		"Aneurysm of descending"
		"Aneurysm of descending aorta"
		"Aneurysm of ascending aorta"
		"descending aortic aneurysm"
		"descending aorta aneurysm"
		"Aneurysm of thoracic"
		"Aortic aneurysm"
		"History of stable aneurysm of abdominal aorta"
	}
}
