/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with CHD */
Clinic GPMPCHD {
	Id c1b26fe5-f89c-46b3-bd2b-b2f37d6c5415
	Filter CHD
	Questions {
		Patient {
			BPMonitorOwnershipNeeded
			LipidLoweringAgentUseNeeded
			StatinConcern
			SaltIntakeHigh
			NationalSupportOrganisationReferralNeeded
			
		}

		Checklist {
		}

		Staff {
			CoronaryHeartDisease4
			AnginaActionPlanNeeded
			CoronaryHeartDisease3
			CardiologyReviewNeeded
			LipidResultsNotWithinRangeOnMedication
			FamilialHypercholesterolaemiaRisk
			StatinInitiationBaselineTestsNeeded
		}

	}
}

Clinic GPMPCHDCore {
	Id c1b26fe5-f89c-46b3-bd2b-b2f37d6c5416
	Filter CHD
	Questions {
		Patient {
			BPMonitorOwnershipNeeded
			LipidLoweringAgentUseNeeded
			StatinConcern
			SaltIntakeHigh
			NationalSupportOrganisationReferralNeeded
			
		}

		Checklist {
		}

		Staff {
			CoronaryHeartDisease4
		}

	}
}

Filter CHD {
	Match {
		keyword "ACS"
		keyword "AMI"
		keyword "Angina"
		keyword "Angioplasty"
		keyword "Arteriosclerotic heart"
		keyword "CAD"
		keyword "CHD"
		keyword "Coronary arteriosclerosis"
		keyword "Coronary artery disease"
		keyword "Coronary artery stent"
		keyword "Coronary heart disease"
		keyword "Coronary sclerosis"
		keyword "IHD"
		keyword "Ischaemic heart"
		keyword "MI"
		keyword "Myocardial infarct"
		keyword "Myocardial ischaemia"
		keyword "NSTEMI"
		keyword "STEMI"
		keyword "Stent, coronary artery"
	}
	Tests matching only this {
		"ACS"
		"AMI"
		"Angina"
		"Prinzmetal angina"
		"atypical angina"
		"exercise-induced angina"
		"CAD"
		"CHD"
		"Coronary artery disease"
		"Coronary heart disease"
		"Ischaemic heart"
		"Ischaemic heart disease"
        "ISCHAEMIC HEART DISEASE"
		"Arteriosclerotic heart disease"
		"Coronary sclerosis"
		"Coronary arteriosclerosis"
		"IHD"
		"MI"
		"myocardial infarction"
		"myocardial infarct"
		"myocardial ischemia"
		"myocardial ischaemia"
		"NSTEMI"
		"STEMI"
	}
}

Question StatinInitiationBaselineTestsNeeded {
	"#Statin associated muscle symptoms# (SAMS) are common with up to 30% patients experiencing some symptoms.
	
	So #baseline documentation# of musculoskeletal symptoms as well as ordering baseline pathology for CK and LFTs should be done before initiation.
	
    Note that SAMS are usually proximal and symmetrical. Click below for more information on SAMS.

    Do you want to perform both tasks?
    "
    
    YesNoNA
    NAText "Already on statin"
    Category "Heart / Brain"
    
    Triggers {
		Yes: problem StatinInitiationBaselineTestsNeeded
		Yes: education optional SAMSGuide
    }
    Id 61979385-65a1-4a20-86f9-8ad7bfeadc96
    Changelog {
        2017-10-18: ""
    }
}

Problem StatinInitiationBaselineTestsNeeded {
	Goal "Accurately delineate future muscle symptoms"
    Plan "Order CK and E&LFTs. Document any musculoskeletal symptoms."
    Id c64c32cf-84cd-47cb-afcb-9be0bdd3e912
    Changelog {
        2017-10-18: ""
    }
}


Education SAMSGuide {
	Message "SAMS"
    Link "https://www.nps.org.au/professionals/managing-lipids/statin-associated-muscle-symptoms-sams"
    Id de8ac081-3485-43de-8f26-b97ce02bb3ff
    Changelog {
        2017-10-18: ""
    }
}




Question CoronaryHeartDisease4 {
	"If coronary heart disease (#CHD#) is present (even if not active now) it is recommended for the patient to be on #four groups# of drugs - the 'cardiac 4' or #S.A.B.A# (statin, antiplatelet, beta-blocker, ACEI/ARB). 
	
	Do you want to change medications? 
	
	image:CoronaryHeartDisease4.jpg[width=743]"
	
	YesNoNA
	NAText "N/A - no CHD"
	Category "Heart / Brain", "medications"

	Education "Cardiac 4 medicines" {"
		* #S:# Statin for all CHD unless contraindicated regardless of cholesterol level. 
		* #A:# Antiplatelet agent. Aspirin for all patients unless contraindicated (#DAP# = dual antiplatelet therapy in patients for 12 months post MI, or unstable angina, or post stent, unless contraindicated). Use DAPT calculator to determine which patients stay on after 12 months 
		* #B:# Beta blocker for 12 months for all patients *post MI*, unless contraindicated then review need. 
		* #A:# ACEI/ARB for all patients unless contraindicated.

		== References

		* https://www.heartfoundation.org.au/images/uploads/publications/Reducing-risk-in-heart-disease.pdf[NHF: Reducing risk in heart disease (2012)]
	"}
	
	Triggers {
		Yes: AutoAnswer {
			CoronaryHeartDisease3 NA
		}
		Yes: problem CHDMedicationNeeded
		Yes No: education optional DAPTCalculator
	}
	Id cfd06a57-04e5-42d1-8b29-86c63920cfe3
	Changelog {
		2017-01-14: ""
	}
}


Question AnginaActionPlanNeeded {
	"If the patient has a past history of coronary heart disease (#even if not active now#) they should have a chest pain action plan including an #in date# GTN spray and know how to use it.

	1 puff, wait 5 minutes, then if pain is still present repeat 1 dose. If still present at 10 minutes call 000 for ambulance.
	
	Do you want to put a #chest pain action plan# in place?
    
    == References

		* https://www.heartfoundation.org.au/images/uploads/publications/Reducing-risk-in-heart-disease.pdf[NHF: Reducing risk in heart disease (2012)]
    
    "
	
	YesNoNA
	NAText "N/A - no CHD"
	Category "Heart / Brain"
	
	Triggers {
		Yes: problem AnginaActionPlanNeeded
	}
	Id a0bedd2f-d452-48c3-80be-9cc33d83e3a0
	Changelog {
		2017-01-14: ""
	}
}


Question CoronaryHeartDisease3 {
	"If not on the four groups of drugs for CHD, are they on #all three# of the following? 
	
	* Antiplatelet agent. 
	* Statin for all CHD unless contraindicated regardless of cholesterol level. 
	* ACEI/ARB for all patients unless contraindicated.
    
    == References

		* https://www.heartfoundation.org.au/images/uploads/publications/Reducing-risk-in-heart-disease.pdf[NHF: Reducing risk in heart disease (2012)]   
    
    "
	
	YesNoNA
	NAText "N/A - no CHD"
	Category "Heart / Brain", "medications"
	
	Triggers {
		No: problem CHDMedicationNeeded
	}
	Id e008746b-7c46-4cd2-9fee-6c781e7523bc
	Changelog {
		2017-01-14: ""
	}
}


Question CardiologyReviewNeeded {
	"Do you want to arrange cardiology referral for review of CHD or recent onset angina?"
	
	YesNoNA
	NAText "N/A - stable CHD"
	Category "Heart / Brain"
	
	Triggers {
		Yes: problem CHDCardiologyReviewNeeded
	}
	Id 8e87cf28-a161-48dc-9041-9ffc61330f4c
	Changelog {
		2017-01-14: ""
	}
}


Problem CHDMedicationNeeded {
	Goal "Protect from further episodes"
	Plan "Ensure is on all 4 drugs for secondary cardiac prevention (if post MI) or all 3 if no MI or > 12 months MI, and explain their individual actions. Ensure has treatment for acute episode."
	Id 8f3d8e33-506a-4522-aeb4-6bddeab3883b
	Changelog {
		2017-01-14: ""
	}
}


Problem AnginaActionPlanNeeded {
	Goal "Ensure all CHD patients have plan."
	Plan "Supply GTN and instruct in use."
	Id df69018f-b74d-4616-9133-6d3be9c0d31b
	Changelog {
		2017-01-14: ""
	}
}


Problem CHDCardiologyReviewNeeded {
	Goal "Confirm diagnosis of angina or review other CHD problems."
	Plan "Refer for cardiology evaluation. Include latest pathology in referral letter."
	Id 5595e797-bf01-486c-9076-eff049d6f5b0
	Changelog {
		2017-01-14: ""
	}
}


Education DAPTCalculator {
	Message "DAPT calculator"
	Link "https://qxmd.com/calculate/calculator_373/dapt-score"
	Id 88bf54a8-5046-43a5-a98b-7510f543aab2
	Changelog {
		2017-01-14: ""
	}
}


