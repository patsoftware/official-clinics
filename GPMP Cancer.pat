/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with cancer */
Clinic GPMPCancer {
	Id 7db4371a-b970-40b8-abcb-64872bae569c
	Filter Cancer
	Questions {
		Patient {
			SelfHelpGroupReferralNeeded
		}

		Checklist {
		}

		Staff {
			CancerPH
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter Cancer {
	Match {
		keyword "AML"
		keyword "Astrocytoma"
		keyword "BCC"
		keyword "Burkitt"
		keyword "Cancer"
		keyword "Carcinoma"
		keyword "Colonic polyp"
		keyword "Colonic polyps"
		keyword "Glioma"
		keyword "Heavy chain disease"
		keyword "Klatskin"
		keyword "Krukenberg"
		keyword "Leukaemia"
		keyword "Leukaemic"
		keyword "Light chain disease"
		keyword "Lymphoma"
		keyword "Melanoma"
		keyword "Mesothelioma"
		keyword "Metastasis"
		keyword "Metastatic"
		keyword "Mycosis fungoides"
		keyword "Myelodysplastic"
		keyword "Myeloma"
		keyword "Plasmacytoma"
		keyword "Polyp, colonic"
		keyword "Polyp, rectal"
		keyword "Polyps, colonic"
		keyword "Pseudomyxoma peritonei"
		keyword "Rectal polyp"
		keyword "SCC"
		keyword "Sezary"
		regex "((?!.?non.).{4}|^.{0,2})malignant"
		regex ".*[Mm]alignancy.*"
		regex ".*adenoma"
		regex ".*blastoma"
		regex ".*carcinoma"
		regex ".*sarcoma"
	}
	Tests matching only this {
		"Cancer"
		"Carcinoma"
		"Squamous cell carcinoma"
		"SCC"
		"basal cell carcinoma"
		"BCC"
		"colonic polyps"
		"rectal polyps"
		"polyps, colonic"
		"polyps rectal"
		"polyps - rectal"
		"polyps (rectal)"
		"adenocarcinoma"
		"Lymphoma"
		"leukaemia"
		"acute myeloid leukaemia"
		"AML"
		"Non-hodgkin's Lymphoma"
		"Leukaemic infiltrate of kidney"
		"T-cell leukaemia/lymphoma"
		"myeloma"
		"plasma cell myeloma"
		"Sézary disease"
		"Sezary disease"
		"plasmacytoma"
		"gamma heavy chain disease"
		"Melanoma"
		"malignant melanoma"
		"Kaposi's sarcoma"
		"sarcoma"
		"fibrosarcoma"
		"rhabdomyosarcoma"
		"osteosarcoma"
		"lymphangiosarcoma"
		"liposarcoma"
		"angiosarcoma"
		"malignant tumour"
		"malignant"
		"metastasis"
		"Metastatic carcinoid tumour"
		"Myelodysplastic syndrome"
		"mast cell malignancy"
		"Klatskin's tumour"
		"Burkitt's tumour of spleen"
		"Krukenberg tumor"
		"glioma"
		"glioblastoma multiforme"
		"astrocytoma"
		"neuroblastoma"
		"mesothelioma"
		"Esthesioneuroblastoma"
		"Pseudomyxoma peritonei"
		"Mycosis fungoides"
	}
}


Question CancerPH {
	"If they have a PH of any type of:
	
	* #cancer# 
	* #skin cancer# (both melanoma and NMSC) 
	* #or blood dyscrasia# 
	* #or precancerous lesions# (including colonic polyps) 
	
	Do you need to arrange appropriate follow up?"
	
	YesNoNA
	NAText "N/A - no PH cancer"
	Category "cancer"
	Education "RACGP Red Book - skin cancer guidelines" {"
	https://www.racgp.org.au/clinical-resources/clinical-guidelines/key-racgp-guidelines/view-all-racgp-guidelines/red-book/early-detection-of-cancers/skin-cancer
	"}
	Triggers {
		Yes: problem CancerFollowUpNeeded
	}
	Id 51e95057-c9c4-429b-bd89-e5fcf57ae23e
	Changelog {
		2017-01-14: ""
	}
}


Problem CancerFollowUpNeeded {
	Goal "Appropriate follow-up to minimise risk of recurrence or treatment side-effects"
	Plan "
    Add appropriate reminders for check-ups.
    Ensure appropriate referrals.
    If on tamoxifen or aromatase inhibitors: pre-order bone density every 2 years.
    "
	Id ae37367c-a3d2-47a4-886e-637f1b25c1df
	Changelog {
		2017-01-14: ""
	}
}
