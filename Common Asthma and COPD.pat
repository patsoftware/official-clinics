/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

 Question AsthmaScoreAdministered {
	"If has PH or current H of asthma administer asthma score"
	
	Number optional
	Category "lungs"
	Id c9c0766b-8cac-5e5c-96cd-79d41f26d522
	Changelog {
		2017-01-14: ""
	}
}

Question AsthmaScoreFromHome {
	"Record the asthma score here from patient's home score sheet."
	
	Number required
	Category "lungs"
	Id c9c0766b-8cac-5e5c-96cd-79d41f26d511
	Changelog {
		2017-01-14: ""
	}
}

Question AdministerSalbutamol4puffs {
	"Request the patient to self administer FOUR puffs of salbutamol (ventolin) via spacer."
	
	Check required
	Category "lungs"
	Id 36206b73-9e39-9553-8e5b-0e90d63b732e
	Changelog {
		2017-01-14: ""
	}
}


Problem AnnualReview {
	Goal "Annual review."
	Plan "Address any concerns re the need for an annual review."
	Id 2d88dceb-9834-40c2-a917-2deba32f9955
	Changelog {
		2017-01-14: ""
	}
}

Problem AssessmentOfMDIDevice {
	Goal "All asthmatics and COPD patients need usage checked annually."
	Plan "Check patient usage of MDI device and if asthmatic their PFM use as well."
	Id c5eb73cc-3a48-42c4-a2ec-82f25a7077aa
	Changelog {
		2017-01-14: ""
	}
}

Question AnnualReviewOK {
	"
	[.big]
	It is essential to have a regular #annual review# when you are #well#. 
	
	* New medications emerge and new knowledge improves our management. 
	* It is not a #'set and leave' dose# with preventers - we adjust both up and down as the control score indicates. 
	* The initial dose of preventer will not be the long term dose. 
	* Annual review also provides an opportunity for us to educate you so you can manage your asthma or COPD better. 
	
	[.big]
	Are you comfortable with the need for an annual review?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		No: problem AnnualReview
	}
	Id 7614e795-a8dd-4229-9047-90ab4251dbf7
	Changelog {
		2017-01-14: ""
	}
}



Question Asthma-ControlLessThan20 {
	"Are you #off-target# with an asthma control score of less than 20?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem AsthmaControlScoreLessThan20
	}
	Id 914f6ad7-1b93-4912-bb10-c87571c20e4b
	Changelog {
		2017-01-14: ""
	}
}


Problem AsthmaControlScoreLessThan20 {
	Goal "Good asthma control"
	Plan "Review triggers and adherence.
    Then titrate preventer upwards if clinically indicated."
	Id 0a4928a9-d02c-402b-9bda-16fa4c2fe321
	Changelog {
		2017-01-14: ""
	}
}


Problem AsthmaPlusCOPD {
	Goal "Detect and treat asthma in COPD."
	Plan "Treat as asthma rather than COPD if asthma is present."
	Id 8109fff4-5a3c-4937-a525-1c51166a249b
	Changelog {
		2017-01-14: ""
	}
}


Problem COPDCycleOfCare {
	Goal "Every patient to have an action plan."
	Plan "Arrange a COPD cycle of care reminder. Make sure to use the COPDX template plan to individualize the treatment at this consultation."
	Id ecebf9b0-9187-45b4-95c8-363765755e7e
	Changelog {
		2017-01-14: ""
	}
}


Question COPDMildSeverity {
	"
	[.big]
	#C# Classify #COPD severity# according to the British Thoracic Society Classification (ALF). 
	
	#Mild COPD is# FEV1 between 60 to 80% of predicted. 
	
	* Few symptoms. 
	* No effect on daily activities. 
	* Breathless on normal exertion. 
	* No complications. 
	
		
	Does the patient have mild COPD?"
	
	YesNo
	Category "lungs"
	Education "Severity classification COPD based on spirometry" {"
     * https://lungfoundation.com.au/wp-content/uploads/2018/09/Information-paper-Stepwise-Management-of-Stable-COPD-Jan19.pdf
	"}
	Triggers {
		Yes: AutoAnswer {
			COPDModerateSeverity No
			COPDSevere No
			COPDModerateSeverity No
			COPDSevere No
			PulmonaryRehabilitationNeeded No
			COPDAndRoleOfICSExplanationNeeded No
			LongActingBronchodilatorsNeeded No
			COPDTripleTherapyNeeded NA
		}
		Yes: problem COPDMildSeverity
		Yes: education optional COPDStepwiseManagement
	}
	Id d793a713-8a89-481a-9e25-783f44faff65
	Changelog {
		2017-01-14: ""
	}
}


Problem COPDMildSeverity {
	Goal "Appropriate treatment for grade."
	Plan "Mild COPD only requires intermittent reliever therapy with beta agonist (salbutamol, terbutaline) and/or anti-cholinergic (ipratropium)."
	Id 50e258b3-145c-40be-a622-375b637c0c8a
	Changelog {
		2017-01-14: ""
	}
}


Question COPDModerateSeverity {
	"#Moderate COPD is# FEV1 between 40 to 59% of predicted. Symptoms include: 
	
	* Increasing dyspnoea. 
	* Breathless on the flat. 
	* Increasing limitation of daily activities. 
	* Complications #may# be present such as hypoxaemia, pulmonary hypertension, polycythaemia, heart failure. Consider sleep apnoea if there is pulmonary hypertension present. 
	
	Does the patient have moderate COPD?"
	
	YesNo
	Category "lungs"
	Education "Severity classification COPD based on spirometry" {"
     * https://lungfoundation.com.au/wp-content/uploads/2018/09/Information-paper-Stepwise-Management-of-Stable-COPD-Jan19.pdf
	"}
	Triggers {
		Yes: AutoAnswer {
			COPDSevere No
			COPDSevere No
		}
		Yes: problem COPDModerateSeverity
		Yes: education optional COPDStepwiseManagement
	}
	Id abafc1c7-3b64-4c21-a853-f199eda46dcc
	Changelog {
		2017-01-14: ""
	}
}


Problem COPDModerateSeverity {
	Goal "Appropriate treatment for grade."
	Plan "As well as intermittent reliever therapy, moderate COPD requires regular therapy.
	1. LAMA (long acting muscarinic antagonist) +/- LABA.
	2. LABAs are more effective used with LAMA  than alone. 
	3. ICS for moderate COPD only if FEV1 < 50% + 2 or more infective exacerbations per year + allergic background (asthma, or allergic rhinitis, or eosinophilia)."
	Id 03a3a162-9521-4ca6-9f91-95a31a38931d
	Changelog {
		2017-01-14: ""
	}
}


Question COPDSevere {
	"#Severe COPD is# FEV1 less than 40 % predicted. Symptoms include:
	
	* Dyspnoea on minimal exertion. 
	* Daily activities severely curtailed. 
	* Complications #more likely# to be present such as: severe hypoxemia, pulmonary hypertension, heart failure, and polycythaemia. 
	
	Does the patient have severe COPD?"
	
	YesNo
	Category "lungs"
	Education "Severity classification COPD based on spirometry" {"
     * https://lungfoundation.com.au/wp-content/uploads/2018/09/Information-paper-Stepwise-Management-of-Stable-COPD-Jan19.pdf
	"}
	Triggers {
		Yes: problem COPDSevere
		Yes: education optional COPDStepwiseManagement
	}
	Id 5b9d4e7a-a140-420b-b4d8-bd2660172b33
	Changelog {
		2017-01-14: ""
	}
}


Problem COPDSevere {
	Goal "Appropriate treatment for grade."
	Plan "Severe COPD requires LAMA + LABA  as well as intermittent reliever therapy. ICS if 2 or more exacerbations per year and no pneumonia history. Strongly consider FBC and Pa02. Consider ECG if arrhythmia suspected. Consider ECHO if cor pulmonale suspected. Consider Specialist review."
	Id 3a78e9ff-aa0d-4522-81d7-eda906a9d0bb
	Changelog {
		2017-01-14: ""
	}
}


Education COPDStepwiseManagement {
	Message "Stepwise management of stable COPD"
	Link "https://lungfoundation.com.au/wp-content/uploads/2018/09/Information-paper-Stepwise-Management-of-Stable-COPD-Jan19.pdf"
	Id b2a3c904-6a57-49c4-9da0-a331e233fff4
	Changelog {
		2017-01-14: ""
	}
}


Question DEMTreatment {
	"Have you had to seek treatment at a Department of Emergency Medicine or at a Private Hospital After Hours in the #last 12 months# for your asthma or COPD?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem DEMVisit
	}
	Id 60330dd2-af8d-4aa6-80ae-67ecfa73e8b3
	Changelog {
		2017-01-14: ""
	}
}

Problem DEMVisit {
	Goal "Reduce need for DEM visits"
	Plan "Clarify cause and then include suitable treatment in the next asthma action plan."
	Id ec536616-65da-4769-b1a7-c055078ba9f8
	Changelog {
		2017-01-14: ""
	}
}


Question FirstAid4by4by4 {
	"
	[.big]
	First aid for a #sudden severe asthma or COPD# attack is to call the ambulance and #administer '4 by 4 by 4'.# 
	
	- #4 puffs# of salbutamol (such as ventolin) metered aerosol #every 4 minutes# until the ambulance arrives. 
	- If using a spacer, each salbutamol puff requires 4 breaths to empty the spacer. 
	- If using a metered aerosol alone, just hold your breath for 4 seconds after each salbutamol puff and repeat until 4 puffs are inhaled. 
	
	[.big]
	Do you require any more information on emergency first aid?"
	
	YesNo
	Category "lungs", "medications"
	
	Triggers {
		Yes: problem FirstAid4by4by4
	}
	Id 86e78f26-0b24-4337-9e5f-f53c3edf5dcf
	Changelog {
		2017-01-14: ""
	}
}


Problem FirstAid4by4by4 {
	Goal "Understanding of 4 by 4 by 4"
	Plan "Clarify the regimen after discovering the deficient area of knowledge."
	Id 86a0d791-fe51-42c4-bdc4-e9c3d4ce99c6
	Changelog {
		2017-01-14: ""
	}
}



Question HospitalAdmission {
	"Have you been hospitalised with asthma or COPD in the last 12 months?"
	
	YesNo
	Category "lungs"
	
	Triggers {
		Yes: problem HospitalAdmission
	}
	Id 0e35c099-85a0-49aa-a3f9-f3dcc89bceec
	Changelog {
		2017-01-14: ""
	}
}


Problem HospitalAdmission {
	Goal "Clarify cause."
	Plan "Incorporate treatment for the cause in Asthma or COPDX Action Plan if appropriate. 
	Consider escalation of preventer dose."
	Id db846777-b636-4149-ad99-389cbb11ce40
	Changelog {
		2017-01-14: ""
	}
}


Problem PreventerUse {
	Goal "Clarify role"
	Plan "Clarify the function after discovering the deficient area of knowledge."
	Id 6e6d1e08-0283-4475-b585-116d289c85cc
	Changelog {
		2017-01-14: ""
	}
}


Question RecordHeightWeight {
	"Record height and weight in the clincal notes."
	
	Check required
	Category "general"
	Id 166560bb-cd09-4452-8715-8f23c7c269a9
	Changelog {
		2017-01-14: ""
	}
}


Question RecordSmoking {
	"Record smoking (current, ex-smoker or none) in the appropriate section of clinical software"
	
	Check required
	Category "lungs"
	Id 960cb1b5-9f5b-8051-ba7a-725ce40bfda9
	Changelog {
		2017-01-14: ""
	}
}


Question SpacerMDIUse {
	"#PBS# now requires check of inhaler device technique. MDIs should be used with a #spacer device# which: 
	
	- Optimises delivery (15 puffs of salbutamol via MDI spacer = a 5mg Ventolin neb)

	- Overcomes difficulties with manual coordination. 

	- Reduces oral thrush with ICS. 

	- Reduces hoarseness with ICS. 
	
	Have you #assessed# spacer + inhaler device technique?"
	
	YesNo
	Category "lungs", "medications"
	Education "How to use a standard MDI and spacer" {"
	*
	https://www.nationalasthma.org.au/living-with-asthma/how-to-videos/how-to-use-a-standard-mdi-and-spacer[Australian Asthma Handbook v1.3.Video on how to use a spacer]
	"}
	Triggers {
		No: problem AssessmentOfMDIDevice
	}
	Id f1be3a3f-1e32-4dd8-b8af-64464cf1b896
	Changelog {
		2017-01-14: ""
	}
}


Question SpirometryBestOf3 {
	"Record the best of 3 baseline spirometry readings in the clinical notes."
	
	Check required
	Category "lungs"
	Id 2d2fe8fd-221f-4955-b771-d9162f51ad7a
	Changelog {
		2017-01-14: ""
	}
}

