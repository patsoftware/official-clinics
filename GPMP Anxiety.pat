/* 
 * Copyright (c) 2018 PAT Pty Ltd.
 * PAT licensees may customise these clinical materials for use at their practice.
 * Any other use or redistribution requires the permission of PAT Software.
 */

/* General GPMP sub-clinic for those diagnosed with anxiety */
Clinic GPMPAnxiety {
	Id a47eeaad-830a-4071-acf9-453512b3d84f
	Filter Anxiety
	Questions {
		Patient {
		}

		Checklist {
		}

		Staff {
			MentalHealthProblemPresent
		}

	}
	Changelog {
		2017-01-14: ""
	}
}

Filter Anxiety {
	Match {
		keyword "Anxiety"
		keyword "Anxious"
		keyword "Panic"
	}
	Tests matching only this {
		"anxiety"
		"Anxiety"
		"H/O: anxiety"
		"PMH anxiety"
		"mild anxiety"
		"moderate anxiety"
		"Anxiousness"
		"anxious"
		"anxIOUS"
		"panic"
		"panic attack"
		"panic disorder"
	}
}